`timescale 1ns/100ps
module testbench;
    parameter       REQ_BITS    = 16;
    parameter       SEL         = 6;
    localparam      ENC_BITS    = $clog2(REQ_BITS);

    logic            [REQ_BITS-1:0] req;
    logic   [SEL-1:0][ENC_BITS-1:0] enc;
    logic   [SEL-1:0]               valid;

    pe_n #(.REQ_BITS(REQ_BITS), .SEL(SEL)) pe_n1(req, enc, valid);

    initial begin
        $monitor("time = %4d, req = %b, enc[5] = %b, valid[5] = %b \n", $time, req, enc[5], valid[5],
                 "                                      enc[4] = %b, valid[4] = %b\n", enc[4], valid[4],
                 "                                      enc[3] = %b, valid[3] = %b\n", enc[3], valid[3],
                 "                                      enc[2] = %b, valid[2] = %b\n", enc[2], valid[2],
                 "                                      enc[1] = %b, valid[1] = %b\n", enc[1], valid[1],
                 "                                      enc[0] = %b, valid[0] = %b\n", enc[0], valid[0]);

        req = 0;    //valid should be high
        #5
        req = 16'b0111_1110_0000_0000;  //15
        #5
        req = 16'b0000_1000_0000_1000;  //11
        #5
        req = 16'b0010_1100_0100_1000;  //3
        #5
        req = 16'b0000_0000_0000_0001;  //0
        #5
        req = 16'b0000_0000_0000_0000;  //0
        #5

        $finish;
    end
endmodule

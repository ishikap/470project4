// Synthesized clock T = 2.5 for SEL = 4, T = 1.5 for SEL = 2
`timescale 1ns/100ps
module ps_select_n #(parameter REQ_BITS = 16, parameter SEL = 2) (
    input   [SEL-1:0]   [REQ_BITS-1:0]  req,
    input   [SEL-1:0]                   en,
    output  [SEL-1:0]   [REQ_BITS-1:0]  grant
);

    wire    [SEL-1:0]   [REQ_BITS-1:0]  grant_prev;
    wire    [SEL-1:0]                   req_up;

    generate
        if (SEL == 2) begin
            ps_select_2 #(.REQ_BITS(REQ_BITS)) sel2(
                .req(req),
                .en(en),
                .grant(grant)
            );
        end
    endgenerate
endmodule

module ps_select_2 #(parameter REQ_BITS = 16) (
    input   wire    [1:0][REQ_BITS-1:0]  req,
    input   wire    [1:0]                en,
    output  logic   [1:0][REQ_BITS-1:0]  grant
);

    logic   [REQ_BITS-1:0]  reverse_req;
    logic   [REQ_BITS-1:0]  reverse_grant;

    // Hopefully faster 2 case
    ps #(.NUM_BITS(REQ_BITS)) ps_single2(
        .req(req[0]),
        .en(en[0]),
        .gnt(grant[0]),
        .req_up()
    );

    ps #(.NUM_BITS(REQ_BITS)) ps_single1(
        .req(reverse_req),
        .en(en[1]),
        .gnt(reverse_grant),
        .req_up()
    );

    always_comb begin
        for (int i = 0; i < REQ_BITS; ++i) begin
            reverse_req[i] = req[1][REQ_BITS - i - 1];
        end
        for (int i = 0; i < REQ_BITS; ++i) begin
            grant[1][i] = reverse_grant[REQ_BITS - i - 1] & ~grant[0][i];
        end
    end

endmodule

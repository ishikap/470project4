`timescale 1ns/100ps
module testbench;
    parameter       REQ_BITS    = 16;
    parameter       SEL         = 6; 
    localparam      ENC_BITS    = $clog2(REQ_BITS); 

    logic   [REQ_BITS-1:0]  req;
    logic   [SEL-1:0][ENC_BITS-1:0]  enc;
    logic   [SEL-1:0]                valid;

    pe_n #(.REQ_BITS(16), .SEL(6)) pe1(req, enc, valid);

    initial begin
        $monitor("time = %4d, req = %b, enc[0] = %b, enc[1]= %b, enc[2]=%b, enc[3] = %b, enc[4]= %b, enc[5]=%b, valid = %b \n", $time, req, enc[0], enc[1], enc[2], enc[3], enc[4], enc[5], valid);

        req = 0;    //valid should be high
        #5
        req = 16'b0111_1100_0000_0000;  //15
        #5

        $finish;
    end
endmodule

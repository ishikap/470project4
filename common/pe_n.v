`timescale 1ns/100ps
`default_nettype none
module pe_n #(parameter REQ_BITS = 16, parameter SEL = 6, localparam ENC_BITS = $clog2(REQ_BITS)) (
    input   wire             [REQ_BITS-1:0] req,
    output  logic   [SEL-1:0][ENC_BITS-1:0] enc,
    output  logic   [SEL-1:0]               valid
);

    wire    [SEL-1:0][REQ_BITS-1:0] grant_prev;

    generate
        if (SEL > 2) begin
            // Generic case
            genvar i;
            for (i = SEL-1; i >= 0; i -= 2) begin
                if (i == SEL-1) begin
                    assign grant_prev[SEL-1]    = req;
                    assign grant_prev[SEL-2]    = req;
                end
                else begin
                    assign grant_prev[i]        = (grant_prev[i+1] ^ (1 << enc[i+1])) & (grant_prev[i+1] ^ (1 << enc[i+2])) & grant_prev[i+1];
                    assign grant_prev[i-1]      = (grant_prev[i+1] ^ (1 << enc[i+1])) & (grant_prev[i+1] ^ (1 << enc[i+2])) & grant_prev[i+1];
                end

                pe_2 #(.REQ_BITS(REQ_BITS)) pe_single(
                    .req(grant_prev[i]),
                    .enc({enc[i], enc[i-1]}),
                    .valid({valid[i], valid[i-1]})
                );
            end
        end
        else if (SEL == 2)begin
            pe_2 #(.REQ_BITS(REQ_BITS)) sel2(
                .req(req),
                .enc(enc),
                .valid(valid)
            );
        end
        else begin
            // Hopefully faster 2 case
            pe #(.OUT_WIDTH(ENC_BITS)) pe_single2(
                .gnt(req),
                .enc(enc[0]),
                .valid(valid[0])
            );
        end
    endgenerate

endmodule


module pe_2 #(parameter REQ_BITS = 16, localparam ENC_BITS = $clog2(REQ_BITS)) (
    input   wire             [REQ_BITS-1:0] req,
    output  logic       [1:0][ENC_BITS-1:0] enc,
    output  logic       [1:0]               valid
);
    logic   [REQ_BITS-1:0] reverse_req;
    logic   [ENC_BITS-1:0] reverse_enc,    normalized_enc;
    logic                  reverse_valid,  normalized_valid;

    // Hopefully faster 2 case
    pe #(.OUT_WIDTH(ENC_BITS)) pe_single2(
        .gnt(req),
        .enc(enc[1]),
        .valid(valid[1])
    );

    pe #(.OUT_WIDTH(ENC_BITS)) pe_single1(
        .gnt(reverse_req),
        .enc(reverse_enc),
        .valid(reverse_valid)
    );

    always_comb begin
        for (int i = 0; i < REQ_BITS; ++i) begin
            reverse_req[i] = req[REQ_BITS - i - 1];

        end
        normalized_enc  = ~reverse_enc;
        enc[0]          = normalized_enc;
        valid[0]        = (enc[1] != enc[0]) && reverse_valid;
    end
endmodule
`default_nettype wire

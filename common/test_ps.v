`timescale 1ns/100ps
module testbench;
    parameter SELS = 2;
    logic [SELS-1:0][15:0] req;
    logic [SELS-1:0][15:0] grant;
    logic [SELS-1:0]       en;
    ps_select_n #(.REQ_BITS(16), .SEL(SELS)) ps(req, en, grant);
    //ps_select_2 #(.REQ_BITS(16)) ps(req[0], req[1], 1'b1, grant[0], grant[1]);
    initial begin
        //$monitor("time = %d, req[3] = %b, req[2] = %b, req[1] = %b, req[0] = %b", $time, req[3], req[2], req[1], req[0]);
        $monitor("time = %d, grant[1] = %b, grant[0] = %b , en = %b", $time, grant[1], grant[0], en);
        req = 0;
        en  = 2'b11;
        #5;
        req[0] = 16'h8;
        req[1] = req[0];
        #5;
        en = 2'b10;
        req[0] = 16'h9152;
        req[1] = req[0];
        #5;
        en = 2'b01;
        req[0] = 16'h9152;
        req[1] = req[0];
        #5;
        en = 2'b11;
        req[0] = 16'h9152;
        req[1] = req[0];
        #5;
        $finish;
    end

endmodule

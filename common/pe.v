// EECS 470 - Winter 2009
//
// parametrized priority encoder (really just an encoder)
// parameter is output width
//

module pe(gnt,enc,valid);
    //synopsys template
    parameter OUT_WIDTH=4;
    parameter IN_WIDTH=1<<OUT_WIDTH;

	input   [IN_WIDTH-1:0] gnt;
	output logic    [OUT_WIDTH-1:0] enc;
    output logic                    valid;

    always_comb begin
        valid   = 1'b0;
        enc     = 0;

        for (int i=IN_WIDTH-1; i>=0; --i) begin
            if (gnt[i]) begin
                enc     = i;
                valid   = 1'b1;
            end
        end
    end    
/*
        wor              [OUT_WIDTH-1:0]    enc;
       
        assign   valid = |gnt;

        genvar i,j;
        generate
            for(i=0;i<OUT_WIDTH;i=i+1)
            begin : foo
                for(j=1;j<IN_WIDTH;j=j+1)
                begin : bar
                    if (j[i])
                        assign  enc[i]  = gnt[j];
                end
            end
        endgenerate
*/
endmodule

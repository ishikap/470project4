`default_nettype none
`timescale 1ns/100ps
// Passes testbench when he Parameters are the same as testbench's
// Clock:   (3/9): 7.3125 < t <= 7.40624
// Clock:   (3/28):         t <= 7.5 (with r_gshare)
// have proc2imem valid
module fetch #(parameter NUM_BTB_ENTRIES = 32, parameter NUM_BTB_WAYS = 4, parameter NUM_BIMODAL_BRANCH_PRED_ENTRIES=32, parameter NUM_GSHARE_BRANCH_PRED_ENTRIES=32,
                parameter BRANCH_COUNTER_BITS = 2, parameter RAS_SIZE = 16,  parameter TOURNAMENT_ENTRIES = 16,
                localparam QUADWORD = 64) (
    // INPUTS
    input   wire                                    clock,                          // system clock
    input   wire                                    reset,                          // system reset

    // From the committing instructions in the ROB
    input   wire             [1:0]                  update_valid,
    input   wire             [1:0][QUADWORD-1:0]    update_pc,
    input   wire             [1:0][QUADWORD-1:0]    update_next_pc,                 // Where does the nuking instruction want us to go next?
    input   wire             [1:0]                  update_call_from_rob,           // Push in RRAS
    input   wire             [1:0]                  update_ret_from_rob,            // Pop out of RRAS
    input   wire             [1:0]                  update_branch_actually_taken,
    input   wire             [1:0]                  update_branch_unconditional,
    input   wire             [1:0]                  update_branch_conditional,
    input   wire             [1:0]                  nuke,                           // whether we need to nuke based on commiting ROB instructions
                                                                                    // First committing instruction in nuke[1], then nuke[0]

    // From memory (will change once we get the ICache going)
    input   wire             [1:0][QUADWORD/2-1:0]  Imem2proc_data,                 // Data coming back from instruction-memory (current pc's data)
    input   wire             [1:0]                  Imem_valid,

    input   wire             [1:0]                  incoming_stall,                 // Stall from decode/later on
    input   wire             [1:0]                  id_disp_illegal_out,
    // OUTPUTS
    output  logic            [1:0][QUADWORD-1:0]    proc2Imem_addr,                 // Addresses sent to instruction memory (current pc's data)
    output  logic            [1:0][QUADWORD-1:0]    proc2Imem_next_pc,
    output  logic            [1:0]                  proc2Imem_addr_valid,

    output  logic            [1:0][QUADWORD-1:0]    if_PC_out,                      // PC
    output  logic            [1:0][QUADWORD-1:0]    if_NPC_out,                     // Predicted next PC
    output  logic            [1:0][QUADWORD/2-1:0]  if_IR_out,                      // fetched instruction out
    output  logic            [1:0]                  if_valid_inst_out               // when low, instruction is garbage
);
    wire             [1:0]                  taken;

    logic   [QUADWORD-1:0]                  PC_reg;               // PC we are currently fetching

    logic   [QUADWORD-1:0]                  next_PC;
    wire             [1:0]                  valid_fetch;
    wire             [1:0][QUADWORD-1:0]    pred_next_pc;
    logic                                   fetch_invalid, fetch_invalid_next;

    // Stored state of update signals to reduce delay
    logic            [1:0]                  update_valid_prev;
    logic            [1:0][QUADWORD-1:0]    update_pc_prev;
    logic            [1:0][QUADWORD-1:0]    update_next_pc_prev;
    logic            [1:0]                  update_call_from_rob_prev;
    logic            [1:0]                  update_ret_from_rob_prev;
    logic            [1:0]                  update_branch_actually_taken_prev;
    logic            [1:0]                  update_branch_unconditional_prev;
    logic            [1:0]                  update_branch_conditional_prev;
    logic            [1:0]                  nuke_prev;

    assign valid_fetch = Imem_valid & ~incoming_stall;      // Can valid be 01 (messes up prediction holder)?

    prediction_holder #(.NUM_BTB_ENTRIES(NUM_BTB_ENTRIES), .NUM_BTB_WAYS(NUM_BTB_WAYS), .NUM_BIMODAL_BRANCH_PRED_ENTRIES(NUM_BIMODAL_BRANCH_PRED_ENTRIES),
                        .NUM_GSHARE_BRANCH_PRED_ENTRIES(NUM_GSHARE_BRANCH_PRED_ENTRIES), .COUNTER_BITS(BRANCH_COUNTER_BITS), .RAS_SIZE(RAS_SIZE), .TOURNAMENT_ENTRIES(TOURNAMENT_ENTRIES)) ph1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .pc_in({PC_reg, PC_reg + 4}),   // PC, PC+4 (in this order, way 1 is higher in program order)
        .instr_regs(Imem2proc_data),    // MUST CHECK: this data will be set to 0 when reading memory for data, want to not influence the RAS (solved with valid_fetch)
        .valid_fetch(valid_fetch),
        .update_valid(update_valid_prev),
        .update_pc(update_pc_prev),
        .update_next_pc(update_next_pc_prev),
        .update_call_from_rob(update_call_from_rob_prev),
        .update_ret_from_rob(update_ret_from_rob_prev),
        .update_branch_actually_taken(update_branch_actually_taken_prev),
        .update_branch_unconditional(update_branch_unconditional_prev),
        .update_branch_conditional(update_branch_conditional_prev),
        .nuke(nuke_prev),
        // OUTPUTS
        .taken(taken),
        .next_pc(pred_next_pc)
    );


    always_comb begin
        next_PC                 = PC_reg;
        fetch_invalid_next      = fetch_invalid;
        if_IR_out               = 0;
        if_valid_inst_out       = 0;
        if_NPC_out              = 0;
        if_PC_out               = 0;
        proc2Imem_addr_valid    = 2'b11;
        proc2Imem_addr          = {{PC_reg[QUADWORD-1:2], 2'b0}, {PC_reg[QUADWORD-1:2] + 1, 2'b0}}; // To get the instr. from PC, PC+4
        proc2Imem_next_pc       = {{PC_reg[QUADWORD-1:2] + 2, 2'b0}, {PC_reg[QUADWORD-1:2] + 3, 2'b0}};     // give PC + 8, PC + 12

        // Do default setting of moving next_PC forward by 8 and if_NPC_out[i] = PC + 4
        // Increment next_PC only if the icache data is valid and not a stall
        // First look at way 1, then way 0 (if way 1 invalid, don't do way 0)
        for (int i=1; i>=0; --i) begin
            if (!valid_fetch[i]) begin
                break;
            end
            if_valid_inst_out[i]    = 1'b1;
            if_IR_out[i]            = Imem2proc_data[i];
            if_PC_out[i]            = next_PC;
            next_PC                 = next_PC + 4;
            if_NPC_out[i]           = next_PC;
        end

        // Override if we predict taken
        for (int i=0; i<2; ++i) begin // MUST BE IN THIS ORDER, WAY1 overwrites WAY0
            if (taken[i] && if_valid_inst_out[i]) begin
                next_PC                 = pred_next_pc[i];
                if_NPC_out[i]           = pred_next_pc[i];
            end
            /*if (taken[i]) begin
                proc2Imem_next_pc[i]    = pred_next_pc[i];
            end*/
        end
        if (taken[1]) begin
            if_valid_inst_out[0]    = 0;
            // proc2Imem_next_pc[0]    = proc2Imem_next_pc[1] + 4;
        end

        // If the instruction is illegal (past the program) stop PC from incrementing
        if (id_disp_illegal_out) begin
            fetch_invalid_next          = `TRUE;
        end

        if (fetch_invalid_next) begin
            proc2Imem_addr_valid        = 0;
            if_valid_inst_out           = 0;
            if_IR_out[1]                = `NOOP_INST;
            if_IR_out[0]                = `NOOP_INST;
        end

        // Handle that bad nuke
        for (int i=0; i<2; ++i) begin
            if (nuke[i]) begin
                next_PC                 = update_next_pc[i];
                if_valid_inst_out       = 0;
                fetch_invalid_next      = `FALSE;
            end
        end

        if (fetch_invalid_next) begin
            next_PC = PC_reg;
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            PC_reg                              <= `SD 0;       // initial PC value is 0
            fetch_invalid                       <= `SD `FALSE;
            update_valid_prev                   <= `SD 0;
            update_pc_prev                      <= `SD 0;
            update_next_pc_prev                 <= `SD 0;
            update_call_from_rob_prev           <= `SD 0;
            update_ret_from_rob_prev            <= `SD 0;
            update_branch_actually_taken_prev   <= `SD 0;
            update_branch_unconditional_prev    <= `SD 0;
            update_branch_conditional_prev      <= `SD 0;
            nuke_prev                           <= `SD 0;
        end
        else begin
            fetch_invalid                       <= `SD fetch_invalid_next;
            PC_reg                              <= `SD next_PC; // transition to next PC
            update_valid_prev                   <= `SD update_valid;
            update_pc_prev                      <= `SD update_pc;
            update_next_pc_prev                 <= `SD update_next_pc;
            update_call_from_rob_prev           <= `SD update_call_from_rob;
            update_ret_from_rob_prev            <= `SD update_ret_from_rob;
            update_branch_actually_taken_prev   <= `SD update_branch_actually_taken;
            update_branch_unconditional_prev    <= `SD update_branch_unconditional;
            update_branch_conditional_prev      <= `SD update_branch_conditional;
            nuke_prev                           <= `SD nuke;
        end
    end
endmodule
`default_nettype wire

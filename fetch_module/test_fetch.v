//For: Testing RAT
//By: Scott
//Date 2/19/18
`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define WAY_0           1'b0
`define WAY_1           1'b1
`define R0              5'd0
`define R1              5'd1
`define R2              5'd2
`define R3              5'd3
`define R4              5'd4
`define R5              5'd5
`define R6              5'd6
`define R7              5'd7
`define R8              5'd8
`define R9              5'd9
`define R10             5'd10
`define R11             5'd11
`define R12             5'd12
`define R13             5'd13
`define R14             5'd14
`define R15             5'd15
`define R16             5'd16
`define R17             5'd17
`define R18             5'd18
`define R19             5'd19
`define R20             5'd20
`define R21             5'd21
`define R22             5'd22
`define R23             5'd23
`define R24             5'd24
`define R25             5'd25
`define R26             5'd26
`define R27             5'd27
`define R28             5'd28
`define R29             5'd29
`define R30             5'd30
`define R31             5'd31

parameter NUM_BTB_ENTRIES           = 4;
parameter NUM_BTB_WAYS              = 2;
parameter NUM_BRANCH_PRED_ENTRIES   = 8;
parameter BRANCH_COUNTER_BITS       = 2;
parameter RAS_SIZE                  = 4;
localparam QUADWORD                 = 64;
localparam NUM_BTB_SETS             = NUM_BTB_ENTRIES/NUM_BTB_WAYS;
localparam NUM_BTB_PLRT_LEVELS      = $clog2(NUM_BTB_WAYS);
localparam RAS_BITS                 = $clog2(RAS_SIZE);
localparam BRANCH_INDEX             = $clog2(NUM_BRANCH_PRED_ENTRIES);

module testbench;

    logic                          clock;
    logic                          reset;
    logic   [1:0]                  update_valid;
    logic   [1:0][QUADWORD-1:0]    update_pc;
    logic   [1:0][QUADWORD-1:0]    update_next_pc;
    logic   [1:0]                  update_call_from_rob;
    logic   [1:0]                  update_ret_from_rob;
    logic   [1:0]                  update_branch_actually_taken;
    logic   [1:0]                  update_branch_unconditional;
    logic   [1:0]                  update_branch_conditional;
    logic   [1:0]                  nuke;
    logic   [1:0][QUADWORD/2-1:0]  Imem2proc_data;
    logic   [1:0]                  Imem_valid;
    logic   [1:0]                  incoming_stall;
    logic   [1:0]                  id_disp_illegal_out;

    logic   [1:0][QUADWORD-1:0]    proc2Imem_addr;
    logic   [1:0][QUADWORD-1:0]    proc2Imem_next_pc;
    logic   [1:0]                  proc2Imem_addr_valid;
    logic   [1:0][QUADWORD-1:0]    if_PC_out;
    logic   [1:0][QUADWORD-1:0]    if_NPC_out;
    logic   [1:0][31:0]            if_IR_out;
    logic   [1:0]                  if_valid_inst_out;



    fetch #(.NUM_BTB_ENTRIES(NUM_BTB_ENTRIES), .NUM_BTB_WAYS(NUM_BTB_WAYS), .NUM_BRANCH_PRED_ENTRIES(NUM_BRANCH_PRED_ENTRIES), .BRANCH_COUNTER_BITS(BRANCH_COUNTER_BITS), .RAS_SIZE(RAS_SIZE)) fetch1(
        .clock(clock),
        .reset(reset),
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_next_pc(update_next_pc),
        .update_call_from_rob(update_call_from_rob),
        .update_ret_from_rob(update_ret_from_rob),
        .update_branch_actually_taken(update_branch_actually_taken),
        .update_branch_unconditional(update_branch_unconditional),
        .update_branch_conditional(update_branch_conditional),
        .nuke(nuke),
        .Imem2proc_data(Imem2proc_data),
        .Imem_valid(Imem_valid),
        .incoming_stall(incoming_stall),
        .id_disp_illegal_out(id_disp_illegal_out),
        .proc2Imem_addr(proc2Imem_addr),
        .proc2Imem_next_pc(proc2Imem_next_pc),
        .proc2Imem_addr_valid(proc2Imem_addr_valid),
        .if_PC_out(if_PC_out),
        .if_NPC_out(if_NPC_out),
        .if_IR_out(if_IR_out),
        .if_valid_inst_out(if_valid_inst_out)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");
        /*
        //display all the current BTB enteries
        $display("===BTB===");
        for (int i=0; i < NUM_BTB_SETS; i++) begin
            $display("\n\nSet Number: %5d ", i);
                for (int j=0; j < NUM_BTB_WAYS; j++) begin
                $display ("\tWay Number: %5d ", j,
                        "Tag: %5d ", fetch1.ph1.btb_pred1.btb1.pc_tag[i][j],
                        "Data: %5d ", fetch1.ph1.btb_pred1.btb1.target_pc[i][j],
                        "Valid: %5d ", fetch1.ph1.btb_pred1.btb1.valid[i][j],
                        "Uncond Branch : %b ", fetch1.ph1.btb_pred1.btb1.unconditional_branch[i][j]);
                end
        end

        //display all the current PLRT enteries
        $display("\n\n===PLRT===");
        for (int i=0; i < NUM_BTB_SETS; i++) begin
            $display("\n\nSet Number: %5d ", i);
            for (int j=1; j< NUM_BTB_WAYS ; j++) begin
                $display ("PLRT index: %5d ", j,
                      "PLRT Entry: %d ", fetch1.ph1.btb_pred1.btb1.plrt[i][j]);
            end
        end

        //display all the current Predictor Table enteries
        $display("\n\n===Branch Predictor===");
        for (int i=0; i < NUM_BRANCH_PRED_ENTRIES; i++) begin
            $display("Entry Number: %5d", i,
                     "Counter Value: %b\n", fetch1.ph1.btb_pred1.bp1.predictor_table[i]);
        end

        //display all the current RAS enteries
        $display("\n\n===RAS===");
        for (int i=0; i < RAS_SIZE; i++)
        begin
            $display ("Entry Number: %5d ", i,
                      "Entry: %5d ", fetch1.ph1.ras_holder1.ras1.ras_entries[i],
                      "Pointer: %5d", fetch1.ph1.ras_holder1.ras1.ras_pointer);
        end
        //display all the current RRAS enteries
        $display("\n\n===RRAS===");
        $display("RRAS Pointer= %5d", fetch1.ph1.ras_holder1.rras1.rras_pointer);
        for (int i=0; i < RAS_SIZE; i++)
        begin
            $display ("RRAS Entry Number: %5d ", i,
                      "RRAS Entry: %5d ", fetch1.ph1.ras_holder1.ras1.rras_entries[i]);
        end
        */
        $fatal("INCORRECT");
    endtask

    task fetch_instr;
        input                   way;
        input [QUADWORD/2-1:0]  instr_data;
        input                   instr_valid;
        input                   stall;

        Imem2proc_data[way]     = instr_data;
        Imem_valid[way]         = instr_valid;
        incoming_stall[way]     = stall;
    endtask

    task commit;
        input                   way;
        input                   valid;
        input  [QUADWORD-1:0]   pc;
        input  [QUADWORD-1:0]   next_pc;
        input                   call_from_rob;
        input                   ret_from_rob;
        input                   branch_actually_taken;
        input                   branch_unconditional;
        input                   branch_conditional;
        input                   nuke_in;

        update_valid[way]                    =     valid;
        update_pc[way]                       =     pc;
        update_next_pc[way]                  =     next_pc;
        update_call_from_rob[way]            =     call_from_rob;
        update_ret_from_rob[way]             =     ret_from_rob;
        update_branch_actually_taken[way]    =     branch_actually_taken;
        update_branch_unconditional[way]     =     branch_unconditional;
        update_branch_conditional[way]       =     branch_conditional;
        nuke[way]                            =     nuke_in;
    endtask

    task check_out;
        input                   way;
        input   [QUADWORD-1:0]  NPC_out;
        input   [31:0]          IR_out;
        input                   valid_inst_out;
        input   [QUADWORD-1:0]  addr;

        if (if_NPC_out[way] != NPC_out && valid_inst_out) exit_on_error();
        if (if_IR_out[way] != IR_out && valid_inst_out) exit_on_error();
        if (if_valid_inst_out[way] != valid_inst_out) exit_on_error();
        if (proc2Imem_addr[way] != addr) exit_on_error();
    endtask

    task check_int;
        input    [QUADWORD-1:0]     pc;
        input    [QUADWORD-1:0]     next_pc;
        input              [1:0]    valid_fetch;
        input              [1:0]    take;
        input    [QUADWORD-1:0]     pred_npc_1;
        input    [QUADWORD-1:0]     pred_npc_2;

/*
        if (fetch1.PC_reg != pc) exit_on_error();
        if (fetch1.next_PC != next_pc) exit_on_error();
        if (fetch1.valid_fetch != valid_fetch) exit_on_error();
        if (fetch1.taken != take) exit_on_error();
        if (fetch1.pred_next_pc[1] != pred_npc_1 && take[1]) exit_on_error();
        if (fetch1.pred_next_pc[0] != pred_npc_2 && take[0]) exit_on_error();
*/
    endtask

    initial
    begin
        /*$monitor("===WAY_0 INPUTS===\nTime= %d  Imem2proc_data= %d Imem_valid= %d incoming_stall= %d update_valid= %b\n",  $time, Imem2proc_data[0], Imem_valid[0], incoming_stall[0], update_valid[0],
                 "===WAY_0 OUTPUTS===\nTime= %d proc2Imem_addr= %d if_NPC_out= %d if_IR_out= %d if_valid_inst_out= %b \n", $time, proc2Imem_addr[0], if_NPC_out[0], if_IR_out[0], if_valid_inst_out[0],
                 "===WAY_0 INTERNAL===\nTime= %d PC_reg= %d next_PC= %d valid_fetch= %d taken= %b pred_next_pc= %d\n", $time, fetch1.PC_reg, fetch1.next_PC, fetch1.valid_fetch[0], fetch1.taken[0], fetch1.pred_next_pc[0],
                 "===WAY_1 INPUTS===\nTime= %d  Imem2proc_data= %d Imem_valid= %d incoming_stall= %d update_valid= %b\n",  $time, Imem2proc_data[1], Imem_valid[1], incoming_stall[1], update_valid[1],
                 "===WAY_1 OUTPUTS===\nTime= %d proc2Imem_addr= %d if_NPC_out= %d if_IR_out= %d if_valid_inst_out= %b \n", $time, proc2Imem_addr[1], if_NPC_out[1], if_IR_out[1], if_valid_inst_out[1],
                 "===WAY_1 INTERNAL===\nTime= %d PC_reg= %d next_PC= %d valid_fetch= %d taken= %b pred_next_pc= %d\n", $time, fetch1.PC_reg, fetch1.next_PC, fetch1.valid_fetch[1], fetch1.taken[1], fetch1.pred_next_pc[1]);*/

        clock=0;
        reset=1;
        update_valid = 0;
        update_pc = 0;
        update_next_pc = 0;
        update_call_from_rob = 0;
        update_ret_from_rob = 0;
        update_branch_actually_taken = 0;
        update_branch_unconditional = 0;
        update_branch_conditional = 0;
        nuke = 0;
        Imem2proc_data = 0;
        Imem_valid = 0;
        incoming_stall = 0;
        id_disp_illegal_out = 0;

        //CHECK reset
        @(negedge clock);
        reset = 0;
/*        check_out(`WAY_1, 0, 0, 0, 0);
        check_out(`WAY_0, 0, 0, 0, 4);
        check_int(0, 0, 0, 0, 0, 0);
        $display("\n@@@Passed reset\n");

        // fetch both ways
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd10, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd15, `TRUE, `FALSE);
        @(posedge clock);;
        check_out(`WAY_1, 64'd4, 32'd10, `TRUE, 64'd0);
        check_out(`WAY_0, 64'd8, 32'd15, `TRUE, 64'd4);
        check_int(64'd0, 64'd8, 2'b11, 2'b00, 64'd0, 64'd0);

        @(negedge clock);
        fetch_instr(`WAY_1, 32'd10, `FALSE, `FALSE);
        fetch_instr(`WAY_0, 32'd15, `FALSE, `FALSE);
        @(posedge clock);
        check_int(64'd8, 64'd8, 2'b00, 2'b00, 64'd0, 64'd0);
        $display("\n@@@Passed fetch both ways\n");

        // update BTB/Branch Pred
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd16, 64'd40, `FALSE, `FALSE, `TRUE, `FALSE, `TRUE, `FALSE);
        commit(`WAY_0, `TRUE, 64'd20, 64'd44, `FALSE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd48, 64'd60, `FALSE, `FALSE, `TRUE, `FALSE, `TRUE, `FALSE);
        commit(`WAY_0, `TRUE, 64'd68, 64'd24, `FALSE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'd56, 64'd80, `FALSE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        commit(`WAY_0, `FALSE, 64'd52, 64'd16, `FALSE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);

        // fetch with stall
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd20, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd15, `TRUE, `TRUE);
        @(posedge clock);
        check_out(`WAY_1, 64'd12, 32'd20, `TRUE, 64'd8);
        check_out(`WAY_0, 64'd0, 32'd0, `FALSE, 64'd12);
        check_int(64'd8, 64'd12, 2'b10, 2'b00, 64'd0, 64'd0);
        $display("\n@@@Passed fetch with stall\n");

        // fetch with imem invalid
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd30, `FALSE, `FALSE);
        fetch_instr(`WAY_0, 32'd35, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd12, 32'd20, `FALSE, 64'd12);
        check_out(`WAY_0, 64'd0, 32'd0, `FALSE, 64'd16);
        check_int(64'd12, 64'd12, 2'b01, 2'b01, 64'd0, 64'd40);
        $display("\n@@@Passed fetch with imem invalid\n");

        // fetch way 0 taken
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd40, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd45, `TRUE, `FALSE);
        @(posedge clock);
        #1;
        check_out(`WAY_1, 64'd16, 32'd40, `TRUE, 64'd12);
        check_out(`WAY_0, 64'd40, 32'd45, `TRUE, 64'd16);
        check_int(64'd12, 64'd40, 2'b11, 2'b01, 64'd0, 64'd40);
        $display("\n@@@Passed fetch with way 0 taken\n");

        // fetch normal
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd50, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd55, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd44, 32'd50, `TRUE, 64'd40);
        check_out(`WAY_0, 64'd48, 32'd55, `TRUE, 64'd44);
        check_int(64'd40, 64'd48, 2'b11, 2'b00, 64'd0, 64'd0);
        $display("\n@@@Passed fetch normal\n");

        // fetch way 1 taken
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd70, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd75, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd60, 32'd70, `TRUE, 64'd48);
        check_out(`WAY_0, 64'd48, 32'd55, `FALSE, 64'd52);
        check_int(64'd48, 64'd60, 2'b11, 2'b10, 64'd60, 64'd0);
        $display("\n@@@Passed fetch way 1 taken\n");

        // fetch normal
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd80, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd85, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd64, 32'd80, `TRUE, 64'd60);
        check_out(`WAY_0, 64'd68, 32'd85, `TRUE, 64'd64);
        check_int(64'd60, 64'd68, 2'b11, 2'b00, 64'd00, 64'd0);
        $display("\n@@@Passed fetch normal\n");

        // fetch way 1 taken
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd90, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd95, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd24, 32'd90, `TRUE, 64'd68);
        check_out(`WAY_0, 64'd68, 32'd85, `FALSE, 64'd72);
        check_int(64'd68, 64'd24, 2'b11, 2'b10, 64'd24, 64'd0);
        $display("\n@@@Passed fetch way 1 taken\n");

        // fetch - put into ras
        @(negedge clock);
        fetch_instr(`WAY_1, 32'h6b414000, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd95, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd28, 32'h6b414000, `TRUE, 64'd24);
        check_out(`WAY_0, 64'd32, 32'd95, `TRUE, 64'd28);
        check_int(64'd24, 64'd32, 2'b11, 2'b00, 64'd0, 64'd0);
        $display("\n@@@Passed fetch put into ras\n");

        // commit into rras
        @(negedge clock);
        fetch_instr(`WAY_1, 32'h6b414000, `FALSE, `FALSE);
        fetch_instr(`WAY_0, 32'd95, `FALSE, `FALSE);
        commit(`WAY_1, `TRUE, 64'd24, 64'd80, `TRUE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        commit(`WAY_0, `FALSE, 64'd20, 64'd44, `FALSE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'd24, 64'd80, `TRUE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        $display("\n@@@Passed commit into rras\n");

        // fetch normal
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd100, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd105, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd36, 32'd100, `TRUE, 64'd32);
        check_out(`WAY_0, 64'd40, 32'd105, `TRUE, 64'd36);
        check_int(64'd32, 64'd40, 2'b11, 2'b00, 64'd00, 64'd0);
        $display("\n@@@Passed fetch normal\n");

        // fetch - pop out of ras
        @(negedge clock);
        fetch_instr(`WAY_1, 32'h6bfa8000, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd105, `TRUE, `FALSE);
        @(posedge clock);
        check_out(`WAY_1, 64'd28, 32'h6bfa8000, `TRUE, 64'd40);
        check_out(`WAY_0, 64'd32, 32'd95, `FALSE, 64'd44);
        check_int(64'd40, 64'd28, 2'b11, 2'b10, 64'd28, 64'd0);
        $display("\n@@@Passed fetch - pop out of ras\n");

        // pop out rras
        @(negedge clock);
        fetch_instr(`WAY_1, 32'h6b414000, `FALSE, `FALSE);
        fetch_instr(`WAY_0, 32'd95, `FALSE, `FALSE);
        commit(`WAY_1, `FALSE, 64'd24, 64'd80, `TRUE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        commit(`WAY_0, `TRUE, 64'd40, 64'd28, `FALSE, `TRUE, `TRUE, `TRUE, `FALSE, `FALSE);
        @(negedge clock);
        commit(`WAY_0, `FALSE, 64'd24, 64'd80, `TRUE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        $display("\n@@@Passed pop out rras\n");

        // nuke
        @(negedge clock);
        fetch_instr(`WAY_1, 32'd100, `TRUE, `FALSE);
        fetch_instr(`WAY_0, 32'd105, `TRUE, `FALSE);
        commit(`WAY_1, `TRUE, 64'd28, 64'd200, `TRUE, `FALSE, `TRUE, `FALSE, `TRUE, `TRUE);
        commit(`WAY_0, `TRUE, 64'd40, 64'd60, `TRUE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
         @(posedge clock);
        check_out(`WAY_1, 64'd200, 32'h100, `FALSE, 64'd28);
        check_out(`WAY_0, 64'd32, 32'd95, `FALSE, 64'd32);
        check_int(64'd28, 64'd200, 2'b11, 2'b00, 64'd0, 64'd0);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'd28, 64'd200, `TRUE, `FALSE, `TRUE, `FALSE, `TRUE, `TRUE);
        commit(`WAY_0, `FALSE, 64'd24, 64'd80, `TRUE, `FALSE, `TRUE, `TRUE, `FALSE, `FALSE);
        fetch_instr(`WAY_1, 32'd100, `FALSE, `FALSE);
        fetch_instr(`WAY_0, 32'd105, `FALSE, `FALSE);
        $display("\n@@@Passed nuke\n");
*/
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);


        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

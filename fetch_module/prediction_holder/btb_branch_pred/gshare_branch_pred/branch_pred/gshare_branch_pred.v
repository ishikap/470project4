`default_nettype none
`timescale 1ns/100ps
module gshare_branch_pred #(parameter NUM_ENTRIES = 8, parameter COUNTER_BITS = 2, localparam GLOBAL_HISTORY_BITS = $clog2(NUM_ENTRIES), localparam QUADWORD = 64) (
    // INPUTS
    input   wire                                                clock,
    input   wire                                                reset,
    input   wire                [1:0][QUADWORD-1:0]             pc_in,
    input   wire                [1:0]                           btb_valid_out,
    input   wire                [1:0]                           btb_unconditional_branch,
    // From rbranch_pred
    input   wire                     [GLOBAL_HISTORY_BITS-1:0]  r_global_history,
    input   wire    [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]         r_predictor_table,
    input   wire                [1:0]                           nuke,

    // OUTPUTS
    output  logic   [1:0]                                       prediction
);

    logic                    [GLOBAL_HISTORY_BITS-1:0]  global_history;
    logic                    [GLOBAL_HISTORY_BITS-1:0]  global_history_next;
    logic   [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]         predictor_table;
    logic   [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]         predictor_table_next;
    logic                                               nuked;

    logic                    [GLOBAL_HISTORY_BITS-1:0]  updated_entry;
    logic                    [GLOBAL_HISTORY_BITS-1:0]  lookup_entry;

    always_comb begin
        global_history_next     = global_history;
        predictor_table_next    = predictor_table;
        prediction              = 0;
        nuked                   = 0;
        updated_entry           = 0;
        lookup_entry            = 0;

        // Update on commit
        for (int i=1; i>=0; --i) begin
            // Assign prediction output
            lookup_entry  = global_history_next ^ pc_in[i][GLOBAL_HISTORY_BITS+1:2];
            prediction[i] = predictor_table_next[lookup_entry][COUNTER_BITS-1];

            if (btb_valid_out[i] && !btb_unconditional_branch[i]) begin
                if (prediction[i]) begin
                    if(predictor_table_next[updated_entry] != (2 ** COUNTER_BITS)-1) begin
                        predictor_table_next[updated_entry] = predictor_table_next[updated_entry] + 1;
                    end
                    global_history_next = {global_history_next, 1'b1};
                end
                else begin
                    if (predictor_table_next[updated_entry] != 0) begin
                        predictor_table_next[updated_entry] = predictor_table_next[updated_entry] - 1;
                    end
                    global_history_next = {global_history_next, 1'b0};
                end
            end
        end

        if (nuke) begin
            global_history_next     = r_global_history;
            predictor_table_next    = r_predictor_table;
        end
    end

    always_ff @(posedge clock) begin
        if(reset) begin
            global_history          <= `SD 0;
            for (int i = 0; i < NUM_ENTRIES; ++i) begin
                predictor_table[i]  <= `SD 2'b01;
            end
        end
        else begin
            predictor_table <= `SD predictor_table_next;
            global_history  <= `SD global_history_next;
        end
    end
endmodule

`default_nettype wire

`default_nettype none
`timescale 1ns/100ps
module r_gshare_branch_pred #(parameter NUM_ENTRIES = 8, parameter COUNTER_BITS = 2, localparam GLOBAL_HISTORY_BITS = $clog2(NUM_ENTRIES), localparam QUADWORD = 64) (
    // INPUTS
    input   wire                                                clock,
    input   wire                                                reset,
    //update on commit, from ROB
    input   wire                [1:0]                           update_valid,
    input   wire                [1:0][QUADWORD-1:0]             update_pc,
    input   wire                [1:0]                           update_branch_actually_taken,
    input   wire                [1:0]                           update_branch_conditional,
    input   wire                [1:0]                           nuke,

    // OUTPUTS
    output  logic                    [GLOBAL_HISTORY_BITS-1:0]  r_global_history,
    output  logic   [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]         r_predictor_table
);

    logic                    [GLOBAL_HISTORY_BITS-1:0]  global_history;
    logic                    [GLOBAL_HISTORY_BITS-1:0]  global_history_next;
    logic   [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]         predictor_table;
    logic   [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]         predictor_table_next;
    logic                                               nuked;

    logic                    [GLOBAL_HISTORY_BITS-1:0]  updated_entry;

    always_comb begin
        global_history_next     = global_history;
        predictor_table_next    = predictor_table;
        nuked                   = 0;
        updated_entry           = 0;

        // Update on commit
        for (int i=1; i>=0; --i) begin
            if (update_valid[i] && update_branch_conditional[i]) begin
                updated_entry = global_history_next ^ update_pc[i][GLOBAL_HISTORY_BITS+1:2];
                if(update_branch_actually_taken[i]) begin
                    if(predictor_table_next[updated_entry] != (2 ** COUNTER_BITS)-1) begin
                        predictor_table_next[updated_entry] = predictor_table_next[updated_entry] + 1;
                    end
                    global_history_next = {global_history_next, 1'b1};
                end
                else begin
                    if(predictor_table_next[updated_entry] != 0) begin
                        predictor_table_next[updated_entry] = predictor_table_next[updated_entry] - 1;
                    end
                    global_history_next = {global_history_next, 1'b0};
                end
            end
            if(nuke[i]) begin
                break;
            end
        end

        r_global_history    = global_history_next;
        r_predictor_table   = predictor_table_next;
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            global_history          <= `SD 0;
            for (int i = 0; i < NUM_ENTRIES; ++i) begin
                predictor_table[i]  <= `SD 2'b01;
            end
        end
        else begin
            predictor_table <= `SD predictor_table_next;
            global_history  <= `SD global_history_next;
        end
    end
endmodule

`default_nettype wire

`default_nettype none
`timescale 1ns/100ps
module gshare_branch_pred_holder #(parameter NUM_ENTRIES = 8, parameter COUNTER_BITS = 2, localparam GLOBAL_HISTORY_BITS = $clog2(NUM_ENTRIES), localparam QUADWORD = 64) (
    // INPUTS
    input   wire                                                clock,
    input   wire                                                reset,
    input   wire                [1:0][QUADWORD-1:0]             pc_in,
    // From the BTB
    input   wire                [1:0]                           btb_valid_out,
    input   wire                [1:0]                           btb_unconditional_branch,
    // Update on commit, from ROB
    input   wire                [1:0]                           update_valid,
    input   wire                [1:0][QUADWORD-1:0]             update_pc,
    input   wire                [1:0]                           update_branch_actually_taken,
    input   wire                [1:0]                           update_branch_conditional,
    input   wire                [1:0]                           nuke,

    // OUTPUTS
    output  logic   [1:0]                                       prediction
);
    wire                     [GLOBAL_HISTORY_BITS-1:0]  r_global_history;
    wire    [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]         r_predictor_table;

    gshare_branch_pred #(.NUM_ENTRIES(NUM_ENTRIES), .COUNTER_BITS(COUNTER_BITS)) branch_pred(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .pc_in(pc_in),
        .btb_valid_out(btb_valid_out),
        .btb_unconditional_branch(btb_unconditional_branch),
        .r_global_history(r_global_history),
        .r_predictor_table(r_predictor_table),
        .nuke(nuke),
        // OUTPUTS
        .prediction(prediction)
    );

    r_gshare_branch_pred #(.NUM_ENTRIES(NUM_ENTRIES), .COUNTER_BITS(COUNTER_BITS)) rbranch_pred(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_branch_actually_taken(update_branch_actually_taken),
        .update_branch_conditional(update_branch_conditional),
        .nuke(nuke),
        // OUTPUTS
        .r_global_history(r_global_history),
        .r_predictor_table(r_predictor_table)
    );
endmodule
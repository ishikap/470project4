`default_nettype none
`timescale 1ns/100ps
// Clock period: 7.3125ns < x <= 7.375ns
module btb #(parameter NUM_ENTRIES=32, parameter NUM_WAYS=4, localparam QUADWORD=64, localparam SET_INDEX_BITS=$clog2(NUM_ENTRIES / NUM_WAYS), localparam NUM_SETS=NUM_ENTRIES/NUM_WAYS, localparam NUM_PLRT_LEVELS=$clog2(NUM_WAYS)) (
    // INPUTS
    input   wire                            clock,
    input   wire                            reset,

    // For determining if branch, where to go?
    input   wire    [1:0][QUADWORD-1:0]     pc_in,

    // For updating the BTB
    // First commiting instruction in way[1], second at way[0]
    input   wire    [1:0]                   update_valid,
    input   wire    [1:0][QUADWORD-1:0]     update_pc,
    input   wire    [1:0][QUADWORD-1:0]     update_next_pc,
    input   wire    [1:0]                   update_branch_actually_taken,
    input   wire    [1:0]                   update_branch_unconditional,

    input   wire    [1:0]                   nuke,

    // OUTPUTS
    output  logic   [1:0]                   valid_out,
    output  logic   [1:0][QUADWORD-1:0]     next_pc_out,
    output  logic   [1:0]                   unconditional_branch_out
);

    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][QUADWORD-1:0]  pc_tag;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][QUADWORD-1:0]  target_pc;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0]                valid;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0]                unconditional_branch;

    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][QUADWORD-1:0]  pc_tag_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][QUADWORD-1:0]  target_pc_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0]                valid_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0]                unconditional_branch_next;



    // The least recently taken eviction algorithm
    // Updated only on commit (because then we know if it was *actually* taken or not)
    logic   [NUM_SETS-1:0][NUM_WAYS-1:1]                plrt;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:1]                plrt_next;
    logic   [NUM_PLRT_LEVELS-1:0]                       plrt_sel;

    logic                                               forwarded_flag;
    logic                                               found_flag;
    logic   [NUM_PLRT_LEVELS-1:0]                       current_index;
    logic                                               nuked;

    always_comb begin
        pc_tag_next                 = pc_tag;
        target_pc_next              = target_pc;
        valid_next                  = valid;
        unconditional_branch_next   = unconditional_branch;
        plrt_next                   = plrt;

        valid_out                   = 0;
        next_pc_out                 = 0;
        unconditional_branch_out    = 0;

        // Temporary variables for comb
        found_flag                  = 0;
        forwarded_flag              = 0;
        current_index               = 1;
        nuked                       = 0;


        // Reads from fetch
        for (int fetch_port=1; fetch_port>=0; --fetch_port) begin
            forwarded_flag = 0;
            // Forward from commit if necessary
            for (int commit_port=1; commit_port>=0; --commit_port) begin
                if (pc_in[fetch_port] == update_pc[commit_port] && update_valid[commit_port] && update_branch_actually_taken[commit_port]) begin
                    next_pc_out[fetch_port]                 = update_next_pc[commit_port];
                    valid_out[fetch_port]                   = 1'b1;
                    unconditional_branch_out[fetch_port]    = update_branch_unconditional[commit_port];
                    forwarded_flag                          = 1'b1;
                end
            end

            if (forwarded_flag == 0) begin
                // Couldn't forward from commit (common case)
                for (int way_num=0; way_num<NUM_WAYS; ++way_num) begin
                    if (valid[pc_in[fetch_port][SET_INDEX_BITS+1:2]][way_num] &&
                        pc_tag[pc_in[fetch_port][SET_INDEX_BITS+1:2]][way_num] == pc_in[fetch_port]) begin

                        next_pc_out[fetch_port]                 = target_pc[pc_in[fetch_port][SET_INDEX_BITS+1:2]][way_num];
                        valid_out[fetch_port]                   = 1'b1;
                        unconditional_branch_out[fetch_port]    = unconditional_branch[pc_in[fetch_port][SET_INDEX_BITS+1:2]][way_num];
                    end
                end
            end
        end

        // Commits from ROB
        for (int port=1; port>=0; --port) begin
            if (update_valid[port] && !nuked && update_branch_actually_taken[port]) begin
                found_flag = 0;
                for (int way_num=0; way_num<NUM_WAYS; ++way_num) begin
                    // If it is currently in the cache, update the plrt for it
                    if (valid_next[update_pc[port][SET_INDEX_BITS+1:2]][way_num] &&
                        pc_tag_next[update_pc[port][SET_INDEX_BITS+1:2]][way_num] == update_pc[port]) begin

                        found_flag = 1;
                        // Update address - jsr or jump's destination could change
                        target_pc_next[update_pc[port][SET_INDEX_BITS+1:2]][way_num] = update_next_pc[port];

                        // Update PLRT bits, wont work for direct mapped
                        for(int i=0; i<NUM_PLRT_LEVELS; ++i) begin
                            plrt_sel = (({NUM_PLRT_LEVELS{1'b0}} | 1'b1) << (NUM_PLRT_LEVELS-i-1)) |  (way_num >> (i+1));
                            plrt_next[update_pc[port][SET_INDEX_BITS+1:2]][plrt_sel] = ~way_num[i];
                        end
                    end
                end
                if (!found_flag) begin
                    // Find entry to replace in the set using PLRT
                    // Start off at the head entry
                    current_index = 1;
                    for(int i=0; i<NUM_PLRT_LEVELS; ++i) begin
                        current_index = { current_index, plrt_next[update_pc[port][SET_INDEX_BITS+1:2]][current_index] };
                    end

                    // Assign all members of that way
                    pc_tag_next[update_pc[port][SET_INDEX_BITS+1:2]][current_index]                  = update_pc[port];
                    target_pc_next[update_pc[port][SET_INDEX_BITS+1:2]][current_index]               = update_next_pc[port];
                    valid_next[update_pc[port][SET_INDEX_BITS+1:2]][current_index]                   = update_branch_actually_taken[port];
                    unconditional_branch_next[update_pc[port][SET_INDEX_BITS+1:2]][current_index]    = update_branch_unconditional[port];

                    // Update PLRT bits
                    for(int i=0; i<NUM_PLRT_LEVELS; ++i) begin
                        plrt_sel = (({NUM_PLRT_LEVELS{1'b0}} | 1'b1) << (NUM_PLRT_LEVELS-i-1)) |  (current_index >> (i+1));
                        plrt_next[update_pc[port][SET_INDEX_BITS+1:2]][plrt_sel] = ~current_index[i];
                    end
                end
            end
            if (nuke[port]) begin
                nuked = 1'b1;
            end
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            pc_tag                  <= `SD 0;
            target_pc               <= `SD 0;
            valid                   <= `SD 0;
            unconditional_branch    <= `SD 0;
            plrt                    <= `SD 0;
        end
        else begin
            pc_tag                  <= `SD pc_tag_next;
            target_pc               <= `SD target_pc_next;
            valid                   <= `SD valid_next;
            unconditional_branch    <= `SD unconditional_branch_next;
            plrt                    <= `SD plrt_next;
        end
    end

endmodule

`default_nettype wire

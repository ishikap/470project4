//For: Testing BTB

//TODO:
`timescale 1ns/100ps
`define NOT_CHECK     2'b10
`define TRUE_2         2'b01
`define FALSE_2     2'b00
`define WAY_0       1'b0
`define WAY_1         1'b1

parameter NUM_ENTRIES=8;
parameter NUM_WAYS=4;
localparam QUADWORD=64;
localparam SET_INDEX_BITS=$clog2(NUM_ENTRIES / NUM_WAYS);
localparam NUM_SETS=NUM_ENTRIES/NUM_WAYS;
localparam NUM_PLRT_LEVELS=$clog2(NUM_WAYS);

module testbench;
    // INPUTS
    logic                           clock;
    logic                           reset;

    // For determining if branch, where to go?
    logic   [1:0][QUADWORD-1:0]     pc_in;

    // For updating the BTB
    // First commiting instruction in way[1], second at way[0]
    logic   [1:0]                   update_valid;
    logic   [1:0][QUADWORD-1:0]     update_pc;
    logic   [1:0][QUADWORD-1:0]     update_next_pc;
    logic   [1:0]                   update_branch_actually_taken;
    logic   [1:0]                   update_branch_unconditional;

    logic   [1:0]                   nuke;

    // OUTPUTS
    logic   [1:0]                   valid_out;
    logic   [1:0][QUADWORD-1:0]     next_pc_out;
    logic   [1:0]                   unconditional_branch_out;



    btb #(.NUM_ENTRIES(NUM_ENTRIES), .NUM_WAYS(NUM_WAYS)) btb1(
        .clock(clock),
        .reset(reset),
        .pc_in(pc_in),
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_next_pc(update_next_pc),
        .update_branch_actually_taken(update_branch_actually_taken),
        .update_branch_unconditional(update_branch_unconditional),
        .nuke(nuke),
        .valid_out(valid_out),
        .next_pc_out(next_pc_out),
        .unconditional_branch_out(unconditional_branch_out)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current BTB enteries
        $display("===BTB===");
        for (int i=0; i < NUM_SETS; i++) begin
            $display("\n\nSet Number: %5d ", i);
                for (int j=0; j < NUM_WAYS; j++) begin
                $display ("\tWay Number: %5d ", j,
                        "Tag: %5d ", btb1.pc_tag[i][j],
                        "Data: %5d ", btb1.target_pc[i][j],
                        "Valid: %5d ", btb1.valid[i][j],
                        "Uncond Branch : %b ", btb1.unconditional_branch[i][j]);
                end
        end

        //display all the current PLRT enteries
        $display("\n\n===PLRT===");
        for (int i=0; i < NUM_SETS; i++) begin
            $display("\n\nSet Number: %5d ", i);
            for (int j=1; j< NUM_WAYS ; j++) begin
                $display ("PLRT index: %5d ", j,
                      "PLRT Entry: %d ", btb1.plrt[i][j]);
            end
        end
        $fatal("INCORRECT");
    endtask

    task fetch;
        input                   way;
        input [QUADWORD-1:0]    pc;

        pc_in[way]  = pc;

    endtask

    task check_fetch;
        input                   way;
        input                   valid;
        input [QUADWORD-1:0]    next_pc;
        input                   uncond_br;

        if (valid_out[way] != valid) exit_on_error();
        if (next_pc_out[way] != next_pc && valid) exit_on_error();
        if (unconditional_branch_out[way] != uncond_br && valid) exit_on_error();
    endtask

    task commit;
        input                   way;
        input                   valid;
        input [QUADWORD-1:0]    pc;
        input [QUADWORD-1:0]    next_pc;
        input                   actually_taken;
        input                   uncond;

        update_valid[way]                       = valid;
        update_pc[way]                          = pc;
        update_next_pc[way]                     = next_pc;
        update_branch_actually_taken[way]       = actually_taken;
        update_branch_unconditional[way]        = uncond;

    endtask


    task check_entry;
        input   [SET_INDEX_BITS-1:0]       set_idx;
        input   [NUM_PLRT_LEVELS-1:0]      way;
        input   [QUADWORD-1:0]             tag;
        input   [QUADWORD-1:0]             data;
        input                              valid;
        input                              uncond;

        if (btb1.pc_tag[set_idx][way] != tag && valid) exit_on_error();
        if (btb1.target_pc[set_idx][way] != data && valid) exit_on_error();
        if (btb1.valid[set_idx][way] != valid) exit_on_error();
        if (btb1.unconditional_branch[set_idx][way] != uncond && valid) exit_on_error();
    endtask

    task check_plrt_entry;
        input   [SET_INDEX_BITS-1:0]       set_idx;
        input   [NUM_PLRT_LEVELS-1:0]      plrt_idx;
        input                              entry;

        if (btb1.plrt[set_idx][plrt_idx] != entry) exit_on_error();
    endtask

    task check_plrt;
        input   [SET_INDEX_BITS-1:0]       set_idx;
        input   [NUM_WAYS-1:1]             entry;

        if (btb1.plrt[set_idx] != entry) exit_on_error();
    endtask



    initial
    begin
        $monitor("===WAY_0 INPUTS===\nTime= %5d  PC in= %5d || UPDATE: Valid= %b PC= %5d Next PC= %5d Actually Taken= %b Uncond= %1b \n",  $time, pc_in[0], update_valid[0], update_pc[0], update_next_pc[0], update_branch_actually_taken[0], update_branch_unconditional[0],
                 "===WAY_0 OUTPUTS===\nTime= %5d PC Out Valid= %b PC Out= %5d PC Out Uncond= %b\n", $time, valid_out[0], next_pc_out[0], unconditional_branch_out[0],
                 "===WAY_1 INPUTS===\nTime= %5d  PC in= %5d || UPDATE: Valid= %b PC= %5d Next PC= %5d Actually Taken= %b Uncond= %1b\n",  $time, pc_in[1], update_valid[1], update_pc[1], update_next_pc[1], update_branch_actually_taken[1], update_branch_unconditional[1],
                 "===WAY_1 OUTPUTS===\nTime= %5d PC Out Valid= %b PC Out= %5d PC Out Uncond= %b\n", $time, valid_out[1], next_pc_out[1], unconditional_branch_out[1]);

        clock=0;
        reset=1;
        pc_in = 0;
        update_valid = 0;
        update_pc = 0;
        update_next_pc = 0;
        update_branch_unconditional = 0;
        update_branch_actually_taken = 0;

        @(negedge clock);
        //CHECK reset
        reset = 0;
        if (valid_out != 0) exit_on_error();
        if (next_pc_out != 0) exit_on_error();
        if (unconditional_branch_out != 0) exit_on_error();
        $display("\n@@@Passed reset\n");

        // CACHE = 8 entries, 4 way, 2 sets.
        // Check empty cache for a value
        @(negedge clock);
        fetch(`WAY_1, 64'd5);
        fetch(`WAY_0, 64'd6);
        @(negedge clock);
        check_fetch(`WAY_1, `FALSE, 64'd0, 0);
        check_fetch(`WAY_0, `FALSE, 64'd1, 1);
        check_plrt(1'd0, 3'd0);
        check_plrt(1'd1, 3'd0);
        $display("\n@@@Passed check empty cache\n");

        // Commit to Cache, way 1
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'h0, 64'd50, `TRUE, `TRUE);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b0, 2'd0, 64'd0, 64'd50, `TRUE, `TRUE);
        check_plrt(1'b0, 3'd3);
        $display("\n@@@Passed commit way 1\n");

        // Commit to Cache, way 0
        @(negedge clock);
        commit(`WAY_0, `TRUE, 64'h4, 64'd60, `TRUE, `TRUE);
        @(negedge clock);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b1, 2'd0, 64'd4, 64'd60, `TRUE, `TRUE);
        check_plrt(1'b1, 3'd3);
        $display("\n@@@Passed commit way 0\n");

        // Commit to Cache, both way
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd12, 64'd70, `TRUE, `FALSE);
        commit(`WAY_0, `TRUE, 64'd8, 64'd80, `TRUE, `FALSE);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b0, 2'd2, 64'd8, 64'd80, `TRUE, `FALSE);
        check_entry(1'b1, 2'd2, 64'd12, 64'd70, `TRUE, `FALSE);
        check_plrt(1'b0, 3'd6);
        check_plrt(1'b1, 3'd6);
        $display("\n@@@Passed commit both ways\n");

        // Commit to Cache, both way but branch not actually taken
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd10, 64'd90, `FALSE, `TRUE);
        commit(`WAY_0, `TRUE, 64'd16, 64'd100, `FALSE, `TRUE);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b0, 2'd1, 64'd8, 64'd80, `FALSE, `FALSE);
        check_entry(1'b1, 2'd1, 64'd12, 64'd70, `FALSE, `FALSE);
        check_plrt(1'b0, 3'd6);
        check_plrt(1'b1, 3'd6);
        $display("\n@@@Passed commit both ways, not a actually taken branch\n");

        // Fetch both ways
        @(negedge clock);
        fetch(`WAY_1, 64'd4);
        fetch(`WAY_0, 64'd8);
        @(negedge clock);
        check_fetch(`WAY_1, `TRUE, 64'd60, 1);
        check_fetch(`WAY_0, `TRUE, 64'd80, 0);
        check_plrt(1'b0, 3'b110);
        check_plrt(1'b1, 3'b110);
        $display("\n@@@Passed fetch both ways\n");

        // Fetch both ways and commit - not forwarding
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd9, 64'd110, `TRUE, `TRUE);
        commit(`WAY_0, `TRUE, 64'd13, 64'd120, `TRUE, `TRUE);
        fetch(`WAY_1, 64'd0);
        fetch(`WAY_0, 64'd12);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b0, 2'd1, 64'd9, 64'd110, `TRUE, `TRUE);
        check_entry(1'b1, 2'd1, 64'd13, 64'd120, `TRUE, `TRUE);
        check_fetch(`WAY_1, `TRUE, 64'd50, `TRUE);
        check_fetch(`WAY_0, `TRUE, 64'd70, `FALSE);
        check_plrt(1'b0, 3'b101);
        check_plrt(1'b1, 3'b101);
        $display("\n@@@Passed fetch and commit both ways\n");

        // Fetch both ways and commit way 1- not forwarding
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd20, 64'd130, `TRUE, `FALSE);
        fetch(`WAY_1, 64'd4);
        fetch(`WAY_0, 64'd13);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b1, 2'd3, 64'd20, 64'd130, `TRUE, `FALSE);
        check_fetch(`WAY_1, `TRUE, 64'd60, `TRUE);
        check_fetch(`WAY_0, `TRUE, 64'd120, `TRUE);
        check_plrt(1'b0, 3'b101);
        check_plrt(1'b1, 3'b000);
        $display("\n@@@Passed fetch both ways and commit way 1\n");

        // Fetch both ways and commit- forwarding
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd11, 64'd140, `TRUE, `TRUE);
        commit(`WAY_0, `TRUE, 64'd15, 64'd150, `TRUE, `FALSE);
        fetch(`WAY_1, 64'd15);
        fetch(`WAY_0, 64'd11);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b1, 2'd0, 64'd15, 64'd150, `TRUE, `FALSE);
        check_entry(1'b0, 2'd3, 64'd11, 64'd140, `TRUE, `TRUE);
        check_fetch(`WAY_1, `TRUE, 64'd150, `FALSE);
        check_fetch(`WAY_0, `TRUE, 64'd140, `TRUE);
        check_plrt(1'b0, 3'b000);
        check_plrt(1'b1, 3'b011);
        $display("\n@@@Passed fetch both ways and commit and forwarding\n");

        // Fetch both ways and commit, 1 invalid- forwarding
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd28, 64'd160, `FALSE, `TRUE);
        commit(`WAY_0, `TRUE, 64'd36, 64'd170, `TRUE, `TRUE);
        fetch(`WAY_1, 64'd36);
        fetch(`WAY_0, 64'd36);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b1, 2'd2, 64'd36, 64'd170, `TRUE, `TRUE);
        check_fetch(`WAY_1, `TRUE, 64'd170, `TRUE);
        check_fetch(`WAY_0, `TRUE, 64'd170, `TRUE);
        check_plrt(1'b0, 3'b000);
        check_plrt(1'b1, 3'b110);
        $display("\n@@@Passed fetch both ways and commit and forwarding\n");

        // Fetch both ways and commit to set 0- forwarding
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd24, 64'd180, `TRUE, `FALSE);
        commit(`WAY_0, `TRUE, 64'd32, 64'd190, `TRUE, `TRUE);
        fetch(`WAY_1, 64'd32);
        fetch(`WAY_0, 64'd24);
        @(negedge clock);
        commit(`WAY_1, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        commit(`WAY_0, `FALSE, 64'h0, 64'd50, `TRUE, `FALSE);
        check_entry(1'b0, 2'd0, 64'd24, 64'd180, `TRUE, `FALSE);
        check_entry(1'b0, 2'd2, 64'd32, 64'd190, `TRUE, `TRUE);
        check_fetch(`WAY_1, `TRUE, 64'd190, `TRUE);
        check_fetch(`WAY_0, `TRUE, 64'd180, `FALSE);
        check_plrt(1'b0, 3'b110);
        check_plrt(1'b1, 3'b110);
        $display("\n@@@Passed fetch both ways and commit and forwarding\n");

        // Commit a value already present- forwarding
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd24, 64'd200, `TRUE, `FALSE);
        @(negedge clock);
        check_entry(1'b0, 2'd0, 64'd24, 64'd200, `TRUE, `FALSE);
        check_entry(1'b0, 2'd1, 64'd9, 64'd110, `TRUE, `TRUE);
        check_plrt(1'b0, 3'b111);
        check_plrt(1'b1, 3'b110);
        $display("\n@@@Passed commit present value\n");

        // Nuke
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd40, 64'd210, `TRUE, `FALSE);
        commit(`WAY_0, `TRUE, 64'd48, 64'd220, `TRUE, `FALSE);
        nuke = 2'b11;
        @(negedge clock);
        check_entry(1'b0, 2'd3, 64'd40, 64'd210, `TRUE, `FALSE);
        check_entry(1'b0, 2'd1, 64'd9, 64'd110, `TRUE, `TRUE);
        check_plrt(1'b0, 3'b010);
        check_plrt(1'b1, 3'b110);
        $display("\n@@@Passed way 1\n");
        //exit_on_error();

        // Nuke
        @(negedge clock);
        commit(`WAY_1, `TRUE, 64'd48, 64'd230, `TRUE, `FALSE);
        commit(`WAY_0, `TRUE, 64'd56, 64'd240, `TRUE, `FALSE);
        nuke = 2'b01;
        @(negedge clock);
        check_entry(1'b0, 2'd1, 64'd48, 64'd230, `TRUE, `FALSE);
        check_entry(1'b0, 2'd2, 64'd56, 64'd240, `TRUE, `FALSE);
        check_plrt(1'b0, 3'b100);
        check_plrt(1'b1, 3'b110);
        $display("\n@@@Passed way 0 nuke\n");

        //exit_on_error();
        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

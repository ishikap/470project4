`default_nettype none
`timescale 1ns/100ps
module btb_branch_pred #(parameter NUM_BTB_ENTRIES=32, parameter NUM_BTB_WAYS=4, parameter NUM_BIMODAL_BRANCH_PRED_ENTRIES=32, parameter NUM_GSHARE_BRANCH_PRED_ENTRIES=32, parameter COUNTER_BITS=2,
                        parameter TOURNAMENT_ENTRIES = 16,
                        localparam QUADWORD=64, localparam INDEX = $clog2(TOURNAMENT_ENTRIES)) (
    // INPUTS
    input   wire                            clock,
    input   wire                            reset,
    input   wire    [1:0][QUADWORD-1:0]     pc_in,
    input   wire    [1:0]                   update_valid,
    input   wire    [1:0][QUADWORD-1:0]     update_pc,
    input   wire    [1:0][QUADWORD-1:0]     update_next_pc,
    input   wire    [1:0]                   update_branch_actually_taken,
    input   wire    [1:0]                   update_branch_unconditional,
    input   wire    [1:0]                   update_branch_conditional,
    input   wire    [1:0]                   nuke,

    // OUTPUTS
    output  logic   [1:0]                   predict_taken,
    output  logic   [1:0][QUADWORD-1:0]     next_pc_out
);

    logic   [1:0]                           branch_pred_prediction, gshare, bimodal;
    wire    [1:0]                           btb_valid_out;
    wire    [1:0]                           unconditional_branch_out;
    logic   [TOURNAMENT_ENTRIES-1:0][3:0]  pred_counter, pred_counter_next;

    always_comb begin
        pred_counter_next       = pred_counter;

        // Update on mispredict
        for (int i=1; i>=0; --i) begin
            if (nuke[i]) begin
                if(!pred_counter_next[update_pc[i][INDEX+1:2]][3]) begin
                    pred_counter_next[update_pc[i][INDEX+1:2]]  = pred_counter_next[update_pc[i][INDEX+1:2]] + 1;
                end
                else begin
                    pred_counter_next[update_pc[i][INDEX+1:2]]  = pred_counter_next[update_pc[i][INDEX+1:2]] - 1;
                end
            end
        end

        // Assign prediction output
        for (int i=1; i>=0; --i) begin
            branch_pred_prediction[i]   = pred_counter_next[pc_in[i][INDEX+1:2]][3] ? gshare[i] : bimodal[i];
        end

        // Predict taken if BTB has entry and it is either unconditional or it is predicted taken
        predict_taken = btb_valid_out & (branch_pred_prediction | unconditional_branch_out);
    end

    btb #(.NUM_ENTRIES(NUM_BTB_ENTRIES), .NUM_WAYS(NUM_BTB_WAYS)) btb1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        // From internal state
        .pc_in(pc_in),

        // From the ROB
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_next_pc(update_next_pc),
        .update_branch_actually_taken(update_branch_actually_taken),
        .update_branch_unconditional(update_branch_unconditional),

        .nuke(nuke),

        // OUTPUTS
        .valid_out(btb_valid_out),
        .next_pc_out(next_pc_out),
        .unconditional_branch_out(unconditional_branch_out)
    );

    gshare_branch_pred_holder #(.NUM_ENTRIES(NUM_GSHARE_BRANCH_PRED_ENTRIES), .COUNTER_BITS(COUNTER_BITS)) bp_gshare (
        // INPUTS
        .clock(clock),
        .reset(reset),
        // From internal state
        .pc_in(pc_in),
        // From the ROB
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_branch_actually_taken(update_branch_actually_taken),
        .update_branch_conditional(update_branch_conditional),
        .nuke(nuke),
        // From the BTB
        .btb_valid_out(btb_valid_out),
        .btb_unconditional_branch(unconditional_branch_out),

        // OUTPUTS
        .prediction(gshare)
    );

    branch_pred #(.NUM_ENTRIES(NUM_BIMODAL_BRANCH_PRED_ENTRIES), .COUNTER_BITS(COUNTER_BITS)) bp_bimodal (
        // INPUTS
        .clock(clock),
        .reset(reset),
        // From internal state
        .pc_in(pc_in),

        // From the ROB
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_branch_actually_taken(update_branch_actually_taken),
        .update_branch_conditional(update_branch_conditional),

        .nuke(nuke),

        // OUTPUTS
        .prediction(bimodal)
    );

    always_ff @(posedge clock) begin
        if(reset) begin
            for (int i = 0; i < TOURNAMENT_ENTRIES; ++i) begin
                pred_counter[i]     <= `SD 3'b0;
            end
        end
        else begin
            pred_counter            <= `SD pred_counter_next;
        end
    end
endmodule
`default_nettype wire
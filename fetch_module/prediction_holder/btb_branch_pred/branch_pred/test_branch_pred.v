//For: Testing Branch Predictor
//Commented certain checks to run synthesis
//TODO:
`timescale 1ns/100ps
`define NOT_CHECK   2'b10
`define TRUE_2      2'b01
`define FALSE_2     2'b00
`define WAY_0       1'b0
`define WAY_1       1'b1

parameter NUM_ENTRIES = 8;
parameter COUNTER_BITS = 2;
localparam INDEX = $clog2(NUM_ENTRIES);
localparam QUADWORD = 64;

module testbench;
    // INPUTS
    logic                            clock;
    logic                            reset;
    logic    [1:0][QUADWORD-1:0]     pc_in;
    //update on commit
    logic    [1:0]                   update_valid;
    logic    [1:0][QUADWORD-1:0]     update_pc;
    logic    [1:0]                   update_branch_actually_taken;
    logic    [1:0]                   update_branch_conditional;
    logic    [1:0]                   nuke;
    // OUTPUTS
    logic    [1:0]                   prediction;


    branch_pred #(.NUM_ENTRIES(NUM_ENTRIES), .COUNTER_BITS(COUNTER_BITS)) bp1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .pc_in(pc_in),
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_branch_actually_taken(update_branch_actually_taken),
        .update_branch_conditional(update_branch_conditional),
        .nuke(nuke),
        // OUTPUT(S)
        .prediction(prediction)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current Predictor Table enteries
        $display("===Branch Predictor===");
        for (int i=0; i < NUM_ENTRIES; i++) begin
            //$display("Entry Number: %d", i,
            //          "Counter Value: %b\n", bp1.predictor_table[i]);
        end

        $fatal("INCORRECT");
    endtask

    task request_prediction;
        input                   way;
        input [QUADWORD-1:0]    pc;

        pc_in[way]  = pc;

    endtask

    task check_prediction;
        input                   way;
        input                   pred;

        if (prediction[way] != pred) exit_on_error();
    endtask

    task update_prediction;
        input                   way;
        input                   valid;
        input [QUADWORD-1:0]    pc;
        input                   actually_taken;
        input                   cond;

        update_valid[way]                       = valid;
        update_pc[way]                          = pc;
        update_branch_actually_taken[way]       = actually_taken;
        update_branch_conditional[way]          = cond;

    endtask


    task check_entry;
        input   [INDEX-1:0]               idx;
        input   [COUNTER_BITS-1:0]        counter;

        // if (bp1.predictor_table[idx] != counter) exit_on_error();
    endtask

    initial
    begin
        $monitor("===WAY_0 INPUTS===\nTime= %d  PC in= %d || UPDATE: Valid= %b PC= %d Actually Taken= %b Cond= %b Nuke= %b \n",  $time, pc_in[0], update_valid[0], update_pc[0], update_branch_actually_taken[0], update_branch_conditional[0], nuke[0],
                 "===WAY_0 OUTPUTS===\nTime= %d Prediction= %b\n", $time, prediction[0],
                 "===WAY_1 INPUTS===\nTime= %d  PC in= %d || UPDATE: Valid= %b PC= %d Actually Taken= %b Cond= %b Nuke= %b \n",  $time, pc_in[1], update_valid[1], update_pc[1], update_branch_actually_taken[1], update_branch_conditional[1], nuke[1],
                 "===WAY_1 OUTPUTS===\nTime= %d Prediction= %b\n", $time, prediction[1]);

        clock                           = 0;
        reset                           = 1;
        pc_in                           = 0;
        update_valid                    = 0;
        update_pc                       = 0;
        update_branch_actually_taken    = 0;
        update_branch_conditional       = 0;
        nuke                            = 0;

        //CHECK reset
        @(negedge clock);
        reset = 0;
        for (int i=0; i<NUM_ENTRIES; ++i) begin
            // if (bp1.predictor_table[i] != 0) exit_on_error();
        end
        check_prediction(`WAY_0, `FALSE);
        check_prediction(`WAY_1, `FALSE);
        $display("\n@@@Passed reset\n");

        //Request Prediction way 1 only
        @(negedge clock);
        request_prediction(`WAY_1, 64'd4);  //Entry 1
        @(negedge clock);
        check_prediction(`WAY_1, `FALSE);
        $display("\n@@@Passed request prediction way 1 only\n");

        //Update Prediction way 1 only - increment one enb11)try
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd4, `TRUE, `TRUE);
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b01);
        $display("\n@@@Passed update prediction way 1 only - increment one entry\n");

        //Update Prediction way 0 only - increment one entry
        @(negedge clock);
        update_prediction(`WAY_0, `TRUE, 64'd7, `TRUE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b10);
        $display("\n@@@Passed update prediction way 0 only - increment one entry\n");

        //Update Prediction update consecutive
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd16, `TRUE, `TRUE);
        request_prediction(`WAY_1, 64'd16);
        #1;
        check_prediction(`WAY_1, `FALSE);
        @(negedge clock);
        check_entry(5'd4, 2'b01);
        update_prediction(`WAY_1, `TRUE, 64'd48, `TRUE, `TRUE);
        #1;
        check_prediction(`WAY_1, `TRUE);
        @(negedge clock);
        check_entry(5'd4, 2'b10);
        update_prediction(`WAY_1, `FALSE, 64'd48, `TRUE, `TRUE);
        $display("\n@@@Passed update consecutive\n");

        //////////////////////
        //Update Prediction update two ways same cycle, saturate
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd16, `TRUE, `TRUE);
        update_prediction(`WAY_0, `TRUE, 64'd48, `TRUE, `TRUE);
        @(negedge clock);
        check_entry(5'd4, 2'b11);
        update_prediction(`WAY_1, `FALSE, 64'd48, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd48, `TRUE, `TRUE);
        $display("\n@@@Passed update two ways same cycle, saturate\n");

        //Request Prediction way 0 only
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        request_prediction(`WAY_0, 64'd5);  //Entry 1
        @(negedge clock);
        check_prediction(`WAY_0, `TRUE);
        $display("\n@@@Passed request prediction way 0 only\n");

        //Request Prediction way 1 only
        @(negedge clock);
        request_prediction(`WAY_1, 64'd4);  //Entry 1
        @(negedge clock);
        check_prediction(`WAY_1, `TRUE);
        $display("\n@@@Passed request prediction way 1 only\n");

        //Update Prediction both ways - increment two different entries
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd2, `TRUE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd7, `TRUE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b11);
        check_entry(5'd0, 2'b01);
        $display("\n@@@Passed update prediction both ways - increment two different entries\n");

        //Update Prediction both ways - increment the same entry
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd1, `TRUE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd0, `TRUE, `TRUE);      //Entry 0
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b11);
        check_entry(5'd0, 2'b11);
        $display("\n@@@Passed update prediction both ways - increment the same entry\n");

        //Request Prediction both ways
        @(negedge clock);
        request_prediction(`WAY_1, 64'd4);  //Entry 1
        request_prediction(`WAY_0, 64'd3);  //Entry 0
        @(negedge clock);
        check_prediction(`WAY_1, `TRUE);
        check_prediction(`WAY_0, `TRUE);
        $display("\n@@@Passed request prediction both ways\n");

        //Update Prediction both ways - saturation at 11
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd1, `TRUE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd7, `TRUE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b11);
        check_entry(5'd0, 2'b11);
        $display("\n@@@Passed update prediction both ways - saturation at 11\n");

        //Update Prediction both ways - decrement two different entries
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd2, `FALSE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd4, `FALSE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b10);
        check_entry(5'd0, 2'b10);
        $display("\n@@@Passed update prediction both ways - decrement two different entries\n");

        //Update Prediction both ways - unconditional branch but actually taken - so no incrementing
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd1, `TRUE, `FALSE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd7, `TRUE, `FALSE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b10);
        check_entry(5'd0, 2'b10);
        $display("\n@@@Passed update prediction both ways - unconditional branch but actually taken\n");

        //Update Prediction both ways - unconditional branch but actually not taken - so not decrementing
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd2, `FALSE, `FALSE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd4, `FALSE, `FALSE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b10);
        check_entry(5'd0, 2'b10);
        $display("\n@@@Passed update prediction both ways - unconditional branch but actually not taken\n");

        //Update Prediction both ways - decrement the same entry
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd7, `FALSE, `TRUE);      //Entry 1
        update_prediction(`WAY_0, `TRUE, 64'd4, `FALSE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b00);
        check_entry(5'd0, 2'b10);
        $display("\n@@@Passed update prediction both ways - decrement the same entry\n");

        //Update Prediction both ways - saturation at 00
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd1, `FALSE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd7, `FALSE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b00);
        check_entry(5'd0, 2'b01);
        $display("\n@@@Passed update prediction both ways - saturation at 11\n");

        //Simultaneous update and request prediction - with forwarding
        @(negedge clock);
        update_prediction(`WAY_0, `TRUE, 64'd7, `TRUE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b01);
        check_entry(5'd0, 2'b01);
        request_prediction(`WAY_1, 64'd4);  //Entry 1
        request_prediction(`WAY_0, 64'd3);  //Entry 0
        @(negedge clock);
        update_prediction(`WAY_1, `TRUE, 64'd1, `TRUE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd7, `TRUE, `TRUE);      //Entry 1
        check_prediction(`WAY_1, `FALSE);
        check_prediction(`WAY_0, `FALSE);
        request_prediction(`WAY_1, 64'd4);  //Entry 1
        request_prediction(`WAY_0, 64'd3);  //Entry 0
        @(negedge clock);
        check_prediction(`WAY_1, `TRUE);
        check_prediction(`WAY_0, `TRUE);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b10);
        check_entry(5'd0, 2'b10);
        $display("\n@@@Passed Simultaneous update and request prediction - with forwarding\n");

        //Nuke way 1
        @(negedge clock);
        nuke = 2'b10;
        update_prediction(`WAY_1, `TRUE, 64'd2, `FALSE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd4, `FALSE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b10);
        check_entry(5'd0, 2'b01);
        $display("\n@@@Passed nuke way 1\n");

        //Nuke way 0
        @(negedge clock);
        nuke = 2'b01;
        update_prediction(`WAY_1, `TRUE, 64'd2, `FALSE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd4, `FALSE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b01);
        check_entry(5'd0, 2'b00);
        $display("\n@@@Passed nuke way 0\n");

        //Nuke both ways
        @(negedge clock);
        nuke = 2'b11;
        update_prediction(`WAY_1, `TRUE, 64'd2, `TRUE, `TRUE);      //Entry 0
        update_prediction(`WAY_0, `TRUE, 64'd4, `TRUE, `TRUE);      //Entry 1
        @(negedge clock);
        update_prediction(`WAY_1, `FALSE, 64'd4, `TRUE, `TRUE);
        update_prediction(`WAY_0, `FALSE, 64'd4, `TRUE, `TRUE);
        check_entry(5'd1, 2'b01);
        check_entry(5'd0, 2'b01);
        $display("\n@@@Passed nuke both ways\n");


        //exit_on_error();
        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

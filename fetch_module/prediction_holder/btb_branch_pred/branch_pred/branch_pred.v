//clock period 4.5 < t <= 4.5625
`default_nettype none
`timescale 1ns/100ps
// BIMODAL PREDICTOR
module branch_pred #(parameter NUM_ENTRIES = 8, parameter COUNTER_BITS = 2, localparam INDEX = $clog2(NUM_ENTRIES), localparam QUADWORD = 64) (
    // INPUTS
    input   wire                            clock,
    input   wire                            reset,
    input   wire    [1:0][QUADWORD-1:0]     pc_in,
    //update on commit, from ROB
    input   wire    [1:0]                   update_valid,
    input   wire    [1:0][QUADWORD-1:0]     update_pc,
    input   wire    [1:0]                   update_branch_actually_taken,
    input   wire    [1:0]                   update_branch_conditional,
    input   wire    [1:0]                   nuke,

    // OUTPUTS
    output  logic   [1:0]                   prediction
);

    logic   [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]     predictor_table;
    logic   [NUM_ENTRIES-1:0][COUNTER_BITS-1:0]     predictor_table_next;
    logic                                           nuked;

    always_comb begin
        predictor_table_next    = predictor_table;
        prediction              = 0;
        nuked                   = 0;

        // Update on commit
        for (int i=1; i>=0; --i) begin
            if (update_valid[i] && update_branch_conditional[i] && !nuked) begin
                if(update_branch_actually_taken[i]) begin
                    if(predictor_table_next[update_pc[i][INDEX+1:2]] != (2 ** COUNTER_BITS)-1) begin
                        predictor_table_next[update_pc[i][INDEX+1:2]] = predictor_table_next[update_pc[i][INDEX+1:2]] + 1;
                    end
                end
                else begin
                    if(predictor_table_next[update_pc[i][INDEX+1:2]] != 0) begin
                        predictor_table_next[update_pc[i][INDEX+1:2]] = predictor_table_next[update_pc[i][INDEX+1:2]] - 1;
                    end
                end
            end
            if(nuke[i]) begin
                nuked = 1;
            end
        end
        // Assign prediction output
        for (int i=1; i>=0; --i) begin
            prediction[i] = predictor_table_next[pc_in[i][INDEX+1:2]][COUNTER_BITS-1];
        end
    end

    always_ff @(posedge clock) begin
        if(reset) begin
            for (int i = 0; i < NUM_ENTRIES; ++i) begin
                predictor_table <= `SD 2'b10;
            end
        end
        else begin
            predictor_table <= `SD predictor_table_next;
        end
    end
endmodule

`default_nettype wire

`default_nettype none
`timescale 1ns/100ps
module ras #(parameter RAS_SIZE = 8, localparam QUADWORD = 64,localparam RAS_BITS = $clog2(RAS_SIZE))(
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,
    input   wire    [1:0][QUADWORD-1:0]             ras_return_addr,
    input   wire    [1:0]                           ras_return_addr_en,             // Push in RAS
    input   wire    [1:0]                           ras_return_addr_request_en,     // Pop out of RAS
    input   wire    [1:0]                           nuke,
    input   wire    [RAS_SIZE-1:0][QUADWORD-1:0]    rras_entries,
    input   wire    [RAS_BITS-1:0]                  rras_pointer,

    // OUTPUTS
    output  logic   [1:0][QUADWORD-1:0]             ras_return_addr_out,
    output  logic   [1:0]                           ras_return_addr_out_valid
);

    logic   [RAS_SIZE-1:0][QUADWORD-1:0]    ras_entries, ras_entries_next;
    logic   [RAS_BITS-1:0]                  ras_pointer, ras_pointer_next;

    always_comb begin
        ras_entries_next                = ras_entries;
        ras_pointer_next                = ras_pointer;
        ras_return_addr_out             = 0;
        ras_return_addr_out_valid       = 0;

        // Nuke
        if (|nuke) begin
            ras_entries_next                        = rras_entries;
            ras_pointer_next                        = rras_pointer;
        end

        for (int i=1; i>=0; --i) begin
            // Pop from RAS
            if (ras_return_addr_request_en[i]) begin
                ras_pointer_next                    = ras_pointer_next - 1;
                ras_return_addr_out_valid[i]        = 1;
                ras_return_addr_out[i]              = ras_entries_next[ras_pointer_next];
            end
            // Push into RAS
            else if (ras_return_addr_en[i]) begin
                ras_entries_next[ras_pointer_next]  = ras_return_addr[i];
                ras_pointer_next                    = ras_pointer_next + 1;
            end
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            ras_entries                 <= `SD 0;
            ras_pointer                 <= `SD 0;
        end
        else begin
            ras_entries                 <= `SD ras_entries_next;
            ras_pointer                 <= `SD ras_pointer_next;
        end
    end
endmodule
`default_nettype wire

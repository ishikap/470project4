//For: Testing RAT
//By: Scott
`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define WAY_0           1'b0
`define WAY_1           1'b1

parameter RAS_SIZE          = 8;
parameter QUADWORD          = 64;
parameter RAS_BITS          = $clog2(RAS_SIZE);

module testbench;

    logic                               clock;
    logic                               reset;
    logic   [1:0][QUADWORD-1:0]         ras_return_addr;
    logic   [1:0]                       ras_return_addr_en;             // Push in RAS
    logic   [1:0]                       ras_return_addr_request_en;     // Pop out of RAS
    logic   [1:0]                       nuke;
    logic   [RAS_SIZE-1:0][QUADWORD-1:0] rras_entries;
    logic   [RAS_BITS-1:0]              rras_pointer;

    // OUTPUTS
    logic   [1:0][QUADWORD-1:0]         ras_return_addr_out;
    logic   [1:0]                       ras_return_addr_out_valid;



    ras #(.RAS_SIZE(RAS_SIZE)) ras1(
           .clock(clock),
           .reset(reset),
           .ras_return_addr(ras_return_addr),
           .ras_return_addr_en(ras_return_addr_en),
           .ras_return_addr_request_en(ras_return_addr_request_en),
           .nuke(nuke),
           .rras_entries(rras_entries),
           .rras_pointer(rras_pointer),
           .ras_return_addr_out(ras_return_addr_out),
           .ras_return_addr_out_valid(ras_return_addr_out_valid)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current RAS enteries
        $display("===RAS===");
        for (int i=0; i < RAS_SIZE; i++)
        begin
            $display ("Entry Number: %d ", i,
                      "Entry: %d ", ras1.ras_entries[i],
                      "Pointer: %d", ras1.ras_pointer);
        end
        $fatal("INCORRECT");
    endtask

    task push;
        input                   way;
        input                   en;
        input [QUADWORD-1:0]    addr;

        ras_return_addr[way]    = addr;
        ras_return_addr_en[way] = en;
    endtask

    task pop;
        input                   way;
        input                   en;

        ras_return_addr_request_en[way] = en;
    endtask

    task check_entry;
        input [RAS_BITS-1:0]    location;
        input [QUADWORD-1:0]    entry;
        input [RAS_BITS-1:0]    ptr;

        if (ras.ras_entries[location] != entry) exit_on_error();
        if (ras.ras_pointer != ptr) exit_on_error();
    endtask

    task check_out;
        input                   way;
        input [QUADWORD-1:0]    addr;
        input                   valid;

        if (ras_return_addr_out[way] != addr && valid[way]) exit_on_error();
        if (ras_return_addr_out_valid[way] != valid) exit_on_error();
    endtask


    initial
    begin
        $monitor("===WAY_0===\nTime= %4d  return_addr_in= %4d return_addr_in_en= %b return_addr_req_en= %b addr_out= %4d addr_out_valid= %b  nuke = %b\n", $time, ras_return_addr[0], ras_return_addr_en[0], ras_return_addr_request_en[0], ras_return_addr_out[0], ras_return_addr_out_valid[0], nuke[0],
                 "\n===WAY_0===\nTime= %4d  return_addr_in= %4d return_addr_in_en= %b return_addr_req_en= %b addr_out= %4d addr_out_valid= %b  nuke = %b\n", $time, ras_return_addr[1], ras_return_addr_en[1], ras_return_addr_request_en[1], ras_return_addr_out[1], ras_return_addr_out_valid[1], nuke[1]);

        clock=0;
        reset=1;
        nuke=0;
        ras_return_addr = 0;
        ras_return_addr_en = 0;
        ras_return_addr_request_en = 0;
        rras_pointer=6; //rras= 5,6,7,8,9
        for (int i = 0; i < rras_pointer; ++i)
        begin
            rras_entries[i] = i+5;
        end

        @(negedge clock);
        //CHECK reset
        reset = 0;
        for (int i = 0; i < RAS_SIZE; ++i) begin
            check_entry(i, 64'd0, 3'd0);
        end
        check_out(`WAY_0, 0, 0);
        check_out(`WAY_1, 0, 0);
        $display("\n@@@Passed reset\n");

        // Push way 1
        @(negedge clock);
        push(`WAY_1, `TRUE, 64'd50);
        @(negedge clock);
        push(`WAY_1, `FALSE, 64'd50);
        check_entry(3'd0, 64'd50, 3'd1);
        $display("\n@@@Passed push way 1\n");

        // Push way 0
        @(negedge clock);
        push(`WAY_0, `TRUE, 64'd51);
        @(negedge clock);
        push(`WAY_0, `FALSE, 64'd50);
        check_entry(3'd1, 64'd51, 3'd2);
        $display("\n@@@Passed push way 0\n");

        // Push both ways
        @(negedge clock);
        push(`WAY_1, `TRUE, 64'd52);
        push(`WAY_0, `TRUE, 64'd53);
        @(negedge clock);
        push(`WAY_1, `FALSE, 64'd50);
        push(`WAY_0, `FALSE, 64'd50);
        check_entry(3'd2, 64'd52, 3'd4);
        check_entry(3'd3, 64'd53, 3'd4);
        $display("\n@@@Passed push both way\n");

        // Pop way 1
        @(negedge clock);
        pop(`WAY_1, `TRUE);
        @(negedge clock);
        pop(`WAY_1, `FALSE);
        check_entry(3'd2, 64'd52, 3'd3);
        check_out(`WAY_1, 64'd53, `TRUE);
        $display("\n@@@Passed pop way 1\n");

        // Pop way 0
        @(negedge clock);
        pop(`WAY_0, `TRUE);
        @(negedge clock);
        pop(`WAY_0, `FALSE);
        check_entry(3'd1, 64'd51, 3'd2);
        check_out(`WAY_0, 64'd52, `TRUE);
        $display("\n@@@Passed pop way 0\n");

        // Pop both way
        @(negedge clock);
        pop(`WAY_1, `TRUE);
        pop(`WAY_0, `TRUE);
        @(negedge clock);
        pop(`WAY_1, `FALSE);
        pop(`WAY_0, `FALSE);
        check_entry(3'd0, 64'd50, 3'd0);
        check_out(`WAY_1, 64'd51, `TRUE);
        check_out(`WAY_0, 64'd50, `TRUE);
        $display("\n@@@Passed pop both way\n");

        // Pop and push both ways
        @(negedge clock);
        push(`WAY_1, `TRUE, 64'd60);
        pop(`WAY_0, `TRUE);
        @(negedge clock);
        push(`WAY_1, `FALSE, 64'd50);
        push(`WAY_0, `FALSE, 64'd50);
        pop(`WAY_1, `FALSE);
        pop(`WAY_0, `FALSE);
        check_entry(3'd0, 64'd60, 3'd0);
        check_out(`WAY_1, 64'd61, `FALSE);
        check_out(`WAY_0, 64'd60, `TRUE);
        $display("\n@@@Passed popa and push both way\n");

        // Pop and push both ways
        @(negedge clock);
        push(`WAY_1, `TRUE, 64'd70);
        push(`WAY_0, `TRUE, 64'd71);
        @(negedge clock);
        pop(`WAY_1, `TRUE);
        push(`WAY_0, `TRUE, 64'd60);
        push(`WAY_1, `FALSE, 64'd50);
        @(negedge clock);
        push(`WAY_1, `FALSE, 64'd50);
        push(`WAY_0, `FALSE, 64'd50);
        pop(`WAY_1, `FALSE);
        pop(`WAY_0, `FALSE);
        check_entry(3'd1, 64'd60, 3'd2);
        check_out(`WAY_1, 64'd71, `TRUE);
        check_out(`WAY_0, 64'd60, `FALSE);
        $display("\n@@@Passed popa and push both way\n");

        // Empty and pop
        pop(`WAY_1, `TRUE);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        push(`WAY_1, `FALSE, 64'd50);
        push(`WAY_0, `FALSE, 64'd50);
        pop(`WAY_1, `FALSE);
        pop(`WAY_0, `FALSE);
        check_entry(3'd0, 64'd70, 3'd7);
        $display("\n@@@Passed popa and push both way\n");

        // Empty and pop
        @(negedge clock);
        push(`WAY_1, `TRUE, 64'd80);
        push(`WAY_0, `TRUE, 64'd81);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        push(`WAY_1, `TRUE, 64'd82);
        push(`WAY_0, `TRUE, 64'd83);
        @(negedge clock);
        pop(`WAY_1, `TRUE);
        pop(`WAY_0, `TRUE);
        push(`WAY_1, `FALSE, 64'd50);
        push(`WAY_0, `FALSE, 64'd50);
        @(negedge clock);
        check_out(`WAY_1, 64'd83, `TRUE);
        check_out(`WAY_0, 64'd82, `TRUE);
        @(negedge clock);
        check_out(`WAY_1, 64'd81, `TRUE);
        check_out(`WAY_0, 64'd80, `TRUE);
        check_entry(3'd0, 64'd83, 3'd5);
        $display("\n@@@Passed popa and push both way\n");

        // Nuke
        @(negedge clock);
        nuke = 2'b10;
        @(negedge clock);
        check_entry(3'd0, 64'd5, 3'd6);
        check_entry(3'd1, 64'd6, 3'd6);
        check_entry(3'd2, 64'd7, 3'd6);
        check_entry(3'd3, 64'd8, 3'd6);
        check_entry(3'd4, 64'd9, 3'd6);
        check_entry(3'd5, 64'd10, 3'd6);
        //exit_on_error();


// can pop when empty , can push and pop at same time
        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

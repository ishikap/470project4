//For: Testing RAT
//By: Scott
`timescale 1ns/100ps
`define NOT_CHECK   2'b10
`define TRUE_2      2'b01
`define FALSE_2     2'b00
`define WAY_0       1'b0
`define WAY_1       1'b1

parameter RRAS_SIZE         = 8;
parameter QUADWORD          = 64;
parameter RRAS_BITS         = $clog2(RRAS_SIZE);

module testbench;

    logic                                    clock;
    logic                                    reset;
    logic    [1:0][QUADWORD-1:0]             rras_commit_return_addr;
    logic    [1:0]                           rras_commit_return_addr_en;             // Push in RRAS
    logic    [1:0]                           rras_commit_return_addr_request_en;     // Pop out of RRAS
    logic    [1:0]                           nuke;

    logic    [RRAS_SIZE-1:0][QUADWORD-1:0]   rras_entries;
    logic    [RRAS_BITS-1:0]                 rras_pointer;



    rras #(.RRAS_SIZE(RRAS_SIZE)) rras1(
            .clock(clock),
            .reset(reset),
            .rras_commit_return_addr(rras_commit_return_addr),
            .rras_commit_return_addr_en(rras_commit_return_addr_en),
            .rras_commit_return_addr_request_en(rras_commit_return_addr_request_en),
            .nuke(nuke),
            .rras_entries(rras_entries),
            .rras_pointer(rras_pointer)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current RRAS enteries
        $display("\n\n===RRAS===");
        $display("RRAS Pointer= %d", rras_pointer);
        for (int i=0; i < RRAS_SIZE; i++)
        begin
            $display ("RRAS Entry Number: %d ", i,
                      "RRAS Entry: %d ", rras_entries[i]);
        end
        $fatal("INCORRECT");
    endtask

    task commit_push;
        input                   way;
        input                   commit_return_addr_en;
        input [QUADWORD-1:0]    commit_return_addr;

        rras_commit_return_addr[way]        = commit_return_addr;
        rras_commit_return_addr_en[way]     = commit_return_addr_en;

    endtask

    task commit_pop;
        input                   way;
        input                   commit_return_addr_request_en;

        rras_commit_return_addr_request_en[way] = commit_return_addr_request_en;
    endtask

    task check_entry;
        input [RRAS_BITS-1:0]   idx;
        input [QUADWORD-1:0]    entry;

        if (rras_entries[idx] != entry) exit_on_error();
    endtask


    task check_pointer;
        input [RRAS_BITS-1:0]   ptr;

        if (rras_pointer != ptr) exit_on_error();
    endtask



    initial
    begin
        $monitor("===WAY_0 INPUTS===\nTime= %d  return addr en= %b return addr en= %d return addr request= %b\n",  $time, rras_commit_return_addr_en[0], rras_commit_return_addr[0], rras_commit_return_addr[0],
                 "===WAY_1 INPUTS===\nTime= %d  return addr en= %b return addr en= %d return addr request= %b\n",  $time, rras_commit_return_addr_en[1], rras_commit_return_addr[1], rras_commit_return_addr[1]);

        clock                               = 0;
        reset                               = 1;
        rras_commit_return_addr             = 0;
        rras_commit_return_addr_en          = 0;
        rras_commit_return_addr_request_en  = 0;
        nuke = 0;

        //CHECK reset
        @(negedge clock);
        reset = 0;
        for (int i=0; i< RRAS_SIZE; ++i) begin
            check_entry(i,0);
        end
        check_pointer(0);
        $display("\n@@@Passed reset\n");

        //Commit push way 1 only
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd45);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd45);
        check_entry(3'd0,64'd45);
        check_pointer(3'd1);
        $display("\n@@@Passed Commit push way 1\n");

        //Commit push way 0 only
        @(negedge clock);
        commit_push(`WAY_0,`TRUE,64'd46);
        @(negedge clock);
        commit_push(`WAY_0,`FALSE,64'd45);
        check_entry(3'd1,64'd46);
        check_entry(3'd0,64'd45);
        check_pointer(3'd2);
        $display("\n@@@Passed Commit push way 0\n");

        //Commit push both ways
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd47);
        commit_push(`WAY_0,`TRUE,64'd48);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd47);
        commit_push(`WAY_0,`FALSE,64'd48);
        check_entry(3'd2,64'd47);
        check_entry(3'd3,64'd48);
        check_pointer(3'd4);
        $display("\n@@@Passed Commit push both ways\n");

        //Commit pop way 1 only
        @(negedge clock);
        commit_pop(`WAY_1,`TRUE);
        @(negedge clock);
        commit_pop(`WAY_1,`FALSE);
        check_pointer(3'd3);
        $display("\n@@@Passed Commit pop way 1\n");

        //Commit pop way 0 only
        @(negedge clock);
        commit_pop(`WAY_0,`TRUE);
        @(negedge clock);
        commit_pop(`WAY_0,`FALSE);
        check_pointer(3'd2);
        $display("\n@@@Passed Commit pop way 0\n");

        //Commit pop both ways
        @(negedge clock);
        commit_pop(`WAY_1,`TRUE);
        commit_pop(`WAY_0,`TRUE);
        @(negedge clock);
        commit_pop(`WAY_1,`FALSE);
        commit_pop(`WAY_0,`FALSE);
        check_pointer(3'd0);
        $display("\n@@@Passed Commit pop both ways\n");

        //Commit push both ways - just to fill something in
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd47);
        commit_push(`WAY_0,`TRUE,64'd48);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd47);
        commit_push(`WAY_0,`FALSE,64'd48);
        check_entry(3'd0,64'd47);
        check_entry(3'd1,64'd48);
        check_pointer(3'd2);
        $display("\n@@@Passed Commit push both ways\n");

        //Simultaneous push way 1 and pop way 0
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd49);
        commit_pop(`WAY_0,`TRUE);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd49);
        commit_pop(`WAY_0,`FALSE);
        check_entry(3'd0,64'd47);
        check_entry(3'd1,64'd48);
        check_pointer(3'd2);
        $display("\n@@@Passed Simultaneous push and pop way 1\n");

        //Simultaneous push way 0 and pop way 1
        @(negedge clock);
        commit_push(`WAY_0,`TRUE,64'd50);
        commit_pop(`WAY_1,`TRUE);
        @(negedge clock);
        commit_push(`WAY_0,`FALSE,64'd49);
        commit_pop(`WAY_1,`FALSE);
        check_entry(3'd1,64'd50);
        check_entry(3'd0,64'd47);
        check_pointer(3'd2);
        $display("\n@@@Passed Simultaneous push and pop way 0\n");

        //Commit pop both ways - just to empty the stack
        @(negedge clock);
        commit_pop(`WAY_1,`TRUE);
        commit_pop(`WAY_0,`TRUE);
        @(negedge clock);
        commit_pop(`WAY_1,`FALSE);
        commit_pop(`WAY_0,`FALSE);
        check_pointer(3'd0);
        $display("\n@@@Passed Commit pop both ways\n");

        //Commit pop both ways - stack is empty - to check if the pointer wraps around
        @(negedge clock);
        commit_pop(`WAY_1,`TRUE);
        commit_pop(`WAY_0,`TRUE);
        @(negedge clock);
        commit_pop(`WAY_1,`FALSE);
        commit_pop(`WAY_0,`FALSE);
        check_pointer(3'd6);
        $display("\n@@@Passed Commit pop - stack is empty - pointer warps around\n");

        //Commit push both ways - to check if the pointer wraps around
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd55);
        commit_push(`WAY_0,`TRUE,64'd56);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd47);
        commit_push(`WAY_0,`FALSE,64'd48);
        check_entry(3'd6,64'd55);
        check_entry(3'd7,64'd56);
        check_pointer(3'd0);
        $display("\n@@@Passed Commit push - pointer wraps around\n");

        //Commit push both ways - to fill the stack completely and check overwriting and pointer wrapping around
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd57);
        commit_push(`WAY_0,`TRUE,64'd58);
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd59);
        commit_push(`WAY_0,`TRUE,64'd60);
        check_entry(3'd0,64'd57);
        check_entry(3'd1,64'd58);
        check_pointer(3'd2);
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd61);
        commit_push(`WAY_0,`TRUE,64'd62);
        check_entry(3'd2,64'd59);
        check_entry(3'd3,64'd60);
        check_pointer(3'd4);
        @(negedge clock);
        commit_push(`WAY_1,`TRUE,64'd63);
        commit_push(`WAY_0,`TRUE,64'd64);
        check_entry(3'd4,64'd61);
        check_entry(3'd5,64'd62);
        check_pointer(3'd6);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd63);
        commit_push(`WAY_0,`FALSE,64'd64);
        check_entry(3'd6,64'd63);
        check_entry(3'd7,64'd64);
        check_pointer(3'd0);
        $display("\n@@@Passed Commit push both ways - to fill the stack completely and check overwriting and pointer wrapping around\n");

        //Nuke=01
        @(negedge clock);
        nuke = 2'b01;
        commit_push(`WAY_1,`TRUE,64'd70);
        commit_push(`WAY_0,`TRUE,64'd71);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd57);
        commit_push(`WAY_0,`FALSE,64'd58);
        check_entry(3'd0,64'd70);
        check_entry(3'd1,64'd71);
        check_pointer(3'd2);
        $display("\n@@@Passed Nuke=01\n");

        //Nuke=10
        @(negedge clock);
        nuke = 2'b10;
        commit_push(`WAY_1,`TRUE,64'd72);
        commit_push(`WAY_0,`TRUE,64'd73);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd57);
        commit_push(`WAY_0,`FALSE,64'd58);
        check_entry(3'd2,64'd72);
        check_entry(3'd3,64'd60);
        check_pointer(3'd3);
        $display("\n@@@Passed Nuke=10\n");

        //Nuke=11
        @(negedge clock);
        nuke = 2'b11;
        commit_push(`WAY_1,`TRUE,64'd74);
        commit_push(`WAY_0,`TRUE,64'd75);
        @(negedge clock);
        commit_push(`WAY_1,`FALSE,64'd57);
        commit_push(`WAY_0,`FALSE,64'd58);
        check_entry(3'd3,64'd74);
        check_entry(3'd4,64'd61);
        check_pointer(3'd4);
        $display("\n@@@Passed Nuke=11\n");

        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

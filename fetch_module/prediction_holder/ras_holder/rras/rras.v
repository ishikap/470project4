//`timescale 1ns/100ps
//clock period 1.853515625 < t <= 1.8623046875
`default_nettype none
`timescale 1ns/100ps
module rras #(parameter RRAS_SIZE = 8, localparam QUADWORD = 64,localparam RRAS_BITS = $clog2(RRAS_SIZE))(
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,
    input   wire    [1:0][QUADWORD-1:0]             rras_commit_return_addr,
    input   wire    [1:0]                           rras_commit_return_addr_en,             // Push in RRAS
    input   wire    [1:0]                           rras_commit_return_addr_request_en,     // Pop out of RRAS
    input   wire    [1:0]                           nuke,

    // OUTPUTS
    output  logic   [RRAS_SIZE-1:0][QUADWORD-1:0]   rras_entries_next,                      // Output the combinational, updated state so we can get the nuke on the upcoming clock
    output  logic   [RRAS_BITS-1:0]                 rras_pointer_next
);

    logic   [RRAS_SIZE-1:0][QUADWORD-1:0]           rras_entries;
    logic   [RRAS_BITS-1:0]                         rras_pointer;

    always_comb begin
        rras_entries_next               = rras_entries;
        rras_pointer_next               = rras_pointer;

        // Way 1 followed by Way 0 - RRAS
        // Nuke[1]: commit way 1, not way 0; Nuke [0]: commit both
        for (int i=1; i>=0; --i) begin
            if (rras_commit_return_addr_request_en[i] && !(nuke[1] && (i == 0))) begin     //Pop
                rras_pointer_next                       = rras_pointer_next - 1;
            end
            else if (rras_commit_return_addr_en[i] && !(nuke[1] && (i == 0))) begin       //Push
                rras_entries_next[rras_pointer_next]    = rras_commit_return_addr[i];
                rras_pointer_next                       = rras_pointer_next + 1;
            end
        end

    end

    always_ff @(posedge clock) begin
        if (reset) begin
            rras_entries            <= `SD 0;
            rras_pointer            <= `SD 0;
        end
        else begin
            rras_entries            <= `SD rras_entries_next;
            rras_pointer            <= `SD rras_pointer_next;
        end
    end
endmodule
`default_nettype wire

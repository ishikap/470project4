`default_nettype none
`timescale 1ns/100ps
module ras_holder #(parameter RAS_SIZE=8, localparam QUADWORD=64, localparam RAS_BITS=$clog2(RAS_SIZE)) (
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,

    // From IF stage
    input   wire    [1:0][QUADWORD-1:0]             ras_return_addr,
    input   wire    [1:0]                           ras_return_addr_en,             // Push in RAS
    input   wire    [1:0]                           ras_return_addr_request_en,     // Pop out of RAS

    // From ROB
    input   wire    [1:0][QUADWORD-1:0]             rras_commit_return_addr,
    input   wire    [1:0]                           rras_commit_return_addr_en,             // Push in RRAS
    input   wire    [1:0]                           rras_commit_return_addr_request_en,     // Pop out of RRAS
    input   wire    [1:0]                           nuke,

    // OUTPUTS
    output  logic   [1:0][QUADWORD-1:0]             ras_return_addr_out,
    output  logic   [1:0]                           ras_return_addr_out_valid
);
    wire    [RAS_SIZE-1:0][QUADWORD-1:0]    rras_entries;
    wire    [RAS_BITS-1:0]                  rras_pointer;

    ras #(.RAS_SIZE(RAS_SIZE)) ras1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .ras_return_addr(ras_return_addr),
        .ras_return_addr_en(ras_return_addr_en),
        .ras_return_addr_request_en(ras_return_addr_request_en),

        .nuke(nuke),

        .rras_entries(rras_entries),
        .rras_pointer(rras_pointer),

        // OUTPUTS
        .ras_return_addr_out(ras_return_addr_out),
        .ras_return_addr_out_valid(ras_return_addr_out_valid)
    );

    rras #(.RRAS_SIZE(RAS_SIZE)) rras1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .rras_commit_return_addr(rras_commit_return_addr),
        .rras_commit_return_addr_en(rras_commit_return_addr_en),
        .rras_commit_return_addr_request_en(rras_commit_return_addr_request_en),

        .nuke(nuke),

        // OUTPUTS
        .rras_entries_next(rras_entries),
        .rras_pointer_next(rras_pointer)
    );
endmodule
`default_nettype wire
`default_nettype none
`timescale 1ns/100ps
module prediction_holder #(parameter NUM_BTB_ENTRIES=32, parameter NUM_BTB_WAYS=4, parameter NUM_BIMODAL_BRANCH_PRED_ENTRIES=32, parameter NUM_GSHARE_BRANCH_PRED_ENTRIES=32,
                            parameter COUNTER_BITS=2, parameter RAS_SIZE=8, parameter TOURNAMENT_ENTRIES = 16,
                            localparam QUADWORD=64, localparam RAS_BITS=$clog2(RAS_SIZE)) (
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,
    // From fetch
    input   wire             [1:0][QUADWORD-1:0]    pc_in,                  // Current PC used for prediction
    input   wire             [1:0][QUADWORD/2-1:0]  instr_regs,             // Instruction associated to the pc_in values
    input   wire             [1:0]                  valid_fetch,            // Whether or not we can trust the incoming instruction_reg
    // From ROB
    input   wire             [1:0]                  update_valid,
    input   wire             [1:0][QUADWORD-1:0]    update_pc,
    input   wire             [1:0][QUADWORD-1:0]    update_next_pc,
    input   wire             [1:0]                  update_call_from_rob,   // Push in RRAS
    input   wire             [1:0]                  update_ret_from_rob,    // Pop out of RRAS
    input   wire             [1:0]                  update_branch_actually_taken,
    input   wire             [1:0]                  update_branch_unconditional,
    input   wire             [1:0]                  update_branch_conditional,
    input   wire             [1:0]                  nuke,

    // OUTPUTS
    output  logic            [1:0]                  taken,
    output  logic            [1:0][QUADWORD-1:0]    next_pc

);
    logic   [1:0]                   ras_fetch_call;     // Push in RAS
    logic   [1:0]                   ras_fetch_ret;      // Pop out of RAS

    wire    [1:0]                   btb_predict_taken;
    wire    [1:0][QUADWORD-1:0]     btb_next_pc;

    wire    [1:0][QUADWORD-1:0]     ras_return_addr_out;
    wire    [1:0]                   ras_return_addr_out_valid;
    logic                           already_taken;

    always_comb begin
        taken           = 0;
        next_pc         = 0;
        already_taken   = 0;
        ras_fetch_call  = 0;
        ras_fetch_ret   = 0;



        // Determine, based on the instruction register,
        // whether to push/pop the RAS
        // If way 1 is predicted taken, don't put way 0's call/ret to RAS
        for (int i=1; i>=0; --i) begin
            if (!already_taken && valid_fetch[i]) begin
                // Bits 31:26 are `JSR_GRP for JSR, JSR_CO, RET, JMP
                // Distinguished with `JMP_INST, `JSR_INST, `RET_INST, `JSR_CO_INST for bits 15:14
                // Bits 31:26 are `BSR_INST
                if (instr_regs[i][31:26] == `JSR_GRP && instr_regs[i][15:14] == `JSR_INST) begin // For JSR

                    ras_fetch_call[i]   = 1'b1;
                end
                else if (instr_regs[i][31:26] == `BSR_INST) begin                               // For BSR
                    ras_fetch_call[i]   = 1'b1;
                    taken[i]            = 1'b1;

                    next_pc[i]          = pc_in[i] + 4 + { {41{instr_regs[i][20]}}, instr_regs[i][20:0], 2'b00 };
                    already_taken       = 1'b1;
                end
                else if (instr_regs[i][31:26] == `BR_INST) begin
                    taken[i]            = 1'b1;
                    next_pc[i]          = pc_in[i] + 4 + { {41{instr_regs[i][20]}}, instr_regs[i][20:0], 2'b00 };
                    already_taken       = 1'b1;
                end
                else if ((instr_regs[i][31:26] == `JSR_GRP && instr_regs[i][15:14] == `JSR_CO_INST) ||      // For JSR_CO
                         (instr_regs[i][31:26] == `JSR_GRP && instr_regs[i][15:14] == `RET_INST)) begin     // For RET

                    ras_fetch_ret[i]    = 1'b1;
                end

            end
            if (!already_taken) begin
                if (ras_return_addr_out_valid[i]) begin
                    taken[i]        = 1'b1;
                    next_pc[i]      = ras_return_addr_out[i];
                    already_taken   = 1'b1;
                end
                else if (btb_predict_taken[i]) begin
                    taken[i]        = 1'b1;
                    next_pc[i]      = btb_next_pc[i];
                    already_taken   = 1'b1;
                end
            end
        end
    end


    btb_branch_pred #(.NUM_BTB_ENTRIES(NUM_BTB_ENTRIES), .NUM_BTB_WAYS(NUM_BTB_WAYS), .NUM_BIMODAL_BRANCH_PRED_ENTRIES(NUM_BIMODAL_BRANCH_PRED_ENTRIES),
                      .NUM_GSHARE_BRANCH_PRED_ENTRIES(NUM_GSHARE_BRANCH_PRED_ENTRIES), .COUNTER_BITS(COUNTER_BITS), .TOURNAMENT_ENTRIES(TOURNAMENT_ENTRIES)) btb_pred1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .pc_in(pc_in),
        .update_valid(update_valid),
        .update_pc(update_pc),
        .update_next_pc(update_next_pc),
        // Update BTB for all unconditional branches EXCEPT ret (because it will have no clue where to go)
        // RAS will handle the rets (BTB still handles the calls, jmp/jsr will just suck)
        .update_branch_actually_taken(update_branch_actually_taken & ~update_ret_from_rob),
        .update_branch_unconditional(update_branch_unconditional),
        .update_branch_conditional(update_branch_conditional),
        .nuke(nuke),

        // OUTPUTS
        .predict_taken(btb_predict_taken),
        .next_pc_out(btb_next_pc)
    );

    ras_holder #(.RAS_SIZE(RAS_SIZE)) ras_holder1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        // From fetch
        .ras_return_addr({pc_in[1] + 4, pc_in[0] + 4}),
        .ras_return_addr_en(ras_fetch_call),
        .ras_return_addr_request_en(ras_fetch_ret),
        // From the ROB
        .rras_commit_return_addr({update_pc[1] + 4, update_pc[0] + 4}),
        .rras_commit_return_addr_en(update_call_from_rob & update_valid),
        .rras_commit_return_addr_request_en(update_ret_from_rob & update_valid),  // ADDED
        .nuke(nuke),

        // OUTPUTS
        .ras_return_addr_out(ras_return_addr_out),
        .ras_return_addr_out_valid(ras_return_addr_out_valid)
    );

endmodule
`default_nettype wire
// TODO:    Valid bits
//         -CLK_PERIOD = 2.0 < x < 2.5


//`timescale 1ns/100ps
`default_nettype none
module prf #(parameter NUM_PRF_ENTRIES = 96, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES)) (
	// INPUTS
	input   wire													clock,
	input 	wire													reset,
    input   wire            [1:0][PRF_BITS-1:0]						rda_idx, 
    input   wire            [1:0][PRF_BITS-1:0]						rdb_idx, 
    input   wire            [1:0][PRF_BITS-1:0]						wr_idx, 
	input   wire            [1:0][QUADWORD-1:0]						wr_data,					// write data
	input   wire            [1:0]									wr_en,
	input	wire			[1:0]									nuke,						//for branch mispredict
	input   wire			[NUM_PRF_ENTRIES-1:0][PRF_BITS-1:0]		rrat_free_list,
	input  	wire			[PRF_BITS-1:0]							rrat_pointer,					

	output  logic  			[1:0][QUADWORD-1:0]                   	rda_out, 
	output  logic  			[1:0][QUADWORD-1:0]                   	rdb_out,
	output 	logic			[1:0]									prf_valid_a_out,				//if this is equal to 0, then operand value equals prf index
	output 	logic			[1:0]									prf_valid_b_out				//if this is equal to 0, then operand value equals prf index
);
  
	logic   [NUM_PRF_ENTRIES-1:0][QUADWORD-1:0] prfs;   // 32, 64-bit Registers

	logic                   [1:0][QUADWORD-1:0] rda_reg;
	logic                   [1:0][QUADWORD-1:0] rdb_reg;
	logic 					[NUM_PRF_ENTRIES-1:0] valid;
	logic 					[NUM_PRF_ENTRIES-1:0] next_valid;
   
    always_comb
        for (int i=0; i<2; ++i) begin
            rda_reg[i] = prfs[rda_idx[i]];
            rdb_reg[i] = prfs[rdb_idx[i]];
        end
	//
	// Read port A
	//
	always_comb begin
		prf_valid_a_out = 0;
        for (int i=0; i<2; ++i) 
            if (wr_en[0] && (wr_idx[0] == rda_idx[i])) begin
                rda_out[i] = wr_data[0];  // internal forwarding case 1
				prf_valid_a_out[i] = 1;
			end
            else if (wr_en[1] && (wr_idx[1] == rda_idx[i])) begin
                rda_out[i] = wr_data[1];  // internal forwarding case 2
				prf_valid_a_out[i] = 1;
			end
            else if (valid[rda_idx[i]])	begin
                rda_out[i] = rda_reg[i];
				prf_valid_a_out[i] = 1;
			end
			else
                rda_out[i] = rda_idx[i];
    end

	//
	// Read port B
	//
    always_comb begin
		prf_valid_b_out = 0;
        for (int i=0; i<2; ++i)
            if (wr_en[0] && (wr_idx[0] == rdb_idx[i])) begin
                rdb_out[i] = wr_data[0];  // internal forwarding case 1
				prf_valid_b_out[i] = 1;
			end
            else if (wr_en[1] && (wr_idx[1] == rdb_idx[i])) begin
                rdb_out[i] = wr_data[1];  // internal forwarding case 2
				prf_valid_b_out[i] = 1;
			end
            else if (valid[rdb_idx[i]])	begin
                rdb_out[i] = rdb_reg[i];
				prf_valid_b_out[i] = 1;
			end
			else
                rdb_out[i] = rdb_idx[i];
    end

    //
    // For Nuking synthisis issues
    //
    always_comb begin
        for (int i=0; i<NUM_PRF_ENTRIES; ++i) begin
                next_valid[rrat_free_list[i]] <= `SD (i < rrat_pointer) ? 0 : valid[i];
        end
    end




	//
	// Write port
	//
	always_ff @(posedge clock) begin
		if(reset)
			valid	<=	`SD 0;
		else if(nuke == 0) begin		
            // When writing: write to both parallely since both wont write to
            // same PRF        
        	for (int i=1; i>=0; --i)
            	if (wr_en[i]) begin
                	prfs[wr_idx[i]] <= `SD wr_data[i];
					valid[wr_idx[i]] 	 <=	`SD 1;
				end
			end
		else begin
			// When nuking, invalidate all prf entries not pointed by RRAT
            // Did this because of synthisis issues
            valid <= `SD next_valid;
		end
	end
endmodule
`default_nettype wire

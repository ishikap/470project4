#! /usr/bin/python3
# Scott Louzon Feb 17, 2018
# Python script to find smallest clock

import os
import fileinput
import sys
import re

#File locations
TCL_FILE = "prf.tcl"

Clk_prev = 1.52
Clk = 115

#Change clock period
with open(TCL_FILE, 'r') as file :
    filedata = file.read()

#regex = re.compile(r"set CLK_PERIOD .{1,10}")
#oldText = 'set CLK_PERIOD ' + str(Clk_prev)
newText = 'set CLK_PERIOD ' + str(Clk)
#newText = 'set CLK_PERIOD'

#filedata = filedata.replace(oldText, newText)
Clk = str(Clk)

filedata = re.sub(r"set CLK_PERIOD .{1,10}", newText, filedata)

with open(TCL_FILE, 'w') as file:
    file.write(filedata)


#beep when done
print('\a\a')

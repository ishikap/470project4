// TODO:    Valid bits
//         -CLK_PERIOD = 2.0 < x < 2.5


//`timescale 1ns/100ps
`default_nettype none
module prf #(parameter NUM_PRF_ENTRIES = 96, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES)) (
	// INPUTS
	input   wire													clock,
	input 	wire													reset,

    output logic [31:0]                                             count
);
  
	always_ff @(posedge clock) begin
		if(reset)
			count	<=	`SD 0;
		else
            count   <=  `SD count + 1;        
	end
endmodule
`default_nettype wire

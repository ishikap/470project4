#!/usr/bin/python3

# For reading in file
import sys
# For iterating over all files in directory
import os
# For branch predictors
from gshare import gshare_predictor
from gselect import gselect_predictor
from bimodal_predictor import bimodal_predictor
from global_history import global_history
from tournament import tournament
from always_taken import always_taken
from local_history import local_history

UNCONDITIONAL_BRANCH_LIST = [
    "br",
    "bsr",
    "ret",
    "jmp",
    "jsr",
    "jsr_coroutine"
]

BRANCH_LIST = [
    "beq",
    "bne",
    "ble",
    "blt",
    "bge",
    "bgt",
    "blbc",
    "blbs",
    "br",
    "bsr",
    "ret",
    "jmp",
    "jsr",
    "jsr_coroutine"     # Need to do some checking about this one
]

class branch_instr(object):
    def __init__(self, _instr_tuple):
        self.address = int(_instr_tuple[0])
        self.instruction = _instr_tuple[1]
        self.taken = False

def run_test_on_filename(filename):
    lines = []
    # Read in file
    with open(filename, 'r') as pipeline_file:
        lines = pipeline_file.readlines()

    # Strip all whitespace from ends
    lines = [x.strip() for x in lines]
    # Get rid of header lines
    lines = lines[2:]
    # Only keep data in the writeback stage
    for i in range(len(lines)):
        left_bound = lines[i].rindex("|")
        lines[i] = lines[i][left_bound + 1:left_bound + 14].strip()
        #print(lines[i])

    # Break lines into lineno-instruction tuples
    # tuple[0] gives the address, tuple[1] gives instr.
    instructions = [branch_instr(x.split(":")) for x in lines]

    for i in range(len(instructions)):
        instructions[i].clock = i

    for i in range(len(instructions)):
        # Only check up to second to last instruction (last would cause error)
        # Set to taken if next instruction is a '-'
        if i + 1 < len(instructions) and instructions[i + 1].instruction == "-":
            instructions[i].taken = True

    branches = [x for x in instructions if x.instruction in BRANCH_LIST]

    predictors = [
        tournament(4, 4, 2),
        gshare_predictor(3, 2),
        gselect_predictor(2, 2, 2),
        bimodal_predictor(3, 2),
        global_history(4, 2),
        local_history(4, 4, 2),
        always_taken()
    ]
    for predictor in predictors:
        predictor.num_correct = 0
        predictor.num_incorrect = 0

    num_taken = 0
    num_not_taken = 0
    unconditional_branches = 0
    conditional_branches = 0

    uncommitted_instructions = []

    for branch in branches:
        while len(uncommitted_instructions) != 0 and branch.clock - 0 > uncommitted_instructions[0].clock:
            committing_branch = uncommitted_instructions[0]
            # Can commit the instruction
            for i, predictor in enumerate(predictors):
                prediction = predictor.predict_taken(committing_branch)
                actual = committing_branch.taken

                if not actual:
                    num_not_taken += 1
                    predictor.was_not_taken(committing_branch)
                else:
                    num_taken += 1
                    predictor.was_taken(committing_branch)

                if committing_branch.prediction[i] == actual:
                    predictor.num_correct += 1
                else:
                    predictor.num_incorrect += 1

            # Remove that instruction
            uncommitted_instructions.pop(0)

        branch.prediction = [0] * len(predictors)
        for i, predictor in enumerate(predictors):
            branch.prediction[i] = predictor.predict_taken(branch)

        if branch.instruction in UNCONDITIONAL_BRANCH_LIST:
            unconditional_branches += 1
        else:
            conditional_branches += 1
            # Only work with conditional branches for prediction
            uncommitted_instructions.append(branch)

    while len(uncommitted_instructions) != 0:
        committing_branch = uncommitted_instructions[0]
        # Can commit the instruction
        for i, predictor in enumerate(predictors):
            prediction = predictor.predict_taken(committing_branch)
            actual = committing_branch.taken

            if not actual:
                num_not_taken += 1
                predictor.was_not_taken(committing_branch)
            else:
                num_taken += 1
                predictor.was_taken(committing_branch)

            if committing_branch.prediction[i] == actual:
                predictor.num_correct += 1
            else:
                predictor.num_incorrect += 1

        # Remove that instruction
        uncommitted_instructions.pop(0)



    print("Filename: %s" % filename)
    for predictor in predictors:
        print("Predictor: %s" % predictor.__class__.__name__)
        print("Correct guesses: %d" % predictor.num_correct)
        print("Incorrect guesses: %d" % predictor.num_incorrect)
        if predictor.num_correct != 0 or predictor.num_incorrect != 0:
            print("Prediction percentage: %f" % (predictor.num_correct / (predictor.num_correct + predictor.num_incorrect)))

    print("Unconditional Branches: %d" % unconditional_branches)
    print("Conditional Branches: %d" % conditional_branches)

    print("\n\n")

    # Output results to a CSV string
    res = filename + ","
    for predictor in predictors:
        if predictor.num_correct != 0 or predictor.num_incorrect != 0:
            percentage = predictor.num_correct / (predictor.num_correct + predictor.num_incorrect)
        else:
            percentage = 0
        res += str(predictor.num_correct) + "," + str(predictor.num_incorrect) + "," + str(percentage) + ","

    header = ","
    for predictor in predictors:
        header += predictor.__class__.__name__ + ",,,"
    header += "\n"
    res += "\n"

    return (header, res)



def main():
    if len(sys.argv) != 3:
        print("Sorry, you need to tell us which folder you want to test and where you want to output.")
        exit(1)

    folder = sys.argv[1]

    header = ""
    res = ""
    for filename in os.listdir(folder):
        if filename.endswith(".pipeline.out"):
            tup = run_test_on_filename(folder + "/" + filename)
            header = tup[0]
            res += tup[1]

    with open(sys.argv[2], 'w') as f:
        f.write(header)
        f.write(res)


if __name__ == "__main__":
    main()

from branch_pred import branch_predictor
from sat_counter import sat_counter

class bimodal_predictor(branch_predictor):
    def __init__(self, _num_bits_from_address, _num_bits_per_sat):
        self.num_bits_from_address = _num_bits_from_address
        self.predictor_table = []

        num_counts = pow(2, self.num_bits_from_address)
        for i in range(num_counts):
            self.predictor_table.append(sat_counter(_num_bits_per_sat))

    def get_index(self, _instr):
        return _instr.address % len(self.predictor_table)

    def predict_taken(self, _instr):
        return self.predictor_table[self.get_index(_instr)].predict_taken()

    def was_taken(self, _instr):
        # Update the predictor
        self.predictor_table[self.get_index(_instr)].was_taken()

    def was_not_taken(self, _instr):
        # Update the predictor
        self.predictor_table[self.get_index(_instr)].was_not_taken()
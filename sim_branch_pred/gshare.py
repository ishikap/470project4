from g_predictor import g_predictor
from sat_counter import sat_counter

class gshare_predictor(g_predictor):
    def __init__(self, _num_bits_global_history, _num_bits_per_sat):
        self.num_bits_global_history = _num_bits_global_history
        self.predictor_table = []
        self.max_index = pow(2, self.num_bits_global_history)

        num_counts = pow(2, self.num_bits_global_history)
        for i in range(num_counts):
            self.predictor_table.append(sat_counter(_num_bits_per_sat))

        self.global_history = 0

    def get_index(self, _instr):
        return (self.global_history ^ (_instr.address >> 2)) % self.max_index

from bimodal_predictor import bimodal_predictor
from gshare import gshare_predictor
from branch_pred import branch_predictor
from sat_counter import sat_counter

class tournament(branch_predictor):
    def __init__(self, _num_bits_global_history, _num_bits_from_address, _num_bits_per_sat):
        self.gshare = gshare_predictor(_num_bits_global_history, _num_bits_per_sat)
        self.bimodal = bimodal_predictor(_num_bits_from_address, _num_bits_per_sat)
        self.predictor_table = []

        num_counts = pow(2, _num_bits_from_address)
        for i in range(num_counts):
            self.predictor_table.append(sat_counter(_num_bits_per_sat))

    def get_index(self, _instr):
        return _instr.address % len(self.predictor_table)

    def predict_taken(self, _instr):
        index = self.get_index(_instr)
        if self.predictor_table[self.get_index(_instr)].predict_taken():
            # Go with the GShare for this address
            return self.gshare.predict_taken(_instr)
        else:
            # Go with bimodal
            return self.bimodal.predict_taken(_instr)

    def was_taken(self, _instr):
        # Update the predictor predictor
        if self.gshare.predict_taken(_instr) != self.bimodal.predict_taken(_instr):
            # They predicted differently
            if self.gshare.predict_taken(_instr) == _instr.taken:
                # GShare got it right
                self.predictor_table[self.get_index(_instr)].was_taken()
            else:
                # Bimodal got it right
                self.predictor_table[self.get_index(_instr)].was_not_taken()

        # Update two subpredictors
        self.gshare.was_taken(_instr)
        self.bimodal.was_taken(_instr)

    def was_not_taken(self, _instr):
        # Update the predictor predictor
        if self.gshare.predict_taken(_instr) != self.bimodal.predict_taken(_instr):
            # They predicted differently
            if self.gshare.predict_taken(_instr) == _instr.taken:
                # GShare got it right
                self.predictor_table[self.get_index(_instr)].was_taken()
            else:
                # Bimodal got it right
                self.predictor_table[self.get_index(_instr)].was_not_taken()

        # Update two subpredictors
        self.gshare.was_not_taken(_instr)
        self.bimodal.was_not_taken(_instr)
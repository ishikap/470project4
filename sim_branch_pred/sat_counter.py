class sat_counter(object):
    def __init__(self, _num_bits):
        self.num_bits = _num_bits
        self.max_value = pow(2, self.num_bits) - 1
        self.count = 0

    def was_taken(self):
        if self.count < self.max_value:
            self.count += 1

    def was_not_taken(self):
        if self.count > 0:
            self.count -= 1

    def predict_taken(self):
        return self.count > self.max_value / 2

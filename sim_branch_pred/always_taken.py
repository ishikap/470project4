from branch_pred import branch_predictor

class always_taken(branch_predictor):
    def get_index(self, _instr):
        return self.global_history

    def predict_taken(self, _instr):
        return True

    def was_taken(self, _instr):
        pass

    def was_not_taken(self, _instr):
        pass
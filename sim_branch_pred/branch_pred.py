from abc import ABC, abstractmethod

class branch_predictor(ABC):
    @abstractmethod
    def get_index(self, _instr):
        pass

    @abstractmethod
    def predict_taken(self, _instr):
        pass

    @abstractmethod
    def was_taken(self, _instr):
        pass

    @abstractmethod
    def was_not_taken(self, _instr):
        pass

from branch_pred import branch_predictor
from abc import ABC, abstractmethod

class g_predictor(branch_predictor, ABC):
    @abstractmethod
    def get_index(self, _instr):
        pass

    def predict_taken(self, _instr):
        index = self.get_index(_instr)
        return self.predictor_table[index].predict_taken()

    def was_taken(self, _instr):
        index = self.get_index(_instr)
        # Update the predictor
        self.predictor_table[index].was_taken()
        # Update global history
        self.global_history = self.global_history >> 1
        self.global_history += pow(2, self.num_bits_global_history - 1)

    def was_not_taken(self, _instr):
        index = self.get_index(_instr)
        # Update the predictor
        self.predictor_table[index].was_not_taken()
        # Update global history
        self.global_history = self.global_history >> 1
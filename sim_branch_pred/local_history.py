from branch_pred import branch_predictor
from sat_counter import sat_counter

class local_history(branch_predictor):
    def __init__(self, _num_bits_for_first_level, _num_bits_local_history, _num_bits_per_sat):
        self.num_bits_for_first_level = _num_bits_for_first_level
        self.num_bits_local_history = _num_bits_local_history
        self.history_table = []
        self.predictor_table = []

        num_first_entries = pow(2, self.num_bits_for_first_level)
        for i in range(num_first_entries):
            self.history_table.append(0)

        num_counts = pow(2, self.num_bits_for_first_level)
        for i in range(num_counts):
            self.predictor_table.append(sat_counter(_num_bits_per_sat))

    def get_index(self, _instr):
        return (_instr.address >> 2) % (pow(2, self.num_bits_for_first_level))

    def predict_taken(self, _instr):
        return self.predictor_table[self.history_table[self.get_index(_instr)]].predict_taken()

    def was_taken(self, _instr):
        # Update the predictor
        first_level_index = self.history_table[self.get_index(_instr)]
        self.predictor_table[first_level_index].was_taken()
        # Update local history
        self.history_table[self.get_index(_instr)] = first_level_index >> 1
        self.history_table[self.get_index(_instr)] += pow(2, self.num_bits_local_history - 1)

    def was_not_taken(self, _instr):
        # Update the predictor
        first_level_index = self.history_table[self.get_index(_instr)]
        self.predictor_table[first_level_index].was_not_taken()
        # Update local history
        self.history_table[self.get_index(_instr)] = first_level_index >> 1
        self.history_table[self.get_index(_instr)] += pow(2, self.num_bits_local_history - 1)
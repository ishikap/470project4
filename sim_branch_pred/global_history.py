from branch_pred import branch_predictor
from sat_counter import sat_counter

class global_history(branch_predictor):
    def __init__(self, _num_bits_global_history, _num_bits_per_sat):
        self.num_bits_global_history = _num_bits_global_history
        self.predictor_table = []

        num_counts = pow(2, self.num_bits_global_history)
        for i in range(num_counts):
            self.predictor_table.append(sat_counter(_num_bits_per_sat))

        self.global_history = 0

    def get_index(self, _instr):
        return self.global_history

    def predict_taken(self, _instr):
        return self.predictor_table[self.get_index(_instr)].predict_taken()

    def was_taken(self, _instr):
        # Update the predictor
        self.predictor_table[self.get_index(_instr)].was_taken()
        # Update global history
        self.global_history = self.global_history >> 1
        self.global_history += pow(2, self.num_bits_global_history - 1)

    def was_not_taken(self, _instr):
        # Update the predictor
        self.predictor_table[self.get_index(_instr)].was_not_taken()
        # Update global history
        self.global_history = self.global_history >> 1
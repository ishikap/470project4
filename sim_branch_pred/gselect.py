from g_predictor import g_predictor
from sat_counter import sat_counter

class gselect_predictor(g_predictor):
    def __init__(self, _num_bits_global_history, _num_bits_per_sat, _num_bits_from_address):
        self.num_bits_global_history = _num_bits_global_history
        self.num_bits_from_address = _num_bits_from_address
        self.predictor_table = []
        self.address_mask = pow(2, self.num_bits_from_address) - 1

        num_counts = pow(2, self.num_bits_global_history + self.num_bits_from_address)
        for i in range(num_counts):
            self.predictor_table.append(sat_counter(_num_bits_per_sat))

        self.global_history = 0

    def get_index(self, _instr):
        final = self.global_history << self.num_bits_from_address
        address_bits = self.address_mask & (_instr.address >> 2)
        return final | address_bits

#! /usr/bin/python3
# Scott Louzon Feb 25, 2018
# Python script to find smallest clock

import os
import fileinput
import sys
import re
import datetime 
datetime

#File locations
if len(sys.argv) == 6:
    mod_name = sys.argv[1]

    REP_FILE        = "synth/" + mod_name + ".rep"
    TCL_FILE        = "Makefile"
    SYN_OUT_FILE    = "synth/" + mod_name + "_synth.out"

    Clk_initial = float(sys.argv[2])
    Clk_lower   = float(sys.argv[3])
    Range       = float(sys.argv[4])
    supress     = str(sys.argv[5])
else:
    print('\n!!!WRONG number of inputs incorrect WRONG!!!')
    print('1st input is mod name,           Ex: \'prf\' ')
    print('2nd input is starting clock,     Ex: \'10\' ')
    print('3rd input is lower clock bound,  Ex: \'1\' ')
    print('4th input is range of accuracy,  Ex: \'.1\' ')
    print('5th input is supress output?,    Ex: \'yes\' ')
    print('All together, Ex: \'python bs_clock.py prf 10 1 .1 yes\' \n\n')
    sys.exit()

Clk_upper = Clk_initial
Clk_current = Clk_initial
Clk_best = Clk_initial
Clk_prev = Clk_initial

Threshold = Clk_initial

#Get total sym time start
tot_begin = datetime.datetime.now()

#Loop as long as difference is small
while Threshold > Range:

    #Change clock period
    with open(TCL_FILE, 'r') as file :
        filedata = file.read()
    
    newText = 'export CLOCK_PERIOD = ' + str(Clk_current)
    filedata = re.sub(r"export CLOCK_PERIOD = .{1,}", newText, filedata)

    with open(TCL_FILE, 'w') as file:
        file.write(filedata)

    #Get current time
    begin = datetime.datetime.now()
    
    #Synthisize the file
    s = '\nSynthisizing when CLK_PERIOD = ' + str(Clk_current)
    print(s)
    if supress == "yes" :
        os.system("make nuke &>/dev/null")
        os.system("make syn &>/dev/null")
        os.system("cp synth/pipeline.rep pipeline.rep_%s" % str(Clk_current))
    else:
        os.system("make nuke")
        os.system("make syn")
        os.system("cp synth/pipeline.rep pipeline.rep_%s" % str(Clk_current))
    
    #Check for timing loop in _synth.out file
    if 'loop' in open(SYN_OUT_FILE).read():
        s = '!!! ERROR in _synth.out file !!! '
        print(s)
        sys.exit()

    #Check for error in _synth.out file
    if 'ERROR' in open(SYN_OUT_FILE).read():
        s = '!!! ERROR in _synth.out file !!! '
        print(s)
        sys.exit()
    
    #Check for Latch in _synth.out file
    if 'Latch' in open(SYN_OUT_FILE).read():
        s = '!!! Latch in _synth.out file !!! '
        print(s)

    #Get end time and display difference
    end = datetime.datetime.now()
    diff = end - begin
    m = diff.seconds/60
    sec = diff.seconds - (m * 60)  
    s = 'Last synthisis time (min:sec): ' + str(m) + ':' + str(sec)
    print(s)

    # Find out if it was violated and update
    if 'slack (VIOLATED)' in open(REP_FILE).read():
        s = 'VIOLATED when clock period = ' + str(Clk_current)
        print(s)
        Clk_lower = Clk_current
        Clk_prev = Clk_current
        Clk_current = (float(Clk_current) + float(Clk_upper)) / 2
    else:
        s = 'All MET when clock period = ' + str(Clk_current)
        print(s)
        Clk_best = Clk_current
        Clk_upper = Clk_current
        Clk_prev = Clk_current
        Clk_current = (float(Clk_current) + float(Clk_lower)) / 2

    Threshold = float(Clk_upper) - float(Clk_lower)

#Get total sym time end and display differnce
tot_end = datetime.datetime.now()
tot_diff = tot_end - tot_begin
tot_m = tot_diff.seconds/60
tot_sec = tot_diff.seconds - (tot_m * 60)  


#beep when done
print('\a\a')
s = '\n\n\n***** THE LOWEST CLOCK PERIOD IS:             ' + str(Clk_best) + ' *****'
print(s)
s = '***** THE LOWEST CLOCK PERIOD IS BETWEEN:     ' + str(Clk_lower) + ' AND ' + str(Clk_upper) + ' *****'
print(s)
s = '***** THE TOTAL SIMULATION TIME IS (min:sec): ' + str(tot_m) + ':' + str(tot_sec) + ' *****\n\n\n'
print(s)

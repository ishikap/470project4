`timescale 1ns/100ps
parameter STAGES = 4;
parameter NUM_PRF_ENTRIES = 96;
localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES);
localparam QUADWORD=64;
module testbench();

    logic                            clock;
    logic                            reset;
    logic    [1:0][QUADWORD-1:0]     mcand;
    logic    [1:0][QUADWORD-1:0]     mplier;
    logic    [1:0]                   start;
    logic    [1:0][PRF_BITS-1:0]     prf_tag_in;
    logic    [1:0]                   stall;
    logic    [1:0]                   nuke;
    logic    [1:0]                   thread_id;

    wire     [1:0][QUADWORD-1:0]     product;
    wire     [1:0]                   done;
    wire     [1:0][PRF_BITS-1:0]     prf_tag_out;
    wire     [1:0]                   thread_id_out;
    wire     [1:0]                   stall_out;

	mult  #(.STAGES(STAGES), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) m0(
        .clock(clock),
        .reset(reset),
        .mcand(mcand),
        .mplier(mplier),
        .start(start),
        .prf_tag_in(prf_tag_in),
        .stall(stall),
        .nuke(nuke),
        .thread_id(thread_id),
        .product(product),
        .done(done),
        .prf_tag_out(prf_tag_out),
        .thread_id_out(thread_id_out),
        .stall_out(stall_out)
    );


	always begin
		#5;
		clock=~clock;
	end

	// Some students have had problems just using "@(posedge done)" because their
	// "done" signals glitch (even though they are the output of a register). This
	// prevents that by making sure "done" is high at the clock edge.
	task wait_until_done;
		forever begin : wait_loop
			@(posedge done);
			@(negedge clock);
			if(done) disable wait_until_done;
		end
	endtask



	initial begin

		//$vcdpluson;
		$monitor("\nWAY 1 INPUTS Time:%4.0f mcand:%d mplier:%d start:%b stall:%b nuke=%b prf_tag_in=%d",$time,mcand[1],mplier[1],start[1],stall[1],nuke[1],prf_tag_in[1],
                "\nWAY 1 OUTPUTS Time:%4.0f product:%d done=%b prf_tag_out=%d stall_out=%b", $time,product[1], done[1],prf_tag_out[1],stall_out[1],
                "\nWAY 1 STAGE 0 VALUES Time:%4.0f internal_products:%d prod_in_reg:%d internal_mcands:%d internal_mpliers:%d internal_dones:%b internal_prf_tag=%d internal_thread_id=%d stall_in=%b stall_out:%b",
                $time,m0.mstage[0].product_out[1], m0.mstage[0].prod_in_reg[1], m0.mstage[0].mcand_out[1], m0.mstage[0].mplier_out[1], m0.mstage[0].done[1], m0.mstage[0].prf_tag_out[1], m0.mstage[0].thread_id_out[1], m0.mstage[0].stall[1], m0.mstage[0].stall_out[1],
                "\nWAY 1 STAGE 1 VALUES Time:%4.0f internal_products:%d prod_in_reg:%d internal_mcands:%d internal_mpliers:%d internal_dones:%b internal_prf_tag=%d internal_thread_id=%d stall_in=%b stall_out:%b",
                $time,m0.mstage[1].product_out[1], m0.mstage[1].prod_in_reg[1], m0.mstage[1].mcand_out[1], m0.mstage[1].mplier_out[1], m0.mstage[1].done[1], m0.mstage[1].prf_tag_out[1], m0.mstage[1].thread_id_out[1], m0.mstage[1].stall[1], m0.mstage[1].stall_out[1],
                "\nWAY 1 STAGE 2 VALUES Time:%4.0f internal_products:%d prod_in_reg:%d internal_mcands:%d internal_mpliers:%d internal_dones:%b internal_prf_tag=%d internal_thread_id=%d stall_in=%b stall_out:%b",
                $time,m0.mstage[2].product_out[1], m0.mstage[2].prod_in_reg[1], m0.mstage[2].mcand_out[1], m0.mstage[2].mplier_out[1], m0.mstage[2].done[1], m0.mstage[2].prf_tag_out[1], m0.mstage[2].thread_id_out[1], m0.mstage[2].stall[1], m0.mstage[2].stall_out[1],
                "\nWAY 1 STAGE 3 VALUES Time:%4.0f internal_products:%d prod_in_reg:%d internal_mcands:%d internal_mpliers:%d internal_dones:%b internal_prf_tag=%d internal_thread_id=%d stall_in=%b stall_out:%b",
                $time,m0.mstage[3].product_out[1], m0.mstage[3].prod_in_reg[1], m0.mstage[3].mcand_out[1], m0.mstage[3].mplier_out[1], m0.mstage[3].done[1], m0.mstage[3].prf_tag_out[1], m0.mstage[3].thread_id_out[1], m0.mstage[3].stall[1], m0.mstage[3].stall_out[1]);

        clock = 0;
        reset = 1;
        mcand = 0;
        mplier = 0;
        start = 0;
        prf_tag_in = 0;
        stall = 0;
        nuke = 0;
        thread_id = 0;

		@(negedge clock);
		reset=0;
		@(negedge clock);
		start[1]=1;
		mcand[1] =5;
        mplier[1]=4;
		@(negedge clock);
        mcand[1] =6;
        mplier[1]=6;

        stall[1] = 1;
        @(negedge clock);
        start[1] = 0;
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        stall[1] = 0;
        start[1] = 1;
        mcand[1] =7;
        mplier[1]=7;
        @(negedge clock);
        stall[1] = 1;
        start[1] = 0;
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        stall[1] = 0;
        start[1] = 1;
        mcand[1] =8;
        mplier[1]=8;
        @(negedge clock);
        mcand[1] =9;
        mplier[1]=9;
        @(negedge clock);
        mcand[1] =10;
        mplier[1]=10;
        @(negedge clock);
        mcand[1] =11;
        mplier[1]=11;
        @(negedge clock);
        start[1]=0;
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);

		$finish;
	end

endmodule






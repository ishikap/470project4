// This is an 8 stage (9 depending on how you look at it) pipelined
// multiplier that multiplies 2 64-bit integers and returns the low 64 bits
// of the result.  This is not an ideal multiplier but is sufficient to
// allow a faster clock period than straight *
// This module instantiates 8 pipeline stages as an array of submodules.
`default_nettype none
`timescale 1ns/100ps
module mult #(parameter STAGES = 4, parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam QUADWORD=64) (
    // INPUTS
    input   wire                            clock,
    input   wire                            reset,
    input   wire    [1:0][QUADWORD-1:0]     mcand,
    input   wire    [1:0][QUADWORD-1:0]     mplier,
    input   wire    [1:0]                   start,
    input   wire    [1:0][PRF_BITS-1:0]     prf_tag_in,
    input   wire    [1:0]                   stall,
    input   wire    [1:0]                   nuke,
    // OUTPUTS
    output  logic   [1:0][QUADWORD-1:0]     product,
    output  logic   [1:0]                   done,
    output  logic   [1:0][PRF_BITS-1:0]     prf_tag_out,
    output  logic   [1:0]                   stall_out
);

    logic [1:0][QUADWORD-1:0]               mcand_out, mplier_out;
    logic [1:0][((STAGES-1)*QUADWORD)-1:0]  internal_products, internal_mcands, internal_mpliers;
    logic [1:0][STAGES-2:0]                 internal_dones;
    logic [1:0][((STAGES-1)*PRF_BITS)-1:0]  internal_prf_tag;
    wire  [1:0][STAGES-2:0]                 internal_stall;

    logic [1:0][QUADWORD-1:0]               in;

    logic [1:0][QUADWORD-1:0]               mcand_prev;
    logic [1:0][QUADWORD-1:0]               mplier_prev;
    logic [1:0]                             start_prev;
    logic [1:0][PRF_BITS-1:0]               prf_tag_in_prev;
    logic [1:0]                             stall_prev;

    assign in = 0;
    always_comb begin
        for (int i=1; i>=0; --i) begin
            stall_out[i] = stall_prev[i] && start_prev[i];
        end
    end

	mult_stage #(.STAGES(STAGES), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) mstage [STAGES-1:0]  (
		// INPUTS
        .clock(clock),
		.reset(reset),
        .nuke(nuke),
		.start({internal_dones, start_prev}),
		.product_in({internal_products, in}),
		.mplier_in({internal_mpliers, mplier_prev}),
		.mcand_in({internal_mcands, mcand_prev}),
        .prf_tag_in({internal_prf_tag, prf_tag_in_prev}),
        .stall({stall, internal_stall}),
        // OUTPUTS
		.product_out({product, internal_products}),
		.mplier_out({mplier_out, internal_mpliers}),
		.mcand_out({mcand_out, internal_mcands}),
		.done({done, internal_dones}),
        .prf_tag_out({prf_tag_out, internal_prf_tag}),
        .stall_out({internal_stall, stall_prev})
	);


    // Insert pipeline register between RS and 1st stage of mult
    always_ff @ (posedge clock) begin
        if (reset) begin
            mcand_prev                  <= `SD 0;
            mplier_prev                 <= `SD 0;
            start_prev                  <= `SD 0;
            prf_tag_in_prev             <= `SD 0;
        end
        else if (nuke) begin
            mcand_prev                  <= `SD 0;
            mplier_prev                 <= `SD 0;
            start_prev                  <= `SD 0;
            prf_tag_in_prev             <= `SD 0;
        end
        else begin
            for (int i=1; i>=0; --i) begin
                if (!stall_out[i]) begin
                    mcand_prev[i]       <= `SD mcand[i];
                    mplier_prev[i]      <= `SD mplier[i];
                    start_prev[i]       <= `SD start[i];
                    prf_tag_in_prev[i]  <= `SD prf_tag_in[i];
                end
            end
        end
    end
endmodule
`default_nettype wire
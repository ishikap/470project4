// This is one stage of an 8 stage (9 depending on how you look at it)
// pipelined multiplier that multiplies 2 64-bit integers and returns
// the low 64 bits of the result.  This is not an ideal multiplier but
// is sufficient to allow a faster clock period than straight *
`default_nettype none
`timescale 1ns/100ps
module mult_stage #(parameter STAGES = 4, parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam QUADWORD=64) (
    // INPUTS
    input   wire                            clock,
    input   wire                            reset,
    input   wire    [1:0]                   start,
    input   wire    [1:0][QUADWORD-1:0]     product_in,
    input   wire    [1:0][QUADWORD-1:0]     mplier_in,
    input   wire    [1:0][QUADWORD-1:0]     mcand_in,
    input   wire    [1:0][PRF_BITS-1:0]     prf_tag_in,
    input   wire    [1:0]                   stall,
    input   wire    [1:0]                   nuke,
    // OUTPUTS
    output  logic   [1:0]                   done,
    output  logic   [1:0][QUADWORD-1:0]     product_out,
    output  logic   [1:0][QUADWORD-1:0]     mplier_out,
    output  logic   [1:0][QUADWORD-1:0]     mcand_out,
    output  logic   [1:0][PRF_BITS-1:0]     prf_tag_out,
    output  logic   [1:0]                   stall_out
);

	logic   [1:0][QUADWORD-1:0] prod_in_reg, partial_prod_reg;
	logic   [1:0][QUADWORD-1:0] partial_product, next_mplier, next_mcand;

    logic   [1:0]               done_next;

    always_comb begin
        // By default, don't do anything
        next_mcand          = next_mcand;
        next_mplier         = next_mplier;
        partial_product     = partial_product;
        stall_out           = 2'b11;
        done_next           = done;

        for (int i=1; i>=0; --i) begin
            product_out[i]      = prod_in_reg[i] + partial_prod_reg[i];
            partial_product[i]  = mplier_in[i][(QUADWORD/STAGES)-1:0] * mcand_in[i];
            next_mplier[i]      = {{QUADWORD/STAGES{1'b0}}, mplier_in[i][QUADWORD-1:(QUADWORD/STAGES)]};
            next_mcand[i]       = {mcand_in[i][QUADWORD-(QUADWORD/STAGES)-1:0], {QUADWORD/STAGES{1'b0}}};
            if (!stall[i] || !done[i]) begin
                // Should not be stalling because either
                //      a) We weren't told to stall, so go ham
                //      b) We don't currently have a valid input, so fill in the empty spot
                stall_out[i]        = 0;
                done_next[i]        = start[i];
            end
        end
        if (nuke) begin
            done_next       = 0;
            partial_product = 0;
            next_mplier     = 0;
            next_mcand      = 0;
        end
    end

	//synopsys sync_set_reset "reset"
	always_ff @(posedge clock) begin
        if (reset) begin
			done                <= `SD 0;
            prod_in_reg         <= `SD 0;
            partial_prod_reg    <= `SD 0;
            mplier_out          <= `SD 0;
            mcand_out           <= `SD 0;
            prf_tag_out         <= `SD 0;
        end
		else begin
			done <= `SD done_next;
            for (int i=1; i>=0; --i) begin
                if (!stall[i] || !done[i]) begin
                    prod_in_reg[i]      <= `SD product_in[i];
                    partial_prod_reg[i] <= `SD partial_product[i];
                    mplier_out[i]       <= `SD next_mplier[i];
                    mcand_out[i]        <= `SD next_mcand[i];
                    prf_tag_out[i]      <= `SD prf_tag_in[i];
                end
            end
        end
	end
endmodule
`default_nettype wire
//For: Testing Execute Holder
//Date: 3/12/17
`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define TRUE_2          2'b01
`define FALSE_2         2'b00
`define WAY_0           1'b0
`define WAY_1           1'b1
`define R0              5'd0
`define R1              5'd1
`define R2              5'd2
`define R3              5'd3
`define R4              5'd4
`define R5              5'd5
`define R6              5'd6
`define R7              5'd7
`define R8              5'd8
`define R9              5'd9
`define R10             5'd10
`define R11             5'd11
`define R12             5'd12
`define R13             5'd13
`define R14             5'd14
`define R15             5'd15
`define R16             5'd16
`define R17             5'd17
`define R18             5'd18
`define R19             5'd19
`define R20             5'd20
`define R21             5'd21
`define R22             5'd22
`define R23             5'd23
`define R24             5'd24
`define R25             5'd25
`define R26             5'd26
`define R27             5'd27
`define R28             5'd28
`define R29             5'd29
`define R30             5'd30
`define R31             5'd31

`define WAY_1_ALU       3'd5
`define WAY_0_ALU       3'd4
`define WAY_1_MULT      3'd3
`define WAY_0_MULT      3'd2
`define WAY_1_BR        3'd1
`define WAY_0_BR        3'd0

parameter NUM_PRF_ENTRIES       = 96;
parameter MULT_STAGES           = 8;
parameter FINISHED_INSTR        = 6;
localparam QUADWORD             = 64;
localparam PRF_BITS             = $clog2(NUM_PRF_ENTRIES);
localparam NUM_ARF_ENTRIES      = 32;
localparam ARF_BITS             = $clog2(NUM_ARF_ENTRIES);


module testbench;

    //INPUTS
    logic                                               clock;
    logic                                               reset;
    // Coming from RS
    logic    [1:0]                                      valid_in;
    logic    [1:0][QUADWORD-1:0]                        rs_opa_out;
    logic    [1:0][QUADWORD-1:0]                        rs_opb_out;
    logic    [1:0][PRF_BITS-1:0]                        rs_dest_out;
    logic    [1:0][4:0]                                 rs_alu_func_out;
    logic    [1:0][1:0]                                 rs_opa_select_out;
    logic    [1:0][1:0]                                 rs_opb_select_out;
    logic    [1:0]                                      rs_cond_branch_out;
    logic    [1:0]                                      rs_uncond_branch_out;
    logic    [1:0]                                      rs_halt_out;
    logic    [1:0]                                      rs_illegal_out;
    logic    [1:0]                                      rs_mem_op;
    logic    [1:0][QUADWORD-1:0]                        rs_PC;
    logic    [1:0][QUADWORD-1:0]                        rs_NPC;
    logic    [1:0][31:0]                                rs_IR;
    logic    [1:0]                                      rs_thread_id;
    logic    [1:0]                                      mult_stall;
    logic    [1:0]                                      nuke;

    // OUTPUTS
    // To CDB Buffer
    logic   [FINISHED_INSTR-1:0]                        valid;
    logic   [FINISHED_INSTR-1:0][PRF_BITS-1:0]          pr_tag;
    logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]          results;
    logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]          next_pc;
    logic   [FINISHED_INSTR-1:0]                        branch_taken;
    logic   [FINISHED_INSTR-1:0]                        thread_id;
    // To LSQ Buffer
    logic   [1:0][QUADWORD-1:0]                         lsq_mem_addr;
    logic   [1:0][QUADWORD-1:0]                         lsq_next_pc;
    logic   [1:0][PRF_BITS-1:0]                         lsq_prf_tag;
    logic   [1:0]                                       lsq_valid;
    logic   [1:0]                                       lsq_thread_id;
    // To Pipeline between Issue and Execute
    logic   [1:0]                                       mult_stall_out;


    //OUTPUTS


    execute_holder #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES), .MULT_STAGES(MULT_STAGES), .FINISHED_INSTR(FINISHED_INSTR)) execute_holder1(

        //INPUTS
        .clock(clock),
        .reset(reset),
        .valid_in(valid_in),
        .rs_opa_out(rs_opa_out),
        .rs_opb_out(rs_opb_out),
        .rs_dest_out(rs_dest_out),
        .rs_alu_func_out(rs_alu_func_out),
        .rs_opa_select_out(rs_opa_select_out),
        .rs_opb_select_out(rs_opb_select_out),
        .rs_cond_branch_out(rs_cond_branch_out),
        .rs_uncond_branch_out(rs_uncond_branch_out),
        .rs_halt_out(rs_halt_out),
        .rs_illegal_out(rs_illegal_out),
        .rs_mem_op(rs_mem_op),
        .rs_PC(rs_PC),
        .rs_NPC(rs_NPC),
        .rs_IR(rs_IR),
        .rs_thread_id(rs_thread_id),
        .mult_stall(mult_stall),
        .nuke(nuke),
        .valid(valid),
        .pr_tag(pr_tag),
        .results(results),
        .next_pc(next_pc),
        .branch_taken(branch_taken),
        .thread_id(thread_id),
        .lsq_mem_addr(lsq_mem_addr),
        .lsq_next_pc(lsq_next_pc),
        .lsq_prf_tag(lsq_prf_tag),
        .lsq_valid(lsq_valid),
        .lsq_thread_id(lsq_thread_id),
        .mult_stall_out(mult_stall_out)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");
/*
        //display all the current RAT FREE LIST enteries
        $display("===RAT_FREE_LIST===");
        for (int i=0; i < FREE_LIST_SIZE; i++)
        begin
            $display ("Free List Entry Number: %d ", i,
                      "Free List Entry: %d ", decode_holder1.rat_holder1.rat1.rat_free_list[i],
                      "Pointer: %d", decode_holder1.rat_holder1.rat1.rat_pointer);
        end

        //display all the current RAT enteries
        $display("\n\n===RAT===");64'd0,
        for (int i=0; i < NUM_ARF_ENTRIES; i++)
        begin
            $display ("RAT Entry Number: %d ", i,
                      "RAT Entry: %d ", decode_holder1.rat_holder1.rat1.rat_entries[i]);
        end

        //display all the current RRAT FREE LIST enteries
        $display("\n\n===RRAT_FREE_LIST===");
        for (int i=0; i < FREE_LIST_SIZE; i++)
        begin
            $display ("Free List Entry Number: %d ", i,
                      "Free List Entry: %d ", decode_holder1.rat_holder1.rrat1.rrat_free_list[i]);
        end

        //display all the current RRAT enteries
        $display("\n\n===RRAT===");
        for (int i=0; i < NUM_ARF_ENTRIES; i++)
        begin
            $display ("RRAT Entry Number: %d ", i,
                      "RRAT Entry: %d ", decode_holder1.rat_holder1.rrat1.rrat_entries[i]);
        end
*/
        $fatal("\n\nINCORRECT");
    endtask


    task issue1;
        input                                       way;
        input                                       valid_in_tmp;
        input   [QUADWORD-1:0]                      rs_opa_out_tmp;
        input   [QUADWORD-1:0]                      rs_opb_out_tmp;
        input   [PRF_BITS-1:0]                      rs_dest_out_tmp;
        input   [4:0]                               rs_alu_func_out_tmp;
        input   [1:0]                               rs_opa_select_out_tmp;
        input   [1:0]                               rs_opb_select_out_tmp;

        valid_in[way]                   = valid_in_tmp;
        rs_opa_out[way]                 = rs_opa_out_tmp;
        rs_opb_out[way]                 = rs_opb_out_tmp;
        rs_dest_out[way]                = rs_dest_out_tmp;
        rs_alu_func_out[way]            = rs_alu_func_out_tmp;
        rs_opa_select_out[way]          = rs_opa_select_out_tmp;
        rs_opb_select_out[way]          = rs_opb_select_out_tmp;

    endtask

    task issue2;
        input                                       way;
        input                                       rs_cond_branch_out_tmp;
        input                                       rs_uncond_branch_out_tmp;
        input                                       rs_halt_out_tmp;
        input                                       rs_illegal_out_tmp;
        input                                       rs_mem_op_tmp;

        rs_cond_branch_out[way]         = rs_cond_branch_out_tmp;
        rs_uncond_branch_out[way]       = rs_uncond_branch_out_tmp;
        rs_halt_out[way]                = rs_halt_out_tmp;
        rs_illegal_out[way]             = rs_illegal_out_tmp;
        rs_mem_op[way]                  = rs_mem_op_tmp;

    endtask

    task issue3;
        input                                       way;
        input   [QUADWORD-1:0]                      rs_PC_tmp;
        input   [QUADWORD-1:0]                      rs_NPC_tmp;
        input   [31:0]                              rs_IR_tmp;
        input                                       rs_thread_id_tmp;
        input                                       mult_stall_tmp;

        rs_PC[way]                      = rs_PC_tmp;
        rs_NPC[way]                     = rs_NPC_tmp;
        rs_IR[way]                      = rs_IR_tmp;
        rs_thread_id[way]               = rs_thread_id_tmp;
        mult_stall[way]                 = mult_stall_tmp;

    endtask

    task check_output_cdb;
        input            [2:0]                      way;
        input                                       valid_tmp;
        input   [PRF_BITS-1:0]                      pr_tag_tmp;
        input   [QUADWORD-1:0]                      results_tmp;
        input   [QUADWORD-1:0]                      next_pc_tmp;
        input                                       branch_taken_tmp;
        input                                       thread_id_tmp;

        if (valid[way]                      != valid_tmp)                     exit_on_error();
        if (pr_tag[way]                     != pr_tag_tmp       && valid_tmp) exit_on_error();
        if (results[way]                    != results_tmp      && valid_tmp) exit_on_error();
        if (next_pc[way]                    != next_pc_tmp      && valid_tmp) exit_on_error();
        if (branch_taken[way]               != branch_taken_tmp && valid_tmp) exit_on_error();
        if (thread_id[way]                  != thread_id_tmp    && valid_tmp) exit_on_error();

    endtask

    task check_output_lsq;
        input                                       way;
        input                                       lsq_valid_tmp;
        input   [QUADWORD-1:0]                      lsq_mem_addr_tmp;
        input   [PRF_BITS-1:0]                      lsq_prf_tag_tmp;
        input   [QUADWORD-1:0]                      lsq_next_pc_tmp;
        input                                       lsq_thread_id_tmp;

        if (lsq_valid[way]                  != lsq_valid_tmp)                         exit_on_error();
        if (lsq_mem_addr[way]               != lsq_mem_addr_tmp     && lsq_valid_tmp) exit_on_error();
        if (lsq_next_pc[way]                != lsq_next_pc_tmp     && lsq_valid_tmp) exit_on_error();
        if (lsq_prf_tag[way]                != lsq_prf_tag_tmp      && lsq_valid_tmp) exit_on_error();
        if (lsq_thread_id[way]              != lsq_thread_id_tmp    && lsq_valid_tmp) exit_on_error();

    endtask

    task check_output_mult_stall;
        input                                       way;
        input                                       m_stall;

        if (mult_stall_out[way]           != m_stall) exit_on_error();

    endtask


    initial
    begin
        $monitor("=== WAY 1 INPUTS ===\n Time: %d rs_opa_out= %d rs_opb_out= %d rs_dest_out= %d alu_func_out= %s opa_select_out= %s opb_select_out= %s cond_br= %b uncond_br= %b\n", $time, rs_opa_out[1], rs_opb_out[1], rs_dest_out[1], rs_alu_func_out[1], rs_opa_select_out[1], rs_opb_select_out[1], rs_cond_branch_out[1], rs_uncond_branch_out[1],
                "=== WAY 1 INPUTS ===\n Time: %d rs_halt_out= %b rs_illegal_out= %b rs_mem_op= %b rs_PC= %d rs_NPC= %d rs_IR= %h rs_thread_id= %b mult_stall= %b nuke= %b\n", $time, rs_halt_out[1], rs_illegal_out[1], rs_mem_op[1],rs_PC[1], rs_NPC[1], rs_IR[1], rs_thread_id[1], mult_stall[1], nuke[1],
                "=== WAY 0 INPUTS ===\n Time: %d rs_opa_out= %d rs_opb_out= %d rs_dest_out= %d alu_func_out= %s opa_select_out= %s opb_select_out= %s cond_br= %b uncond_br= %b\n", $time, rs_opa_out[0], rs_opb_out[0], rs_dest_out[0], rs_alu_func_out[0], rs_opa_select_out[0], rs_opb_select_out[0], rs_cond_branch_out[0], rs_uncond_branch_out[0],
                "=== WAY 0 INPUTS ===\n Time: %d rs_halt_out= %b rs_illegal_out= %b rs_mem_op= %b rs_PC= %d rs_NPC= %d rs_IR= %h rs_thread_id= %b mult_stall= %b nuke= %b\n", $time, rs_halt_out[0], rs_illegal_out[0], rs_mem_op[0], rs_PC[0], rs_NPC[0], rs_IR[0], rs_thread_id[0], mult_stall[0], nuke[0],
                "=== OUTPUTS CDB ALU WAY 1===\n Time: %d valid= %b pr_tag= %d results= %d next_pc= %d branch_taken= %b thread_id= %b\n", $time, valid[5], pr_tag[5], results[5], next_pc[5], branch_taken[5], thread_id[5],
                "=== OUTPUTS CDB ALU WAY 0===\n Time: %d valid= %b pr_tag= %d results= %d next_pc= %d branch_taken= %b thread_id= %b\n", $time, valid[4], pr_tag[4], results[4], next_pc[4], branch_taken[4], thread_id[4],
                "=== OUTPUTS CDB MULT WAY 1===\n Time: %d valid= %b pr_tag= %d results= %d next_pc= %d branch_taken= %b thread_id= %b\n", $time, valid[3], pr_tag[3], results[3], next_pc[3], branch_taken[3], thread_id[3],
                "=== OUTPUTS CDB MULT WAY 0===\n Time: %d valid= %b pr_tag= %d results= %d next_pc= %d branch_taken= %b thread_id= %b\n", $time, valid[2], pr_tag[2], results[2], next_pc[2], branch_taken[2], thread_id[2],
                "=== OUTPUTS CDB BR WAY 1===\n Time: %d valid= %b pr_tag= %d results= %d next_pc= %d branch_taken= %b thread_id= %b\n", $time, valid[1], pr_tag[1], results[1], next_pc[1], branch_taken[1], thread_id[1],
                "=== OUTPUTS CDB BR WAY 0===\n Time: %d valid= %b pr_tag= %d results= %d next_pc= %d branch_taken= %b thread_id= %b\n", $time, valid[0], pr_tag[0], results[0], next_pc[0], branch_taken[0], thread_id[0],
                "=== OUTPUTS LSQ WAY1===\n Time: %d valid= %b pr_tag= %d mem_addr= %d next_pc= %d thread_id= %b\n", $time, lsq_valid[1], lsq_prf_tag[1], lsq_mem_addr[1], lsq_next_pc[1], lsq_thread_id[1],
                "=== OUTPUTS LSQ WAY0===\n Time: %d valid= %b pr_tag= %d mem_addr= %d next_pc= %d thread_id= %b\n", $time, lsq_valid[0], lsq_prf_tag[0], lsq_mem_addr[0], lsq_next_pc[0], lsq_thread_id[0],
                "=== OUTPUTS Issue Pipe WAY1===\n Time: %d mult_stall_out= %b\n", $time, mult_stall_out[1],
                "=== OUTPUTS Issue Pipe WAY0===\n Time: %d mult_stall_out= %b\n", $time, mult_stall_out[0]);


        //Intialize
        clock                   = 0;
        reset                   = 1;
        valid_in                = 0;
        rs_opa_out              = 0;
        rs_opb_out              = 0;
        rs_dest_out             = 0;
        rs_alu_func_out         = 0;
        rs_opa_select_out       = 0;
        rs_opb_select_out       = 0;
        rs_cond_branch_out      = 0;
        rs_uncond_branch_out    = 0;
        rs_halt_out             = 0;
        rs_illegal_out          = 0;
        rs_mem_op               = 0;
        rs_NPC                  = 0;
        rs_IR                   = 0;
        rs_thread_id            = 0;
        mult_stall              = 0;
        nuke                    = 0;

        //CHECK reset
        @(negedge clock);
        reset = 0;
        @(negedge clock);
        if (valid != 0) exit_on_error();
        if (lsq_valid != 0) exit_on_error();
        $display("\n@@@Passed reset\n");

        //  WAY_1: ADDQ $r1, $r1, $r3
        //  WAY_0: ADDQ $r1, $r2, $r3
        @(negedge clock);
        issue1(`WAY_1, `TRUE, 64'd100, 64'd100, 7'd95, `ALU_ADDQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h40210403, 1'b0, 1'b0); //Thread 0
        issue1(`WAY_0, `TRUE, 64'd100, 64'd200, 7'd94, `ALU_ADDQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h40220403, 1'b1, 1'b0); //Thread 1
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `TRUE, 7'd95, 64'd200, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `TRUE, 7'd94, 64'd300, 64'd23456, `FALSE, 1);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'd0, 7'd0, 64'd0, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'd0, 7'd0, 64'd0, 0);
        $display("\n@@@Passed both ways addq to alu\n");

        //  WAY_1: ADDQ $r3, $r3, $r3
        //  WAY_0: SUBQ $r3, $r1, $r2
        issue1(`WAY_1, `TRUE, 64'd300, 64'd300, 7'd93, `ALU_ADDQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h40630403, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd600, 64'd100, 7'd92, `ALU_SUBQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h40610522, 1'b0, 1'b0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `TRUE, 7'd93, 64'd600, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `TRUE, 7'd92, 64'd500, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'd0, 7'd0, 64'd0, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'd0, 7'd0, 64'd0, 0);
        $display("\n@@@Passed way1 addq to alu and way0 subq to alu\n");

        //  WAY_1: lda $r20, data (data = 0x1000)
        //  WAY_0: ldq $r21, 0($r20)
        //nuke = 2'b11; //(Tested Nuke = 11, 10, 01 for MEM ADDR)
        issue1(`WAY_1, `TRUE, 64'd0, 64'd0, 7'd91, `ALU_ADDQ, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h229f1000, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd0, 64'h1000, 7'd90, `ALU_ADDQ, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'ha6b40000, 1'b1, 1'b0);  //Thread 1
        @(negedge clock);
        nuke = 2'b00;
        check_output_cdb(`WAY_1_ALU, `TRUE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd92, 64'd500, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'd0, 7'd0, 64'd0, 0);
        check_output_lsq(`WAY_0, `TRUE, 64'h1000, 7'd90, 64'd23456, 1);
        $display("\n@@@Passed way1 lda to alu and way0 ldq to mem \n");

        //  WAY_1: stq $r21, 0x3000($r20)
        //  WAY_0: nop
        issue1(`WAY_1, `TRUE, 64'd0, 64'h2000, 7'd89, `ALU_ADDQ, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'hb6b43000, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd0, 64'd0, 7'd88, `ALU_BIS, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h47ff041f, 1'b0, 1'b0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `TRUE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_lsq(`WAY_1, `TRUE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed way1 stq to mem and way0 nop to alu \n");

        //  WAY_1: beq $r20 loop (Not taken)
        //  WAY_0: br  loop
        //nuke = 2'b11; //(Tested Nuke = 11, 10, 01 for BR ADDR)
        issue1(`WAY_1, `TRUE, 64'd800, 64'd0, 7'd87, `ALU_ADDQ, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP);
        issue2(`WAY_1,`TRUE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'he69ffffe, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd0, 64'd0, 7'd86, `ALU_ADDQ, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP);
        issue2(`WAY_0,`FALSE, `TRUE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'hc3fffffd, 1'b0, 1'b0);
        @(negedge clock);
        nuke = 2'b00;
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `TRUE, 7'd87, 64'd0, 64'd00127, `FALSE, 0); //Cond Branch not taken, result is zero, NPC is current PC plus 4
        check_output_cdb(`WAY_0_BR, `TRUE, 7'd86, 64'd01238, 64'h4ca, `TRUE, 0); //Uncond Branch taken, result is return address, NPC is PC plus 4 plus br disp
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed way1 beq to branch_addr and way0 br to branch_addr \n");

        //  WAY_1: beq $r20 loop (Taken)
        //  WAY_0: br  loop
        issue1(`WAY_1, `TRUE, 64'd00, 64'd0, 7'd85, `ALU_ADDQ, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP);
        issue2(`WAY_1,`TRUE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'he69ffffe, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd0, 64'd0, 7'd84, `ALU_ADDQ, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP);
        issue2(`WAY_0,`FALSE, `TRUE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'hc3fffffd, 1'b0, 1'b0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `TRUE, 7'd85, 64'h77, 64'h77, `TRUE, 0); //Cond Branch taken, result is , NPC is current PC plus 4
        check_output_cdb(`WAY_0_BR, `TRUE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0); //Uncond Branch taken, result is return address, NPC is PC plus 4 plus br disp
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed way1 beq to branch_addr and way0 br to branch_addr \n");

        //  WAY_1: mulq $r25 = $r23 * $r24  (No stall tested - passed)
        //  WAY_0: mulq $r15 = $r13 * $r14
        issue1(`WAY_1, `TRUE, 64'd400, 64'd450, 7'd83, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h4ef80419, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd550, 64'd600, 7'd82, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h4dae040f, 1'b0, 1'b0);
        @(negedge clock);
        issue1(`WAY_1, `TRUE, 64'd40, 64'd45, 7'd81, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h4ef80419, 1'b1, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd55, 64'd60, 7'd80, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h4dae040f, 1'b0, 1'b0);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        issue1(`WAY_1, `FALSE, 64'd40, 64'd45, 7'd81, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h4ef80419, 1'b0, 1'b0);
        issue1(`WAY_0, `FALSE, 64'd55, 64'd60, 7'd80, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h4dae040f, 1'b0, 1'b0);
        @(negedge clock);
        @(negedge clock);
        mult_stall = 2'b11;
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        // Mult Stall Type 1
        /*@(negedge clock);
        mult_stall = 2'b00;
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd81, 64'd1800, 64'd12345, `FALSE, 1);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd80, 64'd3300, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed both ways mulq to MULT \n");*/
        // Mult Stall Type 2
        /*@(negedge clock);
        mult_stall = 2'b01;
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd81, 64'd1800, 64'd12345, `FALSE, 1);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        @(negedge clock);
        mult_stall = 2'b00;
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd81, 64'd1800, 64'd12345, `FALSE, 1);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd81, 64'd1800, 64'd12345, `FALSE, 1);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd80, 64'd3300, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed both ways mulq to MULT \n");*/
        // Mult Stall Type 2
        @(negedge clock);
        mult_stall = 2'b10;
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd80, 64'd3300, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        @(negedge clock);
        mult_stall = 2'b00;
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd80, 64'd3300, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd81, 64'd1800, 64'd12345, `FALSE, 1);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd80, 64'd3300, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed both ways mulq to MULT \n");


        //  WAY_1: ADDQ $r6, 200, $r7
        //  WAY_0: ADDQ $r8, 130, $r9
        @(negedge clock);
        //nuke = 2'b11; (Tested Nuke = 11, 10, 01 for ALU)
        issue1(`WAY_1, `TRUE, 64'd500, 64'd0, 7'd79, `ALU_ADDQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_ALU_IMM);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h40d91407, 1'b0, 1'b0); //Thread 0
        issue1(`WAY_0, `TRUE, 64'd550, 64'd0, 7'd78, `ALU_ADDQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_ALU_IMM);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h41105529, 1'b1, 1'b0); //Thread 1
        @(negedge clock);
        nuke = 2'b00;
        check_output_cdb(`WAY_1_ALU, `TRUE, 7'd79, 64'd700, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `TRUE, 7'd78, 64'd680, 64'd23456, `FALSE, 1);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd0, 64'd0, 64'd0, `FALSE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'd0, 7'd0, 64'd0, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'd0, 7'd0, 64'd0, 0);
        $display("\n@@@Passed both ways addq to alu with immediate values\n");


        //  WAY_1: mulq $r25 = $r23 * $r24
        //  WAY_0: mulq $r15 = $r13 * $r14
        issue1(`WAY_1, `TRUE, 64'd400, 64'd450, 7'd83, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h4ef80419, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd550, 64'd600, 7'd82, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h4dae040f, 1'b0, 1'b0);
        @(negedge clock);
        issue1(`WAY_1, `TRUE, 64'd40, 64'd45, 7'd81, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h4ef80419, 1'b1, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd55, 64'd60, 7'd80, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h4dae040f, 1'b0, 1'b0);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        issue1(`WAY_1, `FALSE, 64'd40, 64'd45, 7'd81, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h4ef80419, 1'b0, 1'b0);
        issue1(`WAY_0, `FALSE, 64'd55, 64'd60, 7'd80, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h4dae040f, 1'b0, 1'b0);
        @(negedge clock);
        @(negedge clock);
        //mult_stall = 2'b11;  //Tested nuke gets higher preference to stall
        @(negedge clock);
        nuke = 2'b10; //(Tested Nuke = 11, 10, 01 for MULT)
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        nuke = 2'b00;
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd81, 64'd1800, 64'd12345, `FALSE, 1);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd80, 64'd3300, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed both ways MULT nuke\n");

        // Mult stall out to issue pipeline
        //  WAY_1: mulq $r25 = $r23 * $r24
        //  WAY_0: mulq $r15 = $r13 * $r14
        issue1(`WAY_1, `TRUE, 64'd400, 64'd450, 7'd83, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_1,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_1, 64'd00123, 64'd12345, 32'h4ef80419, 1'b0, 1'b0);
        issue1(`WAY_0, `TRUE, 64'd550, 64'd600, 7'd82, `ALU_MULQ, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB);
        issue2(`WAY_0,`FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        issue3(`WAY_0, 64'd01234, 64'd23456, 32'h4dae040f, 1'b0, 1'b0);
        @(negedge clock);
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `FALSE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `FALSE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        mult_stall = 2'b11;
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        if (mult_stall_out != 2'b00) exit_on_error();
        @(negedge clock);
        if (mult_stall_out != 2'b11) exit_on_error();
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        @(negedge clock);
        if (mult_stall_out != 2'b11) exit_on_error();
        check_output_cdb(`WAY_1_ALU, `FALSE, 7'd91, 64'h1000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_ALU, `FALSE, 7'd88, 64'd0, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_MULT, `TRUE, 7'd83, 64'd180000, 64'd12345, `FALSE, 0);
        check_output_cdb(`WAY_0_MULT, `TRUE, 7'd82, 64'd330000, 64'd23456, `FALSE, 0);
        check_output_cdb(`WAY_1_BR, `FALSE, 7'd85, 64'h77, 64'h77, `TRUE, 0);
        check_output_cdb(`WAY_0_BR, `FALSE, 7'd84, 64'd01238, 64'h4ca, `TRUE, 0);
        check_output_lsq(`WAY_1, `FALSE, 64'h5000, 7'd89, 64'd12345, 0);
        check_output_lsq(`WAY_0, `FALSE, 64'h1000, 7'd90, 64'd23456, 0);
        $display("\n@@@Passed MULT stall to Issue pipeline \n");


/*


        //NUKE
        nuke = 2'b10;        //NOTE: Instructions are invalid on nuke!
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        nuke = 2'b00;
        check_rat_entry(`R2, 7'd92);
        check_rat_entry(`R3, 7'd93);
        check_rat_entry(`R11, 7'd11);
        $display("\n@@@Passed NUKE, but no updating RRAT\n");
        //exit_on_error();




        //Dispatch  WAY_1: call_pal (HALT)
        //          WAY_0: INVALID
        if_id_pipe(`WAY_1, 32'h00000555, `TRUE);
        if_id_pipe(`WAY_0, 32'h47ff041f, `FALSE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        check_invalid_inst(`WAY_0);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd00, 7'd00, 7'd89, `TRUE, `FALSE);  //popped out but not renamed
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R31, 7'd31);         //not renamed
        //Disable Inputs (Comment out if you don't want to)
        //inv_inst(`WAY_1);
        //inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch call_pal (HALT) and invalid instr\n");


*/
        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

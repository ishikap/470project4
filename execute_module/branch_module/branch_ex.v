`default_nettype none
`timescale 1ns/100ps
module branch_ex #(parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam QUADWORD=64) (
    // INPUTS
    input   wire    [1:0]                   valid_in,
    input   wire    [1:0][QUADWORD-1:0]     prf_a_value,         // Value to check against condition
    input   wire    [1:0][4:0]              alu_func,
    input   wire    [1:0][QUADWORD-1:0]     opa_value,          // either npc or NOT3
    input   wire    [1:0][QUADWORD-1:0]     opb_value,          // either disp or prfB
    input   wire    [1:0]                   rs_cond_branch_out,
    input   wire    [1:0]                   rs_uncond_branch_out,
    input   wire    [1:0][2:0]              func_cond,          // Specifies which condition to check
    input   wire    [1:0][PRF_BITS-1:0]     prf_tag_in,
    input   wire    [1:0]                   nuke,
    input   wire    [1:0][QUADWORD-1:0]     pc,
    input   wire    [1:0][QUADWORD-1:0]     next_pc,
    // OUTPUTS
    output  logic   [1:0]                   br_actually_taken,
    output  logic   [1:0]                   result_valid,
    output  logic   [1:0][QUADWORD-1:0]     result_out,
    output  logic   [1:0][PRF_BITS-1:0]     prf_tag_out,
    output  logic   [1:0][QUADWORD-1:0]     next_pc_out
);

    logic    [1:0]   cond;                               // 0/1 condition result (False/True)

	always_comb begin
        result_out          = 0;
        br_actually_taken   = 0;
        result_valid        = 0;
        prf_tag_out         = prf_tag_in;
        next_pc_out[0]      = pc[0] + 64'd4;
        next_pc_out[1]      = pc[1] + 64'd4;

        for (int i=1; i>=0; --i) begin
            case (func_cond[i][1:0]) // 'full-case'  All cases covered, no need for a default
                2'b00: cond[i] = (prf_a_value[i][0] == 0);  // LBC: (lsb(opa) == 0) ?
                2'b01: cond[i] = (prf_a_value[i] == 0);     // EQ: (opa == 0) ?
                2'b10: cond[i] = (prf_a_value[i][63] == 1); // LT: (signed(opa) < 0) : check sign bit
                2'b11: cond[i] = (prf_a_value[i][63] == 1) || (prf_a_value[i] == 0); // LE: (signed(opa) <= 0)
            endcase
            // negate cond if func_cond[2] is set
            if (func_cond[i][2]) begin
                cond[i] = ~cond[i];
            end

            br_actually_taken[i] = !(|nuke) && (rs_uncond_branch_out[i] ||
                                    (rs_cond_branch_out[i] && cond[i]));

            result_valid[i] = !(|nuke) && (rs_cond_branch_out[i] || rs_uncond_branch_out[i]) && valid_in[i];

            if (br_actually_taken[i] && alu_func[i] == `ALU_ADDQ) begin
                result_out[i]       = pc[i] + 64'd4 + opb_value[i];
                next_pc_out[i]      = pc[i] + 64'd4 + opb_value[i];
                // BSR and BR: CDB result is return addr
                if (rs_uncond_branch_out[i]) begin
                    result_out[i]   = pc[i] + 64'd4;
                    next_pc_out[i]  = pc[i] + 64'd4 + opb_value[i];
                end
            end
            else if (br_actually_taken[i] && alu_func[i] == `ALU_AND) begin
                result_out[i]       = pc[i] + 64'd4;
                next_pc_out[i]      = opa_value[i] & opb_value[i];
            end
        end
	end
endmodule
`default_nettype wire

// Clock Period:
//                 5.125 < x <= 5.5

`default_nettype none
`timescale 1ns/100ps
module execute_holder #(parameter NUM_PRF_ENTRIES = 96, parameter MULT_STAGES = 8, parameter FINISHED_INSTR = 6, localparam QUADWORD = 64, localparam PRF_BITS=$clog2(NUM_PRF_ENTRIES), localparam NUM_ARF_ENTRIES=32, localparam ARF_BITS=$clog2(NUM_ARF_ENTRIES)) (
    // INPUTS
    input   wire                                        clock,
    input   wire                                        reset,
    // Coming from RS
    // Memory
    input   wire    [1:0]                               mem_valid,
    input   wire    [1:0][QUADWORD-1:0]                 mem_opb_out,
    input   wire    [1:0][PRF_BITS-1:0]                 mem_dest_out,
    input   wire    [1:0][QUADWORD/2-1:0]               mem_IR,
    // Non memory
    input   wire    [1:0]                               valid_in,
    input   wire    [1:0][QUADWORD-1:0]                 rs_opa_out,
    input   wire    [1:0][QUADWORD-1:0]                 rs_opb_out,
    input   wire    [1:0][PRF_BITS-1:0]                 rs_dest_out,
    input   wire    [1:0][4:0]                          rs_alu_func_out,
    input   wire    [1:0][1:0]                          rs_opa_select_out,
    input   wire    [1:0][1:0]                          rs_opb_select_out,
    input   wire    [1:0]                               rs_cond_branch_out,
    input   wire    [1:0]                               rs_uncond_branch_out,
    input   wire    [1:0][QUADWORD-1:0]                 rs_PC,
    input   wire    [1:0][QUADWORD-1:0]                 rs_NPC,
    input   wire    [1:0][QUADWORD/2-1:0]               rs_IR,
    input   wire    [1:0]                               mult_stall,
    input   wire    [1:0]                               nuke,

    // OUTPUTS
    // To CDB Buffer
    output  logic   [FINISHED_INSTR-1:0]                valid,
    output  logic   [FINISHED_INSTR-1:0][PRF_BITS-1:0]  pr_tag,
    output  logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]  results,
    output  logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]  next_pc,
    output  logic   [FINISHED_INSTR-1:0]                branch_taken,
    // To LSQ Buffer
    output  logic   [1:0][QUADWORD-1:0]                 lsq_mem_addr,
    output  logic   [1:0][PRF_BITS-1:0]                 lsq_prf_tag,
    output  logic   [1:0]                               lsq_valid,
    // To Pipeline between Issue and Execute
    output  logic   [1:0]                               mult_stall_out
);

    logic   [2:0][1:0]              valid_ex;
    logic   [1:0][QUADWORD-1:0]     opa_mux_out;
    logic   [1:0][QUADWORD-1:0]     opb_mux_out;

    logic   [1:0][QUADWORD-1:0]     mem_only_disp, mem_disp;
	logic   [1:0][QUADWORD-1:0]     br_disp;
	logic   [1:0][QUADWORD-1:0]     alu_imm;
    logic   [1:0][2:0]              branch_func;

    always_comb begin
        // Determine which ex unit to put instr in (branch takes care of it internally)
        valid_ex            = 0;
        branch_taken[5:2]   = 0;

        for (int i=1; i>=0; --i) begin
            mem_only_disp[i]    = { {48{mem_IR[i][15]}}, mem_IR[i][15:0] };
            mem_disp[i]         = { {48{rs_IR[i][15]}}, rs_IR[i][15:0] };
            br_disp[i]          = { {41{rs_IR[i][20]}}, rs_IR[i][20:0], 2'b00 };
            alu_imm[i]          = { 56'b0, rs_IR[i][20:13] };
            branch_func[i]      = rs_IR[i][28:26];
        end

        for (int i=1; i>=0; --i) begin
            if (valid_in[i]) begin
                if (rs_alu_func_out[i] == `ALU_MULQ) begin
                    valid_ex[2][i] = `TRUE;
                end
                else if (!rs_cond_branch_out[i] && !rs_uncond_branch_out[i]) begin
                    valid_ex[0][i] = `TRUE;
                end
            end
            if (mem_valid[i]) begin
                valid_ex[1][i]      = `TRUE;
            end
        end

        for (int i=1; i>=0; --i) begin
            opa_mux_out[i] = 64'hbaadbeefdeadbeef;
            case (rs_opa_select_out[i])
                `ALU_OPA_IS_REGA:     opa_mux_out[i] = rs_opa_out[i];
                `ALU_OPA_IS_MEM_DISP: opa_mux_out[i] = mem_disp[i];
                `ALU_OPA_IS_NPC:      opa_mux_out[i] = rs_NPC[i];
                `ALU_OPA_IS_NOT3:     opa_mux_out[i] = ~64'h3;
            endcase

            opb_mux_out[i] = 64'hbaadbeefdeadbeef;
            case (rs_opb_select_out[i])
                `ALU_OPB_IS_REGB:    opb_mux_out[i] = rs_opb_out[i];
                `ALU_OPB_IS_ALU_IMM: opb_mux_out[i] = alu_imm[i];
                `ALU_OPB_IS_BR_DISP: opb_mux_out[i] = br_disp[i];
            endcase
        end

        next_pc[5:2] = 0;
    end

    alu #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) alu1 (
        // INPUTS
        .valid_in(valid_ex[0]),
        .opa(opa_mux_out),
        .opb(opb_mux_out),
        .func(rs_alu_func_out),
        .prf_tag_in(rs_dest_out),
        .nuke(nuke),
        // OUTPUTS
        .result(results[5:4]),
        .result_valid(valid[5:4]),
        .prf_tag_out(pr_tag[5:4])
    );

    mult #(.STAGES(MULT_STAGES), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) mult1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .mcand(opa_mux_out),
        .mplier(opb_mux_out),
        .start(valid_ex[2]),
        .prf_tag_in(rs_dest_out),
        .stall(mult_stall),
        .nuke(nuke),
        // OUTPUTS
        .product(results[3:2]),
        .done(valid[3:2]),
        .prf_tag_out(pr_tag[3:2]),
        .stall_out(mult_stall_out)
    );

    branch_ex #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) br_ex1 (
        // INPUTS
        .valid_in(valid_in),
        .prf_a_value(rs_opa_out),
        .alu_func(rs_alu_func_out),
        .opa_value(opa_mux_out),
        .opb_value(opb_mux_out),
        .rs_cond_branch_out(rs_cond_branch_out),
        .rs_uncond_branch_out(rs_uncond_branch_out),
        .func_cond(branch_func),
        .prf_tag_in(rs_dest_out),
        .nuke(nuke),
        .pc(rs_PC),
        .next_pc(rs_NPC),
        // OUTPUTS
        .br_actually_taken(branch_taken[1:0]),
        .result_valid(valid[1:0]),
        .result_out(results[1:0]),
        .prf_tag_out(pr_tag[1:0]),
        .next_pc_out(next_pc[1:0])
    );

    mem_addr_ex #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) mem_addr_ex1(
        // INPUTS
        .valid_in(valid_ex[1]),
        .opa_value(mem_only_disp),
        .opb_value(mem_opb_out),
        .prf_tag_in(mem_dest_out),
        .nuke(nuke),
        // OUTPUTS
        .mem_addr_out(lsq_mem_addr),
        .prf_tag_out(lsq_prf_tag),
        .valid_out(lsq_valid)
    );
endmodule
`default_nettype wire

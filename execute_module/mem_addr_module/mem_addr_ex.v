`default_nettype none
`timescale 1ns/100ps
module mem_addr_ex #(parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam QUADWORD=64) (
    // INPUTS
    input   wire    [1:0]                   valid_in,
    input   wire    [1:0][QUADWORD-1:0]     opa_value,
    input   wire    [1:0][QUADWORD-1:0]     opb_value,
    input   wire    [1:0][PRF_BITS-1:0]     prf_tag_in,
    input   wire    [1:0]                   nuke,
    // OUTPUTS
    output  logic   [1:0][QUADWORD-1:0]     mem_addr_out,
    output  logic   [1:0][PRF_BITS-1:0]     prf_tag_out,
    output  logic   [1:0]                   valid_out
);

    always_comb begin
        mem_addr_out    = 0;
        valid_out       = 0;
        prf_tag_out     = prf_tag_in;
        for (int i=1; i>=0; --i) begin
            if (valid_in[i]& ~(|nuke)) begin
                mem_addr_out[i]     = opa_value[i] + opb_value[i];
                valid_out[i]        = 1;
            end
        end
    end
endmodule
`default_nettype wire
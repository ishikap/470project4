`default_nettype none
`timescale 1ns/100ps
module alu #(parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES),  localparam QUADWORD=64) (
    // INPUTS
    input   wire    [1:0]                   valid_in,
    input   wire    [1:0][QUADWORD-1:0]     opa,
    input   wire    [1:0][QUADWORD-1:0]     opb,
    input   wire    [1:0] [4:0]             func,
    input   wire    [1:0][PRF_BITS-1:0]     prf_tag_in,
    input   wire    [1:0]                   nuke,
    // OUTPUTS
    output  logic   [1:0][QUADWORD-1:0]     result,
    output  logic   [1:0]                   result_valid,
    output  logic   [1:0][PRF_BITS-1:0]     prf_tag_out
);

    // This function computes a signed less-than operation
	function signed_lt;
		input [QUADWORD-1:0] a, b;

		if (a[QUADWORD-1] == b[QUADWORD-1])
			signed_lt = (a < b); // signs match: signed compare same as unsigned
		else
			signed_lt = a[QUADWORD-1];   // signs differ: a is smaller if neg, larger if pos
	endfunction

	always_comb begin
        result_valid    = 0;
        result          = 0;
        prf_tag_out     = prf_tag_in;
        for (int i=1; i>=0; --i) begin
            if (valid_in[i] & ~(|nuke)) begin
                result_valid[i] = 1;
                case (func[i])
                    `ALU_ADDQ:   result[i]  =   opa[i] + opb[i];
                    `ALU_SUBQ:   result[i]  =   opa[i] - opb[i];
                    `ALU_AND:    result[i]  =   opa[i] & opb[i];
                    `ALU_BIC:    result[i]  =   opa[i] & ~opb[i];
                    `ALU_BIS:    result[i]  =   opa[i] | opb[i];
                    `ALU_ORNOT:  result[i]  =   opa[i] | ~opb[i];
                    `ALU_XOR:    result[i]  =   opa[i] ^ opb[i];
                    `ALU_EQV:    result[i]  =   opa[i] ^ ~opb[i];
                    `ALU_SRL:    result[i]  =   opa[i] >> opb[i][5:0];
                    `ALU_SLL:    result[i]  =   opa[i] << opb[i][5:0];
                    `ALU_SRA:    result[i]  =   (opa[i] >> opb[i][5:0]) | ({64{opa[i][63]}} << (64 -
                                                opb[i][5:0])); // arithmetic from logical shift (fills vacancies with opa[63], for neg num)
                    `ALU_CMPULT: result[i]  =   { 63'd0, (opa[i] < opb[i]) };
                    `ALU_CMPEQ:  result[i]  =   { 63'd0, (opa[i] == opb[i]) };
                    `ALU_CMPULE: result[i]  =   { 63'd0, (opa[i] <= opb[i]) };
                    `ALU_CMPLT:  result[i]  =   { 63'd0, signed_lt(opa[i], opb[i]) };
                    `ALU_CMPLE:  result[i]  =   { 63'd0, (signed_lt(opa[i], opb[i]) || (opa[i] == opb[i])) };
                    default:     result[i]  =   64'hdeadbeefbaadbeef;
                endcase
            end
        end
	end
endmodule
`default_nettype wire
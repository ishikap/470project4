// CLK:     t <= 11
`default_nettype none
`timescale 1ns/100ps
module cache_holder #(parameter I_NUM_WAYS = 2, parameter D_NUM_WAYS = 2,
               parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES),
               localparam MSHR_SIZE = 15,
               localparam BLOCK_SIZE = 8, localparam BLOCK_OFFSET = $clog2(BLOCK_SIZE), localparam I_CACHE_SIZE= 256,
               localparam I_NUM_SETS = I_CACHE_SIZE / (BLOCK_SIZE * I_NUM_WAYS), localparam I_SET_INDEX_BITS = $clog2(I_NUM_SETS), localparam MEM_ADDR_BITS = 13,
               localparam I_TAG_BITS = MEM_ADDR_BITS - I_SET_INDEX_BITS, localparam QUADWORD = 64, localparam D_CACHE_SIZE = 256,
               localparam D_NUM_SETS = D_CACHE_SIZE / (BLOCK_SIZE * D_NUM_WAYS), localparam D_SET_INDEX_BITS = $clog2(D_NUM_SETS),
               localparam D_TAG_BITS = MEM_ADDR_BITS - D_SET_INDEX_BITS,
               localparam MSHR_BITS = $clog2(MSHR_SIZE)) (

    // INPUTS
    input   wire                                clock,
    input   wire                                reset,

    input   wire    [1:0][QUADWORD-1:0]         proc2Icache_addr,
    input   wire    [1:0][QUADWORD-1:0]         proc2Icache_next_pc,
    input   wire    [1:0]                       proc2Icache_addr_valid,
    input   wire         [MSHR_BITS-1:0]        mem2Mshr_response,
    input   wire         [MSHR_BITS-1:0]        mem2Mshr_tag,
    input   wire         [QUADWORD-1:0]         mem2Mshr_data,

    input   wire                                stq_wr_en,
    input   wire         [QUADWORD-1:0]         stq_wr_addr,
    input   wire         [QUADWORD-1:0]         stq_wr_data,

    input   wire    [1:0]                       ldb_rd_en,
    input   wire    [1:0][QUADWORD-1:0]         ldb_rd_addr,
    input   wire    [1:0][PRF_BITS-1:0]         ldb_rd_prf,
    input   wire    [1:0]                       halt,
    input   wire    [1:0]                       nuke,

    // OUTPUTS
    output  logic   [1:0][QUADWORD/2-1:0]       icache_data_out,
    output  logic   [1:0]                       icache_valid_out,

    output  logic        [1:0]                  mshr2Mem_command,
    output  logic        [QUADWORD-1:0]         mshr2Mem_address,
    output  logic        [QUADWORD-1:0]         mshr2Mem_data,

    output  logic                               stq_stall,

    output  logic   [2:0][QUADWORD-1:0]         ldb_data_out,
    output  logic   [2:0][QUADWORD-1:0]         ldb_addr_out,
    output  logic   [2:0][PRF_BITS-1:0]         ldb_prf_out,
    output  logic   [2:0]                       ldb_valid_out,
    output  logic   [1:0]                       ldb_stall_out,

    output  logic                               mshr2proc_finished
);

    // MSHR to Icache
    wire         [I_TAG_BITS-1:0]               mshr2Icache_tag;
    wire         [I_SET_INDEX_BITS-1:0]         mshr2Icache_index;
    wire                                        mshr2Icache_en;
    wire         [QUADWORD-1:0]                 mshr2Icache_data;

    // To Icache controller from Icache
    wire    [2:0][I_SET_INDEX_BITS-1:0]         icache_index;
    wire    [2:0][I_TAG_BITS-1:0]               icache_tag;
    wire    [2:0][QUADWORD-1:0]                 icachemem_data;
    wire    [2:0]                               icachemem_valid;

    // To MSHR from Icache controller
    wire    [1:0]                               mshr2Icache_contr_stall;
    wire    [1:0][1:0]                          icache_cont2Mshr_command;
    wire    [1:0][QUADWORD-1:0]                 icache_cont2Mshr_addr;
    wire    [1:0]                               icache_cont2Mshr_pref;

    // MSHR to Dcache
    wire         [D_TAG_BITS-1:0]               mshr2Dcache_tag;
    wire         [D_SET_INDEX_BITS-1:0]         mshr2Dcache_index;
    wire                                        mshr2Dcache_en;
    wire         [QUADWORD-1:0]                 mshr2Dcache_data;
    wire         [PRF_BITS-1:0]                 mshr2Dcache_prf;

    // MSHR to Dcache controller
    wire         [3:0]                          mshr2Dcache_contr_stall;

    // Dcache to Dcache controller
    wire    [1:0]                               evicted_valid;
    wire    [1:0][D_TAG_BITS-1:0]               evicted_tag;
    wire    [1:0][D_SET_INDEX_BITS-1:0]         evicted_idx;
    wire    [1:0][QUADWORD-1:0]                 evicted_data;

    wire    [1:0]                               d_cachemem_valid;
    wire    [1:0][QUADWORD-1:0]                 d_cachemem_data;

    // Dcache controller to Dcache
    wire    [1:0]                               dcache_en;
    wire    [1:0][D_TAG_BITS-1:0]               dcache_tag;
    wire    [1:0][D_SET_INDEX_BITS-1:0]         dcache_index;

    wire    [1:0]                               dcache_stall;

    // Dcache controller to MSHR
    wire    [3:0][1:0]                          dcache_cont2Mshr_command;
    wire    [3:0][QUADWORD-1:0]                 dcache_cont2Mshr_addr;
    wire    [3:0][QUADWORD-1:0]                 dcache_cont2Mshr_data;
    wire    [3:0][PRF_BITS-1:0]                 dcache_cont2Mshr_prf;


    // Logics for splicing
    logic        [D_TAG_BITS-1:0]               stq_tag_in;
    logic        [D_SET_INDEX_BITS-1:0]         stq_set_index_in;

    always_comb begin
        {stq_tag_in, stq_set_index_in}  = stq_wr_addr[QUADWORD-1:3];
    end

    icache #(.NUM_WAYS(I_NUM_WAYS)) i_cache(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .wr_en(mshr2Icache_en),
        .wr_idx(mshr2Icache_index),
        .wr_tag(mshr2Icache_tag),
        .wr_data(mshr2Icache_data),
        .rd_en({1'b1, proc2Icache_addr_valid}),
        .rd_idx(icache_index),
        .rd_tag(icache_tag),
        // OUTPUTS
        .rd_data(icachemem_data),
        .rd_valid(icachemem_valid)
    );

    icache_controller #(.NUM_WAYS(I_NUM_WAYS)) i_controller(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .mshr2Icache_stall(mshr2Icache_contr_stall),
        .proc2Icache_addr(proc2Icache_addr),
        .proc2Icache_next_pc(proc2Icache_next_pc),
        .proc2Icache_addr_valid(proc2Icache_addr_valid),
        .cachemem_data(icachemem_data),
        .cachemem_valid(icachemem_valid),
        // OUTPUTS
        .icache_cont2Mshr_command(icache_cont2Mshr_command),
        .icache_cont2Mshr_addr(icache_cont2Mshr_addr),
        .icache_cont2Mshr_pref(icache_cont2Mshr_pref),
        .icache_data_out(icache_data_out),
        .icache_valid_out(icache_valid_out),
        .icache_index(icache_index),
        .icache_tag(icache_tag)
    );

    mshr #(.MSHR_SIZE(MSHR_SIZE), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES), .I_CACHE_SET_INDEX_BITS(I_SET_INDEX_BITS), .I_CACHE_TAG_BITS(I_TAG_BITS), .D_CACHE_SET_INDEX_BITS(D_SET_INDEX_BITS), .D_CACHE_TAG_BITS(D_TAG_BITS)) m(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .proc2Imem_command(icache_cont2Mshr_command),
        .proc2Imem_addr(icache_cont2Mshr_addr),
        .prefetch(icache_cont2Mshr_pref),
        .proc2Dmem_command(dcache_cont2Mshr_command),
        .proc2Dmem_addr(dcache_cont2Mshr_addr),
        .proc2Dmem_data(dcache_cont2Mshr_data),
        .proc2Dmem_prf(dcache_cont2Mshr_prf),
        .mem2Mshr_response(mem2Mshr_response),
        .mem2Mshr_tag(mem2Mshr_tag),
        .mem2Mshr_data(mem2Mshr_data),
        .halt(halt),
        .nuke(nuke),
        // OUTPUTS
        .mshr2Icache_tag(mshr2Icache_tag),
        .mshr2Icache_index(mshr2Icache_index),
        .mshr2Icache_en(mshr2Icache_en),
        .mshr2Icache_data(mshr2Icache_data),
        .mshr2Icache_contr_stall(mshr2Icache_contr_stall),

        .mshr2Dcache_tag(mshr2Dcache_tag),
        .mshr2Dcache_index(mshr2Dcache_index),
        .mshr2Dcache_en(mshr2Dcache_en),
        .mshr2Dcache_data(mshr2Dcache_data),
        .mshr2Dcache_prf(mshr2Dcache_prf),
        .mshr2Dcache_contr_stall(mshr2Dcache_contr_stall),

        .mshr2Mem_command(mshr2Mem_command),
        .mshr2Mem_address(mshr2Mem_address),
        .mshr2Mem_data(mshr2Mem_data),
        .mshr2proc_finished(mshr2proc_finished)
    );

    dcache_controller #(.NUM_WAYS(D_NUM_WAYS), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) d_cache_contr (
        // INPUTS
        .clock(clock),
        .reset(reset),

        .mshr2Dcache_stall(mshr2Dcache_contr_stall),

        .lsq2Dcache_addr_valid(ldb_rd_en),
        .lsq2Dcache_addr(ldb_rd_addr),
        .lsq2Dcache_prf(ldb_rd_prf),

        .evicted2Dcache_tag(evicted_tag),
        .evicted2Dcache_idx(evicted_idx),
        .evicted2Dcache_data(evicted_data),
        .evicted2Dcache_addr_valid(evicted_valid),

        .cachemem_data(d_cachemem_data),
        .cachemem_valid(d_cachemem_valid),
        .nuke(nuke),

        // OUTPUTS
        .dcache_cont2Mshr_command(dcache_cont2Mshr_command),
        .dcache_cont2Mshr_addr(dcache_cont2Mshr_addr),
        .dcache_cont2Mshr_data(dcache_cont2Mshr_data),
        .dcache_cont2Mshr_prf(dcache_cont2Mshr_prf),

        .lsq_data_out(ldb_data_out[1:0]),
        .lsq_addr_out(ldb_addr_out[1:0]),
        .lsq_prf_out(ldb_prf_out[1:0]),
        .lsq_valid_out(ldb_valid_out[1:0]),
        .lsq_stall_out(ldb_stall_out),

        .dcache_en(dcache_en),
        .dcache_index(dcache_index),
        .dcache_tag(dcache_tag),

        .dcache_stall(dcache_stall)
    );

    dcache #(.NUM_WAYS(D_NUM_WAYS), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) d_cache (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .proc_wr_en(stq_wr_en),
        .proc_wr_idx(stq_set_index_in),
        .proc_wr_tag(stq_tag_in),
        .proc_wr_data(stq_wr_data),
        .mem_wr_en(mshr2Dcache_en),
        .mem_wr_idx(mshr2Dcache_index),
        .mem_wr_tag(mshr2Dcache_tag),
        .mem_wr_data(mshr2Dcache_data),
        .mem_wr_prf(mshr2Dcache_prf),
        .rd_en(dcache_en),
        .rd_idx(dcache_index),
        .rd_tag(dcache_tag),
        .incoming_stall(dcache_stall),
        .halt(halt),
        .nuke(nuke),
        // OUTPUTS
        .rd_data(d_cachemem_data),
        .rd_valid(d_cachemem_valid),
        .evicted_data(evicted_data),
        .evicted_idx(evicted_idx),
        .evicted_tag(evicted_tag),
        .evicted_valid(evicted_valid),
        .lsq_stall(stq_stall),
        .lsq_load_data_valid(ldb_valid_out[2]),
        .lsq_load_data(ldb_data_out[2]),
        .lsq_load_prf(ldb_prf_out[2]),
        .lsq_load_addr(ldb_addr_out[2])
    );






endmodule
`default_nettype wire

`default_nettype none
`timescale 1ns/100ps
module icache_controller #(parameter NUM_WAYS = 2, localparam BLOCK_SIZE = 8, localparam CACHE_SIZE= 256, localparam BLOCK_OFFSET = $clog2(BLOCK_SIZE), localparam NUM_SETS = CACHE_SIZE / (BLOCK_SIZE * NUM_WAYS), localparam SET_INDEX_BITS = $clog2(NUM_SETS), localparam MEM_ADDR_BITS = 13, localparam TAG_BITS = MEM_ADDR_BITS - SET_INDEX_BITS, localparam QUADWORD = 64, localparam NUM_LRU_LEVELS = $clog2(NUM_WAYS))(
    // INPUTS
    input   wire                                clock,
    input   wire                                reset,

    input   wire    [1:0]                       mshr2Icache_stall,

    input   wire    [1:0][QUADWORD-1:0]         proc2Icache_addr,
    input   wire    [1:0][QUADWORD-1:0]         proc2Icache_next_pc,
    input   wire    [1:0]                       proc2Icache_addr_valid,

    input   wire    [2:0][QUADWORD-1:0]         cachemem_data,              // result from icache
    input   wire    [2:0]                       cachemem_valid,

    // OUTPUTS
    output  logic   [1:0][1:0]                  icache_cont2Mshr_command,
    output  logic   [1:0][QUADWORD-1:0]         icache_cont2Mshr_addr,
    output  logic   [1:0]                       icache_cont2Mshr_pref,

    output  logic   [1:0][QUADWORD/2-1:0]       icache_data_out,            // To fetch
    output  logic   [1:0]                       icache_valid_out,

    output  logic   [2:0][SET_INDEX_BITS-1:0]   icache_index,               // used for read to icache
    output  logic   [2:0][TAG_BITS-1:0]         icache_tag
);

    logic   [1:0][TAG_BITS-1:0]         tag, tag_next;
    logic   [1:0][SET_INDEX_BITS-1:0]   set_index, set_index_next;
    logic   [1:0]                       offset, offset_next;                // offset high EX PC = 4, not 0
    logic   [1:0]                       invalid, invalid_next;
    logic   [1:0][1:0]                  mem_command, mem_command_next;
    logic   [1:0]                       serviced, serviced_next;
    logic                               pref_en, pref_en_next;
    logic        [TAG_BITS-1:0]         pref_tag, pref_tag_next;
    logic        [SET_INDEX_BITS-1:0]   pref_set_index, pref_set_index_next;
    logic        [1:0]                  pref_mem_command, pref_mem_command_next;

    always_comb begin
        icache_cont2Mshr_command    = 0;
        icache_cont2Mshr_addr       = 0;
        icache_cont2Mshr_pref       = 0;
        icache_data_out             = 0;
        icache_valid_out            = 0;
        icache_index                = 0;
        icache_tag                  = 0;

        tag_next                    = tag;
        set_index_next              = set_index;
        offset_next                 = offset;
        invalid_next                = invalid;
        mem_command_next            = mem_command;
        serviced_next               = serviced;
        pref_en_next                = pref_en;
        pref_tag_next               = pref_tag;
        pref_set_index_next         = pref_set_index;
        pref_mem_command_next       = pref_mem_command;

        // Send last values to MSHR if needed
        if (pref_en_next && (serviced_next == 2'b11)) begin
            icache_cont2Mshr_command[1]         = pref_mem_command_next;
            icache_cont2Mshr_addr[1]            = {pref_tag_next, pref_set_index_next, 3'b0} | 64'd0;
            icache_cont2Mshr_pref[1]            = `TRUE;
        end
        for (int i = 0; i < 2; ++i) begin
            if (!invalid_next[i] && !serviced_next[i]) begin
                icache_cont2Mshr_command[i]     = mem_command_next[i];
                icache_cont2Mshr_addr[i]        = {tag_next[i], set_index_next[i], 3'b0} | 64'd0;
            end
            if (!invalid_next[i] && !serviced_next[i] && mshr2Icache_stall[i]) begin
                serviced_next[i]                = `FALSE;
            end
            else if (!invalid_next[i] && !serviced_next[i] && !mshr2Icache_stall[i]) begin
                serviced_next[i]                = `TRUE;
            end
        end

        // Increment prefetch counter- removed stall dependence
        if (pref_en_next) begin
            {pref_tag_next, pref_set_index_next}    = {pref_tag_next, pref_set_index_next} + 1;
        end

        // Check the cache
        for (int i = 1; i >= 0; --i) begin
            if ((proc2Icache_addr[i][1:0]==2'b0) && (proc2Icache_addr[i] < `MEM_SIZE_IN_BYTES) && proc2Icache_addr_valid[i]) begin

                // Only say not serviced if new address is different from both previous addresses or mshr stalled
                if (((({tag_next[1], set_index_next[1], offset_next[1], 2'b0} | 64'd0) != proc2Icache_addr[i]) &&
                     (({tag_next[0], set_index_next[0], offset_next[0], 2'b0} | 64'd0) != proc2Icache_addr[i])) ) begin

                    serviced_next[i]                                = `FALSE;
                    pref_en_next                                    = `FALSE;
                end
                // We are still waiting for MSHR to respond, start prefetching, initialize pref address, if way 0 invalid then branch
                else if (!pref_en_next) begin
                    pref_en_next                                    = `TRUE;
                    {pref_tag_next, pref_set_index_next}            = proc2Icache_next_pc[i][QUADWORD-1:3];
                end
                // Give icache the address to read
                {tag_next[i], set_index_next[i], offset_next[i]}    = proc2Icache_addr[i][QUADWORD-1:2];
                icache_tag[i]                                       = tag_next[i];
                icache_index[i]                                     = set_index_next[i];
                invalid_next[i]                                     = `FALSE;
                icache_tag[2]                                       = pref_tag_next;
                icache_index[2]                                     = pref_set_index_next;
            end
            else begin
                invalid_next[i]                                         = `TRUE;
            end
        end

        // Check for HIT and send if valid
        for (int i = 0; i < 2; ++i) begin
            if (cachemem_valid[i]) begin
                serviced_next[i]        = `TRUE;
                mem_command_next[i]     = `BUS_NONE;

                icache_data_out[i]      = offset_next[i] ?  cachemem_data[i][QUADWORD-1:QUADWORD/2] :
                                                            cachemem_data[i][QUADWORD/2-1:0];
                icache_valid_out[i]     = `TRUE;
            end
            else begin
                mem_command_next[i]     = `BUS_LOAD;
                // if both ways' addresses match, send way 1's
                if ({tag_next[1], set_index_next[1]} == {tag_next[0], set_index_next[0]} && !mshr2Icache_stall[0]) begin
                    serviced_next[0]    = `TRUE;
                end
            end
        end

        // if cache hit, dont prefetch it, try again later
        if (pref_en_next && cachemem_valid[2]) begin
            pref_mem_command_next                     = `BUS_NONE;
        end
        else if (pref_en_next) begin
            pref_mem_command_next                     = `BUS_LOAD;
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            tag                 <= `SD -1;
            set_index           <= `SD -1;
            pref_tag            <= `SD -1;
            pref_set_index      <= `SD -1;
            offset              <= `SD 0;
            invalid             <= `SD 2'b11;
            mem_command         <= `SD 0;
            serviced            <= `SD 0;
            pref_en             <= `SD 0;
            pref_mem_command    <= `SD 0;
        end
        else begin
            tag                 <= `SD tag_next;
            set_index           <= `SD set_index_next;
            pref_tag            <= `SD pref_tag_next;
            pref_set_index      <= `SD pref_set_index_next;
            offset              <= `SD offset_next;
            invalid             <= `SD invalid_next;
            mem_command         <= `SD mem_command_next;
            serviced            <= `SD serviced_next;
            pref_en             <= `SD pref_en_next;
            pref_mem_command    <= `SD pref_mem_command_next;
        end
    end
endmodule
`default_nettype wire

// CLK: t <= 6.0625
`default_nettype none
`timescale 1ns/100ps
module mshr #(parameter MSHR_SIZE = 15, parameter I_CACHE_SET_INDEX_BITS = 4, parameter I_CACHE_TAG_BITS = 9, parameter D_CACHE_SET_INDEX_BITS = 4, parameter D_CACHE_TAG_BITS = 9,
              parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES),
              localparam MSHR_BITS = $clog2(MSHR_SIZE), localparam QUADWORD = 64) (
    //INPUTS
    input   wire                                        clock,
    input   wire                                        reset,

    input   wire    [1:0][1:0]                          proc2Imem_command,
    input   wire    [1:0][QUADWORD-1:0]                 proc2Imem_addr,
    input   wire    [1:0]                               prefetch,

    input   wire    [3:0][1:0]                          proc2Dmem_command,      // 3:2 is for evictions from D-cache that need to store to mem
    input   wire    [3:0][QUADWORD-1:0]                 proc2Dmem_addr,         // 1:0 is for loads that need to read from memory
    input   wire    [3:0][QUADWORD-1:0]                 proc2Dmem_data,
    input   wire    [3:0][PRF_BITS-1:0]                 proc2Dmem_prf,

    input   wire         [MSHR_BITS-1:0]                mem2Mshr_response,
    input   wire         [MSHR_BITS-1:0]                mem2Mshr_tag,
    input   wire         [QUADWORD-1:0]                 mem2Mshr_data,

    input   wire    [1:0]                               halt,
    input   wire    [1:0]                               nuke,

    //OUTPUTS
    output  logic        [I_CACHE_TAG_BITS-1:0]         mshr2Icache_tag,
    output  logic        [I_CACHE_SET_INDEX_BITS-1:0]   mshr2Icache_index,
    output  logic                                       mshr2Icache_en,
    output  logic        [QUADWORD-1:0]                 mshr2Icache_data,
    output  logic   [1:0]                               mshr2Icache_contr_stall,

    output  logic        [D_CACHE_TAG_BITS-1:0]         mshr2Dcache_tag,
    output  logic        [D_CACHE_SET_INDEX_BITS-1:0]   mshr2Dcache_index,
    output  logic        [PRF_BITS-1:0]                 mshr2Dcache_prf,
    output  logic                                       mshr2Dcache_en,
    output  logic        [QUADWORD-1:0]                 mshr2Dcache_data,
    output  logic   [3:0]                               mshr2Dcache_contr_stall,

    output  logic        [1:0]                          mshr2Mem_command,
    output  logic        [QUADWORD-1:0]                 mshr2Mem_address,
    output  logic        [QUADWORD-1:0]                 mshr2Mem_data,

    output  logic                                       mshr2proc_finished
);

    logic   [MSHR_SIZE-1:0]                             cache_sel, cache_sel_next;          // 1 is Icache, 0 is Dcache
    logic   [MSHR_SIZE-1:0]                             mem_op, mem_op_next;                // 1 is Load, 0 is Store
    logic   [MSHR_SIZE-1:0]                             valid, valid_next;
    logic   [MSHR_SIZE-1:0]                             pref, pref_next;
    logic   [MSHR_SIZE-1:0][MSHR_BITS-1:0]              mem_tag, mem_tag_next;
    logic   [MSHR_SIZE-1:0][I_CACHE_TAG_BITS-1:0]       i_tag, i_tag_next;
    logic   [MSHR_SIZE-1:0][I_CACHE_SET_INDEX_BITS-1:0] i_set_index, i_set_index_next;
    logic   [MSHR_SIZE-1:0][D_CACHE_TAG_BITS-1:0]       d_tag, d_tag_next;
    logic   [MSHR_SIZE-1:0][D_CACHE_SET_INDEX_BITS-1:0] d_set_index, d_set_index_next;
    logic   [MSHR_SIZE-1:0][PRF_BITS-1:0]               prf, prf_next;
    logic   [MSHR_SIZE-1:0][QUADWORD-1:0]               data, data_next;
    logic   [MSHR_SIZE-1:0]                             requested, requested_next;// could be memtag = 0
    logic   [MSHR_BITS-1:0]                             mem2Mshr_response_prev;

    logic                  [MSHR_BITS-1:0]              mshr_tag, mshr_tag_next;
    logic                  [QUADWORD-1:0]               mshr_data, mshr_data_next;

    logic             [5:0][MSHR_BITS-1:0]              sel_en;
    logic             [5:0]                             sel_en_valid;
    logic             [1:0]                             halt_prev, halt_prev2;
    logic   [MSHR_BITS-1:0]                             service_store_idx;
    logic                                               service_store_val;
    logic   [MSHR_BITS-1:0]                             service_load_idx;
    logic                                               service_load_val;
    logic   [MSHR_BITS-1:0]                             service_instr_idx;
    logic                                               service_instr_val;
    logic   [MSHR_BITS-1:0]                             service_instr_pref_idx;
    logic                                               service_instr_pref_val;

    // Priority selector -odd size
    pe_n #(.REQ_BITS(MSHR_SIZE+1), .SEL(6)) mshr_enc(
        .req({1'b0,~valid}),
        .enc(sel_en),
        .valid(sel_en_valid)
    );

    // Select servicing stores to memory
    pe_n #(.REQ_BITS(MSHR_SIZE+1), .SEL(1)) mshr_store_enc(
        .req({1'b0,valid & ~requested & ~mem_op & ~cache_sel}),
        .enc(service_store_idx),
        .valid(service_store_val)
    );

    // Select servicing loads to memory
    pe_n #(.SEL(1), .REQ_BITS(MSHR_SIZE+1)) mshr_load_sel(
        .req({1'b0,valid & ~requested & mem_op  & ~cache_sel}),
        .enc(service_load_idx),
        .valid(service_load_val)
    );

    // Select servicing instr to memory
    pe_n #(.SEL(1), .REQ_BITS(MSHR_SIZE+1)) mshr_instr_sel(
        .req({1'b0,valid & ~requested & mem_op  & cache_sel & ~pref}),
        .enc(service_instr_idx),
        .valid(service_instr_val)
    );

    // Select servicing instr to memory
    pe_n #(.SEL(1), .REQ_BITS(MSHR_SIZE+1)) mshr_instr_pref_sel(
        .req({1'b0,valid & ~requested & mem_op  & cache_sel & pref}),
        .enc(service_instr_pref_idx),
        .valid(service_instr_pref_val)
    );

    always_comb begin
        cache_sel_next              = cache_sel;
        mem_op_next                 = mem_op;
        valid_next                  = valid;
        pref_next                   = pref;
        data_next                   = data;
        prf_next                    = prf;
        mem_tag_next                = mem_tag;
        i_set_index_next            = i_set_index;
        d_set_index_next            = d_set_index;
        i_tag_next                  = i_tag;
        d_tag_next                  = d_tag;
        requested_next              = requested;
        mshr_tag_next               = mshr_tag;
        mshr_data_next              = mshr_data;

        mshr2Icache_tag             = 0;
        mshr2Icache_index           = 0;
        mshr2Icache_en              = `FALSE;
        mshr2Icache_data            = 0;
        mshr2Icache_contr_stall     = 0;
        mshr2Dcache_tag             = 0;
        mshr2Dcache_index           = 0;
        mshr2Dcache_prf             = `ZERO_REG_PRF;
        mshr2Dcache_en              = `FALSE;
        mshr2Dcache_data            = 0;
        mshr2Dcache_contr_stall     = 0;
        mshr2Mem_command            = 0;
        mshr2Mem_address            = 0;
        mshr2Mem_data               = 0;
        mshr2proc_finished          = 0;

        // Read from memory and send to cache(s)
        for (int i = 0; i < MSHR_SIZE; ++i) begin
            if (valid_next[i] && requested_next[i] && (mem_tag_next[i] == mshr_tag_next)) begin
                if (cache_sel_next[i] && mem_op_next[i]) begin          // I-Cache Load
                    mshr2Icache_data    = mshr_data;
                    mshr2Icache_tag     = i_tag_next[i];
                    mshr2Icache_index   = i_set_index_next[i];
                    mshr2Icache_en      = `TRUE;
                end
                else if (!cache_sel_next[i] && mem_op_next[i]) begin    // D-Cache Load
                    mshr2Dcache_data    = mshr_data;
                    mshr2Dcache_tag     = d_tag_next[i];
                    mshr2Dcache_index   = d_set_index_next[i];
                    mshr2Dcache_prf     = prf_next[i];
                    mshr2Dcache_en      = `TRUE;
                end

                valid_next[i]       = `FALSE;
                i_tag_next[i]       = 0;
                d_tag_next[i]       = 0;
                prf_next[i]         = `ZERO_REG_PRF;
                i_set_index_next[i] = 0;
                d_set_index_next[i] = 0;
                mem_tag_next[i]     = 0;
                pref_next[i]        = 0;
            end
        end

        // Service anything waiting
        if (service_store_val) begin
            mshr2Mem_command    = `BUS_STORE;
            mshr2Mem_address    = {d_tag_next[service_store_idx], d_set_index_next[service_store_idx], 3'b0} | 64'd0;
            mshr2Mem_data       = data_next[service_store_idx];

            if (mem2Mshr_response != 0 && mem2Mshr_response_prev != mem2Mshr_response) begin
                requested_next[service_store_idx]   = `TRUE;
                mem_tag_next[service_store_idx]     = mem2Mshr_response;

                // Stores dont stick around
                valid_next[service_store_idx]       = `FALSE;
                d_tag_next[service_store_idx]       = 0;
                prf_next[service_store_idx]         = `ZERO_REG_PRF;
                d_set_index_next[service_store_idx] = 0;
                mem_tag_next[service_store_idx]     = 0;
            end
        end
        else if (service_load_val) begin
            mshr2Mem_command    = `BUS_LOAD;
            mshr2Mem_address    = {d_tag_next[service_load_idx], d_set_index_next[service_load_idx], 3'b0} | 64'd0;
            mshr2Mem_data       = data_next[service_load_idx];

            if (mem2Mshr_response != 0 && mem2Mshr_response_prev != mem2Mshr_response) begin
                requested_next[service_load_idx]    = `TRUE;
                mem_tag_next[service_load_idx]      = mem2Mshr_response;
            end
        end
        else if (service_instr_val) begin
            mshr2Mem_command    = `BUS_LOAD;
            mshr2Mem_address    = {i_tag_next[service_instr_idx], i_set_index_next[service_instr_idx], 3'b0} | 64'd0;
            mshr2Mem_data       = data_next[service_instr_idx];

            if (mem2Mshr_response != 0 && mem2Mshr_response_prev != mem2Mshr_response) begin
                requested_next[service_instr_idx]   = `TRUE;
                mem_tag_next[service_instr_idx]     = mem2Mshr_response;
            end
        end
        else if (service_instr_pref_val) begin
            mshr2Mem_command    = `BUS_LOAD;
            mshr2Mem_address    = {i_tag_next[service_instr_pref_idx], i_set_index_next[service_instr_pref_idx], 3'b0} | 64'd0;
            mshr2Mem_data       = data_next[service_instr_pref_idx];

            if (mem2Mshr_response != 0 && mem2Mshr_response_prev != mem2Mshr_response) begin
                requested_next[service_instr_pref_idx]   = `TRUE;
                mem_tag_next[service_instr_pref_idx]     = mem2Mshr_response;
            end
        end

        // Write to MSHR D-Cache
        for (int i = 3; i >= 0; --i) begin
            if (!sel_en_valid[i+2]) begin
                mshr2Dcache_contr_stall[i]                                  = `TRUE;
            end
            else if (sel_en_valid[i+2] && proc2Dmem_command[i] != `BUS_NONE) begin
                cache_sel_next[sel_en[i+2]]                                 = 0;
                mem_op_next[sel_en[i+2]]                                    = (proc2Dmem_command[i] == `BUS_LOAD);
                valid_next[sel_en[i+2]]                                     = `TRUE;
                {d_tag_next[sel_en[i+2]], d_set_index_next[sel_en[i+2]]}    = proc2Dmem_addr[i][QUADWORD-1:3];
                i_tag_next[sel_en[i+2]]                                     = 0;
                i_set_index_next[sel_en[i+2]]                               = 0;
                data_next[sel_en[i+2]]                                      = proc2Dmem_data[i];
                prf_next[sel_en[i+2]]                                       = proc2Dmem_prf[i];
                mem_tag_next[sel_en[i+2]]                                   = 0;
                requested_next[sel_en[i+2]]                                 = `FALSE;
            end
        end

        // Write to MSHR I-Cache
        for (int i = 1; i >= 0; --i) begin
            if (!sel_en_valid[i]) begin
                mshr2Icache_contr_stall[i]                              = `TRUE;
            end
            else if (sel_en_valid[i] && proc2Imem_command[i] != `BUS_NONE) begin
                cache_sel_next[sel_en[i]]                               = 1;
                mem_op_next[sel_en[i]]                                  = (proc2Imem_command[i] == `BUS_LOAD);
                valid_next[sel_en[i]]                                   = `TRUE;
                {i_tag_next[sel_en[i]], i_set_index_next[sel_en[i]]}    = proc2Imem_addr[i][QUADWORD-1:3];
                d_tag_next[sel_en[i]]                                   = 0;
                d_set_index_next[sel_en[i]]                             = 0;
                data_next[sel_en[i]]                                    = 0;
                prf_next[sel_en[i]]                                     = `ZERO_REG_PRF;
                mem_tag_next[sel_en[i]]                                 = 0;
                requested_next[sel_en[i]]                               = `FALSE;
                pref_next[sel_en[i]]                                    = prefetch[i];
            end
        end

        // Halt program if no stores in MSHR
        if (halt_prev2) begin
            for (int i = 0; i < MSHR_SIZE; ++i) begin
                if (!(mshr2Dcache_contr_stall[3:2]) && (!valid_next[i] || (valid_next[i] && mem_op_next[i]))) begin
                    mshr2proc_finished  = `TRUE;
                end
                else begin
                    mshr2proc_finished = `FALSE;
                    break;
                end
            end
        end

        mshr_tag_next   = mem2Mshr_tag;
        mshr_data_next  = mem2Mshr_data;

        // Nuke
        if (nuke) begin
            for (int i=0; i<MSHR_SIZE; ++i) begin
                if (!cache_sel_next[i] && mem_op_next[i]) begin    // D-Cache Load
                    valid_next[i]       = `FALSE;
                    d_tag_next[i]       = 0;
                    d_set_index_next[i] = 0;
                    prf_next[i]         = `ZERO_REG_PRF;
                    mem_tag_next[i]     = 0;
                    pref_next[i]        = 0;
                    requested_next[i]   = 0;
                end
            end
        end

    end

    always_ff @(posedge clock) begin
        if (reset) begin
            cache_sel               <= `SD 0;
            mem_op                  <= `SD 0;
            valid                   <= `SD 0;
            pref                    <= `SD 0;
            data                    <= `SD 0;
            for (int i = 0; i < MSHR_SIZE; ++i) begin
                prf[i]              <= `SD `ZERO_REG_PRF;
            end
            mem_tag                 <= `SD 0;
            i_set_index             <= `SD 0;
            d_set_index             <= `SD 0;
            d_tag                   <= `SD 0;
            i_tag                   <= `SD 0;
            requested               <= `SD 0;
            halt_prev               <= `SD 0;
            halt_prev2              <= `SD 0;

            mshr_tag                <= `SD 0;
            mshr_data               <= `SD 0;
            mem2Mshr_response_prev  <= `SD 0;
        end
        else begin
            cache_sel               <= `SD cache_sel_next;
            mem_op                  <= `SD mem_op_next;
            valid                   <= `SD valid_next;
            pref                    <= `SD pref_next;
            data                    <= `SD data_next;
            prf                     <= `SD prf_next;
            mem_tag                 <= `SD mem_tag_next;
            i_set_index             <= `SD i_set_index_next;
            d_set_index             <= `SD d_set_index_next;
            i_tag                   <= `SD i_tag_next;
            d_tag                   <= `SD d_tag_next;
            requested               <= `SD requested_next;
            halt_prev               <= `SD halt ? halt : halt_prev;
            halt_prev2              <= `SD halt_prev ? halt_prev : halt_prev2;

            mshr_tag                <= `SD mshr_tag_next;
            mshr_data               <= `SD mshr_data_next;
            mem2Mshr_response_prev  <= `SD mem2Mshr_response;
        end
    end
endmodule
`default_nettype wire

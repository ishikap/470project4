`default_nettype none
`timescale 1ns/100ps
module icache #(parameter NUM_WAYS = 2, localparam BLOCK_SIZE = 8, localparam CACHE_SIZE= 256, localparam BLOCK_OFFSET = $clog2(BLOCK_SIZE), localparam NUM_SETS = CACHE_SIZE / (BLOCK_SIZE * NUM_WAYS), localparam SET_INDEX_BITS = $clog2(NUM_SETS), localparam MEM_SIZE = `MEM_SIZE_IN_BYTES, localparam MEM_ADDR_BITS = $clog2(MEM_SIZE), localparam TAG_BITS = MEM_ADDR_BITS - SET_INDEX_BITS - BLOCK_OFFSET, localparam QUADWORD = 64, localparam NUM_LRU_LEVELS = $clog2(NUM_WAYS))(
    // INPUTS
    input   wire                                clock,
    input   wire                                reset,
    input   wire                                wr_en,
    input   wire    [SET_INDEX_BITS-1:0]        wr_idx,
    input   wire    [TAG_BITS-1:0]              wr_tag,
    input   wire    [QUADWORD-1:0]              wr_data,
    input   wire    [2:0]                       rd_en,
    input   wire    [2:0][SET_INDEX_BITS-1:0]   rd_idx,
    input   wire    [2:0][TAG_BITS-1:0]         rd_tag,
    // OUTPUTS
    output  logic   [2:0][QUADWORD-1:0]         rd_data,
    output  logic   [2:0]                       rd_valid
);

    // Cache state
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][QUADWORD-1:0]      data, data_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][TAG_BITS-1:0]      tags, tags_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0]                    valids, valids_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:1]                    plru, plru_next;
    logic   [NUM_LRU_LEVELS-1:0]                            plru_sel;

    logic   [NUM_LRU_LEVELS-1:0]                            current_index;

    always_comb begin
        rd_valid        = 0;
        rd_data         = 0;
        plru_sel        = 0;
        current_index   = 0;
        data_next       = data;
        tags_next       = tags;
        valids_next     = valids;
        plru_next       = plru;

        // Read and update Pseudo LRU
        for (int port=2; port>=0; --port) begin
            if (rd_en[port]) begin
                if (rd_idx[port] == wr_idx && rd_tag[port] == wr_tag && wr_en) begin
                    rd_valid[port]                                  = `TRUE;
                    rd_data[port]                                   = wr_data;
                end
                else begin
                    for (int way_num=0; way_num<NUM_WAYS; ++way_num) begin
                        if (valids_next[rd_idx[port]][way_num] && tags_next[rd_idx[port]][way_num] == rd_tag[port]) begin
                            // Read data
                            rd_valid[port]                          = `TRUE;
                            rd_data[port]                           = data_next[rd_idx[port]][way_num];
                            // Update Pseudo LRU
                            if (port != 2) begin
                                for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                                    plru_sel                            = (({NUM_LRU_LEVELS{1'b0}} | 1'b1) << (NUM_LRU_LEVELS-i-1)) |  (way_num >> (i+1));
                                    plru_next[rd_idx[port]][plru_sel]   = ~way_num[i];
                                end
                            end
                        end
                    end
                end
            end
        end

        // Write from mem (won't write if data is already in cache)
        if (wr_en) begin
            // Find entry to replace in the set using PLRT
            // Start off at the head entry
            current_index = 1;
            for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                current_index = { current_index, plru_next[wr_idx][current_index] };
            end
            // Do replacement
            data_next[wr_idx][current_index]      = wr_data;
            tags_next[wr_idx][current_index]      = wr_tag;
            valids_next[wr_idx][current_index]    = `TRUE;
            // Update plru
            for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                plru_sel                          = (({NUM_LRU_LEVELS{1'b0}} | 1'b1) << (NUM_LRU_LEVELS-i-1)) |  (current_index >> (i+1));
                plru_next[wr_idx][plru_sel]       = ~current_index[i];
            end
        end
    end

    always_ff @(posedge clock)
    begin
        if (reset) begin
            data        <= `SD 0;
            tags        <= `SD 0;
            valids      <= `SD 0;
            plru        <= `SD 0;
        end
        else begin
            data        <= `SD data_next;
            tags        <= `SD tags_next;
            valids      <= `SD valids_next;
            plru        <= `SD plru_next;
        end
    end


endmodule
`default_nettype wire

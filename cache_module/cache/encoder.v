`default_nettype none
`timescale 1ns/100ps
module encoder #(parameter MSHR_SIZE = 15, localparam MSHR_BITS = $clog2(MSHR_SIZE), parameter SEL = 7) (
    input   wire    [SEL-1:0][MSHR_SIZE-1:0]     in,
    output  logic   [SEL-1:0][MSHR_BITS-1:0]     out
);

    always_comb begin
        for (int i = SEL-1; i >= 0; --i) begin
            out[i] = 15;
            case (in[i])
                15'h0001 : out[i] = 0;
                15'h0002 : out[i] = 1;
                15'h0004 : out[i] = 2;
                15'h0008 : out[i] = 3;
                15'h0010 : out[i] = 4;
                15'h0020 : out[i] = 5;
                15'h0040 : out[i] = 6;
                15'h0080 : out[i] = 7;
                15'h0100 : out[i] = 8;
                15'h0200 : out[i] = 9;
                15'h0400 : out[i] = 10;
                15'h0800 : out[i] = 11;
                15'h1000 : out[i] = 12;
                15'h2000 : out[i] = 13;
                15'h4000 : out[i] = 14;
            endcase
        end
    end
endmodule
`default_nettype wire
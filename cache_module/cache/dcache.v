// Write back, write allocate cache with block size = quadword. When write allocating,
// just put write data into cache, no need to bring from mem since the sizes are the same.
`default_nettype none
`timescale 1ns/100ps
module dcache #(parameter NUM_WAYS = 2,
                parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES),
                localparam BLOCK_SIZE = 8, localparam CACHE_SIZE= 256, localparam BLOCK_OFFSET = $clog2(BLOCK_SIZE),
                localparam NUM_SETS = CACHE_SIZE / (BLOCK_SIZE * NUM_WAYS), localparam SET_INDEX_BITS = $clog2(NUM_SETS),
                localparam MEM_ADDR_BITS = 13, localparam TAG_BITS = MEM_ADDR_BITS - SET_INDEX_BITS,
                localparam QUADWORD = 64, localparam NUM_LRU_LEVELS = $clog2(NUM_WAYS))(
    // INPUTS
    input   wire                                clock,
    input   wire                                reset,
    input   wire                                proc_wr_en,     // from lsq (for stores)
    input   wire         [SET_INDEX_BITS-1:0]   proc_wr_idx,
    input   wire         [TAG_BITS-1:0]         proc_wr_tag,
    input   wire         [QUADWORD-1:0]         proc_wr_data,
    input   wire                                mem_wr_en,      // from controller (for loads)
    input   wire         [SET_INDEX_BITS-1:0]   mem_wr_idx,
    input   wire         [TAG_BITS-1:0]         mem_wr_tag,
    input   wire         [QUADWORD-1:0]         mem_wr_data,
    input   wire         [PRF_BITS-1:0]         mem_wr_prf,
    input   wire    [1:0]                       rd_en,
    input   wire    [1:0][SET_INDEX_BITS-1:0]   rd_idx,
    input   wire    [1:0][TAG_BITS-1:0]         rd_tag,
    input   wire    [1:0]                       incoming_stall,
    input   wire    [1:0]                       halt,
    input   wire    [1:0]                       nuke,
    // OUTPUTS
    output  logic   [1:0][QUADWORD-1:0]         rd_data,            // read from controller to see if cache hit
    output  logic   [1:0]                       rd_valid,           // give an address back too
    output  logic   [1:0][QUADWORD-1:0]         evicted_data,       // 4 ports since eviction when stores and mem writes at same time
    output  logic   [1:0][SET_INDEX_BITS-1:0]   evicted_idx,
    output  logic   [1:0][TAG_BITS-1:0]         evicted_tag,
    output  logic   [1:0]                       evicted_valid,
    output  logic                               lsq_stall,
    output  logic                               lsq_load_data_valid,
    output  logic        [QUADWORD-1:0]         lsq_load_data,
    output  logic        [PRF_BITS-1:0]         lsq_load_prf,
    output  logic        [QUADWORD-1:0]         lsq_load_addr
);

    // Cache state
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][QUADWORD-1:0]      data, data_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0][TAG_BITS-1:0]      tags, tags_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0]                    valids, valids_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:0]                    dirty, dirty_next;
    logic   [NUM_SETS-1:0][NUM_WAYS-1:1]                    plru, plru_next;
    logic   [NUM_LRU_LEVELS-1:0]                            plru_sel;

    logic   [NUM_LRU_LEVELS-1:0]                            current_index;
    logic                                                   found_flag;

    logic   [NUM_SETS*NUM_WAYS-1:0]                         dirty_req, dirty_req_next;
    logic   [1:0][NUM_SETS*NUM_WAYS-1:0]                    dirty_grant;
    logic   [1:0]                                           halt_prev;

    always_comb begin
        rd_valid                        = 0;
        rd_data                         = 0;
        plru_sel                        = 0;
        current_index                   = 0;
        evicted_data                    = 0;
        evicted_idx                     = 0;
        evicted_tag                     = 0;
        evicted_valid                   = 0;
        lsq_stall                       = 0;

        data_next                       = data;
        tags_next                       = tags;
        valids_next                     = valids;
        dirty_next                      = dirty;
        plru_next                       = plru;
        dirty_req_next                  = dirty_req;

        // Write from processor- stores
        found_flag = 0;
        if (incoming_stall[0]) begin
            lsq_stall                                       = `TRUE;
        end
        else if (proc_wr_en) begin
            // Is write data in cache already
            for (int way_num=0; way_num<NUM_WAYS; ++way_num) begin
                if (valids_next[proc_wr_idx][way_num] && tags_next[proc_wr_idx][way_num] == proc_wr_tag) begin
                    found_flag                                      = `TRUE;
                    data_next[proc_wr_idx][way_num]                 = proc_wr_data;
                    dirty_next[proc_wr_idx][way_num]                = `TRUE;
                    // Update PLRU
                    for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                        plru_sel                                    = (({NUM_LRU_LEVELS{1'b0}} | 1'b1) << (NUM_LRU_LEVELS-i-1)) |  (way_num >> (i+1));
                        plru_next[proc_wr_idx][plru_sel]            = ~way_num[i];
                    end
                end
            end
            if(!found_flag) begin
                // Find entry to replace in the set using PLRT
                // Start off at the head entry
                current_index = 1;
                for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                    current_index                                   = { current_index, plru_next[proc_wr_idx][current_index] };
                end
                // Evicted data
                evicted_data[0]                                     = data_next[proc_wr_idx][current_index];
                evicted_idx[0]                                      = proc_wr_idx;
                evicted_tag[0]                                      = tags_next[proc_wr_idx][current_index];
                evicted_valid[0]                                    = dirty_next[proc_wr_idx][current_index] && valids_next[proc_wr_idx][current_index];

                // Do replacement
                data_next[proc_wr_idx][current_index]           = proc_wr_data;
                tags_next[proc_wr_idx][current_index]           = proc_wr_tag;
                valids_next[proc_wr_idx][current_index]         = `TRUE;
                dirty_next[proc_wr_idx][current_index]          = `TRUE;

                // Update plru
                for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                    plru_sel                                        = (({NUM_LRU_LEVELS{1'b0}} | 1'b1) << (NUM_LRU_LEVELS-i-1)) |  (current_index >> (i+1));
                    plru_next[proc_wr_idx][plru_sel]                = ~current_index[i];
                end
            end
        end

        // Write from memory- loads
        if (!incoming_stall[1] && mem_wr_en && !halt_prev) begin
            // Find entry to replace in the set using PLRT
            // Start off at the head entry
            current_index = 1;
            for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                current_index                               = { current_index, plru_next[mem_wr_idx][current_index] };
            end

            // Evicted data to victim cache, stored state because plru after proc wr changed so unmodified
            evicted_data[1]                                 = data[mem_wr_idx][current_index];
            evicted_idx[1]                                  = mem_wr_idx;
            evicted_tag[1]                                  = tags[mem_wr_idx][current_index];
            evicted_valid[1]                                = dirty[mem_wr_idx][current_index] && valids[mem_wr_idx][current_index];

            // if receive a stall, don't store the data, just pass it
            // Do replacement, dont set dirty
            data_next[mem_wr_idx][current_index]        = mem_wr_data;
            tags_next[mem_wr_idx][current_index]        = mem_wr_tag;
            valids_next[mem_wr_idx][current_index]      = `TRUE;
            dirty_next[mem_wr_idx][current_index]       = `FALSE;

            // Update plru
            for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                plru_sel                                    = (({NUM_LRU_LEVELS{1'b0}} | 1'b1) << (NUM_LRU_LEVELS-i-1)) |  (current_index >> (i+1));
                plru_next[mem_wr_idx][plru_sel]             = ~current_index[i];
            end
        end

        lsq_load_data_valid     = mem_wr_en;
        lsq_load_data           = mem_wr_data;
        lsq_load_prf            = mem_wr_prf;
        lsq_load_addr           = {mem_wr_tag, mem_wr_idx, 3'b0} | 64'd0;

        // Read and update Pseudo LRU
        for (int port=1; port>=0; --port) begin
            if (rd_en[port]) begin
                if (proc_wr_en && rd_idx[port] == proc_wr_idx && rd_tag[port] == proc_wr_tag) begin
                    rd_data[port]                               = proc_wr_data;
                    rd_valid[port]                              = `TRUE;
                end
                else if (mem_wr_en && rd_idx[port] == mem_wr_idx && rd_tag[port] == mem_wr_tag) begin
                    rd_data[port]                               = mem_wr_data;
                    rd_valid[port]                              = `TRUE;
                end
                else begin
                    for (int way_num=0; way_num<NUM_WAYS; ++way_num) begin
                        if (valids[rd_idx[port]][way_num] && tags[rd_idx[port]][way_num] == rd_tag[port]) begin
                            //Read data
                            rd_valid[port]                          = `TRUE;
                            rd_data[port]                           = data[rd_idx[port]][way_num];
                            //Update Pseudo LRU
                            for(int i=0; i<NUM_LRU_LEVELS; ++i) begin
                                plru_sel                            = (({NUM_LRU_LEVELS{1'b0}} | 1'b1) << (NUM_LRU_LEVELS-i-1)) |  (way_num >> (i+1));
                                plru_next[rd_idx[port]][plru_sel]   = ~way_num[i];
                            end
                        end
                    end
                end
            end
        end

        // Flush Dcache when halt
        if (halt_prev) begin
            for (int i = 0; i < 2; ++i) begin
                if (dirty_grant[i]) begin
                    for (int j = 0; j < NUM_WAYS*NUM_SETS; ++j) begin
                        if (dirty_grant[i][j]) begin
                            evicted_data[i]                                 = data_next[j/NUM_WAYS][j%NUM_WAYS];
                            evicted_idx[i]                                  = j/NUM_WAYS;
                            evicted_tag[i]                                  = tags_next[j/NUM_WAYS][j%NUM_WAYS];
                            evicted_valid[i]                                = `TRUE;
                        end
                        if (dirty_grant[i][j] && !incoming_stall[i]) begin
                            dirty_next[j/NUM_WAYS][j%NUM_WAYS]              = `FALSE;
                        end
                    end
                end
            end
        end
        for (int i = 0; i < NUM_SETS; ++i) begin
            for (int j = 0; j < NUM_WAYS; ++j) begin
                dirty_req_next[i*NUM_WAYS+j]  = dirty_next[i][j] && valids_next[i][j];
            end
        end
        if (nuke) begin
            lsq_load_data_valid     = 0;
            lsq_load_data           = 0;
            lsq_load_addr           = 0;
            lsq_load_prf            = `ZERO_REG_PRF;
        end
    end

    ps_select_n #(.SEL(2), .REQ_BITS(NUM_WAYS*NUM_SETS)) mshr_select(
        .req({2{dirty_req}}),
        .en(2'b11),
        .grant(dirty_grant)
    );


    always_ff @(posedge clock) begin
        if (reset) begin
            data                    <= `SD 0;
            tags                    <= `SD 0;
            valids                  <= `SD 0;
            dirty                   <= `SD 0;
            plru                    <= `SD 0;
            halt_prev               <= `SD 0;
            dirty_req               <= `SD 0;
        end
        else begin
            data                    <= `SD data_next;
            tags                    <= `SD tags_next;
            valids                  <= `SD valids_next;
            dirty                   <= `SD dirty_next;
            plru                    <= `SD plru_next;
            halt_prev               <= `SD halt ? halt : halt_prev;
            dirty_req               <= `SD dirty_req_next;
        end
    end


endmodule
`default_nettype wire

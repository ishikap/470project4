//For: Testing dcache module
//Date: 3/20/18

//TODO:
`timescale 1ns/100ps
`define NOT_CHECK   2'b10
`define TRUE_2      2'b01
`define FALSE_2     2'b00
`define WAY_0       1'b0
`define WAY_1       1'b1

parameter BLOCK_SIZE        = 8; //BYTES, not bits...
parameter NUM_WAYS          = 2;
localparam CACHE_SIZE       = 256;
localparam QUADWORD         = 64;
localparam BLOCK_OFFSET     = $clog2(BLOCK_SIZE);
localparam NUM_SETS         = CACHE_SIZE/(BLOCK_SIZE * NUM_WAYS);
localparam SET_INDEX_BITS   = $clog2(NUM_SETS);
localparam MEM_SIZE         = `MEM_SIZE_IN_BYTES;
localparam MEM_ADDR_BITS    = $clog2(MEM_SIZE); //Total mem address space is 16 bits, -3 for BO is 13
localparam TAG_BITS         = MEM_ADDR_BITS - SET_INDEX_BITS - BLOCK_OFFSET; //16-4-3 = 9 bits for tag
localparam NUM_LRU_LEVELS   = $clog2(NUM_WAYS);

module testbench;
    // INPUTS
    logic                                clock;
    logic                                reset;
    logic                                wr_en;
    logic    [SET_INDEX_BITS-1:0]        wr_idx;
    logic    [TAG_BITS-1:0]              wr_tag;
    logic    [QUADWORD-1:0]              wr_data;
    logic    [1:0]                       rd_en;
    logic    [1:0][SET_INDEX_BITS-1:0]   rd_idx;
    logic    [1:0][TAG_BITS-1:0]         rd_tag;
    // OUTPUTS
    logic   [1:0][QUADWORD-1:0]          rd_data;
    logic   [1:0]                        rd_valid;

    icache #( .BLOCK_SIZE(BLOCK_SIZE), .NUM_WAYS(NUM_WAYS)) icache1(
        .clock(clock),
        .reset(reset),
        .wr_en(wr_en),
        .wr_idx(wr_idx),
        .wr_tag(wr_tag),
        .wr_data(wr_data),
        .rd_en(rd_en),
        .rd_idx(rd_idx),
        .rd_tag(rd_tag),
        .rd_data(rd_data),
        .rd_valid(rd_valid)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current icache enteries
        $display("===I-CACHE===");
        $display("found_flag:      %d", icache1.found_flag);
        for (int i=0; i < NUM_SETS; i++) begin
            $display("\n\nSet Number: %5d ", i);
                for (int j=0; j < NUM_WAYS; j++) begin
                $display ("\tWay Number: %5d ", j,
                          "Tag:   %5d ", icache1.tags[i][j],
                          "Valid: %b ", icache1.valids[i][j],
                          "Data:  %5d ", icache1.data[i][j]);
                end
        end

        //display all the current PLRU enteries
        $display("\n\n===PLRU===");
        $display("plru_sel:      %d", icache1.plru_sel);
        $display("current_index: %d", icache1.plru_sel);
        for (int i=0; i < NUM_SETS; i++) begin
            $display("\n\nSet Number: %5d ", i);
            for (int j=1; j < NUM_WAYS ; j++) begin
                $display ("PLRU index: %5d ", j,
                          "PLRU Entry: %d ", icache1.plru[i][j]);
            end
        end
        $fatal("INCORRECT");
    endtask

    task write;
        input                       wr_en_in;
        input [SET_INDEX_BITS-1:0]  wr_idx_in;
        input [TAG_BITS-1:0]        wr_tag_in;
        input [QUADWORD-1:0]        wr_data_in;

        wr_en      = wr_en_in;
        wr_idx     = wr_idx_in;
        wr_tag     = wr_tag_in;
        wr_data    = wr_data_in;
    endtask

    task read;
        input                       way;
        input                       rd_en_in;
        input [SET_INDEX_BITS-1:0]  rd_idx_in;
        input [TAG_BITS-1:0]        rd_tag_in;

        rd_en[way]      = rd_en_in;
        rd_idx[way]     = rd_idx_in;
        rd_tag[way]     = rd_tag_in;
    endtask

    task check_outputs;
        input                   way;
        input                   rd_valid_in;
        input [QUADWORD-1:0]    rd_data_in;

        if (rd_data[way]    != rd_data_in && rd_valid_in)   exit_on_error();
        if (rd_valid[way]   != rd_valid_in)                 exit_on_error();
    endtask

    task check_entry;
        input [NUM_SETS-1:0]        set;
        input [NUM_WAYS-1:0]        way;
        input [TAG_BITS-1:0]        tag;
        input                       valid;
        input [QUADWORD-1:0]        data;


        if (icache1.tags[set][way]      != tag     && valid)    exit_on_error();
        if (icache1.valids[set][way]    != valid)               exit_on_error();
        if (icache1.data[set][way]      != data     && valid)   exit_on_error();
    endtask

    task check_plrt_entry;
        input [NUM_SETS-1:0]        set;
        input [NUM_WAYS-1:0]        way;
        input [TAG_BITS-1:0]        plru;

        if (icache1.plru[set][way] != plru) exit_on_error();
    endtask


    initial
    begin
        $monitor("===WRITE INPUTS===\nTime= %5d wr_en= %b wr_idx= %d wr_tag = %d wr_data = %d\n",  $time, wr_en, wr_idx, wr_tag, wr_data,
                 "===WAY_0 INPUTS===\nTime= %5d rd_en = %b rd_idx = %d rd_tag = %d \n",  $time, rd_en[0], rd_idx[0], rd_tag[0],
                 "===WAY_0 OUTPUTS===\nTime= %5d rd_data = %d rd_valid = %b\n", $time, rd_data[0], rd_valid[0],
                 "===WAY_1 INPUTS===\nTime= %5d rd_en = %b rd_idx = %d rd_tag = %d \n",  $time, rd_en[1], rd_idx[1], rd_tag[1],
                 "===WAY_1 OUTPUTS===\nTime= %5d rd_data = %d rd_valid = %b\n", $time, rd_data[1], rd_valid[1]);

        clock   = 0;
        reset   = 1;
        wr_en   = `FALSE;
        wr_idx  = 4'd00;
        wr_tag  = 9'd00;
        wr_data = 64'd00;
        rd_en   = `FALSE;
        rd_idx  = 4'd00;
        rd_tag  = 9'd00;

        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `FALSE, 64'd00);
        check_outputs(`WAY_0, `FALSE, 64'd00);
        @(negedge clock);
        //CHECK reset
        reset = 0;
        for (int i = 0; i < NUM_SETS; ++i)
        begin
            for (int j = 0; j < NUM_WAYS; ++j)
            begin
                check_entry(i, j, 9'd00, `FALSE, 64'd00);
            end
        end
        $display("\n@@@Passed reset\n");
        //exit_on_error();

        //Write is not enabled
        write(`FALSE, 4'd01, 9'd12, 64'd22);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `FALSE, 64'd00);
        check_outputs(`WAY_0, `FALSE, 64'd00);
        @(negedge clock);
        //Ensure all entries are invalid
        for (int i = 0; i < NUM_SETS; ++i)
        begin
            for (int j = 0; j < NUM_WAYS; ++j)
            begin
                check_entry(i, j, 9'd00, `FALSE, 64'd00);
            end
        end
        $display("\n@@@Passed write not enabled\n");
        //exit_on_error();

        //Write a value to the cache, and read invalid
        write(`TRUE, 4'd00, 9'd00, 64'd01);
        read(`WAY_1, `TRUE, 4'd01, 9'd15);
        read(`WAY_0, `TRUE, 4'd02, 9'd22);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `FALSE, 64'd00);
        check_outputs(`WAY_0, `FALSE, 64'd00);
        @(negedge clock);
        check_entry(4'd00, 1'b0, 9'd00, `TRUE, 64'd01);
        $display("\n@@@Passed basic write, invalid read\n");
        //exit_on_error();

        //Write a value to the cache, and read WAY_1
        write(`TRUE, 4'd00, 9'd02, 64'd55);
        read(`WAY_1, `TRUE, 4'd00, 9'd00);
        read(`WAY_0, `TRUE, 4'd02, 9'd22);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd01);
        check_outputs(`WAY_0, `FALSE, 64'd00);
        @(negedge clock);
        check_entry(4'd00, 1'b1, 9'd02, `TRUE, 64'd55);
        $display("\n@@@Passed basic write, and read value WAY_1\n");
        //exit_on_error();

        //Write a value to the cache, and read WAY_0
        write(`TRUE, 4'd01, 9'd14, 64'd25);
        read(`WAY_1, `TRUE, 4'd12, 9'd14);
        read(`WAY_0, `TRUE, 4'd00, 9'd02);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `FALSE, 64'd01);
        check_outputs(`WAY_0, `TRUE, 64'd55);
        @(negedge clock);
        check_entry(4'd01, 1'b0, 9'd14, `TRUE, 64'd25);
        $display("\n@@@Passed basic write, and read value WAY_0\n");
        //exit_on_error();

        //Write a value to the cache, and read both ways
        write(`TRUE, 4'd01, 9'd13, 64'd08);
        read(`WAY_1, `TRUE, 4'd00, 9'd00);
        read(`WAY_0, `TRUE, 4'd01, 9'd14);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd01);
        check_outputs(`WAY_0, `TRUE, 64'd25);
        @(negedge clock);
        check_entry(4'd01, 1'b1, 9'd13, `TRUE, 64'd08);
        $display("\n@@@Passed basic write, and read both ways\n");
        //exit_on_error();

        //Write a value to the cache, and forward read WAY_1
        write(`TRUE, 4'd02, 9'd05, 64'd04);
        read(`WAY_1, `TRUE, 4'd02, 9'd05);
        read(`WAY_0, `TRUE, 4'd05, 9'd14);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd04);
        check_outputs(`WAY_0, `FALSE, 64'd25);
        @(negedge clock);
        check_entry(4'd02, 1'b0, 9'd05, `TRUE, 64'd04);
        $display("\n@@@Passed basic write, and forward read WAY_1\n");
        //exit_on_error();

        //Write a value to the cache, and forward read WAY_0
        write(`TRUE, 4'd02, 9'd07, 64'd15);
        read(`WAY_1, `TRUE, 4'd06, 9'd05);
        read(`WAY_0, `TRUE, 4'd02, 9'd07);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `FALSE, 64'd04);
        check_outputs(`WAY_0, `TRUE, 64'd15);
        @(negedge clock);
        check_entry(4'd02, 1'b1, 9'd07, `TRUE, 64'd15);
        $display("\n@@@Passed basic write, and forward read WAY_0\n");
        //exit_on_error();

        //Write a value to the cache, and forward both ways
        write(`TRUE, 4'd03, 9'd13, 64'd36);
        read(`WAY_1, `TRUE, 4'd03, 9'd13);
        read(`WAY_0, `TRUE, 4'd03, 9'd13);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd36);
        check_outputs(`WAY_0, `TRUE, 64'd36);
        @(negedge clock);
        check_entry(4'd03, 1'b0, 9'd13, `TRUE, 64'd36);
        $display("\n@@@Passed basic write, and forward both ways\n");
        //exit_on_error();

        //Write a value to the cache, and forward WAY_1, regular WAY_0
        write(`TRUE, 4'd03, 9'd14, 64'd63);
        read(`WAY_1, `TRUE, 4'd03, 9'd14);
        read(`WAY_0, `TRUE, 4'd02, 9'd07);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd63);
        check_outputs(`WAY_0, `TRUE, 64'd15);
        @(negedge clock);
        check_entry(4'd03, 1'b1, 9'd14, `TRUE, 64'd63);
        $display("\n@@@Passed basic write, and forward WAY_1, regular WAY_0\n");
        //exit_on_error();

        //Write a value to the cache, and forward WAY_0, regular WAY_1
        write(`TRUE, 4'd04, 9'd07, 64'd26);
        read(`WAY_1, `TRUE, 4'd03, 9'd13);
        read(`WAY_0, `TRUE, 4'd04, 9'd07);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd36);
        check_outputs(`WAY_0, `TRUE, 64'd26);
        @(negedge clock);
        check_entry(4'd04, 1'b0, 9'd07, `TRUE, 64'd26);
        $display("\n@@@Passed basic write, and forward WAY_0, regular WAY_1\n");
        //exit_on_error();

        //DO SOME EVICTION SHIT, LIKE PLRU STUFF....
        //Write a value to overwrite cache, and forward and invalid
        write(`TRUE, 4'd00, 9'd08, 64'd34);
        read(`WAY_1, `TRUE, 4'd00, 9'd02);
        read(`WAY_0, `TRUE, 4'd00, 9'd08);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `FALSE, 64'd55);
        check_outputs(`WAY_0, `TRUE, 64'd34);
        @(negedge clock);
        check_entry(4'd00, 1'b1, 9'd08, `TRUE, 64'd34); //PLRU got it write
        $display("\n@@@Passed Overwrite value in cache, and forward and invalid\n");
        //exit_on_error();

        //Write a value to overwrite cache, and forward and invalid
        write(`TRUE, 4'd00, 9'd10, 64'd86);
        read(`WAY_1, `TRUE, 4'd00, 9'd10);
        read(`WAY_0, `TRUE, 4'd00, 9'd00);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd86);
        check_outputs(`WAY_0, `FALSE, 64'd01);
        @(negedge clock);
        check_entry(4'd00, 1'b0, 9'd10, `TRUE, 64'd86); //PLRU got it write
        $display("\n@@@Passed Overwrite value in cache, and forward and invalid\n");
        //exit_on_error();

        //Reads are invalid, no write
        write(`FALSE, 4'd00, 9'd10, 64'd47);
        read(`WAY_1, `TRUE, 4'd00, 9'd10);
        read(`WAY_0, `TRUE, 4'd00, 9'd08);
        @(posedge clock);
        //Check outputs
        check_outputs(`WAY_1, `TRUE, 64'd86);
        check_outputs(`WAY_0, `TRUE, 64'd34);
        @(negedge clock);
        check_entry(4'd00, 1'b0, 9'd10, `TRUE, 64'd86);
        check_entry(4'd00, 1'b1, 9'd08, `TRUE, 64'd34);
        $display("\n@@@Passed Reads are valid, no write\n");
        //exit_on_error();

        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

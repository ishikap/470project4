`default_nettype none
`timescale 1ns/100ps
module dcache_controller #(parameter NUM_WAYS = 2,
                           parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES),
                           localparam BLOCK_SIZE = 8, localparam CACHE_SIZE= 256, localparam BLOCK_OFFSET = $clog2(BLOCK_SIZE),
                           localparam NUM_SETS = CACHE_SIZE / (BLOCK_SIZE * NUM_WAYS), localparam SET_INDEX_BITS = $clog2(NUM_SETS),
                           localparam MEM_ADDR_BITS = 13, localparam TAG_BITS = MEM_ADDR_BITS - SET_INDEX_BITS,
                           localparam QUADWORD = 64, localparam NUM_LRU_LEVELS = $clog2(NUM_WAYS))(
    // INPUTS
    input   wire                                clock,
    input   wire                                reset,

    input   wire    [3:0]                       mshr2Dcache_stall,

    input   wire    [1:0][QUADWORD-1:0]         lsq2Dcache_addr,            // loads from lsq that either go to mem or cache
    input   wire    [1:0]                       lsq2Dcache_addr_valid,
    input   wire    [1:0][PRF_BITS-1:0]         lsq2Dcache_prf,

    input   wire    [1:0][TAG_BITS-1:0]         evicted2Dcache_tag,         // evicted from dcache go to mem as a store
    input   wire    [1:0][SET_INDEX_BITS-1:0]   evicted2Dcache_idx,
    input   wire    [1:0][QUADWORD-1:0]         evicted2Dcache_data,
    input   wire    [1:0]                       evicted2Dcache_addr_valid,

    input   wire    [1:0][QUADWORD-1:0]         cachemem_data,              // result from dcache
    input   wire    [1:0]                       cachemem_valid,
    input   wire    [1:0]                       nuke,

    // OUTPUTS
    output  logic   [3:0][1:0]                  dcache_cont2Mshr_command,
    output  logic   [3:0][QUADWORD-1:0]         dcache_cont2Mshr_addr,
    output  logic   [3:0][QUADWORD-1:0]         dcache_cont2Mshr_data,
    output  logic   [3:0][PRF_BITS-1:0]         dcache_cont2Mshr_prf,

    output  logic   [1:0][QUADWORD-1:0]         lsq_data_out,               // To lsq
    output  logic   [1:0][QUADWORD-1:0]         lsq_addr_out,
    output  logic   [1:0]                       lsq_valid_out,
    output  logic   [1:0][PRF_BITS-1:0]         lsq_prf_out,
    output  logic   [1:0]                       lsq_stall_out,

    output  logic   [1:0]                       dcache_en,
    output  logic   [1:0][SET_INDEX_BITS-1:0]   dcache_index,               // used for read to dcache
    output  logic   [1:0][TAG_BITS-1:0]         dcache_tag,

    output  logic   [1:0]                       dcache_stall
);

    // D-Cache Controller States
    logic   [3:0][TAG_BITS-1:0]         tag, tag_next;
    logic   [3:0][SET_INDEX_BITS-1:0]   set_index, set_index_next;
    logic   [3:0][1:0]                  mem_command, mem_command_next;
    logic   [3:0][PRF_BITS-1:0]         prf, prf_next;
    logic   [3:0]                       serviced, serviced_next;
    logic   [3:0][QUADWORD-1:0]         data, data_next;
    logic                               check_dcache;

    always_comb begin
        dcache_cont2Mshr_command    = 0;
        dcache_cont2Mshr_addr       = 0;
        dcache_cont2Mshr_data       = 0;
        for (int i = 0; i < 4; ++i) begin
            dcache_cont2Mshr_prf[i] = `ZERO_REG_PRF;
        end
        lsq_data_out                = 0;
        lsq_addr_out                = 0;
        lsq_valid_out               = 0;
        for (int i = 0; i < 2; ++i) begin
            lsq_prf_out[i]          = `ZERO_REG_PRF;
        end
        lsq_stall_out               = 0;
        dcache_en                   = 0;
        dcache_index                = 0;
        dcache_tag                  = 0;
        dcache_stall                = 0;
        check_dcache                = 0;

        tag_next                    = tag;
        set_index_next              = set_index;
        mem_command_next            = mem_command;
        prf_next                    = prf;
        serviced_next               = serviced;
        data_next                   = data;

        // Send last values for evicted to MSHR if needed
        for (int i = 0; i < 2; ++i) begin
            if (mshr2Dcache_stall[i+2] && !serviced_next[i+2]) begin
                serviced_next[i+2]              = `FALSE;
            end
            // Send it
            if (!mshr2Dcache_stall[i+2] && !serviced_next[i+2]) begin
                dcache_cont2Mshr_command[i+2]   = mem_command_next[i+2];
                dcache_cont2Mshr_prf[i+2]       = prf_next[i+2];
                dcache_cont2Mshr_addr[i+2]      = {tag_next[i+2], set_index_next[i+2], 3'b0} | 64'd0;
                dcache_cont2Mshr_data[i+2]      = data_next[i+2];
                serviced_next[i+2]              = `TRUE;
                tag_next[i+2]                   = 0;
                set_index_next[i+2]             = 0;
                data_next[i+2]                  = 0;
                prf_next[i+2]                   = `ZERO_REG_PRF;
                mem_command_next[i+2]           = `BUS_NONE;
            end
        end

        // Send last values for load to MSHR if needed
        for (int i = 0; i < 2; ++i) begin
            if (mshr2Dcache_stall[i] && !serviced_next[i]) begin
                serviced_next[i]                = `FALSE;
            end
            // Send it
            if (!mshr2Dcache_stall[i] && !serviced_next[i]) begin
                dcache_cont2Mshr_command[i]     = mem_command_next[i];
                dcache_cont2Mshr_prf[i]         = prf_next[i];
                dcache_cont2Mshr_addr[i]        = {tag_next[i], set_index_next[i], 3'b0} | 64'd0;
                serviced_next[i]                = `TRUE;
                tag_next[i]                     = 0;
                set_index_next[i]               = 0;
                data_next[i]                    = 0;
                prf_next[i]                     = `ZERO_REG_PRF;
                mem_command_next[i]             = `BUS_NONE;
            end
        end

        // Add to Controller queue evicted
        for (int i = 1; i >= 0; --i) begin
            if (!serviced_next[i+2]) begin
                dcache_stall[i]         = `TRUE;
            end
            else if (serviced_next[i+2] && evicted2Dcache_addr_valid[i]) begin
                // Put into queue to store into memory
                tag_next[i+2]           = evicted2Dcache_tag[i];
                set_index_next[i+2]     = evicted2Dcache_idx[i];
                data_next[i+2]          = evicted2Dcache_data[i];
                mem_command_next[i+2]   = `BUS_STORE;
                prf_next[i+2]           = `ZERO_REG_PRF;
                serviced_next[i+2]      = `FALSE;
            end
        end

        // Add to Controller queue loads
        for (int i = 1; i >= 0; --i) begin
            if (!serviced_next[i]) begin
                lsq_stall_out[i]                    = `TRUE;
            end
            else if (serviced_next[i] && lsq2Dcache_addr_valid[i]) begin
                // Put into queue to load from memory, or get from cache
                {tag_next[i], set_index_next[i]}    = lsq2Dcache_addr[i][QUADWORD-1:3];
                mem_command_next[i]                 = `BUS_LOAD;
                prf_next[i]                         = lsq2Dcache_prf[i];
                serviced_next[i]                    = `FALSE;
                check_dcache                        = `TRUE;
            end
            // D-Check cache
            dcache_en[i]                            = `TRUE;
            dcache_tag[i]                           = tag_next[i];
            dcache_index[i]                         = set_index_next[i];
        end

        // Check for HIT and send if valid
        for (int i = 0; i < 2; ++i) begin
            if (cachemem_valid[i] && check_dcache) begin
                serviced_next[i]        = `TRUE;
                mem_command_next[i]     = `BUS_NONE;
                prf_next[i]             = `ZERO_REG_PRF;

                lsq_data_out[i]         = cachemem_data[i];
                lsq_valid_out[i]        = `TRUE;
                lsq_addr_out[i]         = {tag_next[i], set_index_next[i], 3'b0} | 64'd0;
                lsq_prf_out[i]          = lsq2Dcache_prf[i];
            end
        end

        // Nuke
        if (nuke) begin
            for (int i = 1; i >= 0; --i) begin
                serviced_next[i]        = `TRUE;
                tag_next[i]             = 0;
                set_index_next[i]       = 0;
                data_next[i]            = 0;
                mem_command_next[i]     = `BUS_NONE;
                prf_next[i]             = `ZERO_REG_PRF;
            end
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            tag                 <= `SD -1;
            set_index           <= `SD -1;
            for (int i = 0; i < 4; ++i) begin
                prf[i]          <= `SD `ZERO_REG_PRF;
            end
            mem_command         <= `SD 0;
            serviced            <= `SD 0;
            data                <= `SD 0;
        end
        else begin
            tag                 <= `SD tag_next;
            set_index           <= `SD set_index_next;
            prf                 <= `SD prf_next;
            mem_command         <= `SD mem_command_next;
            serviced            <= `SD serviced_next;
            data                <= `SD data_next;
        end
    end
endmodule
`default_nettype wire

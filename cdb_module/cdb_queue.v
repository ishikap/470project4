`default_nettype none
`timescale 1ns/100ps
module cdb_queue #(parameter NUM_PRF_ENTRIES=64, parameter NUM_EX_UNITS=8, localparam QUADWORD=64, localparam NUM_EX_BITS=$clog2(NUM_EX_UNITS), localparam PRF_BITS=$clog2(NUM_PRF_ENTRIES)) (
    // INPUTS
    input   wire                                        clock,
    input   wire                                        reset,

    input   wire    [NUM_EX_UNITS-1:0]                  valid,
    input   wire    [NUM_EX_UNITS-1:0][PRF_BITS-1:0]    pr_tag,         // physical register tag
    input   wire    [NUM_EX_UNITS-1:0][QUADWORD-1:0]    results,        // results from the EX unit
    input   wire    [NUM_EX_UNITS-1:0][QUADWORD-1:0]    next_pc,        // Next PC
    input   wire    [NUM_EX_UNITS-1:0]                  branch_taken,   // whether or not the branch was taken
    // OUTPUTS
    output  logic                [1:0]                  valid_out,
    output  logic                [1:0][PRF_BITS-1:0]    pr_tag_out,
    output  logic                [1:0][QUADWORD-1:0]    result_out,
    output  logic                [1:0][QUADWORD-1:0]    next_pc_out,
    output  logic                [1:0]                  branch_taken_out,
    output  logic   [NUM_EX_UNITS-1:0]                  cdb_stall       // CDB is full and we
                                                                        // cannot accept all EX units inputs
);

    logic   [1:0][NUM_EX_BITS-1:0]  grants_enc;
    logic   [1:0]                   valids_enc;

    // Priority Encoder
    pe_n #(.REQ_BITS(NUM_EX_UNITS + 1), .SEL(2)) cdb_select(
        .req({1'b0, valid}),
        .enc(grants_enc),
        .valid(valids_enc)
    );

    always_comb begin

        // Default the outputs
        valid_out           = 0;
        pr_tag_out          = 0;
        result_out          = 0;
        next_pc_out         = 0;
        branch_taken_out    = 0;
        cdb_stall           = -1;

        // CDB Stall
        for (int i=0; i<NUM_EX_UNITS; ++i) begin
            if ((i == grants_enc[1] && valids_enc[1]) || (i == grants_enc[0] && valids_enc[0])) begin
                cdb_stall[i] = `FALSE;
            end
        end

        // Air two on CDB
        for (int i=0; i<2; ++i) begin
            if (valids_enc[i]) begin
                valid_out[i]        = valids_enc[i];
                pr_tag_out[i]       = pr_tag[grants_enc[i]];
                result_out[i]       = results[grants_enc[i]];
                next_pc_out[i]      = next_pc[grants_enc[i]];
                branch_taken_out[i] = branch_taken[grants_enc[i]];
            end
        end
    end
endmodule
`default_nettype wire

//For: Testing CDB
//By: T-Dawg
//Date some time (I forget)

`timescale 1ns/100ps
parameter   NUM_PRF_ENTRIES = 64;
parameter   CDB_QUEUE_SIZE  = 8;
parameter   NUM_EX_UNITS    = 6;
localparam  PRF_BITS        = $clog2(NUM_PRF_ENTRIES);
localparam  CDB_BITS        = $clog2(CDB_QUEUE_SIZE);
localparam  QUADWORD        = 64;
localparam  EX_BITS         = $clog2(NUM_EX_UNITS);

module testbench;

    logic                                       clock;
    logic                                       reset;

    logic   [NUM_EX_UNITS-1:0]                  valid;
    logic   [NUM_EX_UNITS-1:0][PRF_BITS-1:0]    pr_tag;
    logic   [NUM_EX_UNITS-1:0][QUADWORD-1:0]    results;
    logic   [NUM_EX_UNITS-1:0]                  branch_taken;

    logic                [1:0]                  valid_out;
    logic                [1:0][PRF_BITS-1:0]    pr_tag_out;
    logic                [1:0][QUADWORD-1:0]    result_out;
    logic                [1:0]                  branch_taken_out;

    logic   [NUM_EX_UNITS-1:0]                  cdb_stall;



    cdb_queue #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES),
                .CDB_QUEUE_SIZE(CDB_QUEUE_SIZE),
                .NUM_EX_UNITS(NUM_EX_UNITS)) cdb(

            .clock(clock),
            .reset(reset),

            .valid(valid),
            .pr_tag(pr_tag),
            .results(results),
            .branch_taken(branch_taken),

            .valid_out(valid_out),
            .pr_tag_out(pr_tag_out),
            .result_out(result_out),
            .branch_taken_out(branch_taken_out),

            .cdb_stall(cdb_stall)
        );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
	    $display("!!!FATAL ERROR!!!");
        $fatal("INCORRECT");
    endtask

    task dispatch;
        input  [EX_BITS-1:0]    ex_unit;
        input [PRF_BITS-1:0]    prf_reg;
        input [QUADWORD-1:0]    res;
        input                   b_taken;

        valid[ex_unit]          = 1'b1;
        pr_tag[ex_unit]         = prf_reg;
        results[ex_unit]        = res;
        branch_taken[ex_unit]   = b_taken;
    endtask


	initial
	begin
        $monitor("===EX 0===\ntime= %d valid= %b pr_tag= %d res= %4d b_taken= %b",
                 $time, valid[0], pr_tag[0], results[0], branch_taken[0]);

        clock = 0;
        reset = 1;
        valid = 0; // No inputs
        @(negedge clock);
        // Check the reset
        if (valid_out != 0) exit_on_error();
        reset = 0;
        @(negedge clock);
        // Check that nothing aired on CDB when empty
        if (valid_out != 0) exit_on_error();

        // Add one ex to CDB, check that only one is valid
        dispatch(0, 1, 2, 0);
        @(negedge clock);
        if (valid_out != 2'b01) exit_on_error();
        if (pr_tag_out[0] != 1) exit_on_error();
        if (result_out[0] != 2) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();


        valid = 0;
        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();

        // Try sending two now, next cycle it should empty out again
        dispatch(0, 1, 2, 0);
        dispatch(1, 3, 4, 1);

        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 1) exit_on_error();
        if (result_out[0] != 2) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 3) exit_on_error();
        if (result_out[1] != 4) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        valid = 0;
        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();

        // Test airing two and then one
        dispatch(2, 1, 2, 0);
        dispatch(3, 3, 4, 1);
        dispatch(4, 5, 6, 0);

        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 1) exit_on_error();
        if (result_out[0] != 2) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 3) exit_on_error();
        if (result_out[1] != 4) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        valid = 0;


        @(negedge clock);
        if (valid_out != 2'b01) exit_on_error();
        if (pr_tag_out[0] != 5) exit_on_error();
        if (result_out[0] != 6) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();

        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();


        // Finish 6 in one cycle, should go out in 3 pairs of twos
        dispatch(0, 1, 2, 0);
        dispatch(1, 3, 4, 1);
        dispatch(2, 5, 6, 0);
        dispatch(3, 7, 8, 1);
        dispatch(4, 9, 10, 0);
        dispatch(5, 11, 12, 1);
        if (cdb_stall != 0) exit_on_error();

        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 1) exit_on_error();
        if (result_out[0] != 2) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 3) exit_on_error();
        if (result_out[1] != 4) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        valid = 0;
        #1

        if (cdb_stall != 0) exit_on_error();
        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 5) exit_on_error();
        if (result_out[0] != 6) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 7) exit_on_error();
        if (result_out[1] != 8) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 9) exit_on_error();
        if (result_out[0] != 10) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 11) exit_on_error();
        if (result_out[1] != 12) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();


        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        // Try to fill up queue and make a stall
        dispatch(0, 1, 2, 0);
        dispatch(1, 3, 4, 1);
        dispatch(2, 5, 6, 0);
        dispatch(3, 7, 8, 1);
        dispatch(4, 9, 10, 0);
        dispatch(5, 11, 12, 1);

        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 1) exit_on_error();
        if (result_out[0] != 2) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 3) exit_on_error();
        if (result_out[1] != 4) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 6'b110000) exit_on_error();    // Only let the two lowest in to fill the last
                                                        // Next two are allowed to fill for forwarding
        @(negedge clock);
        valid = 0;
        #1
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 5) exit_on_error();
        if (result_out[0] != 6) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 7) exit_on_error();
        if (result_out[1] != 8) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();

        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 9) exit_on_error();
        if (result_out[0] != 10) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 11) exit_on_error();
        if (result_out[1] != 12) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();

        // Check the few that were able to be put into the queue
        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 1) exit_on_error();
        if (result_out[0] != 2) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 3) exit_on_error();
        if (result_out[1] != 4) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();

        @(negedge clock);
        if (valid_out != 2'b11) exit_on_error();
        if (pr_tag_out[0] != 5) exit_on_error();
        if (result_out[0] != 6) exit_on_error();
        if (branch_taken_out[0] != 0) exit_on_error();
        if (pr_tag_out[1] != 7) exit_on_error();
        if (result_out[1] != 8) exit_on_error();
        if (branch_taken_out[1] != 1) exit_on_error();
        if (cdb_stall != 0) exit_on_error();

        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();
        @(negedge clock);
        // Just aired everything in queue, should be empty
        if (valid_out != 0) exit_on_error();
        if (cdb_stall != 0) exit_on_error();


		$display("\n@@@Passed ALLLLLL\n");
		$finish;
	end
endmodule

// 3/22/2018 - clock period <= 7.5ns
`timescale 1ns/100ps
`default_nettype none
module lsq #(parameter LDB_SIZE = 8, parameter STQ_SIZE = 8, parameter NUM_PRF_ENTRIES = 96, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam STQ_BITS = $clog2(STQ_SIZE), localparam QUADWORD = 64, localparam ADDR_BITS = 13) (
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,
    // inputs from dispatch
    input   wire             [1:0]                  id_disp_valid_inst,
    input   wire             [1:0]                  id_disp_wr_mem,
    input   wire             [1:0]                  id_disp_rd_mem,
    input   wire             [1:0]                  id_disp_stc_mem,        // Store-conditional
    input   wire             [1:0]                  id_disp_ldl_mem,        // Load-lock
    input   wire             [1:0][PRF_BITS-1:0]    id_disp_rat_dest,       // Where the store is going/way to identify a memory operation
    input   wire             [1:0]                  id_disp_rat_dest_valid,
    input   wire             [1:0][QUADWORD-1:0]    store_data,             // For store queue, is the register data ready in the PRF
    input   wire             [1:0]                  store_data_valid,
    // inputs from dispatch to ldb
    // from the EX-WB pipeline register
    input   wire             [1:0]                  ex_calc_address_valid,
    input   wire             [1:0][PRF_BITS-1:0]    ex_calc_address_prf,
    input   wire             [1:0][QUADWORD-1:0]    ex_calc_address,
    // from CDB to stq
    input   wire             [1:0]                  cdb_valid,
    input   wire             [1:0][PRF_BITS-1:0]    cdb_prf,
    input   wire             [1:0][QUADWORD-1:0]    cdb_data,
    // from CDB to ldb
    input   wire    [LDB_SIZE-1:0]                  cdb_stall,
    // from the Dcache to stq
    input   wire                                    stq_dcache_stall,
    input   wire             [1:0]                  ldb_dcache_stall,
    input   wire             [2:0]                  ldb_dcache_load_data_valid,
    input   wire             [2:0][QUADWORD-1:0]    ldb_dcache_load_data,
    input   wire             [2:0][QUADWORD-1:0]    ldb_dcache_load_addr,
    input   wire             [2:0][PRF_BITS-1:0]    ldb_dcache_load_prf,
    // from ROB on commit
    input   wire                                    store_requested,   // ROB wants to commit a store
    input   wire                                    store_executed,
    input   wire             [1:0]                  nuke,


    // OUTPUTS
    // to ID-DISP pipeline
    output  logic            [1:0]                  stq_stall,
    output  logic            [1:0]                  ldb_stall,
    // to the CDB
    output  logic                                   stored_valid,
    output  logic                 [PRF_BITS-1:0]    stored_prf,
    output  logic                 [QUADWORD-1:0]    stored_result,
    output  logic   [LDB_SIZE-1:0]                  load_data_valid,
    output  logic   [LDB_SIZE-1:0][PRF_BITS-1:0]    load_data_prf_tag,
    output  logic   [LDB_SIZE-1:0][QUADWORD-1:0]    load_data,
    // to d-cache
    output  logic                                   stq_data_to_dcache_valid,
    output  logic                                   stq_write_to_dcache,
    output  logic                 [QUADWORD-1:0]    stq_address_to_dcache,
    output  logic                 [QUADWORD-1:0]    stq_data_to_dcache,
    output  logic            [1:0]                  ldb_addr_to_dcache_valid,
    output  logic            [1:0]                  ldb_read_from_dcache,
    output  logic            [1:0][QUADWORD-1:0]    ldb_address_to_dcache,
    output  logic            [1:0][PRF_BITS-1:0]    ldb_prf_to_dcache

);
    // from the Store Queue to ldb
    wire             [1:0][STQ_SIZE-1:0]    stq_valid_on_disp;
    wire    [STQ_SIZE-1:0][ADDR_BITS-1:0]   stq_address;
    wire    [STQ_SIZE-1:0]                  stq_address_valid;
    wire    [STQ_SIZE-1:0][QUADWORD-1:0]    stq_data;
    wire    [STQ_SIZE-1:0]                  stq_data_valid;
    wire    [STQ_SIZE-1:0]                  stq_committing_inst;
    wire    [STQ_BITS-1:0]                  stq_head;
    wire    [STQ_BITS-1:0]                  stq_tail;

    wire    [1:0]                           ldb_en, stq_en;

    assign  ldb_en = stq_stall[1] & id_disp_wr_mem[1] ? 0 : 2'b11;
    assign  stq_en = ldb_stall[1] & id_disp_rd_mem[1] ? 0 : 2'b11;

    load_buffer #(.LDB_SIZE(LDB_SIZE), .STQ_SIZE(STQ_SIZE), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) ldb(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .id_disp_rd_mem(id_disp_rd_mem),
        .id_disp_ldl_mem(id_disp_ldl_mem),
        .id_disp_valid_inst(id_disp_valid_inst & ldb_en),
        .id_disp_rat_dest(id_disp_rat_dest),
        .id_disp_rat_dest_valid(id_disp_rat_dest_valid),
        .ex_calc_address_valid(ex_calc_address_valid),
        .ex_calc_address_prf(ex_calc_address_prf),
        .ex_calc_address(ex_calc_address),
        .dcache_stall(ldb_dcache_stall),
        .dcache_load_data_valid(ldb_dcache_load_data_valid),
        .dcache_load_data(ldb_dcache_load_data),
        .dcache_load_addr(ldb_dcache_load_addr),
        .dcache_load_prf(ldb_dcache_load_prf),
        .nuke(nuke),
        .stq_valid_on_disp(stq_valid_on_disp),
        .stq_address(stq_address),
        .stq_address_valid(stq_address_valid),
        .stq_data(stq_data),
        .stq_data_valid(stq_data_valid),
        .stq_committing_inst(stq_committing_inst),
        .stq_head(stq_head),
        .stq_tail(stq_tail),
        .cdb_stall(cdb_stall),
        // OUTPUTS
        .ldb_stall(ldb_stall),
        .load_data_valid(load_data_valid),
        .load_data_prf_tag(load_data_prf_tag),
        .load_data(load_data),
        .addr_to_dcache_valid(ldb_addr_to_dcache_valid),
        .read_from_dcache(ldb_read_from_dcache),
        .address_to_dcache(ldb_address_to_dcache),
        .prf_to_dcache(ldb_prf_to_dcache)
    );

    store_queue #(.STQ_SIZE(STQ_SIZE), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) store_queue1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .id_disp_valid_inst(id_disp_valid_inst & stq_en),
        .id_disp_wr_mem(id_disp_wr_mem),
        .id_disp_stc_mem(id_disp_stc_mem),
        .id_disp_rat_dest(id_disp_rat_dest),
        .id_disp_rat_dest_valid(id_disp_rat_dest_valid),
        .store_data(store_data),
        .store_data_valid(store_data_valid),
        .ex_calc_address_valid(ex_calc_address_valid),
        .ex_calc_address_prf(ex_calc_address_prf),
        .ex_calc_address(ex_calc_address),
        .cdb_valid(cdb_valid),
        .cdb_prf(cdb_prf),
        .cdb_data(cdb_data),
        .dcache_stall(stq_dcache_stall),
        .store_requested(store_requested),
        .store_executed(store_executed),
        .nuke(nuke),
        // OUTPUTS
        .stq_stall(stq_stall),
        .stored_valid(stored_valid),
        .stored_prf(stored_prf),
        .stored_result(stored_result),
        .stq_valid_on_disp(stq_valid_on_disp),
        .stq_address(stq_address),
        .stq_address_valid(stq_address_valid),
        .stq_data(stq_data),
        .stq_data_valid(stq_data_valid),
        .stq_committing_inst(stq_committing_inst),
        .stq_head(stq_head),
        .stq_tail(stq_tail),
        .data_to_dcache_valid(stq_data_to_dcache_valid),
        .write_to_dcache(stq_write_to_dcache),
        .address_to_dcache(stq_address_to_dcache),
        .data_to_dcache(stq_data_to_dcache)
    );
endmodule
`default_nettype wire

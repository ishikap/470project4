`timescale 1ns/100ps
parameter LDB_SIZE = 8;
parameter STQ_SIZE = 8;
parameter NUM_PRF_ENTRIES = 96;
localparam LDB_BITS = $clog2(LDB_SIZE);
localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES);
localparam STQ_BITS = $clog2(STQ_SIZE);
localparam QUADWORD = 64;

module testbench;
    // INPUTS
    logic                                       clock;
    logic                                       reset;
    // inputs from dispatch
    logic             [1:0]                     id_rd_mem;
    logic             [1:0]                     id_ldl_mem;
    logic             [1:0]                     id_valid_inst;
    logic             [1:0][PRF_BITS-1:0]       id_rat_dest;
    logic             [1:0]                     id_rat_dest_valid;
    // from the EX-WB pipeline register
    logic             [1:0]                     ex_calc_address_valid;
    logic             [1:0][PRF_BITS-1:0]       ex_calc_address_prf;
    logic             [1:0][QUADWORD-1:0]       ex_calc_address;
    // from the Dcache
    logic             [1:0]                     dcache_stall;
    logic             [1:0]                     dcache_load_data_valid;
    logic             [1:0][QUADWORD-1:0]       dcache_load_data;
    logic             [1:0][QUADWORD-1:0]       dcache_load_addr;
    // from ROB on commit
    logic             [1:0]                     nuke;
    // from the Store Queue
    logic             [1:0][STQ_SIZE-1:0]       stq_valid_on_disp;
    logic    [STQ_SIZE-1:0][QUADWORD-1:0]       stq_address;
    logic    [STQ_SIZE-1:0]                     stq_address_valid;
    logic    [STQ_SIZE-1:0][QUADWORD-1:0]       stq_data;
    logic    [STQ_SIZE-1:0]                     stq_data_valid;
    logic    [STQ_SIZE-1:0]                     stq_committing_inst;
    logic    [STQ_BITS-1:0]                     stq_head;
    logic    [STQ_BITS-1:0]                     stq_tail;
    // from the CDB queue
    logic    [LDB_SIZE-1:0]                     cdb_stall;
    // OUTPUTS
    // to ID-DISP pipeline
    logic            [1:0]                      ldb_stall;
    // to the CDB queue
    logic   [LDB_SIZE-1:0]                      load_data_valid;
    logic   [LDB_SIZE-1:0][PRF_BITS-1:0]        load_data_prf_tag;
    logic   [LDB_SIZE-1:0][QUADWORD-1:0]        load_data;
    // to dcache
    logic            [1:0]                      addr_to_dcache_valid;
    logic            [1:0]                      read_from_dcache;
    logic            [1:0][QUADWORD-1:0]        address_to_dcach;
// 3/21/2018 - synthesized at 5ns, 8 entries

`default_nettype none
`timescale 1ns/100ps
module load_buffer #(parameter LDB_SIZE = 8, parameter STQ_SIZE = 8, parameter NUM_PRF_ENTRIES = 96, localparam LDB_BITS = $clog2(LDB_SIZE), localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam STQ_BITS = $clog2(STQ_SIZE), localparam QUADWORD = 64, localparam ADDR_BITS = 13) (
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,
    // inputs from dispatch
    input   wire             [1:0]                  id_disp_rd_mem,
    input   wire             [1:0]                  id_disp_ldl_mem,             // Load-lock
    input   wire             [1:0]                  id_disp_valid_inst,
    input   wire             [1:0][PRF_BITS-1:0]    id_disp_rat_dest,            // Where the store is going/way to identify a memory operation
    input   wire             [1:0]                  id_disp_rat_dest_valid,
    // from the EX-WB pipeline register
    input   wire             [1:0]                  ex_calc_address_valid,
    input   wire             [1:0][PRF_BITS-1:0]    ex_calc_address_prf,
    input   wire             [1:0][QUADWORD-1:0]    ex_calc_address,
    // from the Dcache
    input   wire             [1:0]                  dcache_stall,
    input   wire             [2:0]                  dcache_load_data_valid,
    input   wire             [2:0][QUADWORD-1:0]    dcache_load_data,
    input   wire             [2:0][QUADWORD-1:0]    dcache_load_addr,
    input   wire             [2:0][PRF_BITS-1:0]    dcache_load_prf,
    // from ROB on commit
    input   wire             [1:0]                  nuke,
    // from the Store Queue
    input   wire             [1:0][STQ_SIZE-1:0]    stq_valid_on_disp,
    input   wire    [STQ_SIZE-1:0][ADDR_BITS-1:0]   stq_address,
    input   wire    [STQ_SIZE-1:0]                  stq_address_valid,
    input   wire    [STQ_SIZE-1:0][QUADWORD-1:0]    stq_data,
    input   wire    [STQ_SIZE-1:0]                  stq_data_valid,
    input   wire    [STQ_SIZE-1:0]                  stq_committing_inst,
    input   wire    [STQ_BITS-1:0]                  stq_head,
    input   wire    [STQ_BITS-1:0]                  stq_tail,
    // from the CDB queue
    input   wire    [LDB_SIZE-1:0]                  cdb_stall,

    // OUTPUTS
    // to ID-DISP pipeline
    output  logic            [1:0]                  ldb_stall,
    // to the CDB queue
    output  logic   [LDB_SIZE-1:0]                  load_data_valid,
    output  logic   [LDB_SIZE-1:0][PRF_BITS-1:0]    load_data_prf_tag,
    output  logic   [LDB_SIZE-1:0][QUADWORD-1:0]    load_data,
    // to dcache
    output  logic            [1:0]                  addr_to_dcache_valid,
    output  logic            [1:0]                  read_from_dcache,
    output  logic            [1:0][QUADWORD-1:0]    address_to_dcache,
    output  logic            [1:0][PRF_BITS-1:0]    prf_to_dcache
);
    `define NOT_DEPENDENT           2'b00
    `define UNKNOWN_DATA_ADDRESS    2'b01
    `define TRUE_DEPENDENCY         2'b10

    logic   [LDB_SIZE-1:0]                  ldl_mem;
    logic   [LDB_SIZE-1:0][PRF_BITS-1:0]    dest_reg;
    logic   [LDB_SIZE-1:0][ADDR_BITS-1:0]   address;
    logic   [LDB_SIZE-1:0]                  address_valid;
    logic   [LDB_SIZE-1:0][STQ_SIZE-1:0]    valid_store_inst_on_disp;
    logic   [LDB_SIZE-1:0]                  valid_inst;
    logic   [LDB_SIZE-1:0]                  request_sent_to_dcache;

    logic   [LDB_SIZE-1:0]                  next_ldl_mem;
    logic   [LDB_SIZE-1:0][PRF_BITS-1:0]    next_dest_reg;
    logic   [LDB_SIZE-1:0][ADDR_BITS-1:0]   next_address;
    logic   [LDB_SIZE-1:0]                  next_address_valid;
    logic   [LDB_SIZE-1:0][STQ_SIZE-1:0]    next_valid_store_inst_on_disp;
    logic   [LDB_SIZE-1:0]                  next_valid_inst;
    logic   [LDB_SIZE-1:0]                  next_request_sent_to_dcache;

    /*
        When we have multiple entries in the store queue that have the same address as an entry
        in the load buffer, we need to prefer the latest store (the ones closest to the tail) over
        earlier ones. To handle this case when the store queue is wrapped around, we use this variable
        to indicate that the variable was already assigned by something with a higher precedence.
    */
    logic                                   assigned;
    logic   [LDB_SIZE-1:0][1:0]             state, next_state;
    logic   [LDB_SIZE-1:0]                  send_to_mem;
    logic            [1:0][LDB_SIZE-1:0]    grants;
    logic            [1:0][LDB_SIZE-1:0]    grants_on_dispatch;


    // Select ldb entries to be sent to dcache
    ps_select_n #(.SEL(2), .REQ_BITS(LDB_SIZE)) ps_sel1 (
        .req({send_to_mem, send_to_mem}),
        .en(2'b11),
        .grant(grants)
    );

    // Accept dispatching instructions
    // Select empty ldb entries for dispatching instr
    ps_select_n #(.REQ_BITS(LDB_SIZE), .SEL(2)) ps_sel2 (
        .req(~{valid_inst, valid_inst}),
        .en(2'b11),
        .grant({grants_on_dispatch[0], grants_on_dispatch[1]})
    );

    always_comb begin
        next_ldl_mem                    = ldl_mem;
        next_dest_reg                   = dest_reg;
        next_address                    = address;
        next_address_valid              = address_valid;
        next_valid_store_inst_on_disp   = valid_store_inst_on_disp;
        next_valid_inst                 = valid_inst;
        next_request_sent_to_dcache     = request_sent_to_dcache;
        next_state                      = state;

        // Default the outputs
        ldb_stall               = 0;
        load_data_valid         = 0;
        for (int i = 0; i < LDB_SIZE; ++i) begin
            load_data_prf_tag   = `ZERO_REG_PRF;
        end
        load_data               = 0;
        addr_to_dcache_valid    = 0;
        read_from_dcache        = 0;
        address_to_dcache       = 0;
        for (int i = 0; i < 2; ++i) begin
            prf_to_dcache[i]    = `ZERO_REG_PRF;
        end

        // To DCache
        // Choose load buffer entries with state NOT_DEPENDENT
        send_to_mem = 0;
        for (int ldb_index = 0; ldb_index < LDB_SIZE; ++ldb_index) begin
            if (next_valid_inst[ldb_index] && next_address_valid[ldb_index] && next_state[ldb_index] == `NOT_DEPENDENT && !next_request_sent_to_dcache[ldb_index]) begin
                send_to_mem[ldb_index] = 1'b1;
            end
        end

        // Muxing load requests to dcache
        for (int i = 0; i < 2; ++i) begin
            for (int ldb_index = 0; ldb_index < LDB_SIZE; ++ldb_index) begin
                if (grants[i][ldb_index]) begin
                    addr_to_dcache_valid[i]         = 1'b1;
                    read_from_dcache[i]             = 1'b1;
                    address_to_dcache[i]            = {48'd0, next_address[ldb_index], 3'b000};
                    prf_to_dcache[i]                = next_dest_reg[ldb_index];
                end
            end
        end

        for (int i = 0; i < 2; ++i) begin
            if (!dcache_stall[i]) begin
                next_request_sent_to_dcache |= grants[i];
            end
        end

        // Output the data from dcache onto cdb
        for (int i = 0; i < 3; ++i) begin
            if (dcache_load_data_valid[i]) begin
                for (int ldb_index = 0; ldb_index< LDB_SIZE; ++ldb_index) begin
                    if (next_valid_inst[ldb_index] && next_address_valid[ldb_index] && dcache_load_prf[i] == next_dest_reg[ldb_index]) begin
                        load_data_valid[ldb_index]      = 1'b1;
                        load_data_prf_tag[ldb_index]    = next_dest_reg[ldb_index];
                        load_data[ldb_index]            = dcache_load_data[i];
                    end
                end
            end
        end

        // Update all current loads with address from pipeline
        for (int cdb_way = 1; cdb_way >= 0; --cdb_way) begin
            for (int load_buffer_index = 0; load_buffer_index < LDB_SIZE; ++load_buffer_index) begin
                if (ex_calc_address_valid[cdb_way] &&
                    next_valid_inst[load_buffer_index] &&
                    !next_address_valid[load_buffer_index] &&
                    next_dest_reg[load_buffer_index] == ex_calc_address_prf[cdb_way]) begin

                    next_address[load_buffer_index]         = ex_calc_address[cdb_way][ADDR_BITS+2:3];
                    next_address_valid[load_buffer_index]   = 1'b1;
                end
            end
        end

        // Update based on state of the Store Queue
        for (int ldb_index = 0; ldb_index < LDB_SIZE; ++ldb_index) begin
            next_state[ldb_index]   = `NOT_DEPENDENT;
            assigned                = 0;
            for (int stq_index = 0; stq_index < STQ_SIZE; ++stq_index) begin
                if (next_valid_inst[ldb_index] &&
                    next_valid_store_inst_on_disp[ldb_index][stq_index]) begin
                    // Dealing with an instruction we care about for this load
                    if (stq_index < stq_tail) begin
                        // Same code as else statement, just with the assigned var.
                        // TODO: look at how to refactor
                        assigned = 1'b1;
                        // If either address is unknown, freak out.
                        if (stq_address_valid[stq_index] && next_address_valid[ldb_index]) begin
                            // If a write to a different address than we read,
                            // we don't care.
                            if (stq_address[stq_index] == next_address[ldb_index]) begin
                                // Check that the store has actual data to give us.
                                if (stq_data_valid[stq_index]) begin
                                    next_state[ldb_index]           = `TRUE_DEPENDENCY;
                                    load_data_valid[ldb_index]      = 1'b1;
                                    load_data_prf_tag[ldb_index]    = next_dest_reg[ldb_index];
                                    load_data[ldb_index]            = stq_data[stq_index];
                                end
                                else begin
                                    next_state[ldb_index]           = `UNKNOWN_DATA_ADDRESS;
                                    load_data_valid[ldb_index]      = 0;
                                    load_data_prf_tag[ldb_index]    = `ZERO_REG_PRF;
                                    load_data[ldb_index]            = 0;
                                end
                            end
                        end
                        else begin
                            next_state[ldb_index]           = `UNKNOWN_DATA_ADDRESS;
                            load_data_valid[ldb_index]      = 0;
                            load_data_prf_tag[ldb_index]    = `ZERO_REG_PRF;
                            load_data[ldb_index]            = 0;
                        end
                    end
                    else if (stq_index >= stq_head && (!assigned || next_state[ldb_index] == `NOT_DEPENDENT)) begin
                        assigned = 0;
                        // If either address is unknown, freak out.
                        if (stq_address_valid[stq_index] && next_address_valid[ldb_index]) begin
                            // If a write to a different address than we read,
                            // we don't care.
                            if (stq_address[stq_index] == next_address[ldb_index]) begin
                                // Check that the store has actual data to give us.
                                if (stq_data_valid[stq_index]) begin
                                    next_state[ldb_index]           = `TRUE_DEPENDENCY;
                                    load_data_valid[ldb_index]      = 1'b1;
                                    load_data_prf_tag[ldb_index]    = next_dest_reg[ldb_index];
                                    load_data[ldb_index]            = stq_data[stq_index];
                                end
                                else begin
                                    next_state[ldb_index]           = `UNKNOWN_DATA_ADDRESS;
                                    load_data_valid[ldb_index]      = 0;
                                    load_data_prf_tag[ldb_index]    = `ZERO_REG_PRF;
                                    load_data[ldb_index]            = 0;
                                end
                            end
                        end
                        else begin
                            next_state[ldb_index]           = `UNKNOWN_DATA_ADDRESS;
                            load_data_valid[ldb_index]      = 0;
                            load_data_prf_tag[ldb_index]    = `ZERO_REG_PRF;
                            load_data[ldb_index]            = 0;
                        end
                    end
                end
            end
        end

        // Nullify old entries that got put into CDB
        for (int ldb_index = 0; ldb_index < LDB_SIZE; ++ldb_index) begin
            if (cdb_stall[ldb_index]) begin
                next_request_sent_to_dcache[ldb_index] = 1'b0;
            end
            else if (load_data_valid[ldb_index]) begin
                // Remove things from LDB after making requests to dcache
                next_ldl_mem[ldb_index]                     = 0;
                next_dest_reg[ldb_index]                    = `ZERO_REG_PRF;
                next_address[ldb_index]                     = 0;
                next_address_valid[ldb_index]               = 0;
                next_valid_store_inst_on_disp[ldb_index]    = 0;
                next_valid_inst[ldb_index]                  = 0;
                next_request_sent_to_dcache[ldb_index]      = 0;
            end
        end

        // Put into ldb
        for (int i = 1; i >= 0; --i) begin
            if (grants_on_dispatch[i] == 0) begin
                ldb_stall[i]    = 1'b1;
            end
            else begin
                if (id_disp_valid_inst[i] && id_disp_rd_mem[i] && id_disp_rat_dest_valid[i]) begin
                    for (int ldb_index = 0; ldb_index < LDB_SIZE; ++ldb_index) begin
                        if (grants_on_dispatch[i][ldb_index]) begin
                            next_ldl_mem[ldb_index]                     = id_disp_ldl_mem[i];
                            next_dest_reg[ldb_index]                    = id_disp_rat_dest[i];
                            next_address[ldb_index]                     = id_disp_rat_dest[i];
                            next_address_valid[ldb_index]               = 0;
                            next_valid_store_inst_on_disp[ldb_index]    = stq_valid_on_disp[i];
                            next_valid_inst[ldb_index]                  = 1'b1;
                            next_request_sent_to_dcache[ldb_index]      = 0;
                        end
                    end
                end
            end
        end

        // Remove the committing instruction from store instructions to
        // check against.
        for (int ldb_index = 0; ldb_index < LDB_SIZE; ++ldb_index) begin
            next_valid_store_inst_on_disp[ldb_index] &= ~stq_committing_inst;
        end

        // Nuke
        if (nuke) begin
            next_ldl_mem                    = 0;
            next_dest_reg                   = 0;
            next_address                    = 0;
            next_address_valid              = 0;
            next_valid_store_inst_on_disp   = 0;
            next_valid_inst                 = 0;
            next_request_sent_to_dcache     = 0;
            next_state                      = 0;
        end
    end

    always @(posedge clock) begin
        if (reset) begin
            ldl_mem                     <= `SD 0;
            dest_reg                    <= `SD 0;
            address                     <= `SD 0;
            address_valid               <= `SD 0;
            valid_store_inst_on_disp    <= `SD 0;
            valid_inst                  <= `SD 0;
            request_sent_to_dcache      <= `SD 0;
            state                       <= `SD 0;
        end
        else begin
            ldl_mem                     <= `SD next_ldl_mem;
            dest_reg                    <= `SD next_dest_reg;
            address                     <= `SD next_address;
            address_valid               <= `SD next_address_valid;
            valid_store_inst_on_disp    <= `SD next_valid_store_inst_on_disp;
            valid_inst                  <= `SD next_valid_inst;
            request_sent_to_dcache      <= `SD next_request_sent_to_dcache;
            state                       <= `SD next_state;
        end
    end


endmodule
`default_nettype wire

`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define TRUE_2          2'b01
`define FALSE_2         2'b00
`define WAY_0           1'b0
`define WAY_1           1'b1

parameter STQ_SIZE = 4;
parameter NUM_PRF_ENTRIES = 96;
localparam STQ_BITS = $clog2(STQ_SIZE);
localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES);
localparam QUADWORD = 64;

module testbench;

    // INPUTS
    logic                                   clock;
    logic                                   reset;
    // inputs from dispatch
    logic             [1:0]                 id_disp_valid_inst;
    logic             [1:0]                 id_disp_wr_mem;
    logic             [1:0]                 id_disp_stc_mem;        // Store-con
    logic             [1:0][PRF_BITS-1:0]   id_disp_rat_dest;       // Where the
    logic             [1:0]                 id_disp_rat_dest_valid;
    logic             [1:0][QUADWORD-1:0]   store_data;             // For store
    logic             [1:0]                 store_data_valid;
    // from the EX-WB pipeline register
    logic             [1:0]                 ex_calc_address_valid;
    logic             [1:0][PRF_BITS-1:0]   ex_calc_address_prf;
    logic             [1:0][QUADWORD-1:0]   ex_calc_address;
    // from CDB
    logic             [1:0]                 cdb_valid;
    logic             [1:0][PRF_BITS-1:0]   cdb_prf;
    logic             [1:0][QUADWORD-1:0]   cdb_data;
    // from the Dcache
    logic                                   dcache_stall;
    // from ROB on commit
    logic                                   store_requested;   // ROB wants to c
    logic             [1:0]                 nuke;
    // OUTPUTS
    // to ID-DISP pipeline
    logic            [1:0]                  stq_stall;
    // to the CDB
    logic                                   stored_valid;
    logic                 [PRF_BITS-1:0]    stored_prf;
    logic                 [QUADWORD-1:0]    stored_result;
    // to load buffer
    logic            [1:0][STQ_SIZE-1:0]    stq_valid_on_disp;
    logic   [STQ_SIZE-1:0][QUADWORD-1:0]    stq_address;
    logic   [STQ_SIZE-1:0]                  stq_address_valid;
    logic   [STQ_SIZE-1:0][QUADWORD-1:0]    stq_data;
    logic   [STQ_SIZE-1:0]                  stq_data_valid;
    logic   [STQ_SIZE-1:0]                  stq_committing_inst;
    logic   [STQ_BITS-1:0]                  stq_head;
    logic   [STQ_BITS-1:0]                  stq_tail;
    // to the d-cache
    logic                                   data_to_dcache_valid;
    logic                                   write_to_dcache;
    logic                 [QUADWORD-1:0]    address_to_dcache;
    logic                 [QUADWORD-1:0]    data_to_dcache;

    store_queue #(.STQ_SIZE(STQ_SIZE), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) store_queue1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .id_disp_valid_inst(id_disp_valid_inst),
        .id_disp_wr_mem(id_disp_wr_mem),
        .id_disp_stc_mem(id_disp_stc_mem),
        .id_disp_rat_dest(id_disp_rat_dest),
        .id_disp_rat_dest_valid(id_disp_rat_dest_valid),
        .store_data(store_data),
        .store_data_valid(store_data_valid),
        .ex_calc_address_valid(ex_calc_address_valid),
        .ex_calc_address_prf(ex_calc_address_prf),
        .ex_calc_address(ex_calc_address),
        .cdb_valid(cdb_valid),
        .cdb_prf(cdb_prf),
        .cdb_data(cdb_data),
        .dcache_stall(dcache_stall),
        .store_requested(store_requested),
        .nuke(nuke),
        // OUTPUTS
        .stq_stall(stq_stall),
        .stored_valid(stored_valid),
        .stored_prf(stored_prf),
        .stored_result(stored_result),
        .stq_valid_on_disp(stq_valid_on_disp),
        .stq_address(stq_address),
        .stq_address_valid(stq_address_valid),
        .stq_data(stq_data),
        .stq_data_valid(stq_data_valid),
        .stq_committing_inst(stq_committing_inst),
        .stq_head(stq_head),
        .stq_tail(stq_tail),
        .data_to_dcache_valid(data_to_dcache_valid),
        .write_to_dcache(write_to_dcache),
        .address_to_dcache(address_to_dcache),
        .data_to_dcache(data_to_dcache)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        // Display all SQ entries
        $display("===SQ===");
        $display("\n\n\nSQ Head: %d Tail: %d Full: %d Stall: %b", store_queue1.head, store_queue1.tail, store_queue1.full, stq_stall);

        for (int i = 0; i < STQ_SIZE; i++)
        begin
            if (i == store_queue1.head)
                $display("\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---  <--What store queue is pointing to! HEAD");

            $display("SQ Entry Number: %d ", i,
                      "\n     Valid Inst: %b ", store_queue1.valid_inst[i],
                      "\n     Dest Reg: %d ", store_queue1.dest_reg[i],
                      "\n     Data: %d ", store_queue1.data[i],
                      "\n     Data Valid: %b ", store_queue1.data_valid[i],
                      "\n     Address: %h ", store_queue1.address[i],
                      "\n     Address Valid: %b ", store_queue1.address_valid[i],
                      "\n     STC: %b ", store_queue1.stc_mem[i]);

             if (i == (store_queue1.tail - 1))
                $display("/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---  <--What store queue is pointing to! TAIL");
        end
        $fatal("\n\nINCORRECT");

    endtask

    task dispatch;
        input                           way;
        input                           valid_inst;
        input                           wr_mem;
        input                           stc_mem;
        input                           rat_dest_valid;
        input       [PRF_BITS-1:0]      rat_dest;
        input                           data_valid;
        input       [QUADWORD-1:0]      data;

    id_disp_valid_inst[way]     = valid_inst;
    id_disp_wr_mem[way]         = wr_mem;
    id_disp_stc_mem[way]        = stc_mem;
    id_disp_rat_dest_valid[way] = rat_dest_valid;
    id_disp_rat_dest[way]       = rat_dest;
    store_data_valid[way]       = data_valid;
    store_data[way]             = data;

    endtask

    task address_from_exwb;
        input                           way;
        input                           calc_address_valid;
        input       [PRF_BITS-1:0]      calc_address_prf;
        input       [QUADWORD-1:0]      calc_address;

    ex_calc_address_valid[way]  = calc_address_valid;
    ex_calc_address_prf[way]    = calc_address_prf;
    ex_calc_address[way]        = calc_address;

    endtask

    task data_from_cdb;
        input                           way;
        input                           valid;
        input       [PRF_BITS-1:0]      prf;
        input       [QUADWORD-1:0]      data;

   cdb_valid[way]               = valid;
   cdb_prf[way]                 = prf;
   cdb_data[way]                = data;

    endtask

    task check_entry;
        input       [STQ_BITS-1:0]      entry;
        input                           int_valid_inst;
        input       [PRF_BITS-1:0]      int_dest_reg;
        input                           int_data_valid;
        input       [QUADWORD-1:0]      int_data;
        input                           int_address_valid;
        input       [QUADWORD-1:0]      int_address;
        input                           int_stc_mem;

        if (store_queue1.valid_inst[entry] != int_valid_inst) exit_on_error();
        if (store_queue1.dest_reg[entry] != int_dest_reg && int_valid_inst) exit_on_error();
        if (store_queue1.data_valid[entry] != int_data_valid && int_valid_inst) exit_on_error();
        if (store_queue1.data[entry] != int_data && int_data_valid && int_valid_inst) exit_on_error();
        if (store_queue1.address_valid[entry] != int_address_valid && int_valid_inst) exit_on_error();
        if (store_queue1.address[entry] != int_address && int_valid_inst && int_address_valid) exit_on_error();
        if (store_queue1.stc_mem[entry] != int_stc_mem && int_valid_inst) exit_on_error();

    endtask

    task check_ptr;
        input       [STQ_BITS-1:0]      int_head;
        input       [STQ_BITS-1:0]      int_tail;
        input                           int_full;

        if (store_queue1.head != int_head) exit_on_error();
        if (store_queue1.tail != int_tail) exit_on_error();
        if (store_queue1.full != int_full) exit_on_error();

    endtask


    task check_cdb;
        input                           valid;
        input       [PRF_BITS-1:0]      prf;
        input       [QUADWORD-1:0]      result;

        if (stored_valid != valid)  exit_on_error();
        if (stored_prf != prf && valid) exit_on_error();
        if (stored_result != result && valid) exit_on_error();

    endtask

    task check_dcache;
        input                           valid
;
        input                           write;
        input       [QUADWORD-1:0]      address;
        input       [QUADWORD-1:0]      data;

        if (data_to_dcache_valid != valid)  exit_on_error();
        if (write_to_dcache != write && valid) exit_on_error();
        if (address_to_dcache != address && valid) exit_on_error();
        if (data_to_dcache != data && valid) exit_on_error();

    endtask

    task check_ldb;
        input                           way;
        input       [STQ_SIZE-1:0]      valid_on_disp;
        input       [STQ_SIZE-1:0]      data_valid;
        input       [STQ_SIZE-1:0]      address_valid;
        input       [STQ_SIZE-1:0]      committing_inst;
        input       [STQ_BITS-1:0]      head;
        input       [STQ_BITS-1:0]      tail;

        if (stq_valid_on_disp[way] != valid_on_disp)  exit_on_error();
        if (stq_data_valid != data_valid)  exit_on_error();
        if (stq_address_valid != address_valid)  exit_on_error();
        if (stq_committing_inst != committing_inst)  exit_on_error();
        if (stq_head != head)  exit_on_error();
        if (stq_tail != tail)  exit_on_error();

    endtask

    initial
    begin
        $monitor("\n\n===WAY_0 INPUTS DISPATCH===\nTime= %d  valid_inst= %b wr_mem= %b stc_mem= %b rat_dest_valid= %b rat_dest= %d valid= %b data= %d\n", $time, id_disp_valid_inst[0], id_disp_wr_mem[0], id_disp_stc_mem[0], id_disp_rat_dest_valid[0], id_disp_rat_dest[0], store_data_valid[0], store_data[0],
                "===WAY_0 INPUTS ADDRESS FROM EX-WB===\nTime= %d valid= %b prf= %d address= %h\n", $time, ex_calc_address_valid[0], ex_calc_address_prf[0], ex_calc_address[0],
                 "===WAY_0 INPUTS DATA FROM CDB===\nTime= %d valid= %b prf= %d data\n", $time, cdb_valid[0], cdb_prf[0], cdb_data[0],
                 "===WAY_0 OUTPUTS CDB===\nTime= %d valid= %b prf= %d result= %d\n", $time, stored_valid, stored_prf, stored_result,
                 "===WAY_0 OUTPUTS LOAD BUFFER===\nTime= %d valid on disp= %b data valid= %b address valid= %b committing instr= %b head= %d tail= %d\n", $time, stq_valid_on_disp[0], stq_data_valid, stq_address_valid, stq_committing_inst, stq_head, stq_tail,
                 "===WAY_0 OUTPUTS DCACHE===\nTime= %d valid= %b write= %b address= %h data= %d\n", $time, data_to_dcache_valid, write_to_dcache, address_to_dcache, data_to_dcache,
                "\n\n===WAY_1 INPUTS DISPATCH===\nTime= %d  valid_inst= %b wr_mem= %b stc_mem= %b rat_dest_valid= %b rat_dest= %d valid= %b data= %d\n", $time, id_disp_valid_inst[1], id_disp_wr_mem[1], id_disp_stc_mem[1], id_disp_rat_dest_valid[1], id_disp_rat_dest[1], store_data_valid[1], store_data[1],
                "===WAY_1 INPUTS ADDRESS FROM EX-WB===\nTime= %d valid= %b prf= %d address= %h\n", $time, ex_calc_address_valid[1], ex_calc_address_prf[1], ex_calc_address[1],
                 "===WAY_1 INPUTS DATA FROM CDB===\nTime= %d valid= %b prf= %d data\n", $time, cdb_valid[1], cdb_prf[1], cdb_data[1],
                 "===WAY_1 OUTPUTS CDB===\nTime= %d valid= %b prf= %d result= %d\n", $time, stored_valid, stored_prf, stored_result,
                 "===WAY_1 OUTPUTS LOAD BUFFER===\nTime= %d valid on disp= %b\n", $time, stq_valid_on_disp[1],
                 "===WAY_1 OUTPUTS DCACHE===\nTime= %d valid= %b write= %b address= %h data= %d\n", $time, data_to_dcache_valid, write_to_dcache, address_to_dcache, data_to_dcache);


        //Intialize
        clock                   = 0;
        reset                   = 1;
        id_disp_valid_inst      = 0;
        id_disp_wr_mem          = 0;
        id_disp_stc_mem         = 0;
        id_disp_rat_dest        = 0;
        id_disp_rat_dest_valid  = 0;
        store_data              = 0;
        store_data_valid        = 0;
        ex_calc_address_valid   = 0;
        ex_calc_address_prf     = 0;
        ex_calc_address         = 0;
        cdb_valid               = 0;
        cdb_prf                 = 0;
        cdb_data                = 0;
        dcache_stall            = 0;
        store_requested         = 0;
        nuke                    = 0;

        //CHECK reset
        @(negedge clock);
        reset = 0;
        if (stq_stall != 0) exit_on_error();
        check_cdb(`FALSE, 7'd0, 64'd0);
        check_dcache(`FALSE, `FALSE, 64'd0, 64'd0);
        check_ldb(`WAY_1, 4'b0000, 4'b0000, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed reset\n");
        //exit_on_error();

        // Dispatch in way 1 only with invalid instr
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1 only with invalid instr\n");

        // Dispatch in way 1 only with valid instr but not a store
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `FALSE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1 only with valid instr but not store\n");

        // Dispatch in way 1 only with valid store instr but invalid rat dest
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1 only with valid store instr but invalid rat dest\n");

        // Dispatch in way 1 only
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1\n");
        //exit_on_error();

        // Dispatch in way 0 only with invalid instr
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        $display("\n@@@Passed dispatch in way 0 only with invalid instr\n");

        // Dispatch in way 0 only with valid instr but not a store
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `FALSE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        $display("\n@@@Passed dispatch in way 0 only with valid instr but not store\n");

        // Dispatch in way 0 only with valid store instr but invalid rat dest
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `FALSE, 7'd94, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        $display("\n@@@Passed dispatch in way 0 only with valid store instr but invalid rat dest\n");

        // Dispatch in way 0 only
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd2, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        check_ldb(`WAY_1, 4'b0011, 4'b0111, 4'b0000, 4'b0000, 2'd0, 2'd3);      //%%%%%%%%%%%%%%%%%%%%%%%%%
        check_ldb(`WAY_0, 4'b0011, 4'b0111, 4'b0000, 4'b0000, 2'd0, 2'd3);
        $display("\n@@@Passed dispatch in way 0\n");
        //exit_on_error();

        // Dispatch in both ways but data not available in prf at the time of dispatch
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd93, `FALSE, 64'd100);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd200);
        check_ldb(`WAY_1, 4'b0011, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd2);
        check_ldb(`WAY_0, 4'b0011, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd2);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        check_ldb(`WAY_1, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed dispatch in both ways but data not available in prf\n");
        //exit_on_error();

        // Check stall in way 1
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd91, `TRUE, 64'd300);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b10) exit_on_error();
        $display("\n@@@Passed stall in way 1\n");
        //exit_on_error();

        // Check stall in way 0
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd91, `TRUE, 64'd300);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b01) exit_on_error();
        $display("\n@@@Passed stall in way 0\n");
        //exit_on_error();

        // Check stall both ways
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd90, `FALSE, 64'd100);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd89, `FALSE, 64'd200);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b11) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed stall both ways\n");
        //exit_on_error();

        // Do not grab data from CDB
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        @(negedge clock);
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed do not grab data from cdb\n");

        // Grab data from CDB
        @(negedge clock);
        data_from_cdb(`WAY_1, `TRUE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `TRUE, 7'd93, 64'd400);
        @(negedge clock);
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        if (stq_stall != 2'b00) exit_on_error();
        $display("\n@@@Passed do not grab data from cdb\n");
        //exit_on_error();

        // Commit a store which is still waiting for its address
        store_requested = `TRUE;
        @(negedge clock);
        store_requested = `FALSE;
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed commit a store which is still waiting for address\n");
        //exit_on_error();

        // Grab address from EX-WB
        address_from_exwb(`WAY_1, `TRUE, 7'd93, 64'hbad);
        address_from_exwb(`WAY_0, `TRUE, 7'd95, 64'hdad);
        @(negedge clock);
        address_from_exwb(`WAY_1, `TRUE, 7'd94, 64'head);
        address_from_exwb(`WAY_0, `TRUE, 7'd92, 64'hcad);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `TRUE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b0101, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b0101, 4'b0000, 2'd0, 2'd0);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'head);
        address_from_exwb(`WAY_0, `FALSE, 7'd92, 64'hcad);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `TRUE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `TRUE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b1111, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b1111, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed grab address from EX-WB\n");
        //exit_on_error();

        // Commit a store
        @(negedge clock);
        store_requested = `TRUE;
        #5;
        check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);
        @(negedge clock);
        store_requested = `FALSE;
        check_ptr(2'd1, 2'd0, `FALSE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `TRUE, 64'head, `FALSE);
        check_entry(2'd0, `FALSE, 7'd95, `TRUE, 64'd100, `TRUE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        //check_ldb(`WAY_1, 4'b1110, 4'b1110, 4'b1110, 4'b0001, 2'd1, 2'd0);
        //check_ldb(`WAY_0, 4'b1110, 4'b1110, 4'b1110, 4'b0001, 2'd1, 2'd0);
        //check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        #5;
        check_dcache(`FALSE, `TRUE, 64'hdad, 64'd100);
        $display("\n@@@Passed commit a store\n");
        //exit_on_error();

        // Dispatch in way 1 only
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd91, `TRUE, 64'd600);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd1, 2'd1, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `TRUE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        $display("\n@@@Passed dispatch in way 1\n");
        //exit_on_error();

        // Commit and dispatch in same cycle when the queue is full - no stq_stall
        @(negedge clock);
        store_requested = `TRUE;
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd90, `TRUE, 64'd700);
        #5;
        check_dcache(`TRUE, `TRUE, 64'head, 64'd200);
        @(negedge clock);
        store_requested = `FALSE;
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd2, 2'd2, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        //check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //#5;
        //check_dcache(`FALSE, `TRUE, 64'hdad, 64'd100);
        $display("\n@@@Passed commit and dispatch in same cycle\n");
        //exit_on_error();

        // Commit a store
        @(negedge clock);
        store_requested = `TRUE;
        #5;
        check_dcache(`TRUE, `TRUE, 64'hbad, 64'd400);
        @(negedge clock);
        store_requested = `FALSE;
        check_ptr(2'd3, 2'd2, `FALSE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `FALSE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        //check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        #5;
        check_dcache(`FALSE, `TRUE, 64'hdad, 64'd100);
        $display("\n@@@Passed commit a store\n");
        //exit_on_error();

        // Dispatch both ways when there is just one empty spot in the queue
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd89, `TRUE, 64'd800);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd88, `TRUE, 64'd900);
        @(posedge clock);
        if (stq_stall != 2'b01) exit_on_error();
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd3, 2'd3, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd89, `TRUE, 64'd800, `FALSE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        //if (stq_stall != 2'b11) exit_on_error();  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        $display("\n@@@Passed dispatch in both ways when there is just one empty spot in the queue\n");
        //exit_on_error();

        // Nuke - passed 01, 10, 11
        @(negedge clock);
        nuke = 2'b10;
        @(negedge clock);
        nuke = 2'b00;
        //exit_on_error();
        check_ptr(2'd0, 2'd0, `FALSE);
        check_entry(2'd3, `FALSE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `FALSE, 7'd89, `TRUE, 64'd800, `FALSE, 64'hbad, `FALSE);
        check_entry(2'd1, `FALSE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `FALSE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_cdb(`FALSE, 7'd0, 64'd0);
        check_dcache(`FALSE, `FALSE, 64'd0, 64'd0);
        check_ldb(`WAY_1, 4'b0000, 4'b0000, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed nuke\n");


        // Dispatch in both ways but data not available in prf at the time of dispatch
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd93, `FALSE, 64'd100);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd200);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd2, `FALSE);
        check_entry(2'd3, `FALSE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `FALSE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd92, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd93, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        $display("\n@@@Passed dispatch in both ways but data not available in prf\n");
        //exit_on_error();

        // Grab data, address, and try to commit in the same cycle - able to commit
        // Also, Checked the same case when dcache stall high - sends output to
        // dcache, but the internal state do not change(as in the pointer doesnot update and the committing entry is not removed)
        @(negedge clock);
        address_from_exwb(`WAY_1, `TRUE, 7'd93, 64'hbcd);
        address_from_exwb(`WAY_0, `TRUE, 7'd92, 64'habc);
        data_from_cdb(`WAY_1, `TRUE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `TRUE, 7'd93, 64'd400);
        store_requested = `TRUE;
        dcache_stall = 1'b1;                           //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        @(posedge clock);
        check_dcache(`TRUE, `TRUE, 64'hbcd, 64'd400);
        @(negedge clock);
        dcache_stall = 1'b0;
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'head);
        address_from_exwb(`WAY_0, `FALSE, 7'd92, 64'hcad);
        store_requested = `FALSE;
        check_ptr(2'd0, 2'd2, `FALSE);
        check_entry(2'd3, `FALSE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `FALSE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'habc, `FALSE);
        check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbcd, `FALSE);
        $display("\n@@@Passed grab data, address, and try to commit in the same cycle\n");

        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

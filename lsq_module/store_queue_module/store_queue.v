// 3/21/2018 - synthesized at 7ns, 8 entries

`default_nettype none
`timescale 1ns/100ps
module store_queue #(parameter STQ_SIZE = 8, parameter NUM_PRF_ENTRIES = 96, localparam STQ_BITS = $clog2(STQ_SIZE), localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam QUADWORD = 64, localparam ADDR_BITS = 13) (
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,
    // inputs from dispatch
    input   wire             [1:0]                  id_disp_valid_inst,
    input   wire             [1:0]                  id_disp_wr_mem,
    input   wire             [1:0]                  id_disp_stc_mem,        // Store-conditional
    input   wire             [1:0][PRF_BITS-1:0]    id_disp_rat_dest,       // Where the store is going/way to identify a memory operation
    input   wire             [1:0]                  id_disp_rat_dest_valid,
    input   wire             [1:0][QUADWORD-1:0]    store_data,             // For store, is the register data ready in the PRF
    input   wire             [1:0]                  store_data_valid,
    // from the EX-WB pipeline register
    input   wire             [1:0]                  ex_calc_address_valid,
    input   wire             [1:0][PRF_BITS-1:0]    ex_calc_address_prf,
    input   wire             [1:0][QUADWORD-1:0]    ex_calc_address,
    // from CDB
    input   wire             [1:0]                  cdb_valid,
    input   wire             [1:0][PRF_BITS-1:0]    cdb_prf,
    input   wire             [1:0][QUADWORD-1:0]    cdb_data,
    // from the Dcache
    input   wire                                    dcache_stall,
    // from ROB on commit
    input   wire                                    store_requested,        // ROB wants to commit a store
    input   wire                                    store_executed,
    input   wire             [1:0]                  nuke,

    // OUTPUTS
    // to ID-DISP pipeline
    output  logic            [1:0]                  stq_stall,
    // to the CDB
    output  logic                                   stored_valid,
    output  logic                 [PRF_BITS-1:0]    stored_prf,
    output  logic                 [QUADWORD-1:0]    stored_result,
    // to load buffer
    output  logic            [1:0][STQ_SIZE-1:0]    stq_valid_on_disp,
    output  logic   [STQ_SIZE-1:0][ADDR_BITS-1:0]   stq_address,
    output  logic   [STQ_SIZE-1:0]                  stq_address_valid,
    output  logic   [STQ_SIZE-1:0][QUADWORD-1:0]    stq_data,
    output  logic   [STQ_SIZE-1:0]                  stq_data_valid,
    output  logic   [STQ_SIZE-1:0]                  stq_committing_inst,
    output  logic   [STQ_BITS-1:0]                  stq_head,
    output  logic   [STQ_BITS-1:0]                  stq_tail,
    // to the d-cache
    output  logic                                   data_to_dcache_valid,
    output  logic                                   write_to_dcache,
    output  logic                 [QUADWORD-1:0]    address_to_dcache,
    output  logic                 [QUADWORD-1:0]    data_to_dcache
);

    logic   [STQ_SIZE-1:0]                  stc_mem;
    logic   [STQ_SIZE-1:0][QUADWORD-1:0]    data;           // If data_valid, then actual data to store. Else, PRF index of data to store
    logic   [STQ_SIZE-1:0]                  data_valid;
    logic   [STQ_SIZE-1:0][ADDR_BITS-1:0]   address;        // If address_valid, then actual address at which to store. Else, PRF index
    logic   [STQ_SIZE-1:0]                  address_valid;
    logic   [STQ_SIZE-1:0][PRF_BITS-1:0]    dest_reg;
    logic   [STQ_SIZE-1:0]                  valid_inst;

    logic   [STQ_BITS-1:0]                  head;
    logic   [STQ_BITS-1:0]                  tail;
    logic                                   full;

    logic   [STQ_SIZE-1:0]                  next_stc_mem;
    logic   [STQ_SIZE-1:0][QUADWORD-1:0]    next_data;             // If data_valid, then actual data to store. Else, PRF index of data to store
    logic   [STQ_SIZE-1:0]                  next_data_valid;
    logic   [STQ_SIZE-1:0][ADDR_BITS-1:0]   next_address;
    logic   [STQ_SIZE-1:0]                  next_address_valid;
    logic   [STQ_SIZE-1:0][PRF_BITS-1:0]    next_dest_reg;
    logic   [STQ_SIZE-1:0]                  next_valid_inst;

    logic   [STQ_BITS-1:0]                  next_head;
    logic   [STQ_BITS-1:0]                  next_tail;
    logic                                   next_full;

    logic   already_stored, next_already_stored;


    always_comb begin
        next_stc_mem            = stc_mem;
        next_data               = data;
        next_data_valid         = data_valid;
        next_address            = address;
        next_address_valid      = address_valid;
        next_dest_reg           = dest_reg;
        next_valid_inst         = valid_inst;
        next_head               = head;
        next_tail               = tail;
        next_full               = full;
        next_already_stored     = already_stored;


        // Initialize outputs
        stq_valid_on_disp       = 0;
        stq_address             = 0;
        stq_address_valid       = 0;
        stq_data                = 0;
        stq_data_valid          = 0;
        stq_committing_inst     = 0;
        stored_valid            = 0;
        stored_prf              = 0;
        stored_result           = 0;
        data_to_dcache_valid    = 0;
        write_to_dcache         = 0;
        address_to_dcache       = 0;
        data_to_dcache          = 0;
        stq_stall               = 0;

        stq_address         = next_address;
        stq_address_valid   = next_address_valid;
        stq_data            = next_data;
        stq_data_valid      = next_data_valid;
        stq_head            = next_head;
        stq_tail            = next_tail;

        // Accept the dispatching stores
        for (int i = 1; i >= 0; --i) begin
            // Output to load buffer which instructions are valid before dispatch
            stq_valid_on_disp[i] = next_valid_inst;
            if (!next_full) begin
                if (id_disp_valid_inst[i] && id_disp_rat_dest_valid[i] && id_disp_wr_mem[i]) begin
                    // Store either the PRF index or the actual data to store
                    next_data_valid[next_tail]      = store_data_valid[i];
                    next_data[next_tail]            = store_data[i];

                    // Check if we have a valid data to store
                    next_stc_mem[next_tail]         = id_disp_stc_mem[i];
                    next_address[next_tail]         = 0;
                    next_address_valid[next_tail]   = 0;
                    next_dest_reg[next_tail]        = id_disp_rat_dest[i];
                    next_valid_inst[next_tail]      = 1'b1;

                    next_tail                       = next_tail + 1;
                    if (next_head == next_tail) begin
                        next_full = 1'b1;
                    end
                end
            end
            else begin
                // We are full
                stq_stall[i] = 1'b1;
            end
        end


        // Update all current stores with CDB
        for (int cdb_way = 1; cdb_way >= 0; --cdb_way) begin
            for (int store_queue_index = 0; store_queue_index < STQ_SIZE; ++store_queue_index) begin
                if (cdb_valid[cdb_way] &&
                    next_valid_inst[store_queue_index] &&
                    !next_data_valid[store_queue_index] &&
                    next_data[store_queue_index] == cdb_prf[cdb_way]) begin

                    next_data[store_queue_index]        = cdb_data[cdb_way];
                    next_data_valid[store_queue_index]  = 1'b1;
                end
            end
        end

        // Update all current stores with address from pipeline
        for (int cdb_way = 1; cdb_way >= 0; --cdb_way) begin
            for (int store_queue_index = 0; store_queue_index < STQ_SIZE; ++store_queue_index) begin
                if (ex_calc_address_valid[cdb_way] &&
                    next_valid_inst[store_queue_index] &&
                    !next_address_valid[store_queue_index] &&
                    next_dest_reg[store_queue_index] == ex_calc_address_prf[cdb_way]) begin

                    next_address[store_queue_index]         = ex_calc_address[cdb_way][ADDR_BITS+2:3];
                    next_address_valid[store_queue_index]   = 1'b1;
                end
            end
        end

        // Take in a committing store
        // using stored state because this is part of critical path
        if (store_requested && !next_already_stored) begin
            if (address_valid[head] && data_valid[head]) begin
                data_to_dcache_valid    = 1'b1;
                write_to_dcache         = 1'b1;
                address_to_dcache       = {48'd0, address[head], 3'b000};
                data_to_dcache          = data[head];
            end
        end

        // Check that the dcache accepted the request
        if (!dcache_stall && store_requested && !next_already_stored &&
            address_valid[head] && data_valid[head]) begin

            // Output to CDB pipeline register
            // TODO how to handle CDB stall
            // POSSIBLE SOLUTION: never have CDB stall the store instructions
            stored_valid    = 1'b1;
            stored_prf      = dest_reg[head];
            // TODO: will change when working with STQ_C
            stored_result   = 0;

            // Tell the load buffer that this instruction is no longer valid
            stq_committing_inst[head]  = 1'b1;

            // Zero out all the stored state
            next_stc_mem[head]          = 0;
            next_data[head]             = 0;
            next_data_valid[head]       = 0;
            next_address[head]          = 0;
            next_address_valid[head]    = 0;
            next_valid_inst[head]       = 0;
            next_already_stored         = 1'b1;

            next_head                   = next_head + 1;
            next_full                   = 0;
        end

        if (store_executed) begin
            next_already_stored     = 0;
        end

        if (nuke) begin
            next_stc_mem            = 0;
            next_data               = 0;
            next_data_valid         = 0;
            next_address            = 0;
            next_address_valid      = 0;
            next_dest_reg           = 0;
            next_head               = 0;
            next_tail               = 0;
            next_full               = 0;
            next_valid_inst         = 0;
            next_already_stored     = 0;
        end
    end

    always @(posedge clock) begin
        if (reset) begin
            stc_mem         <= `SD 0;
            data            <= `SD 0;
            data_valid      <= `SD 0;
            address         <= `SD 0;
            address_valid   <= `SD 0;
            dest_reg        <= `SD 0;
            head            <= `SD 0;
            tail            <= `SD 0;
            full            <= `SD 0;
            valid_inst      <= `SD 0;
            already_stored  <= `SD 0;
        end
        else begin
            stc_mem         <= `SD next_stc_mem;
            data            <= `SD next_data;
            data_valid      <= `SD next_data_valid;
            address         <= `SD next_address;
            address_valid   <= `SD next_address_valid;
            dest_reg        <= `SD next_dest_reg;
            head            <= `SD next_head;
            tail            <= `SD next_tail;
            full            <= `SD next_full;
            valid_inst      <= `SD next_valid_inst;
            already_stored  <= `SD next_already_stored;
        end
    end

endmodule
`default_nettype wire

`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define TRUE_2          2'b01
`define FALSE_2         2'b00
`define WAY_0           1'b0
`define WAY_1           1'b1

parameter LDB_SIZE = 4;
parameter STQ_SIZE = 4;
parameter NUM_PRF_ENTRIES = 96;
localparam STQ_BITS = $clog2(STQ_SIZE);
localparam LDB_BITS = $clog2(LDB_SIZE);
localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES);
localparam QUADWORD = 64;

module testbench;

    // INPUTS
    logic                                   clock;
    logic                                   reset;
    // inputs from dispatch
    logic             [1:0]                 id_disp_valid_inst;
    logic             [1:0]                 id_disp_wr_mem;
    logic             [1:0]                 id_disp_rd_mem;
    logic             [1:0]                 id_disp_stc_mem;        // Store-con
    logic             [1:0]                 id_disp_ldl_mem;        // Store-con
    logic             [1:0][PRF_BITS-1:0]   id_disp_rat_dest;       // Where the
    logic             [1:0]                 id_disp_rat_dest_valid;
    logic             [1:0][QUADWORD-1:0]   store_data;             // For store
    logic             [1:0]                 store_data_valid;
    // from the EX-WB pipeline register
    logic             [1:0]                 ex_calc_address_valid;
    logic             [1:0][PRF_BITS-1:0]   ex_calc_address_prf;
    logic             [1:0][QUADWORD-1:0]   ex_calc_address;
    // from CDB to ldb
    logic             [1:0]                 cdb_valid;
    logic             [1:0][PRF_BITS-1:0]   cdb_prf;
    logic             [1:0][QUADWORD-1:0]   cdb_data;
    // from CDB to stq
    logic   [LDB_SIZE-1:0]                  cdb_stall;
    // from the Dcache to stq
    logic                                   stq_dcache_stall;
    logic             [1:0]                 ldb_dcache_stall;
    logic             [1:0]                 ldb_dcache_load_data_valid;
    logic             [1:0][QUADWORD-1:0]   ldb_dcache_load_data;
    logic             [1:0][QUADWORD-1:0]   ldb_dcache_load_addr;
    // from ROB on commit
    logic                                   store_requested;   // ROB wants to c
    logic             [1:0]                 nuke;

    // OUTPUTS
    // to ID-DISP pipeline
    logic            [1:0]                  stq_stall;
    logic            [1:0]                  ldb_stall;
    // to the CDB
    logic                                   stored_valid;
    logic                 [PRF_BITS-1:0]    stored_prf;
    logic                 [QUADWORD-1:0]    stored_result;
    logic   [LDB_SIZE-1:0]                  load_data_valid;
    logic   [LDB_SIZE-1:0][PRF_BITS-1:0]    load_data_prf_tag;
    logic   [LDB_SIZE-1:0][QUADWORD-1:0]    load_data;
    // to the d-cache
    logic                                   stq_data_to_dcache_valid;
    logic                                   stq_write_to_dcache;
    logic                 [QUADWORD-1:0]    stq_address_to_dcache;
    logic                 [QUADWORD-1:0]    stq_data_to_dcache;
    logic            [1:0]                  ldb_addr_to_dcache_valid;
    logic            [1:0]                  ldb_read_from_dcache;
    logic            [1:0][QUADWORD-1:0]    ldb_address_to_dcache;

    lsq #(.LDB_SIZE(LDB_SIZE), .STQ_SIZE(STQ_SIZE), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) lsq1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .id_disp_valid_inst(id_disp_valid_inst),
        .id_disp_wr_mem(id_disp_wr_mem),
        .id_disp_rd_mem(id_disp_rd_mem),
        .id_disp_stc_mem(id_disp_stc_mem),
        .id_disp_ldl_mem(id_disp_ldl_mem),
        .id_disp_rat_dest(id_disp_rat_dest),
        .id_disp_rat_dest_valid(id_disp_rat_dest_valid),
        .store_data(store_data),
        .store_data_valid(store_data_valid),
        .ex_calc_address_valid(ex_calc_address_valid),
        .ex_calc_address_prf(ex_calc_address_prf),
        .ex_calc_address(ex_calc_address),
        .cdb_valid(cdb_valid),
        .cdb_prf(cdb_prf),
        .cdb_data(cdb_data),
        .cdb_stall(cdb_stall),
        .stq_dcache_stall(stq_dcache_stall),
        .ldb_dcache_stall(ldb_dcache_stall),
        .ldb_dcache_load_data_valid(ldb_dcache_load_data_valid),
        .ldb_dcache_load_data(ldb_dcache_load_data),
        .ldb_dcache_load_addr(ldb_dcache_load_addr),
        .store_requested(store_requested),
        .nuke(nuke),
        // OUTPUTS
        .stq_stall(stq_stall),
        .ldb_stall(ldb_stall),
        .stored_valid(stored_valid),
        .stored_prf(stored_prf),
        .stored_result(stored_result),
        .load_data_valid(load_data_valid),
        .load_data_prf_tag(load_data_prf_tag),
        .load_data(load_data),
        .stq_data_to_dcache_valid(stq_data_to_dcache_valid),
        .stq_write_to_dcache(stq_write_to_dcache),
        .stq_address_to_dcache(stq_address_to_dcache),
        .stq_data_to_dcache(stq_data_to_dcache),
        .ldb_addr_to_dcache_valid(ldb_addr_to_dcache_valid),
        .ldb_read_from_dcache(ldb_read_from_dcache),
        .ldb_address_to_dcache(ldb_address_to_dcache)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        // Display all SQ entries
        $display("===SQ===");
        $display("\n\n\nSQ Head: %d Tail: %d Full: %d Stall: %b", lsq1.store_queue1.head, lsq1.store_queue1.tail, lsq1.store_queue1.full, stq_stall);

        for (int i = 0; i < STQ_SIZE; i++)
        begin
            if (i == lsq1.store_queue1.head)
                $display("\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---  <--What store queue is pointing to! HEAD");

            $display("SQ Entry Number: %d ", i,
                      "\n     Valid Inst: %b ", lsq1.store_queue1.valid_inst[i],
                      "\n     Dest Reg: %d ", lsq1.store_queue1.dest_reg[i],
                      "\n     Data: %d ", lsq1.store_queue1.data[i],
                      "\n     Data Valid: %b ", lsq1.store_queue1.data_valid[i],
                      "\n     Address: %h ", lsq1.store_queue1.address[i],
                      "\n     Address Valid: %b ", lsq1.store_queue1.address_valid[i],
                      "\n     STC: %b ", lsq1.store_queue1.stc_mem[i]);

             if (i == (lsq1.store_queue1.tail - 1))
                $display("/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---  <--What store queue is pointing to! TAIL");
        end

        // Display all LB entries
        $display("\n\n\n===LB===");
        $display("\n\n\nStall: %b", ldb_stall);

        for (int i = 0; i < LDB_SIZE; i++)
        begin

            $display("LB Entry Number: %d ", i,
                      "\n     Valid Inst: %b ", lsq1.ldb.valid_inst[i],
                      "\n     Dest Reg: %d ", lsq1.ldb.dest_reg[i],
                      "\n     Address: %h ", lsq1.ldb.address[i],
                      "\n     Address Valid: %b ", lsq1.ldb.address_valid[i],
                      "\n     Valid Store on Disp: %b ", lsq1.ldb.valid_store_inst_on_disp[i],
                      "\n     Requested DCache: %b ", lsq1.ldb.request_sent_to_dcache[i],
                      "\n     STC: %b ", lsq1.ldb.ldl_mem[i]);

        end
        $fatal("\n\nINCORRECT");

    endtask

    task dispatch;
        input                           way;
        input                           valid_inst;
        input                           wr_mem;
        input                           rd_mem;
        input                           stc_mem;
        input                           ldl_mem;
        input                           rat_dest_valid;
        input       [PRF_BITS-1:0]      rat_dest;
        input                           data_valid;
        input       [QUADWORD-1:0]      data;

    id_disp_valid_inst[way]     = valid_inst;
    id_disp_wr_mem[way]         = wr_mem;
    id_disp_rd_mem[way]         = rd_mem;
    id_disp_stc_mem[way]        = stc_mem;
    id_disp_ldl_mem[way]        = ldl_mem;
    id_disp_rat_dest_valid[way] = rat_dest_valid;
    id_disp_rat_dest[way]       = rat_dest;
    store_data_valid[way]       = data_valid;
    store_data[way]             = data;

    endtask

    task address_from_exwb;
        input                           way;
        input                           calc_address_valid;
        input       [PRF_BITS-1:0]      calc_address_prf;
        input       [QUADWORD-1:0]      calc_address;

    ex_calc_address_valid[way]  = calc_address_valid;
    ex_calc_address_prf[way]    = calc_address_prf;
    ex_calc_address[way]        = calc_address;

    endtask

    task data_from_cdb;
        input                           way;
        input                           valid;
        input       [PRF_BITS-1:0]      prf;
        input       [QUADWORD-1:0]      data;

   cdb_valid[way]               = valid;
   cdb_prf[way]                 = prf;
   cdb_data[way]                = data;

    endtask

    task dcache_to_ldb;
        input                           way;
        input                           stall;
        input                           valid;
        input       [QUADWORD-1:0]      data;
        input       [QUADWORD-1:0]      addr;

    ldb_dcache_stall[way]               = stall;
    ldb_dcache_load_data_valid[way]     = valid;
    ldb_dcache_load_data[way]           = data;
    ldb_dcache_load_addr[way]           = addr;

    endtask

    task stq_check_entry;
        input       [STQ_BITS-1:0]      entry;
        input                           int_valid_inst;
        input       [PRF_BITS-1:0]      int_dest_reg;
        input                           int_data_valid;
        input       [QUADWORD-1:0]      int_data;
        input                           int_address_valid;
        input       [QUADWORD-1:0]      int_address;
        input                           int_stc_mem;

        if (lsq1.store_queue1.valid_inst[entry] != int_valid_inst) exit_on_error();
        if (lsq1.store_queue1.dest_reg[entry] != int_dest_reg && int_valid_inst) exit_on_error();
        if (lsq1.store_queue1.data_valid[entry] != int_data_valid && int_valid_inst) exit_on_error();
        if (lsq1.store_queue1.data[entry] != int_data && int_data_valid && int_valid_inst) exit_on_error();
        if (lsq1.store_queue1.address_valid[entry] != int_address_valid && int_valid_inst) exit_on_error();
        if (lsq1.store_queue1.address[entry] != int_address && int_valid_inst && int_address_valid) exit_on_error();
        if (lsq1.store_queue1.stc_mem[entry] != int_stc_mem && int_valid_inst) exit_on_error();

    endtask

    task ldb_check_entry;
        input       [LDB_BITS-1:0]      entry;
        input                           int_valid_inst;
        input       [PRF_BITS-1:0]      int_dest_reg;
        input                           int_address_valid;
        input       [QUADWORD-1:0]      int_address;
        input       [STQ_SIZE-1:0]      int_valid_store_inst_on_disp;
        input                           int_request_sent_to_dcache;
        input                           int_ldl_mem;

        if (lsq1.ldb.valid_inst[entry] != int_valid_inst) exit_on_error();
        if (lsq1.ldb.dest_reg[entry] != int_dest_reg && int_valid_inst) exit_on_error();
        if (lsq1.ldb.address_valid[entry] != int_address_valid && int_valid_inst) exit_on_error();
        if (lsq1.ldb.address[entry] != int_address && int_valid_inst && int_address_valid) exit_on_error();
        if (lsq1.ldb.valid_store_inst_on_disp[entry] != int_valid_store_inst_on_disp && int_valid_inst) exit_on_error();
        if (lsq1.ldb.request_sent_to_dcache[entry] != int_request_sent_to_dcache && int_valid_inst) exit_on_error();
        if (lsq1.ldb.ldl_mem[entry] != int_ldl_mem && int_valid_inst) exit_on_error();

    endtask

    task stq_check_ptr;
        input       [STQ_BITS-1:0]      int_head;
        input       [STQ_BITS-1:0]      int_tail;
        input                           int_full;

        if (lsq1.store_queue1.head != int_head) exit_on_error();
        if (lsq1.store_queue1.tail != int_tail) exit_on_error();
        if (lsq1.store_queue1.full != int_full) exit_on_error();

    endtask


    task stq_check_cdb;
        input                           valid;
        input       [PRF_BITS-1:0]      prf;
        input       [QUADWORD-1:0]      result;

        if (stored_valid != valid)  exit_on_error();
        if (stored_prf != prf && valid) exit_on_error();
        if (stored_result != result && valid) exit_on_error();

    endtask

    task stq_check_dcache;
        input                           valid;
        input                           write;
        input       [QUADWORD-1:0]      address;
        input       [QUADWORD-1:0]      data;

        if (stq_data_to_dcache_valid != valid)  exit_on_error();
        if (stq_write_to_dcache != write && valid) exit_on_error();
        if (stq_address_to_dcache != address && valid) exit_on_error();
        if (stq_data_to_dcache != data && valid) exit_on_error();

    endtask

    task ldb_check_cdb;
        input       [LDB_BITS-1:0]      entry;
        input                           valid;
        input       [PRF_BITS-1:0]      prf;
        input       [QUADWORD-1:0]      result;

        if (load_data_valid[entry] != valid)  exit_on_error();
        if (load_data_prf_tag[entry] != prf && valid) exit_on_error();
        if (load_data[entry] != result && valid) exit_on_error();

    endtask

    task ldb_check_dcache;
        input                           way;
        input                           valid;
        input                           read;
        input       [QUADWORD-1:0]      address;

        if (ldb_addr_to_dcache_valid[way] != valid)  exit_on_error();
        if (ldb_read_from_dcache[way] != read && valid) exit_on_error();
        if (ldb_address_to_dcache[way] != address && valid) exit_on_error();

    endtask
/*
    task check_ldb;
        input                           way;
        input       [STQ_SIZE-1:0]      valid_on_disp;
        input       [STQ_SIZE-1:0]      data_valid;
        input       [STQ_SIZE-1:0]      address_valid;
        input       [STQ_SIZE-1:0]      committing_inst;
        input       [STQ_BITS-1:0]      head;
        input       [STQ_BITS-1:0]      tail;

        if (stq_valid_on_disp[way] != valid_on_disp)  exit_on_error();
        if (stq_data_valid != data_valid)  exit_on_error();
        if (stq_address_valid != address_valid)  exit_on_error();
        if (stq_committing_inst != committing_inst)  exit_on_error();
        if (stq_head != head)  exit_on_error();
        if (stq_tail != tail)  exit_on_error();

    endtask
*/
    initial
    begin
        $monitor("\n\n===WAY_0 INPUTS DISPATCH===\nTime= %d  valid_inst= %b wr_mem= %b rd_mem= %b stc_mem= %b ldl_mem= %b rat_dest_valid= %b rat_dest= %d valid= %b data= %d\n", $time, id_disp_valid_inst[0], id_disp_wr_mem[0], id_disp_rd_mem[0], id_disp_stc_mem[0], id_disp_ldl_mem[0], id_disp_rat_dest_valid[0], id_disp_rat_dest[0], store_data_valid[0], store_data[0],
                "===WAY_0 INPUTS ADDRESS FROM EX-WB===\nTime= %d valid= %b prf= %d address= %h\n", $time, ex_calc_address_valid[0], ex_calc_address_prf[0], ex_calc_address[0],
                 "===WAY_0 INPUTS DATA FROM CDB===\nTime= %d valid= %b prf= %d data\n", $time, cdb_valid[0], cdb_prf[0], cdb_data[0],
                 "===WAY_0 INPUTS FROM DCACHE===\nTime= %d stq_dcache_stall= %b ldb_dcache_stall= %b data_valid= %b data= %d addr= %h\n", $time, stq_dcache_stall, ldb_dcache_stall[0], ldb_dcache_load_data_valid[0], ldb_dcache_load_data[0], ldb_dcache_load_addr[0],
                 "===WAY_0 OUTPUTS STQ TO CDB===\nTime= %d valid= %b prf= %d result= %d\n", $time, stored_valid, stored_prf, stored_result,
                 "===WAY_0 OUTPUTS STQ TO DCACHE===\nTime= %d valid= %b write= %b address= %h data= %d\n", $time, stq_data_to_dcache_valid, stq_write_to_dcache, stq_address_to_dcache, stq_data_to_dcache,
                 "===WAY_0 OUTPUTS LDB TO DCACHE===\nTime= %d valid= %b read= %b address= %h\n", $time, ldb_addr_to_dcache_valid[0], ldb_read_from_dcache[0], ldb_address_to_dcache[0],
                 "\n\n===WAY_1 INPUTS DISPATCH===\nTime= %d  valid_inst= %b wr_mem= %b rd_mem= %b stc_mem= %b ldl_mem= %b rat_dest_valid= %b rat_dest= %d valid= %b data= %d\n", $time, id_disp_valid_inst[1], id_disp_wr_mem[1], id_disp_rd_mem[1], id_disp_stc_mem[1], id_disp_ldl_mem[1], id_disp_rat_dest_valid[1], id_disp_rat_dest[1], store_data_valid[1], store_data[1],
                "===WAY_0 INPUTS ADDRESS FROM EX-WB===\nTime= %d valid= %b prf= %d address= %h\n", $time, ex_calc_address_valid[1], ex_calc_address_prf[1], ex_calc_address[1],
                 "===WAY_0 INPUTS DATA FROM CDB===\nTime= %d valid= %b prf= %d data\n", $time, cdb_valid[1], cdb_prf[1], cdb_data[1],
                 "===WAY_0 INPUTS FROM DCACHE===\nTime= %d stq_dcache_stall= %b ldb_dcache_stall= %b data_valid= %b data= %d addr= %h\n", $time, stq_dcache_stall, ldb_dcache_stall[1], ldb_dcache_load_data_valid[1], ldb_dcache_load_data[1], ldb_dcache_load_addr[1],
                 "===WAY_0 OUTPUTS STQ TO CDB===\nTime= %d valid= %b prf= %d result= %d\n", $time, stored_valid, stored_prf, stored_result,
                 "===WAY_0 OUTPUTS STQ TO DCACHE===\nTime= %d valid= %b write= %b address= %h data= %d\n", $time, stq_data_to_dcache_valid, stq_write_to_dcache, stq_address_to_dcache, stq_data_to_dcache,
                 "===WAY_0 OUTPUTS LDB TO DCACHE===\nTime= %d valid= %b read= %b address= %h\n", $time, ldb_addr_to_dcache_valid[1], ldb_read_from_dcache[1], ldb_address_to_dcache[1]);


        //Intialize
        clock                       = 0;
        reset                       = 1;
        id_disp_valid_inst          = 0;
        id_disp_wr_mem              = 0;
        id_disp_rd_mem              = 0;
        id_disp_stc_mem             = 0;
        id_disp_ldl_mem             = 0;
        id_disp_rat_dest            = 0;
        id_disp_rat_dest_valid      = 0;
        store_data                  = 0;
        store_data_valid            = 0;
        ex_calc_address_valid       = 0;
        ex_calc_address_prf         = 0;
        ex_calc_address             = 0;
        cdb_valid                   = 0;
        cdb_prf                     = 0;
        cdb_data                    = 0;
        cdb_stall                   = 0;
        stq_dcache_stall            = 0;
        ldb_dcache_stall            = 0;
        ldb_dcache_load_data_valid  = 0;
        ldb_dcache_load_data        = 0;
        ldb_dcache_load_addr        = 0;
        store_requested             = 0;
        nuke                        = 0;

        //CHECK reset
        @(negedge clock);
        reset = 0;
        stq_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd0, `FALSE);
        stq_check_cdb(`FALSE, 7'd0, 64'd0);
        stq_check_dcache(`FALSE, `FALSE, 64'd0, 64'd0);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd2, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd3, `FALSE, 7'd0, 64'd0);
        ldb_check_dcache(`WAY_1, `FALSE, `FALSE, 64'd0);
        ldb_check_dcache(`WAY_0, `FALSE, `FALSE, 64'd0);
        if (stq_stall != 0) exit_on_error();
        if (ldb_stall != 0) exit_on_error();
        $display("\n@@@Passed reset\n");
        //exit_on_error();

        ////////////////////////////
        //dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);   //Dispatch a store

        // Dispatch load in both ways
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd95, `FALSE, 64'd0);   //Dispatch a load
        dispatch(`WAY_0, `TRUE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd94, `FALSE, 64'd0);   //Dispatch a load
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd95, `FALSE, 64'd0);   //Dispatch a load
        dispatch(`WAY_0, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd94, `FALSE, 64'd0);   //Dispatch a load
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        $display("\n@@@Passed dispatch both ways\n");
        //exit_on_error();

        // Grab address from ex-wb
        address_from_exwb(`WAY_1, `TRUE, 7'd94, 64'hbad);
        address_from_exwb(`WAY_0, `TRUE, 7'd95, 64'hdad);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'hbad);
        address_from_exwb(`WAY_0, `FALSE, 7'd95, 64'hdad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `FALSE, `FALSE);
        ldb_check_dcache(`WAY_1, `TRUE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `TRUE, `TRUE, 64'hbad);
        @(negedge clock);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed grab address from ex-wb\n");
        //exit_on_error();

        //////////////////////////////////////////////////////////////////
        /*
        // yell load result on cdb (one way only) -working
        @(negedge clock);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `TRUE, 64'd100, 64'hbad);
        #5;
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd2, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd3, `TRUE, 7'd94, 64'd100);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `FALSE, 64'd100, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `FALSE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed yell load result on cdb\n");
        */

        ////////////////////
        /*
        // yell load result on cdb (both ways) working
        @(negedge clock);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `TRUE, 64'd200, 64'hdad);
        dcache_to_ldb(`WAY_0, `FALSE, `TRUE, 64'd100, 64'hbad);
        #5;
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd2, `TRUE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `TRUE, 7'd94, 64'd100);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `FALSE, 64'd100, 64'hbad);
        dcache_to_ldb(`WAY_0, `FALSE, `FALSE, 64'd100, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `FALSE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `FALSE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed yell load result on cdb\n");
        */

        ////////////////////
        /*
        // yell load result on cdb (both ways) with cdb stalling one entry - working
        @(negedge clock);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `TRUE, 64'd200, 64'hdad);
        dcache_to_ldb(`WAY_0, `FALSE, `TRUE, 64'd100, 64'hbad);
        cdb_stall = 4'b0100;
        #5;
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd2, `TRUE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `TRUE, 7'd94, 64'd100);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `FALSE, 64'd100, 64'hbad);
        dcache_to_ldb(`WAY_0, `FALSE, `FALSE, 64'd100, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd3, `FALSE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed yell load result on cdb\n");
        */

        ////////////////////
        /*
        // yell load result on cdb (both ways) with cdb stalling both entries - working
        @(negedge clock);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `TRUE, 64'd200, 64'hdad);
        dcache_to_ldb(`WAY_0, `FALSE, `TRUE, 64'd100, 64'hbad);
        cdb_stall = 4'b1100;
        #5;
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd2, `TRUE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `TRUE, 7'd94, 64'd100);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);   //not sending load request to dcache
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        @(negedge clock);
        cdb_stall = 4'b0000;
        dcache_to_ldb(`WAY_1, `FALSE, `FALSE, 64'd100, 64'hbad);
        dcache_to_ldb(`WAY_0, `FALSE, `FALSE, 64'd100, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `FALSE, `FALSE);
        ldb_check_dcache(`WAY_1, `TRUE, `TRUE, 64'hdad);    //sending load request to dcache
        ldb_check_dcache(`WAY_0, `TRUE, `TRUE, 64'hbad);
        @(negedge clock);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed yell load result on cdb\n");
        */

        ////////////////////
        /*
        // yell load result on cdb (both ways) with cdb stalling both entries
        // what if the dcache stalls when the lsq gives out request to the dcache
        @(negedge clock);
        @(negedge clock);
        dcache_to_ldb(`WAY_1, `FALSE, `TRUE, 64'd200, 64'hdad);
        dcache_to_ldb(`WAY_0, `FALSE, `TRUE, 64'd100, 64'hbad);
        cdb_stall = 4'b1100;
        #5;
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd2, `TRUE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `TRUE, 7'd94, 64'd100);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);   //not sending load request to dcache
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        @(negedge clock);
        cdb_stall = 4'b0000;
        dcache_to_ldb(`WAY_1, `FALSE, `FALSE, 64'd100, 64'hbad);
        dcache_to_ldb(`WAY_0, `FALSE, `FALSE, 64'd100, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `FALSE, `FALSE);
        ldb_check_dcache(`WAY_1, `TRUE, `TRUE, 64'hdad);    //sending load request to dcache
        ldb_check_dcache(`WAY_0, `TRUE, `TRUE, 64'hbad);
        ldb_dcache_stall = 2'b11;
        @(negedge clock);
        ldb_dcache_stall = 2'b00;
        ldb_check_dcache(`WAY_1, `TRUE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `TRUE, `TRUE, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `FALSE, `FALSE);
        @(negedge clock);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed yell load result on cdb\n");
        */

        //////////////////////////////////////////////////////////////////
        /*
        // Dispatch store in way 0, load in way 1 - working
        // This testcase and the following one checks the 'valid on disp'
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_1, `TRUE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        stq_check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed dispatch both ways\n");
        //exit_on_error();
        */

        // Load dependent on store, address matches, data forwarding
        ////////////////////
        /*
        // Dispatch store in way 1, load in way 0 - working
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `TRUE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        stq_check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `FALSE, 64'd0, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed dispatch both ways\n");
        //exit_on_error();


        @(negedge clock);
        // Grab address from ex-wb - load dependent on store, address of load and store matches, data forwarding
        address_from_exwb(`WAY_1, `TRUE, 7'd92, 64'habc);
        address_from_exwb(`WAY_0, `TRUE, 7'd93, 64'habc);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'hbad);
        address_from_exwb(`WAY_0, `FALSE, 7'd95, 64'hdad);
        stq_check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd500, `TRUE, 64'habc, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `TRUE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        @(negedge clock);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd92, `TRUE, 64'habc, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed data forwading\n");
        //exit_on_error();
        */

        // Load dependent on store, address matches, data forwarding happens when store gets its data from cdb
        ////////////////////
        /*
        // Dispatch store in way 1, load in way 0 - working
        // data of store unavailable at the time of dispatch
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `FALSE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `TRUE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        stq_check_entry(2'd0, `TRUE, 7'd93, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `FALSE, 64'd0, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed dispatch both ways\n");
        //exit_on_error();

        @(negedge clock);
        // Grab address from ex-wb - load dependent on store, address of load and store matches, data forwarding not happening as of now, cuz the store is waiting for its data
        address_from_exwb(`WAY_1, `TRUE, 7'd92, 64'habc);
        address_from_exwb(`WAY_0, `TRUE, 7'd93, 64'habc);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'hbad);
        address_from_exwb(`WAY_0, `FALSE, 7'd95, 64'hdad);
        stq_check_entry(2'd0, `TRUE, 7'd93, `FALSE, 64'd0, `TRUE, 64'habc, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        @(negedge clock);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        // the store instruction gets its data now from cdb, data forwarding happens
        data_from_cdb(`WAY_1, `TRUE, 7'd92, 64'd700);
        data_from_cdb(`WAY_0, `TRUE, 7'd93, 64'd600);
        #5;
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `TRUE, 7'd92, 64'd600);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        @(negedge clock);
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd700);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd600);
        stq_check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd600, `TRUE, 64'habc, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `FALSE, 7'd92, `TRUE, 64'habc, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd600);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        $display("\n@@@Passed data forwading\n");
        //exit_on_error();
        */
        ////////////////////

        /*
        // Load dependent on store, address does not match, load goes to cache after address resolves
        // Dispatch store in way 1, load in way 0 - working
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `TRUE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        stq_check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `FALSE, 64'd0, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed dispatch both ways\n");
        //exit_on_error();

        @(negedge clock);
        // Grab address from ex-wb - load dependent on store, address of load and store matches, data forwarding
        address_from_exwb(`WAY_1, `TRUE, 7'd92, 64'habc);
        address_from_exwb(`WAY_0, `TRUE, 7'd93, 64'hbcd);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'hbad);
        address_from_exwb(`WAY_0, `FALSE, 7'd95, 64'hdad);
        stq_check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd500, `TRUE, 64'hbcd, `FALSE);
        stq_check_entry(2'd1, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd1, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0001, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        ldb_check_dcache(`WAY_0, `TRUE, `TRUE, 64'habc);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        @(negedge clock);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0001, `TRUE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed data forwading\n");
        //exit_on_error();
        */
        ////////////////////


        // Load dependent on store, address
        // Dispatch store in way 1, load in way 0 - working
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd90, `TRUE, 64'd800);   //Dispatch a store
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd91, `TRUE, 64'd900);   //Dispatch a store
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `TRUE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `FALSE, `FALSE, `TRUE, 7'd93, `TRUE, 64'd500);   //Dispatch a store
        dispatch(`WAY_0, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd0);   //Dispatch a load
        stq_check_entry(2'd0, `TRUE, 7'd90, `TRUE, 64'd800, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd1, `TRUE, 7'd91, `TRUE, 64'd900, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd3, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `FALSE, 64'd0, 4'b0111, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed dispatch both ways\n");
        //exit_on_error();

        @(negedge clock);
        // Grab address from ex-wb - load dependent on store, address of load and store matches, data forwarding
        address_from_exwb(`WAY_1, `TRUE, 7'd92, 64'habc);
        address_from_exwb(`WAY_0, `TRUE, 7'd90, 64'habc);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'hbad);
        address_from_exwb(`WAY_0, `FALSE, 7'd95, 64'hdad);
        stq_check_entry(2'd0, `TRUE, 7'd90, `TRUE, 64'd800, `TRUE, 64'habc, `FALSE);
        stq_check_entry(2'd1, `TRUE, 7'd91, `TRUE, 64'd900, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd3, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0111, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'habc);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        @(negedge clock);
        ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ldb_check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'habc, 4'b0111, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed data forwading\n");
        $display("\n@@@Passed\n\n\n\n\n");

        //exit_on_error();
        address_from_exwb(`WAY_1, `TRUE, 7'd93, 64'habc);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'hbad);
        stq_check_entry(2'd0, `TRUE, 7'd90, `TRUE, 64'd800, `TRUE, 64'habc, `FALSE);
        stq_check_entry(2'd1, `TRUE, 7'd91, `TRUE, 64'd900, `FALSE, 64'd0, `FALSE);
        stq_check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd500, `TRUE, 64'habc, `FALSE);
        stq_check_entry(2'd3, `FALSE, 7'd0, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        stq_check_ptr(2'd0, 2'd3, `FALSE);
        ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        ////////////////////ldb_check_entry(2'd1, `FALSE, 7'd92, `TRUE, 64'habc, 4'b0111, `FALSE, `FALSE);
        ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        //ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'habc);
        //ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hbad);
        //ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        //ldb_check_cdb(2'd1, `TRUE, 7'd92, 64'd500);
        //ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        //ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        //ldb_check_dcache(`WAY_1, `FALSE, `TRUE, 64'hdad);
        //ldb_check_dcache(`WAY_0, `FALSE, `TRUE, 64'hbad);
        //ldb_check_cdb(2'd0, `FALSE, 7'd0, 64'd0);
        //ldb_check_cdb(2'd1, `FALSE, 7'd92, 64'd500);
        //ldb_check_cdb(2'd2, `FALSE, 7'd95, 64'd200);
        //ldb_check_cdb(2'd3, `FALSE, 7'd94, 64'd100);
        //ldb_check_entry(2'd0, `FALSE, 7'd0, `FALSE, 64'd0, 4'b0000, `FALSE, `FALSE);
        //ldb_check_entry(2'd1, `FALSE, 7'd92, `TRUE, 64'habc, 4'b0111, `FALSE, `FALSE);
        //ldb_check_entry(2'd2, `TRUE, 7'd95, `TRUE, 64'hdad, 4'b0000, `TRUE, `FALSE);
        //ldb_check_entry(2'd3, `TRUE, 7'd94, `TRUE, 64'hbad, 4'b0000, `TRUE, `FALSE);
        $display("\n@@@Passed data forwading\n");






/*
        // Dispatch in way 1 only with invalid instr
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1 only with invalid instr\n");

        // Dispatch in way 1 only with valid instr but not a store
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `FALSE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1 only with valid instr but not store\n");

        // Dispatch in way 1 only with valid store instr but invalid rat dest
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `FALSE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1 only with valid store instr but invalid rat dest\n");

        // Dispatch in way 1 only
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        $display("\n@@@Passed dispatch in way 1\n");
        //exit_on_error();

        // Dispatch in way 0 only with invalid instr
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        $display("\n@@@Passed dispatch in way 0 only with invalid instr\n");

        // Dispatch in way 0 only with valid instr but not a store
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `FALSE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        $display("\n@@@Passed dispatch in way 0 only with valid instr but not store\n");

        // Dispatch in way 0 only with valid store instr but invalid rat dest
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `FALSE, 7'd94, `TRUE, 64'd100);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd1, `FALSE);
        $display("\n@@@Passed dispatch in way 0 only with valid store instr but invalid rat dest\n");

        // Dispatch in way 0 only
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd2, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        check_ldb(`WAY_1, 4'b0011, 4'b0111, 4'b0000, 4'b0000, 2'd0, 2'd3);      //%%%%%%%%%%%%%%%%%%%%%%%%%
        check_ldb(`WAY_0, 4'b0011, 4'b0111, 4'b0000, 4'b0000, 2'd0, 2'd3);
        $display("\n@@@Passed dispatch in way 0\n");
        //exit_on_error();

        // Dispatch in both ways but data not available in prf at the time of dispatch
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd93, `FALSE, 64'd100);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd200);
        check_ldb(`WAY_1, 4'b0011, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd2);
        check_ldb(`WAY_0, 4'b0011, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd2);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        check_ldb(`WAY_1, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed dispatch in both ways but data not available in prf\n");
        //exit_on_error();

        // Check stall in way 1
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd91, `TRUE, 64'd300);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b10) exit_on_error();
        $display("\n@@@Passed stall in way 1\n");
        //exit_on_error();

        // Check stall in way 0
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd91, `TRUE, 64'd300);
        @(negedge clock);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b01) exit_on_error();
        $display("\n@@@Passed stall in way 0\n");
        //exit_on_error();

        // Check stall both ways
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd90, `FALSE, 64'd100);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd89, `FALSE, 64'd200);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b11) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed stall both ways\n");
        //exit_on_error();

        // Do not grab data from CDB
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        @(negedge clock);
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b0011, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed do not grab data from cdb\n");

        // Grab data from CDB
        @(negedge clock);
        data_from_cdb(`WAY_1, `TRUE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `TRUE, 7'd93, 64'd400);
        @(negedge clock);
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        if (stq_stall != 2'b00) exit_on_error();
        $display("\n@@@Passed do not grab data from cdb\n");
        //exit_on_error();

        // Commit a store which is still waiting for its address
        store_requested = `TRUE;
        @(negedge clock);
        store_requested = `FALSE;
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `FALSE, 64'd0, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed commit a store which is still waiting for address\n");
        //exit_on_error();

        // Grab address from EX-WB
        address_from_exwb(`WAY_1, `TRUE, 7'd93, 64'hbad);
        address_from_exwb(`WAY_0, `TRUE, 7'd95, 64'hdad);
        @(negedge clock);
        address_from_exwb(`WAY_1, `TRUE, 7'd94, 64'head);
        address_from_exwb(`WAY_0, `TRUE, 7'd92, 64'hcad);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `TRUE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b0101, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b0101, 4'b0000, 2'd0, 2'd0);
        @(negedge clock);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'head);
        address_from_exwb(`WAY_0, `FALSE, 7'd92, 64'hcad);
        check_ptr(2'd0, 2'd0, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `TRUE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd95, `TRUE, 64'd100, `TRUE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_ldb(`WAY_1, 4'b1111, 4'b1111, 4'b1111, 4'b0000, 2'd0, 2'd0);
        check_ldb(`WAY_0, 4'b1111, 4'b1111, 4'b1111, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed grab address from EX-WB\n");
        //exit_on_error();

        // Commit a store
        @(negedge clock);
        store_requested = `TRUE;
        #5;
        check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);
        @(negedge clock);
        store_requested = `FALSE;
        check_ptr(2'd1, 2'd0, `FALSE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `TRUE, 64'head, `FALSE);
        check_entry(2'd0, `FALSE, 7'd95, `TRUE, 64'd100, `TRUE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        //check_ldb(`WAY_1, 4'b1110, 4'b1110, 4'b1110, 4'b0001, 2'd1, 2'd0);
        //check_ldb(`WAY_0, 4'b1110, 4'b1110, 4'b1110, 4'b0001, 2'd1, 2'd0);
        //check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        #5;
        check_dcache(`FALSE, `TRUE, 64'hdad, 64'd100);
        $display("\n@@@Passed commit a store\n");
        //exit_on_error();

        // Dispatch in way 1 only
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd91, `TRUE, 64'd600);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd1, 2'd1, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd94, `TRUE, 64'd200, `TRUE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        $display("\n@@@Passed dispatch in way 1\n");
        //exit_on_error();

        // Commit and dispatch in same cycle when the queue is full - no stq_stall
        @(negedge clock);
        store_requested = `TRUE;
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd90, `TRUE, 64'd700);
        #5;
        check_dcache(`TRUE, `TRUE, 64'head, 64'd200);
        @(negedge clock);
        store_requested = `FALSE;
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        check_ptr(2'd2, 2'd2, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        //check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //#5;
        //check_dcache(`FALSE, `TRUE, 64'hdad, 64'd100);
        $display("\n@@@Passed commit and dispatch in same cycle\n");
        //exit_on_error();

        // Commit a store
        @(negedge clock);
        store_requested = `TRUE;
        #5;
        check_dcache(`TRUE, `TRUE, 64'hbad, 64'd400);
        @(negedge clock);
        store_requested = `FALSE;
        check_ptr(2'd3, 2'd2, `FALSE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `FALSE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        //check_dcache(`TRUE, `TRUE, 64'hdad, 64'd100);  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        #5;
        check_dcache(`FALSE, `TRUE, 64'hdad, 64'd100);
        $display("\n@@@Passed commit a store\n");
        //exit_on_error();

        // Dispatch both ways when there is just one empty spot in the queue
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd89, `TRUE, 64'd800);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd88, `TRUE, 64'd900);
        @(posedge clock);
        if (stq_stall != 2'b01) exit_on_error();
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd3, 2'd3, `TRUE);
        check_entry(2'd3, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `TRUE, 7'd89, `TRUE, 64'd800, `FALSE, 64'hbad, `FALSE);
        check_entry(2'd1, `TRUE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `TRUE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        //if (stq_stall != 2'b11) exit_on_error();  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        $display("\n@@@Passed dispatch in both ways when there is just one empty spot in the queue\n");
        //exit_on_error();

        // Nuke - passed 01, 10, 11
        @(negedge clock);
        nuke = 2'b10;
        @(negedge clock);
        nuke = 2'b00;
        //exit_on_error();
        check_ptr(2'd0, 2'd0, `FALSE);
        check_entry(2'd3, `FALSE, 7'd92, `TRUE, 64'd500, `TRUE, 64'hcad, `FALSE);
        check_entry(2'd2, `FALSE, 7'd89, `TRUE, 64'd800, `FALSE, 64'hbad, `FALSE);
        check_entry(2'd1, `FALSE, 7'd90, `TRUE, 64'd700, `FALSE, 64'head, `FALSE);
        check_entry(2'd0, `FALSE, 7'd91, `TRUE, 64'd600, `FALSE, 64'hdad, `FALSE);
        if (stq_stall != 2'b00) exit_on_error();
        check_cdb(`FALSE, 7'd0, 64'd0);
        check_dcache(`FALSE, `FALSE, 64'd0, 64'd0);
        check_ldb(`WAY_1, 4'b0000, 4'b0000, 4'b0000, 4'b0000, 2'd0, 2'd0);
        $display("\n@@@Passed nuke\n");


        // Dispatch in both ways but data not available in prf at the time of dispatch
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, `TRUE, `FALSE, `TRUE, 7'd93, `FALSE, 64'd100);
        dispatch(`WAY_0, `TRUE, `TRUE, `FALSE, `TRUE, 7'd92, `FALSE, 64'd200);
        @(negedge clock);
        dispatch(`WAY_1, `FALSE, `TRUE, `FALSE, `TRUE, 7'd95, `TRUE, 64'd100);
        dispatch(`WAY_0, `FALSE, `TRUE, `FALSE, `TRUE, 7'd94, `TRUE, 64'd200);
        check_ptr(2'd0, 2'd2, `FALSE);
        check_entry(2'd3, `FALSE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `FALSE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd92, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        check_entry(2'd0, `TRUE, 7'd93, `FALSE, 64'd0, `FALSE, 64'd0, `FALSE);
        $display("\n@@@Passed dispatch in both ways but data not available in prf\n");
        //exit_on_error();

        // Grab data, address, and try to commit in the same cycle - able to commit
        // Also, Checked the same case when dcache stall high - sends output to
        // dcache, but the internal state do not change(as in the pointer doesnot update and the committing entry is not removed)
        @(negedge clock);
        address_from_exwb(`WAY_1, `TRUE, 7'd93, 64'hbcd);
        address_from_exwb(`WAY_0, `TRUE, 7'd92, 64'habc);
        data_from_cdb(`WAY_1, `TRUE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `TRUE, 7'd93, 64'd400);
        store_requested = `TRUE;
        dcache_stall = 1'b1;                           //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        @(posedge clock);
        check_dcache(`TRUE, `TRUE, 64'hbcd, 64'd400);
        @(negedge clock);
        dcache_stall = 1'b0;
        data_from_cdb(`WAY_1, `FALSE, 7'd92, 64'd500);
        data_from_cdb(`WAY_0, `FALSE, 7'd93, 64'd400);
        address_from_exwb(`WAY_1, `FALSE, 7'd94, 64'head);
        address_from_exwb(`WAY_0, `FALSE, 7'd92, 64'hcad);
        store_requested = `FALSE;
        check_ptr(2'd0, 2'd2, `FALSE);
        check_entry(2'd3, `FALSE, 7'd92, `FALSE, 64'd92, `FALSE, 64'd0, `FALSE);
        check_entry(2'd2, `FALSE, 7'd93, `FALSE, 64'd93, `FALSE, 64'd0, `FALSE);
        check_entry(2'd1, `TRUE, 7'd92, `TRUE, 64'd500, `TRUE, 64'habc, `FALSE);
        check_entry(2'd0, `TRUE, 7'd93, `TRUE, 64'd400, `TRUE, 64'hbcd, `FALSE);
        $display("\n@@@Passed grab data, address, and try to commit in the same cycle\n");
*/
        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

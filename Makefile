# Given no targets, 'make' will default to building 'simv', the simulated version
# of the pipeline

# make          <- compile simv if needed

# As shortcuts, any of the following will build if necessary and then run the
# specified target

# make sim      <- runs simv (after compiling simv if needed)
# make vis      <- runs the "visual" debugger (visual/)
# make dve      <- runs int_simv interactively (after compiling it if needed)
# make syn      <- runs syn_simv (after synthesizing if needed then
#                                 compiling synsimv if needed)
# make syn_dve  <- runs DVE on synthesized code


# make clean    <- remove files created during compilations (but not synthesis)
# make nuke     <- remove all files created during compilation and synthesis
#
# To compile additional files, add them to the TESTBENCH or SIMFILES as needed
# Every .vg file will need its own rule and one or more synthesis scripts
# The information contained here (in the rules for those vg files) will be
# similar to the information in those scripts but that seems hard to avoid.
#

################################################################################
## CONFIGURATION
################################################################################

VCS = SW_VCS=2015.09 vcs -sverilog +vc -Mupdate -line -full64 +define+CLOCK_PERIOD=$(CLOCK_PERIOD)
LIB = /afs/umich.edu/class/eecs470/lib/verilog/lec25dscc25.v

SYNTH_DIR = ./synth

PROGRAM = test_progs/given/objsort.s
ASSEMBLED = program.mem
ASSEMBLER = vs-asm

# SIMULATION CONFIG
			  #fetch_module/prediction_holder/btb_branch_pred/gshare_branch_pred/gshare_branch_pred_holder.v \
           	  fetch_module/prediction_holder/btb_branch_pred/gshare_branch_pred/branch_pred/gshare_branch_pred.v fetch_module/prediction_holder/btb_branch_pred/gshare_branch_pred/rbranch_pred/r_gshare_branch_pred.v \

			  #fetch_module/prediction_holder/btb_branch_pred/branch_pred/branch_pred.v \

HEADERS     = $(wildcard *.vh)
TESTBENCH   = $(wildcard testbench/*.v)
TESTBENCH  += $(wildcard testbench/*.c)
PIPEFILES   = fetch_module/fetch.v fetch_module/prediction_holder/prediction_holder.v  fetch_module/prediction_holder/btb_branch_pred/btb_branch_pred.v \
           	  fetch_module/prediction_holder/btb_branch_pred/btb/btb.v \
			  fetch_module/prediction_holder/ras_holder/ras_holder.v fetch_module/prediction_holder/ras_holder/ras/ras.v fetch_module/prediction_holder/ras_holder/rras/rras.v \
			  fetch_module/prediction_holder/btb_branch_pred/branch_pred/branch_pred.v \
			  fetch_module/prediction_holder/btb_branch_pred/gshare_branch_pred/gshare_branch_pred_holder.v \
           	  fetch_module/prediction_holder/btb_branch_pred/gshare_branch_pred/branch_pred/gshare_branch_pred.v fetch_module/prediction_holder/btb_branch_pred/gshare_branch_pred/rbranch_pred/r_gshare_branch_pred.v \
			  decode_module/decode_holder.v decode_module/decode/decode.v decode_module/decode/instruction_decoder.v \
           	  decode_module/rat_holder/rat_holder.v decode_module/rat_holder/rat_module/rat.v decode_module/rat_holder/rrat_module/rrat.v \
           	  common/ps.v common/pe_n.v common/pe.v dispatch_module/dispatch_holder.v dispatch_module/prf_module/prf.v dispatch_module/rob_module/rob.v dispatch_module/rs_module/rs.v dispatch_module/rs_module/rs_entry.v \
           	  common/ps_select_n.v execute_module/execute_holder.v execute_module/alu_module/alu.v execute_module/branch_module/branch_ex.v \
           	  execute_module/mem_addr_module/mem_addr_ex.v execute_module/mult_module/mult_stage.v execute_module/mult_module/pipe_mult.v cdb_module/cdb_queue.v \
		      lsq_module/store_queue_module/store_queue.v lsq_module/load_buffer_module/load_buffer.v lsq_module/lsq.v \
			  pipeline.v # verilog/icache.v
CACHEFILES  = cache_module/cache_holder.v cache_module/cache/mshr.v cache_module/cache/icache_controller.v cache_module/cache/icache.v cache_module/cache/encoder.v \
		      cache_module/cache/dcache.v cache_module/cache/dcache_controller.v

SIMFILES    = $(PIPEFILES) $(CACHEFILES)

# SYNTHESIS CONFIG

export HEADERS
export PIPEFILES
export CACHEFILES

export CACHE_NAME = cache
export PIPELINE_NAME = pipeline

PIPELINE  = $(SYNTH_DIR)/$(PIPELINE_NAME).vg
SYNFILES  = $(PIPELINE) $(SYNTH_DIR)/$(PIPELINE_NAME)_svsim.sv
CACHE     = $(SYNTH_DIR)/$(CACHE_NAME).vg

# Passed through to .tcl scripts:
export CLOCK_NET_NAME = clock
export RESET_NET_NAME = reset
export CLOCK_PERIOD = 7.693

################################################################################
## RULES
################################################################################

# Default target:
all:    sim

.PHONY: all

# Simulation:

sim:	simv $(ASSEMBLED)
	./simv | tee sim_program.out

simv:	$(HEADERS) $(SIMFILES) $(TESTBENCH)
	$(VCS) $^ -o simv

.PHONY: sim

# Programs

#TESTING TODO
print-%  : ; @echo !!!!!!!!!Current file running!!!!!!!!!!! = $($*)

$(ASSEMBLED):	$(PROGRAM) Makefile print-PROGRAM
	./$(ASSEMBLER) < $< > $@

# Synthesis

#$(CACHE): $(CACHEFILES) $(SYNTH_DIR)/$(CACHE_NAME).tcl
#	cd $(SYNTH_DIR) && dc_shell-t -f ./$(CACHE_NAME).tcl | tee $(CACHE_NAME)_synth.out

# $(PIPELINE): $(SIMFILES) $(CACHE) $(SYNTH_DIR)/$(PIPELINE_NAME).tcl
# 	cd $(SYNTH_DIR) && dc_shell-t -f ./$(PIPELINE_NAME).tcl | tee $(PIPELINE_NAME)_synth.out
# 	echo -e -n 'H\n1\ni\n`timescale 1ns/100ps\n.\nw\nq\n' | ed $(PIPELINE)

$(PIPELINE): $(SIMFILES) $(SYNTH_DIR)/$(PIPELINE_NAME).tcl
	cd $(SYNTH_DIR) && dc_shell-t -f ./$(PIPELINE_NAME).tcl | tee $(PIPELINE_NAME)_synth.out
	echo -e -n 'H\n1\ni\n`timescale 1ns/100ps\n.\nw\nq\n' | ed $(PIPELINE)


syn:	syn_simv $(ASSEMBLED)
	./syn_simv | tee syn_program.out

syn_simv:	$(HEADERS) $(SYNFILES) $(TESTBENCH)
	$(VCS) $^ $(LIB) +define+SYNTH_TEST -o syn_simv

.PHONY: syn

# Debugging

dve_simv:	$(HEADERS) $(SIMFILES) $(TESTBENCH)
	$(VCS) +memcbk $^ -o $@ -gui

dve:	dve_simv $(ASSEMBLED)
	./$<

dve_syn_simv:	$(HEADERS) $(PIPELINE) $(TESTBENCH)
	$(VCS) +memcbk $^ $(LIB) +define+SYNTH_TEST -o $@ -gui

dve_syn:	dve_syn_simv $(ASSEMBLED)
	./$<

# For visual debugger
VISFLAGS = -lncurses
VISTESTBENCH = $(TESTBENCH:testbench.v=visual/visual_testbench.v) \
		testbench/visual/visual_c_hooks.c
vis_simv:	$(HEADERS) $(SIMFILES) $(VISTESTBENCH)
	$(VCS) $(VISFLAGS) $^ -o vis_simv
vis:	vis_simv $(ASSEMBLED)
	./vis_simv

.PHONY: dve syn_dve vis

clean:
	rm -rf simv simv.daidir csrc vcs.key ucli.key
	rm -rf vis_simv vis_simv.daidir
	rm -rf syn_simv syn_simv.daidir
	rm -f *.out
	rm -rf synsimv synsimv.daidir csrc vcdplus.vpd vcs.key synprog.out pipeline.out writeback.out vc_hdrs.h
	rm -rf dve_simv dve_syn_simv dve_simv.daidir DVEfiles dve_syn_simv.daidir

nuke:	clean
	rm -f $(SYNTH_DIR)/*.vg $(SYNTH_DIR)/*.rep $(SYNTH_DIR)/*.db $(SYNTH_DIR)/*.chk $(SYNTH_DIR)/command.log
	rm -f $(SYNTH_DIR)/*.out $(SYNTH_DIR)/*.ddc $(SYNTH_DIR)/*.log
	rm -rf $(SYNTH_DIR)/*.pvl $(SYNTH_DIR)/*.syn $(SYNTH_DIR)/*.mr
	rm -rf $(SYNTH_DIR)/*clog* $(SYNTH_DIR)/*.sv*
	rm -f $(ASSEMBLED)

.PHONY: clean nuke dve


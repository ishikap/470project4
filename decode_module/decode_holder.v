//This module was written assuming the IF/ID pipeline register will
//set if_id_valid_inst = 0 when a nuke signal is sent.
//
//Clock period:
//              -3/8/18: 4.43359375 < x < 4.439453125
//              -3/9/18: 4.310546875 < x < 4.31640625

`default_nettype none
`timescale 1ns/100ps
module decode_holder #(parameter NUM_PRF_ENTRIES = 96, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam NUM_ARF_ENTRIES = 32, localparam FREE_LIST_SIZE = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES, localparam FREE_LIST_BITS = $clog2(FREE_LIST_SIZE), localparam ARF_BITS = $clog2(NUM_ARF_ENTRIES)) (
    //INPUTS
    input   wire                                                clock,                  // system clock
    input   wire                                                reset,                  // system reset
    // From IF/ID Pipeline Register
    input   wire                        [1:0][QUADWORD/2-1:0]   if_id_IR,               // incoming instructions
    input   wire                        [1:0]                   if_id_valid_inst,
    // From ROB
    input   wire                        [1:0]                   rrat_commit_en,
    input   wire                        [1:0][PRF_BITS-1:0]     rrat_commit_prf_entry,
    input   wire                        [1:0][ARF_BITS-1:0]     rrat_commit_arf_idx,
    input   wire                        [1:0]                   nuke,



    //OUTPUTS
    output  logic                       [1:0][1:0]              id_opa_select_out,      // ALU opa mux select (ALU_OPA_xxx *)
    output  logic                       [1:0][1:0]              id_opb_select_out,      // ALU opb mux select (ALU_OPB_xxx *)
    output  logic                       [1:0][ARF_BITS-1:0]     id_arf_idx_out,         // Used by RoB

    output  logic                       [1:0][4:0]              id_alu_func_out,        // ALU function select (ALU_xxx *)
    output  logic                       [1:0]                   id_rd_mem_out,          // does inst read memory?
    output  logic                       [1:0]                   id_wr_mem_out,          // does inst write memory?
    output  logic                       [1:0]                   id_ldl_mem_out,         // load-lock inst?
    output  logic                       [1:0]                   id_stc_mem_out,         // store-conditional inst?
    output  logic                       [1:0]                   id_cond_branch_out,     // is inst a conditional branch?
    output  logic                       [1:0]                   id_uncond_branch_out,   // is inst an unconditional branch
                                                                                        // or jump?
    output  logic                       [1:0]                   id_branch_call,
    output  logic                       [1:0]                   id_branch_ret,
    output  logic                       [1:0]                   id_halt_out,
    output  logic                       [1:0]                   id_illegal_out,
    output  logic                       [1:0]                   id_valid_inst_out,       // is inst a valid instruction to be

    output  logic                       [1:0][PRF_BITS-1:0]     rat_rda_idx,
    output  logic                       [1:0][PRF_BITS-1:0]     rat_rdb_idx,
    output  logic                       [1:0][PRF_BITS-1:0]     rat_dest_idx,
    output  logic                       [1:0]                   rat_dest_valid,
    output  logic                       [1:0]                   rat_stall,
    output  logic        [FREE_LIST_SIZE-1:0][PRF_BITS-1:0]     rrat_free_list          // Goes to PRF to invalidate in nuke

    );

    wire   [1:0][ARF_BITS-1:0]          id_rda_idx;              // inst operand A register index
    wire   [1:0][ARF_BITS-1:0]          id_rdb_idx;              // inst operand B register index

    logic  [1:0][ARF_BITS-1:0]          id_rda_idx_corrected;
    logic  [1:0][ARF_BITS-1:0]          id_rdb_idx_corrected;

    always_comb begin
        id_rda_idx_corrected = id_rda_idx;
        id_rdb_idx_corrected = id_rdb_idx;
        for (int i=1; i>=0; --i) begin
            // Rename to R31 so won't read from invalid PRF if not needed
            if (id_opa_select_out[i] != `ALU_OPA_IS_REGA && !id_cond_branch_out[i] && !id_wr_mem_out[i] || id_halt_out[i]) begin
                id_rda_idx_corrected[i] = `ZERO_REG;
            end
            if (id_opb_select_out[i] != `ALU_OPB_IS_REGB || id_halt_out[i]) begin
                id_rdb_idx_corrected[i] = `ZERO_REG;
            end
        end
    end

    decode decode1(
        //INPUTS
        .clock(clock),
        .if_id_IR(if_id_IR),
        .if_id_valid_inst(if_id_valid_inst),
        //OUTPUTS
        .id_opa_select_out(id_opa_select_out),
        .id_opb_select_out(id_opb_select_out),
        .id_ra_idx(id_rda_idx),
        .id_rb_idx(id_rdb_idx),
        .id_dest_reg_idx_out(id_arf_idx_out),
        .id_alu_func_out(id_alu_func_out),
        .id_rd_mem_out(id_rd_mem_out),
        .id_wr_mem_out(id_wr_mem_out),
        .id_ldl_mem_out(id_ldl_mem_out),
        .id_stc_mem_out(id_stc_mem_out),
        .id_cond_branch_out(id_cond_branch_out),
        .id_uncond_branch_out(id_uncond_branch_out),
        .id_branch_call(id_branch_call),
        .id_branch_ret(id_branch_ret),
        .id_halt_out(id_halt_out),
        .id_illegal_out(id_illegal_out),
        .id_valid_inst_out(id_valid_inst_out)
    );

    rat_holder #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rat_holder1(
        //INPUTS
        .clock(clock),
        .reset(reset),
        .id_rda_idx(id_rda_idx_corrected),
        .id_rdb_idx(id_rdb_idx_corrected),
        .id_dest_idx(id_arf_idx_out),
        .id_valid_inst(id_valid_inst_out),
        .rrat_commit_en(rrat_commit_en),
        .rrat_commit_prf_entry(rrat_commit_prf_entry),
        .rrat_commit_arf_idx(rrat_commit_arf_idx),
        .nuke(nuke),
        //OUPUTS
        .rat_rda_idx(rat_rda_idx),
        .rat_rdb_idx(rat_rdb_idx),
        .rat_dest_idx(rat_dest_idx),
        .rat_dest_valid(rat_dest_valid),
        .rat_stall(rat_stall),
        .rrat_free_list(rrat_free_list)
    );

endmodule
`default_nettype wire

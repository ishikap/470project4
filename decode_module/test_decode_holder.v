//For: Testing Decode Holder
//Date: 3/6/17
`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define TRUE_2          2'b01
`define FALSE_2         2'b00
`define WAY_0           1'b0
`define WAY_1           1'b1
`define R0              5'd0
`define R1              5'd1
`define R2              5'd2
`define R3              5'd3
`define R4              5'd4
`define R5              5'd5
`define R6              5'd6
`define R7              5'd7
`define R8              5'd8
`define R9              5'd9
`define R10             5'd10
`define R11             5'd11
`define R12             5'd12
`define R13             5'd13
`define R14             5'd14
`define R15             5'd15
`define R16             5'd16
`define R17             5'd17
`define R18             5'd18
`define R19             5'd19
`define R20             5'd20
`define R21             5'd21
`define R22             5'd22
`define R23             5'd23
`define R24             5'd24
`define R25             5'd25
`define R26             5'd26
`define R27             5'd27
`define R28             5'd28
`define R29             5'd29
`define R30             5'd30
`define R31             5'd31

parameter NUM_PRF_ENTRIES       = 96;
localparam QUADWORD             = 64;
localparam PRF_BITS             = $clog2(NUM_PRF_ENTRIES);
localparam NUM_ARF_ENTRIES      = 32;
localparam FREE_LIST_SIZE       = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES;
localparam FREE_LIST_BITS       = $clog2(FREE_LIST_SIZE);
localparam ARF_BITS             = $clog2(NUM_ARF_ENTRIES);


module testbench;

    //INPUTS
    logic                                               clock;
    logic                                               reset;
    logic                       [1:0][QUADWORD/2-1:0]   if_id_IR;
    logic                       [1:0]                   if_id_valid_inst;
    logic                       [1:0]                   rrat_commit_en;
    logic                       [1:0][PRF_BITS-1:0]     rrat_commit_prf_entry;
    logic                       [1:0][ARF_BITS-1:0]     rrat_commit_arf_idx;
    logic                       [1:0]                   nuke;

    //OUTPUTS
    logic                       [1:0][1:0]              id_opa_select_out;
    logic                       [1:0][1:0]              id_opb_select_out;
    logic                       [1:0][ARF_BITS-1:0]     id_arf_idx_out;

    logic                       [1:0][4:0]              id_alu_func_out;
    logic                       [1:0]                   id_rd_mem_out;
    logic                       [1:0]                   id_wr_mem_out;
    logic                       [1:0]                   id_ldl_mem_out;
    logic                       [1:0]                   id_stc_mem_out;
    logic                       [1:0]                   id_cond_branch_out;
    logic                       [1:0]                   id_uncond_branch_out;

    logic                       [1:0]                   id_branch_call;
    logic                       [1:0]                   id_branch_ret;
    logic                       [1:0]                   id_halt_out;
    logic                       [1:0]                   id_illegal_out;
    logic                       [1:0]                   id_valid_inst_out;

    logic                       [1:0][PRF_BITS-1:0]     rat_rda_idx;
    logic                       [1:0][PRF_BITS-1:0]     rat_rdb_idx;
    logic                       [1:0][PRF_BITS-1:0]     rat_dest_idx;
    logic                       [1:0]                   rat_dest_valid;
    logic                       [1:0]                   rat_stall;
    logic                       [1:0][PRF_BITS-1:0]     rrat_overwritten;
    logic                       [1:0]                   rrat_overwritten_valid;
    logic        [FREE_LIST_SIZE-1:0][PRF_BITS-1:0]     rrat_free_list;


    decode_holder #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) decode_holder1(

        //INPUTS
        .clock(clock),
        .reset(reset),
        .if_id_IR(if_id_IR),
        .if_id_valid_inst(if_id_valid_inst),
        .rrat_commit_en(rrat_commit_en),
        .rrat_commit_prf_entry(rrat_commit_prf_entry),
        .rrat_commit_arf_idx(rrat_commit_arf_idx),
        .nuke(nuke),

        //OUTPUTS
        .id_opa_select_out(id_opa_select_out),
        .id_opb_select_out(id_opb_select_out),
        .id_arf_idx_out(id_arf_idx_out),

        .id_alu_func_out(id_alu_func_out),
        .id_rd_mem_out(id_rd_mem_out),
        .id_wr_mem_out(id_wr_mem_out),
        .id_ldl_mem_out(id_ldl_mem_out),
        .id_stc_mem_out(id_stc_mem_out),
        .id_cond_branch_out(id_cond_branch_out),
        .id_uncond_branch_out(id_uncond_branch_out),

        .id_branch_call(id_branch_call),
        .id_branch_ret(id_branch_ret),
        .id_halt_out(id_halt_out),
        .id_illegal_out(id_illegal_out),
        .id_valid_inst_out(id_valid_inst_out),

        .rat_rda_idx(rat_rda_idx),
        .rat_rdb_idx(rat_rdb_idx),
        .rat_dest_idx(rat_dest_idx),
        .rat_dest_valid(rat_dest_valid),
        .rat_stall(rat_stall),
        .rrat_overwritten(rrat_overwritten),
        .rrat_overwritten_valid(rrat_overwritten_valid),
        .rrat_free_list(rrat_free_list)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current RAT FREE LIST enteries
        $display("===RAT_FREE_LIST===");
        for (int i=0; i < FREE_LIST_SIZE; i++)
        begin
            $display ("Free List Entry Number: %d ", i,
                      "Free List Entry: %d ", decode_holder1.rat_holder1.rat1.rat_free_list[i],
                      "Pointer: %d", decode_holder1.rat_holder1.rat1.rat_pointer);
        end

        //display all the current RAT enteries
        $display("\n\n===RAT===");
        for (int i=0; i < NUM_ARF_ENTRIES; i++)
        begin
            $display ("RAT Entry Number: %d ", i,
                      "RAT Entry: %d ", decode_holder1.rat_holder1.rat1.rat_entries[i]);
        end

        //display all the current RRAT FREE LIST enteries
        $display("\n\n===RRAT_FREE_LIST===");
        for (int i=0; i < FREE_LIST_SIZE; i++)
        begin
            $display ("Free List Entry Number: %d ", i,
                      "Free List Entry: %d ", decode_holder1.rat_holder1.rrat1.rrat_free_list[i]);
        end

        //display all the current RRAT enteries
        $display("\n\n===RRAT===");
        for (int i=0; i < NUM_ARF_ENTRIES; i++)
        begin
            $display ("RRAT Entry Number: %d ", i,
                      "RRAT Entry: %d ", decode_holder1.rat_holder1.rrat1.rrat_entries[i]);
        end

        $fatal("\n\nINCORRECT");
    endtask

    task if_id_pipe;
        input                   way;
        input [QUADWORD/2-1:0]  inst;
        input                   valid_inst;

        if_id_IR[way]           = inst;
        if_id_valid_inst[way]   = valid_inst;
    endtask

    task inv_inst;
        input                   way;

        if_id_valid_inst[way]   = `FALSE;
    endtask

    task rob;
        input                   way;
        input                   en;
        input [PRF_BITS-1:0]    prf;
        input [ARF_BITS-1:0]    arf;
        input                   nk;

        rrat_commit_en[way]         = en;
        rrat_commit_prf_entry[way]  = prf;
        rrat_commit_arf_idx[way]    = arf;
        nuke[way]                   = nk;
    endtask

    task disable_rob;
        input                   way;

        rrat_commit_en[way]         = `FALSE;
    endtask

    task check_rat_entry;
        input [4:0]             idx;
        input [PRF_BITS-1:0]    entry;

       if (testbench.decode_holder1.rat_holder1.rat1.rat_entries[idx] != entry) exit_on_error();
    endtask

    task check_rrat_entry;
        input [4:0]             idx;
        input [PRF_BITS-1:0]    entry;

       if (testbench.decode_holder1.rat_holder1.rrat1.rrat_entries[idx] != entry) exit_on_error();
    endtask

    task check_id_1;
        input                       way;
        input   [1:0]               opa_sel;
        input   [1:0]               opb_sel;
        input   [4:0]               alu_func;
        input                       rd_mem;
        input                       wr_mem;

        if (id_opa_select_out[way]  != opa_sel)     exit_on_error();
        if (id_opb_select_out[way]  != opb_sel)     exit_on_error();
        if (id_alu_func_out[way]    != alu_func)    exit_on_error();
        if (id_rd_mem_out[way]      != rd_mem)      exit_on_error();
        if (id_wr_mem_out[way]      != wr_mem)      exit_on_error();
    endtask

    task check_id_2;
        input                       way;
        input                       ldl;
        input                       stc;
        input                       cond;
        input                       uncond;

        if (id_ldl_mem_out[way]         != ldl)     exit_on_error();
        if (id_stc_mem_out[way]         != stc)     exit_on_error();
        if (id_cond_branch_out[way]     != cond)    exit_on_error();
        if (id_uncond_branch_out[way]   != uncond)  exit_on_error();
    endtask

    task check_id_3;
        input                       way;
        input                       call;
        input                       ret;
        input                       halt;
        input                       cpuid;
        input                       illegal;
        input                       valid_inst;

        if (id_branch_call[way]     != call)        exit_on_error();
        if (id_branch_ret[way]      != ret)         exit_on_error();
        if (id_halt_out[way]        != halt)        exit_on_error();
        //if (id_cpuid_out[way]       != cpuid)       exit_on_error();
        if (id_illegal_out[way]     != illegal)     exit_on_error();
        if (id_valid_inst_out[way]  != valid_inst)  exit_on_error();
    endtask

    task check_invalid_inst;
        input                       way;

        if(id_valid_inst_out[way]   != `FALSE)      exit_on_error();
    endtask

    task check_rat;
        input                       way;
        input [PRF_BITS-1:0]        rda_idx;
        input [PRF_BITS-1:0]        rdb_idx;
        input [PRF_BITS-1:0]        dest_idx;
        input                       dest_valid;
        input                       stall;

        if (rat_rda_idx[way]        != rda_idx)     exit_on_error();
        if (rat_rdb_idx[way]        != rdb_idx)     exit_on_error();
        if (rat_dest_idx[way]       != dest_idx)    exit_on_error();
        if (rat_dest_valid[way]     != dest_valid)  exit_on_error();
        if (rat_stall[way]          != stall)       exit_on_error();
    endtask

    task check_rat_invalid_out;
        input                       way;

        if(rat_dest_valid[way]      != `FALSE)      exit_on_error();
    endtask

    task check_rrat;
        input                       way;
        input [PRF_BITS-1:0]        overwritten;
        input                       ovr_valid;

        if (rrat_overwritten[way]       != overwritten  && ovr_valid)   exit_on_error();
        if (rrat_overwritten_valid[way] != ovr_valid)                   exit_on_error();
    endtask

    initial
    begin
        $monitor("\n\n===============WAY_0 INPUTS===============\nTime= %d  if_id_IR= %h if_id_valid_inst= %b rrat_commit_en= %b rrat_commit_prf_entry= %d rrat_commit_arf_idx= %d  nuke = %b\n", $time, if_id_IR[0], if_id_valid_inst[0], rrat_commit_en[0], rrat_commit_prf_entry[0], rrat_commit_arf_idx[0], nuke[0],
                 "===WAY_0 OUTPUTS ID 1===\nTime= %d id_opa_sel_out= %d id_opb_sel_out= %d id_alu_func_out= %d id_rd_mem_out= %b id_wr_mem_out= %b\n", $time, id_opa_select_out[0], id_opb_select_out[0], id_alu_func_out[0], id_rd_mem_out[0], id_wr_mem_out[0],
                 "===WAY_0 OUTPUTS ID 2===\nTime= %d id_ldl_mem_out= %b id_stc_mem_out= %b id_cond_branch_out= %b id_uncond_branch_out= %b\n", $time, id_ldl_mem_out[0], id_stc_mem_out[0], id_cond_branch_out[0], id_uncond_branch_out[0],
                 "===WAY_0 OUTPUTS ID 3===\nTime= %d id_branch_call= %b id_branch_ret= %b id_halt_out= %b id_illegal_out= %b id_valid_inst_out= %b\n", $time, id_branch_call[0], id_branch_ret[0], id_halt_out[0], 0, id_illegal_out[0], id_valid_inst_out[0],
                 "===WAY_0 OUTPUTS Rat ===\nTime= %d rat_rda_idx= %d rat_rdb_idx= %d rat_dest_idx= %d rat_dest_valid= %b rat_stall= %b\n", $time, rat_rda_idx[0], rat_rdb_idx[0], rat_dest_idx[0], rat_dest_valid[0], rat_stall[0],
                 "===WAY_0 OUTPUTS RRat===\nTime= %d rrat_overwritten= %d rrat_overwritten_valid= %b\n", $time, rrat_overwritten[0], rrat_overwritten_valid[0],
                 "===============WAY_1 INPUTS===============\nTime= %d  if_id_IR= %h if_id_valid_inst= %b rrat_commit_en= %b rrat_commit_prf_entry= %d rrat_commit_arf_idx= %d  nuke = %b\n", $time, if_id_IR[1], if_id_valid_inst[1], rrat_commit_en[1], rrat_commit_prf_entry[1], rrat_commit_arf_idx[1], nuke[1],
                 "===WAY_1 OUTPUTS ID 1===\nTime= %d id_opa_sel_out= %d id_opb_sel_out= %d id_alu_func_out= %d id_rd_mem_out= %b id_wr_mem_out= %b\n", $time, id_opa_select_out[1], id_opb_select_out[1], id_alu_func_out[1], id_rd_mem_out[1], id_wr_mem_out[1],
                 "===WAY_1 OUTPUTS ID 2===\nTime= %d id_ldl_mem_out= %b id_stc_mem_out= %b id_cond_branch_out= %b id_uncond_branch_out= %b\n", $time, id_ldl_mem_out[1], id_stc_mem_out[1], id_cond_branch_out[1], id_uncond_branch_out[1],
                 "===WAY_1 OUTPUTS ID 3===\nTime= %d id_branch_call= %b id_branch_ret= %b id_halt_out= %b id_illegal_out= %b id_valid_inst_out= %b\n", $time, id_branch_call[1], id_branch_ret[1], id_halt_out[1], 1, id_illegal_out[1], id_valid_inst_out[1],
                 "===WAY_1 OUTPUTS Rat ===\nTime= %d rat_rda_idx= %d rat_rdb_idx= %d rat_dest_idx= %d rat_dest_valid= %b rat_stall= %b\n", $time, rat_rda_idx[1], rat_rdb_idx[1], rat_dest_idx[1], rat_dest_valid[1], rat_stall[1],
                 "===WAY_1 OUTPUTS RRat===\nTime= %d rrat_overwritten= %d rrat_overwritten_valid= %b\n", $time, rrat_overwritten[1], rrat_overwritten_valid[1]);



        //Intialize
        clock                   = 0;
        reset                   = 1;
        if_id_IR                = 0;
        if_id_valid_inst        = 0;
        rrat_commit_en          = 0;
        rrat_commit_prf_entry   = 0;
        rrat_commit_arf_idx     = 0;
        nuke                    = 0;

        @(negedge clock);
        //CHECK reset
        reset = 0;
        for (int i=0; i< NUM_ARF_ENTRIES; ++i) begin
              check_rat_entry(i, i);
        end
        for (int i=0; i< NUM_ARF_ENTRIES; ++i) begin
              check_rrat_entry(i, i);
        end
        //id combinational, don't check its outputs on reset!
        check_rat(`WAY_1, 7'd0, 7'd0, 7'd0, `FALSE, `FALSE);
        check_rat(`WAY_0, 7'd0, 7'd0, 7'd0, `FALSE, `FALSE);
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        $display("\n@@@Passed reset\n");

        //Dispatch  WAY_1: ADDQ $r1, $r1, $r3
        //          WAY_0: ADDQ $r1, $r2, $r3
        @(negedge clock);
        if_id_pipe(`WAY_1, 32'h40210403, `TRUE);
        if_id_pipe(`WAY_0, 32'h40220403, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd1, 7'd1, 7'd95, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd1, 7'd2, 7'd94, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R3, 7'd94);         //got renamed twice! 3->95->94
        $display("\n@@@Passed dispatch double rewrite R3\n");

        //Dispatch  WAY_1: ADDQ $r3, $r3, $r3
        //          WAY_0: SUBQ $r3, $r1, $r2
        if_id_pipe(`WAY_1, 32'h40630403, `TRUE);
        if_id_pipe(`WAY_0, 32'h40610522, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_SUBQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd94, 7'd94, 7'd93, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd93, 7'd01, 7'd92, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R3, 7'd93);
        check_rat_entry(`R2, 7'd92);
        $display("\n@@@Passed dispatch double rename R3 and then use it right away\n");

        //Commit    WAY_1: ADDQ $r1, $r1, $r3
        rob(`WAY_1, `TRUE, 7'd95, `R3, `FALSE);
        inv_inst(`WAY_1); //Don't dispatch and commit same cycle for now...
        inv_inst(`WAY_0);
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd3, `TRUE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        $display("\n@@@Passed commit only WAY_1\n");

        //Commit    WAY_1: ADDQ $r1, $r2, $r3
        rob(`WAY_1, `TRUE, 7'd94, `R3, `FALSE);
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd95, `TRUE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        $display("\n@@@Passed commit only WAY_1 again\n");

        //Commit    WAY_1: ADDQ $r03, $r03, $r03
        //          WAY_0: SUBQ $r03, $r01, $r02
        //Dispatch  WAY_1: ADDQ $r10, $r03, $r11
        //          WAY_0: ADDQ $r02, $r11, $r3
        rob(`WAY_1, `TRUE, 7'd93, `R3, `FALSE);
        rob(`WAY_0, `TRUE, 7'd92, `R2, `FALSE);
        if_id_pipe(`WAY_1, 32'h4143040b, `TRUE);
        if_id_pipe(`WAY_0, 32'h404b0403, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd10, 7'd93, 7'd02, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd92, 7'd02, 7'd94, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd94, `TRUE);
        check_rrat(`WAY_0, 7'd02, `TRUE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R3, 7'd94);
        check_rat_entry(`R11, 7'd2);
        //Disable Inputs (Comment out if you don't want to)
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed commit double and dispatch double\n");

        //NUKE
        nuke = 2'b10;        //NOTE: Instructions are invalid on nuke!
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        nuke = 2'b00;
        check_rat_entry(`R2, 7'd92);
        check_rat_entry(`R3, 7'd93);
        check_rat_entry(`R11, 7'd11);
        $display("\n@@@Passed NUKE, but no updating RRAT\n");
        //exit_on_error();

        //Dispatch some more instructions to get ready for a NUKE and commit
        //at same time
        //Dispatch  WAY_1: ADDQ $r05, $r06, $r07
        //          WAY_0: ADDQ $r03, $r03, $r02
        if_id_pipe(`WAY_1, 32'h40a60407, `TRUE);
        if_id_pipe(`WAY_0, 32'h40630402, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd05, 7'd06, 7'd03, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd93, 7'd93, 7'd95, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R2, 7'd95);
        check_rat_entry(`R7, 7'd03);
        //Disable Inputs (Comment out if you don't want to)
        //inv_inst(`WAY_1);
        //inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed double dispatch again\n");

        //Dispatch  WAY_1: lda $r20, data (data = 0x1000)
        //          WAY_0: ldq $r21, 0($r20)
        if_id_pipe(`WAY_1, 32'h229f1000, `TRUE);
        if_id_pipe(`WAY_0, 32'ha6b40000, `TRUE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `ALU_ADDQ, `TRUE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd31, 7'd31, 7'd94, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd31, 7'd94, 7'd02, `TRUE, `FALSE);
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R20, 7'd94);
        check_rat_entry(`R21, 7'd02);
        //Disable Inputs (Comment out if you don't want to)
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch load immediate, and regular load\n");
        @(negedge clock);
        //exit_on_error();

        //Dispatch  WAY_1: stq $r21, 0($r20)
        //          WAY_0: nop
        if_id_pipe(`WAY_1, 32'hb6b40000, `TRUE);
        if_id_pipe(`WAY_0, 32'h47ff041f, `TRUE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `TRUE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_BIS, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd31, 7'd94, 7'd91, `TRUE, `FALSE); //popped out but not renamed
        check_rat(`WAY_0, 7'd31, 7'd31, 7'd90, `TRUE, `FALSE); //popped out but not renamed
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R31, 7'd31);       //not renamed
        //Disable Inputs (Comment out if you don't want to)
        //inv_inst(`WAY_1);
        //inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch stq, and nop\n");

        //Dispatch  WAY_1: call_pal (HALT)
        //          WAY_0: INVALID
        if_id_pipe(`WAY_1, 32'h00000555, `TRUE);
        if_id_pipe(`WAY_0, 32'h47ff041f, `FALSE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        check_invalid_inst(`WAY_0);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd31, 7'd31, 7'd89, `TRUE, `FALSE);  //popped out but not renamed
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R31, 7'd31);         //not renamed
        //Disable Inputs (Comment out if you don't want to)
        //inv_inst(`WAY_1);
        //inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch call_pal (HALT) and invalid instr\n");

        //Dispatch  WAY_1: beq $r20 loop
        //          WAY_0: br  loop
        if_id_pipe(`WAY_1, 32'he69ffffe, `TRUE);
        if_id_pipe(`WAY_0, 32'hc3fffffd, `TRUE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `TRUE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd94, 7'd31, 7'd88, `TRUE, `FALSE); //popped out but not renamed
        check_rat(`WAY_0, 7'd31, 7'd31, 7'd87, `TRUE, `FALSE); //popped out but not renamed
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R31, 7'd31);       //not renamed
        //Disable Inputs (Comment out if you don't want to)
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch beq, and br\n");

        //NUKE and Commit
        rob(`WAY_1, `TRUE, 7'd94, 7'd20, `TRUE);
        rob(`WAY_0, `TRUE, 7'd02, 7'd21, `FALSE);
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd20, `TRUE);
        check_rrat(`WAY_0, 7'd21, `FALSE); //Does not get sent
        @(negedge clock);
        //rat should now be updated
        nuke = 2'b00;
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        check_rat_entry(`R20, 7'd94);
        check_rat_entry(`R21, 7'd21);
        check_rat_entry(`R2, 7'd92);
        check_rat_entry(`R3, 7'd93);
        check_rat_entry(`R7, 7'd7);
        $display("\n@@@Passed NUKE, and commit same time\n");
        //exit_on_error();

        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

`default_nettype none
`timescale 1ns/100ps
// Decode an instruction: given instruction bits IR produce the
// appropriate datapath control signals.
//
// This is a *combinational* module (basically a PLA).
//
module instruction_decoder #(localparam QUADWORD=64)(
    // INPUTS
    input   wire    [QUADWORD/2-1:0]    inst,
    input   wire                        valid_inst_in,      // ignore inst when low, outputs will
                                                            // reflect noop (except valid_inst)

    // OUTPUTS
    output  logic              [1:0]    opa_select,
    output  logic              [1:0]    opb_select,
    output  logic              [1:0]    dest_reg,
    output  logic              [4:0]    alu_func,
    output  logic                       rd_mem,
    output  logic                       wr_mem,
    output  logic                       ldl_mem,        // LOAD LOCK (FOR MULTITHREADING)
    output  logic                       stc_mem,        // STORE CONDITIONAL (FOR MULTITHREADING)
    output  logic                       cond_branch,
    output  logic                       uncond_branch,
    output  logic                       branch_call,    // A bsr or a jsr
    output  logic                       branch_ret,     // A ret or a jsr_co (needed in ROB to update RRAS)
    output  logic                       halt,           // non-zero on a halt
    output  logic                       illegal,        // non-zero on an illegal instruction
    output  logic                       valid_inst      // for counting valid instructions executed
                                                        // and for making the fetch stage die on halts/
                                                        // keeping track of when to allow the next
                                                        // instruction out of fetch
                                                        // 0 for HALT and illegal instructions (die on halt)

);

    assign valid_inst = valid_inst_in & ~illegal;

    always_comb begin
        // default control values:
        // - valid instructions must override these defaults as necessary.
        //   opa_select, opb_select, and alu_func should be set explicitly.
        // - invalid instructions should clear valid_inst.
        // - These defaults are equivalent to a noop
        // * see sys_defs.vh for the constants used here
        opa_select      = 0;
        opb_select      = 0;
        alu_func        = 0;
        dest_reg        = `DEST_NONE;
        rd_mem          = `FALSE;
        wr_mem          = `FALSE;
        ldl_mem         = `FALSE;
        stc_mem         = `FALSE;
        cond_branch     = `FALSE;
        uncond_branch   = `FALSE;
        halt            = `FALSE;
        illegal         = `FALSE;
        branch_call     = `FALSE;
        branch_ret      = `FALSE;

        if (valid_inst_in) begin
            case ({inst[31:29], 3'b0})
                6'h0: begin
                    case (inst[31:26])
                        `PAL_INST: begin
                            if (inst[25:0] == `PAL_HALT) begin
                                halt = `TRUE;
                            end
                            else if (inst[25:0] == `PAL_WHAMI) begin
                                dest_reg = `DEST_IS_REGA;   // get cpuid writes to r0
                            end
                            else begin
                                illegal = `TRUE;
                            end
                        end
                        default: illegal = `TRUE;
                    endcase
                end

                6'h10: begin
                    opa_select = `ALU_OPA_IS_REGA;
                    opb_select = inst[12] ? `ALU_OPB_IS_ALU_IMM : `ALU_OPB_IS_REGB;
                    dest_reg = `DEST_IS_REGC;
                    case (inst[31:26])
                        `INTA_GRP: begin
                            case (inst[11:5])
                                `CMPULT_INST:  alu_func = `ALU_CMPULT;
                                `ADDQ_INST:    alu_func = `ALU_ADDQ;
                                `SUBQ_INST:    alu_func = `ALU_SUBQ;
                                `CMPEQ_INST:   alu_func = `ALU_CMPEQ;
                                `CMPULE_INST:  alu_func = `ALU_CMPULE;
                                `CMPLT_INST:   alu_func = `ALU_CMPLT;
                                `CMPLE_INST:   alu_func = `ALU_CMPLE;
                                default:        illegal = `TRUE;
                            endcase
                        end
                        `INTL_GRP: begin
                            case (inst[11:5])
                                `AND_INST:    alu_func = `ALU_AND;
                                `BIC_INST:    alu_func = `ALU_BIC;
                                `BIS_INST:    alu_func = `ALU_BIS;
                                `ORNOT_INST:  alu_func = `ALU_ORNOT;
                                `XOR_INST:    alu_func = `ALU_XOR;
                                `EQV_INST:    alu_func = `ALU_EQV;
                                default:       illegal = `TRUE;
                            endcase
                        end
                        `INTS_GRP: begin
                            case (inst[11:5])
                                `SRL_INST:  alu_func = `ALU_SRL;
                                `SLL_INST:  alu_func = `ALU_SLL;
                                `SRA_INST:  alu_func = `ALU_SRA;
                                default:    illegal = `TRUE;
                            endcase
                        end
                        `INTM_GRP: begin
                            case (inst[11:5])
                                `MULQ_INST:       alu_func = `ALU_MULQ;
                                default:          illegal = `TRUE;
                            endcase
                        end
                        `ITFP_GRP:       illegal = `TRUE;       // unimplemented
                        `FLTV_GRP:       illegal = `TRUE;       // unimplemented
                        `FLTI_GRP:       illegal = `TRUE;       // unimplemented
                        `FLTL_GRP:       illegal = `TRUE;       // unimplemented
                    endcase
                end

                6'h18:
                    case (inst[31:26])
                        `MISC_GRP:       illegal = `TRUE; // unimplemented
                        `JSR_GRP: begin
                            // JMP, JSR, RET, and JSR_CO have identical semantics
                            if (inst[15:14] == `JSR_INST) begin
                                branch_call = `TRUE;
                            end
                            else if (inst[15:14] == `RET_INST || inst[15:14] == `JSR_CO_INST) begin
                                branch_ret = `TRUE;
                            end
                            opa_select = `ALU_OPA_IS_NOT3;
                            opb_select = `ALU_OPB_IS_REGB;
                            alu_func = `ALU_AND; // clear low 2 bits (word-align)
                            dest_reg = `DEST_IS_REGA;
                            uncond_branch = `TRUE;
                        end
                        `FTPI_GRP:       illegal = `TRUE;       // unimplemented
                    endcase

                6'h08, 6'h20, 6'h28: begin
                    opa_select = `ALU_OPA_IS_MEM_DISP;
                    opb_select = `ALU_OPB_IS_REGB;
                    alu_func = `ALU_ADDQ;
                    dest_reg = `DEST_IS_REGA;
                    case (inst[31:26])
                        `LDA_INST:  /* defaults are OK */;
                        `LDQ_INST: begin
                            rd_mem = `TRUE;
                            dest_reg = `DEST_IS_REGA;
                        end
                        `LDQ_L_INST: begin
                            rd_mem = `TRUE;
                            ldl_mem = `TRUE;
                            dest_reg = `DEST_IS_REGA;
                        end
                        `STQ_INST: begin
                            wr_mem = `TRUE;
                            dest_reg = `DEST_NONE;
                        end
                        `STQ_C_INST: begin
                            wr_mem = `TRUE;
                            stc_mem = `TRUE;
                            dest_reg = `DEST_IS_REGA;
                        end
                        default:       illegal = `TRUE;
                    endcase
                end

                6'h30, 6'h38: begin
                    opa_select = `ALU_OPA_IS_NPC;
                    opb_select = `ALU_OPB_IS_BR_DISP;
                    alu_func = `ALU_ADDQ;
                    case (inst[31:26])
                        `FBEQ_INST, `FBLT_INST, `FBLE_INST,
                        `FBNE_INST, `FBGE_INST, `FBGT_INST: begin
                            // FP conditionals not implemented
                            illegal = `TRUE;
                        end

                        // Call instruction (most common for normal functions aka non-dynamic-dispatch)
                        `BSR_INST: begin
                            dest_reg = `DEST_IS_REGA;
                            uncond_branch = `TRUE;
                            branch_call = `TRUE;
                        end

                        `BR_INST: begin
                            dest_reg = `DEST_IS_REGA;
                            uncond_branch = `TRUE;
                        end

                        default:        cond_branch = `TRUE; // all others are conditional
                    endcase
                end
            endcase
        end
    end
endmodule
`default_nettype wire
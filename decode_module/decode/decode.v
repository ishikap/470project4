`default_nettype none
`timescale 1ns/100ps
module decode #(localparam QUADWORD=64, localparam NUM_ARF_REGS=32, localparam ARF_BITS=$clog2(NUM_ARF_REGS)) (
    // INPUTS
    input   wire                                clock,                  // system clock
    input   wire    [1:0][QUADWORD/2-1:0]       if_id_IR,               // incoming instructions
    input   wire    [1:0]                       if_id_valid_inst,

    // OUTPUTS
    output  logic   [1:0][1:0]                  id_opa_select_out,      // ALU opa mux select (ALU_OPA_xxx *)
    output  logic   [1:0][1:0]                  id_opb_select_out,      // ALU opb mux select (ALU_OPB_xxx *)
    output  logic   [1:0][ARF_BITS-1:0]         id_ra_idx,              // inst operand A register index
    output  logic   [1:0][ARF_BITS-1:0]         id_rb_idx,              // inst operand B register index
    output  logic   [1:0][ARF_BITS-1:0]         id_dest_reg_idx_out,    // destination (writeback) register index
                                                                        // (ZERO_REG if no writeback)

    output  logic   [1:0][4:0]                  id_alu_func_out,        // ALU function select (ALU_xxx *)
    output  logic   [1:0]                       id_rd_mem_out,          // does inst read memory?
    output  logic   [1:0]                       id_wr_mem_out,          // does inst write memory?
    output  logic   [1:0]                       id_ldl_mem_out,         // load-lock inst?
    output  logic   [1:0]                       id_stc_mem_out,         // store-conditional inst?
    output  logic   [1:0]                       id_cond_branch_out,     // is inst a conditional branch?
    output  logic   [1:0]                       id_uncond_branch_out,   // is inst an unconditional branch
                                                                        // or jump?
    output  logic   [1:0]                       id_branch_call,
    output  logic   [1:0]                       id_branch_ret,
    output  logic   [1:0]                       id_halt_out,
    output  logic   [1:0]                       id_illegal_out,
    output  logic   [1:0]                       id_valid_inst_out       // is inst a valid instruction to be
                                                                        // counted for CPI calculations?
);

    logic   [1:0][1:0]          dest_reg_select;

    // instruction fields read from IF/ID pipeline register
    logic   [1:0][ARF_BITS-1:0]         rc_idx;   // inst operand C register index

    // instantiate the instruction decoder
    instruction_decoder instr_decoder [1:0](
        // INPUTS
        .inst(if_id_IR),
        .valid_inst_in(if_id_valid_inst),

        // OUTPUTS
        .opa_select(id_opa_select_out),
        .opb_select(id_opb_select_out),
        .alu_func(id_alu_func_out),
        .dest_reg(dest_reg_select),
        .rd_mem(id_rd_mem_out),
        .wr_mem(id_wr_mem_out),
        .ldl_mem(id_ldl_mem_out),
        .stc_mem(id_stc_mem_out),
        .cond_branch(id_cond_branch_out),
        .uncond_branch(id_uncond_branch_out),
        .branch_call(id_branch_call),
        .branch_ret(id_branch_ret),
        .halt(id_halt_out),
        .illegal(id_illegal_out),
        .valid_inst(id_valid_inst_out)
    );

    // mux to generate dest_reg_idx based on
    // the dest_reg_select output from decoder
    always_comb begin
        // Done this way because Verilog complains if we just do the direct assignments
        for (int i=0; i<2; ++i) begin
            id_ra_idx[i] = if_id_IR[i][25:21];
            id_rb_idx[i] = if_id_IR[i][20:16];
            rc_idx[i] = if_id_IR[i][4:0];
        end

        for (int i=0; i<2; ++i) begin
            case (dest_reg_select[i])
                `DEST_IS_REGC: id_dest_reg_idx_out[i] = rc_idx[i];
                `DEST_IS_REGA: id_dest_reg_idx_out[i] = id_ra_idx[i];
                `DEST_NONE:    id_dest_reg_idx_out[i] = `ZERO_REG;
                default:       id_dest_reg_idx_out[i] = `ZERO_REG;
            endcase
        end
    end

endmodule
`default_nettype wire

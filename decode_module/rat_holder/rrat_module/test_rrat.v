//For: Testing RRAT
`timescale 1ns/100ps
`define NOT_CHECK   2'b10
`define TRUE_2      2'b01
`define FALSE_2     2'b00

`define WAY_0       1'b0
`define WAY_1       1'b1

`define R0          5'd0
`define R1          5'd1
`define R2          5'd2
`define R3          5'd3
`define R4          5'd4
`define R5          5'd5
`define R6          5'd6
`define R7          5'd7
`define R8          5'd8
`define R9          5'd9
`define R10         5'd10
`define R11         5'd11
`define R12         5'd12
`define R13         5'd13
`define R14         5'd14
`define R15         5'd15
`define R16         5'd16
`define R17         5'd17
`define R18         5'd18
`define R19         5'd19
`define R20         5'd20
`define R21         5'd21
`define R22         5'd22
`define R23         5'd23
`define R24         5'd24
`define R25         5'd25
`define R26         5'd26
`define R27         5'd27
`define R28         5'd28
`define R29         5'd29
`define R30         5'd30
`define R31         5'd31

parameter   NUM_PRF_ENTRIES         = 96;
localparam  QUADWORD                = 64;
localparam  PRF_BITS                = $clog2(NUM_PRF_ENTRIES);
localparam  NUM_ARF_ENTRIES         = 32;
localparam  RRAT_FREE_LIST_SIZE     = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES;
localparam  RRAT_FREE_LIST_BITS     = $clog2(RRAT_FREE_LIST_SIZE);
localparam  ARF_BITS                = $clog2(NUM_ARF_ENTRIES);


module testbench;

    logic                                               clock;
    logic                                               reset;
    logic                       [1:0]                   rrat_commit_en;
    logic                       [1:0][PRF_BITS-1:0]     rrat_commit_prf_entry;
    logic                       [1:0][ARF_BITS-1:0]     rrat_commit_arf_idx;
    logic                       [1:0]                   nuke;

    logic       [NUM_ARF_ENTRIES-1:0][PRF_BITS-1:0]     rrat_entries_next;
    logic   [RRAT_FREE_LIST_SIZE-1:0][PRF_BITS-1:0]     rrat_free_list_next;
    logic                       [1:0]                   rrat_overwritten_valid;
    logic                       [1:0][PRF_BITS-1:0]     rrat_overwritten;


    rrat #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rrat1(
           .clock(clock),
           .reset(reset),
           .rrat_commit_en(rrat_commit_en),
           .rrat_commit_prf_entry(rrat_commit_prf_entry),
           .rrat_commit_arf_idx(rrat_commit_arf_idx),
           .nuke(nuke),
           .rrat_entries_next(rrat_entries_next),
           .rrat_free_list_next(rrat_free_list_next),
           .rrat_overwritten_valid(rrat_overwritten_valid),
           .rrat_overwritten(rrat_overwritten)
        );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current RRAT FREE LIST enteries
        $display("===RRAT_FREE_LIST===");
        for (int i=0; i < RRAT_FREE_LIST_SIZE; i++)
        begin
            //$display ("Free List Entry Number: %d ", i,
              //        "Free List Entry: %d ", rrat1.rrat_free_list[i]);
        end

        //display all the current RRAT enteries
        $display("\n\n===RRAT===");
        for (int i=0; i < NUM_ARF_ENTRIES; i++)
        begin
            //$display ("RRAT Entry Number: %d ", i,
              //        "RRAT Entry: %d ", rrat1.rrat_entries[i]);
        end
        $fatal("INCORRECT");
    endtask

    task commit;
        input                   way;
        input                   commit_en;
        input [4:0]             commit_arf_idx;
        input [PRF_BITS-1:0]    commit_prf_entry;

        rrat_commit_en[way]         = commit_en;
        rrat_commit_arf_idx[way]    = commit_arf_idx;
        rrat_commit_prf_entry[way]  = commit_prf_entry;
    endtask

    task check_commit_output;
        input [4:0]                     idx;
        input [PRF_BITS-1:0]            rrat_entry;
        input [RRAT_FREE_LIST_BITS-1:0] location;
        input [PRF_BITS-1:0]            free_list_entry;

        if (rrat_entries_next[idx] != rrat_entry) exit_on_error();
        if (rrat_free_list_next[location] != free_list_entry) exit_on_error();
    endtask

    task check_commit_internal_state;
        input [4:0]                     idx;
        input [PRF_BITS-1:0]            rrat_entry;
        input [RRAT_FREE_LIST_BITS-1:0] location;
        input [PRF_BITS-1:0]            free_list_entry;

        //if (rrat1.rrat_entries[idx] != rrat_entry) exit_on_error();
        //if (rrat1.rrat_free_list[location] != free_list_entry) exit_on_error();
    endtask

    task check_commit_overwritten;
        input                   way;
        input                   valid;
        input [PRF_BITS-1:0]    prf;

        if (rrat_overwritten_valid[way] != valid) exit_on_error();
        if (rrat_overwritten[way] != prf && valid) exit_on_error();

    endtask

    initial
    begin
        $monitor("===WAY_0 INPUTS===\nTime= %d  Commit Enable= %b ARF_idx= %d PRF_entry= %d nuke= %b\n",  $time, rrat_commit_en[0], rrat_commit_arf_idx[0], rrat_commit_prf_entry[0], nuke[0],
                "===WAY_0 OUTPUTS===\nTime= %d  Overwritten Valid= %b Overwritten PRF= %d\n", $time, rrat_overwritten_valid[0], rrat_overwritten[0],
                "===WAY_1 INPUTS===\nTime= %d  Commit Enable= %b ARF_idx= %d PRF_entry= %d nuke= %b\n",  $time, rrat_commit_en[1], rrat_commit_arf_idx[1], rrat_commit_prf_entry[1], nuke[1],
                "===WAY_1 OUTPUTS===\nTime= %d  Overwritten Valid= %b Overwritten PRF= %d\n", $time, rrat_overwritten_valid[1], rrat_overwritten[1]);

        clock=0;
        reset=1;
        rrat_commit_en=0;
        rrat_commit_prf_entry=0;
        rrat_commit_arf_idx=0;
        nuke=0;

        @(negedge clock);
        //CHECK reset
        reset = 0;
        for (int i=0; i< NUM_ARF_ENTRIES; ++i) begin
            //if(rrat1.rrat_entries[i] != i) exit_on_error();
        end
        for (int i=0; i< RRAT_FREE_LIST_SIZE; ++i) begin
            //if(rrat1.rrat_free_list[i] != i + NUM_ARF_ENTRIES) exit_on_error();
        end
        $display("\n@@@Passed reset\n");

        //Commit way 1 only
        @(negedge clock);
        commit(`WAY_1, `TRUE, `R0, 7'd95);
        @(posedge clock);
        check_commit_output(`R0, 7'd95, 7'd63, 7'd0);
        check_commit_overwritten(`WAY_1, `TRUE, 7'd0);
        check_commit_overwritten(`WAY_0, `FALSE, 7'd0);
        @(negedge clock);
        commit(`WAY_1, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R0, 7'd95, 7'd63, 7'd0);
        $display("\n@@@Passed Commit way 1 only\n");

        //Commit way 0 only
        @(negedge clock);
        commit(`WAY_0, `TRUE, `R1, 7'd94);
        @(posedge clock);
        check_commit_output(`R1, 7'd94, 7'd62, 7'd1);
        check_commit_overwritten(`WAY_1, `FALSE, 7'd0);
        check_commit_overwritten(`WAY_0, `TRUE, 7'd1);
        @(negedge clock);
        commit(`WAY_0, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R1, 7'd94, 7'd62, 7'd1);
        $display("\n@@@Passed Commit way 0 only\n");

        //Commit both ways
        @(negedge clock);
        commit(`WAY_1, `TRUE, `R0, 7'd93);
        commit(`WAY_0, `TRUE, `R1, 7'd92);
        @(posedge clock);
        check_commit_output(`R0, 7'd93, 7'd61, 7'd95);
        check_commit_output(`R1, 7'd92, 7'd60, 7'd94);
        check_commit_overwritten(`WAY_1, `TRUE, 7'd95);
        check_commit_overwritten(`WAY_0, `TRUE, 7'd94);
        @(negedge clock);
        commit(`WAY_1, `FALSE, `R0, 7'd95);
        commit(`WAY_0, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R0, 7'd93, 7'd61, 7'd95);
        check_commit_internal_state(`R1, 7'd92, 7'd60, 7'd94);
        $display("\n@@@Passed Commit both ways\n");

        //Commit both ways with R31 in way0
        @(negedge clock);
        commit(`WAY_1, `TRUE, `R30, 7'd33);
        commit(`WAY_0, `TRUE, `R31, 7'd32);
        @(posedge clock);
        check_commit_output(`R30, 7'd33, 7'd1, 7'd30);
        check_commit_output(`R31, 7'd31, 7'd0, 7'd32);
        check_commit_overwritten(`WAY_1, `TRUE, 7'd30);
        check_commit_overwritten(`WAY_0, `TRUE, 7'd32);
        @(negedge clock);
        commit(`WAY_1, `FALSE, `R0, 7'd95);
        commit(`WAY_0, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R30, 7'd33, 7'd1, 7'd30);
        check_commit_internal_state(`R31, 7'd31, 7'd0, 7'd32);
        $display("\n@@@Passed Commit both ways with R31 in way0\n");

        //Commit both ways with R31 in way1
        @(negedge clock);
        commit(`WAY_1, `TRUE, `R31, 7'd34);
        commit(`WAY_0, `TRUE, `R30, 7'd35);
        @(posedge clock);
        check_commit_output(`R31, 7'd31, 7'd2, 7'd34);
        check_commit_output(`R30, 7'd35, 7'd3, 7'd33);
        check_commit_overwritten(`WAY_1, `TRUE, 7'd34);
        check_commit_overwritten(`WAY_0, `TRUE, 7'd33);
        @(negedge clock);
        commit(`WAY_1, `FALSE, `R0, 7'd95);
        commit(`WAY_0, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R31, 7'd31, 7'd2, 7'd34);
        check_commit_internal_state(`R30, 7'd35, 7'd3, 7'd33);
        $display("\n@@@Passed Commit both ways with R31 in way1\n");

        //Commit both ways, Nuke way 1 only
        @(negedge clock);
        commit(`WAY_1, `TRUE, `R2, 7'd91);
        commit(`WAY_0, `TRUE, `R3, 7'd90);
        nuke = 2'b10;
        @(posedge clock);
        check_commit_output(`R2, 7'd91, 7'd59, 7'd2);
        check_commit_output(`R3, 7'd3, 7'd58, 7'd90);
        check_commit_overwritten(`WAY_1, `TRUE, 7'd2);
        check_commit_overwritten(`WAY_0, `FALSE, 7'd90);
        @(negedge clock);
        commit(`WAY_1, `FALSE, `R0, 7'd95);
        commit(`WAY_0, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R2, 7'd91, 7'd59, 7'd2);
        check_commit_internal_state(`R3, 7'd3, 7'd58, 7'd90);
        $display("\n@@@Passed Commit both ways with nuke way 1\n");

        //Commit both ways, Nuke both ways
        @(negedge clock);
        commit(`WAY_1, `TRUE, `R2, 7'd90);
        commit(`WAY_0, `TRUE, `R3, 7'd89);
        nuke = 2'b11;
        @(posedge clock);
        check_commit_output(`R2, 7'd90, 7'd58, 7'd91);
        check_commit_output(`R3, 7'd3, 7'd57, 7'd89);
        check_commit_overwritten(`WAY_1, `TRUE, 7'd91);
        check_commit_overwritten(`WAY_0, `FALSE, 7'd89);
        @(negedge clock);
        commit(`WAY_1, `FALSE, `R0, 7'd95);
        commit(`WAY_0, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R2, 7'd90, 7'd58, 7'd91);
        check_commit_internal_state(`R3, 7'd3, 7'd57, 7'd89);
        $display("\n@@@Passed Commit both ways with nuke both ways\n");

        //Commit both ways, Nuke way 0
        @(negedge clock);
        commit(`WAY_1, `TRUE, `R2, 7'd88);
        commit(`WAY_0, `TRUE, `R3, 7'd87);
        nuke = 2'b01;
        @(posedge clock);
        check_commit_output(`R2, 7'd88, 7'd56, 7'd90);
        check_commit_output(`R3, 7'd87, 7'd55, 7'd3);
        check_commit_overwritten(`WAY_1, `TRUE, 7'd90);
        check_commit_overwritten(`WAY_0, `TRUE, 7'd3);
        @(negedge clock);
        commit(`WAY_1, `FALSE, `R0, 7'd95);
        commit(`WAY_0, `FALSE, `R0, 7'd95);
        check_commit_internal_state(`R2, 7'd88, 7'd56, 7'd90);
        check_commit_internal_state(`R3, 7'd87, 7'd55, 7'd3);
        $display("\n@@@Passed Commit both ways with nuke way 0\n");

        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

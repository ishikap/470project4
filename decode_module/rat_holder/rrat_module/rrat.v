// Clock period : 2.828125 < t <= 2.90625
//`timescale 1ns/100ps
`default_nettype none
`timescale 1ns/100ps
module rrat #(parameter NUM_PRF_ENTRIES = 96, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam NUM_ARF_ENTRIES = 32, localparam RRAT_FREE_LIST_SIZE = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES, localparam RRAT_FREE_LIST_BITS = $clog2(RRAT_FREE_LIST_SIZE), localparam ARF_BITS = $clog2(NUM_ARF_ENTRIES)) (
    // INPUTS
    input   wire                                                clock,
    input   wire                                                reset,
    input   wire                        [1:0]                   rrat_commit_en,
    input   wire                        [1:0][PRF_BITS-1:0]     rrat_commit_prf_entry,
    input   wire                        [1:0][ARF_BITS-1:0]     rrat_commit_arf_idx,
    input   wire                        [1:0]                   nuke,

    // OUTPUTS
    output  logic       [NUM_ARF_ENTRIES-1:0][PRF_BITS-1:0]     rrat_entries_next,        // Output the combinational, updated state so we can get the nuke on the upcoming clock
    output  logic   [RRAT_FREE_LIST_SIZE-1:0][PRF_BITS-1:0]     rrat_free_list_next,
    output  logic                       [1:0]                   rrat_overwritten_valid,   // Got a committing instruction
    output  logic                       [1:0][PRF_BITS-1:0]     rrat_overwritten          // What PRF is now free because of committing instruction
);

    logic       [NUM_ARF_ENTRIES-1:0][PRF_BITS-1:0]     rrat_entries;
    logic   [RRAT_FREE_LIST_SIZE-1:0][PRF_BITS-1:0]     rrat_free_list;

    logic   nuked;


    always_comb begin
        rrat_entries_next           = rrat_entries;
        rrat_free_list_next         = rrat_free_list;

        rrat_overwritten_valid      = 0;
        rrat_overwritten            = 0;

        nuked                       = 0;


        for (int i=1; i>=0; --i) begin
            if (rrat_commit_en[i] && !nuked) begin
                if (rrat_commit_arf_idx[i] != 5'd31) begin
                    // Only update the rrat free list if a commit (not nuked) not to r31
                    rrat_overwritten_valid[i]                   = 1'b1;
                    rrat_overwritten[i]                         = rrat_entries_next[rrat_commit_arf_idx[i]];

                    // In the free list, overwrite the incoming value with the outgoing value
                    for (int j=0; j<RRAT_FREE_LIST_SIZE; ++j) begin
                        if (rrat_free_list_next[j] == rrat_commit_prf_entry[i]) begin
                            rrat_free_list_next[j]              = rrat_overwritten[i];
                        end
                    end

                    // Update the main RRAT
                    rrat_entries_next[rrat_commit_arf_idx[i]]   = rrat_commit_prf_entry[i];
                end
                else begin
                    // Free the register we were using for r31 in the RAT (RRAT's free list already says it's free)
                    rrat_overwritten_valid[i]                   = 1'b1;
                    rrat_overwritten[i]                         = rrat_commit_prf_entry[i];
                end
            end
            if (nuke[i]) begin
                nuked = 1'b1;
            end
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            // Initialize the RRAT
            for (int i=0; i<NUM_ARF_ENTRIES; ++i) begin
                rrat_entries[i]     <= `SD i;
            end
            // Initialize the free list
            for (int i=0; i<RRAT_FREE_LIST_SIZE; ++i) begin
                rrat_free_list[i]   <= `SD i + NUM_ARF_ENTRIES;
            end
        end
        else begin
            rrat_entries            <= `SD rrat_entries_next;
            rrat_free_list          <= `SD rrat_free_list_next;
        end
    end

endmodule
`default_nettype wire




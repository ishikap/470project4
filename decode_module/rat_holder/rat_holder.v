`default_nettype none
`timescale 1ns/100ps
// Clock period : 4.15625 < t <= 4.25
module rat_holder #(parameter NUM_PRF_ENTRIES = 96, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam NUM_ARF_ENTRIES = 32, localparam FREE_LIST_SIZE = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES, localparam FREE_LIST_BITS = $clog2(FREE_LIST_SIZE), localparam ARF_BITS = $clog2(NUM_ARF_ENTRIES)) (
    // INPUTS
    input wire                                                  clock,
    input wire                                                  reset,
    // From decode
    input   wire                        [1:0][4:0]              id_rda_idx,
    input   wire                        [1:0][4:0]              id_rdb_idx,
    input   wire                        [1:0][4:0]              id_dest_idx,
    input   wire                        [1:0]                   id_valid_inst,
    // From ROB
    input   wire                        [1:0]                   rrat_commit_en,
    input   wire                        [1:0][PRF_BITS-1:0]     rrat_commit_prf_entry,
    input   wire                        [1:0][ARF_BITS-1:0]     rrat_commit_arf_idx,
    input   wire                        [1:0]                   nuke,

    // OUTPUTS
    output  logic                       [1:0][PRF_BITS-1:0]     rat_rda_idx,
    output  logic                       [1:0][PRF_BITS-1:0]     rat_rdb_idx,
    output  logic                       [1:0][PRF_BITS-1:0]     rat_dest_idx,
    output  logic                       [1:0]                   rat_dest_valid,
    output  logic                       [1:0]                   rat_stall,
    output  logic        [FREE_LIST_SIZE-1:0][PRF_BITS-1:0]     rrat_free_list              // Goes to PRF to invalidate in nuke
);

    wire                        [1:0]                   rrat_overwritten_valid;
    wire                        [1:0][PRF_BITS-1:0]     rrat_overwritten;
    wire        [NUM_ARF_ENTRIES-1:0][PRF_BITS-1:0]     rrat_entries;

    rrat #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rrat1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .rrat_commit_en(rrat_commit_en),
        .rrat_commit_prf_entry(rrat_commit_prf_entry),
        .rrat_commit_arf_idx(rrat_commit_arf_idx),
        .nuke(nuke),
        // OUTPUTS
        .rrat_entries_next(rrat_entries),
        .rrat_free_list_next(rrat_free_list),
        .rrat_overwritten_valid(rrat_overwritten_valid),
        .rrat_overwritten(rrat_overwritten)
    );

    rat #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rat1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .id_rda_idx(id_rda_idx),
        .id_rdb_idx(id_rdb_idx),
        .id_dest_idx(id_dest_idx),
        .id_valid_inst(id_valid_inst),
        .nuke(nuke),
        .rrat_entries(rrat_entries),
        .rrat_free_list(rrat_free_list),
        .rrat_commit_new_entry(rrat_overwritten),
        .rrat_commit_new_entry_en(rrat_overwritten_valid),
        // OUTPUTS
        .rat_rda_idx(rat_rda_idx),
        .rat_rdb_idx(rat_rdb_idx),
        .rat_dest_idx(rat_dest_idx),
        .rat_dest_valid(rat_dest_valid),
        .rat_stall(rat_stall)
    );

endmodule
`default_nettype wire

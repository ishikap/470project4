//For: Testing RAT
//Date 2/19/18

//TODO: chnage testbecnh. Inputs come in to rat from a buffer after c-q from reg then combinational delay for outputs so should be right at posedge
`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define TRUE_2          2'b01
`define FALSE_2         2'b00
`define WAY_0           1'b0
`define WAY_1           1'b1
`define R0              5'd0
`define R1              5'd1
`define R2              5'd2
`define R3              5'd3
`define R4              5'd4
`define R5              5'd5
`define R6              5'd6
`define R7              5'd7
`define R8              5'd8
`define R9              5'd9
`define R10             5'd10
`define R11             5'd11
`define R12             5'd12
`define R13             5'd13
`define R14             5'd14
`define R15             5'd15
`define R16             5'd16
`define R17             5'd17
`define R18             5'd18
`define R19             5'd19
`define R20             5'd20
`define R21             5'd21
`define R22             5'd22
`define R23             5'd23
`define R24             5'd24
`define R25             5'd25
`define R26             5'd26
`define R27             5'd27
`define R28             5'd28
`define R29             5'd29
`define R30             5'd30
`define R31             5'd31

parameter NUM_PRF_ENTRIES       = 96;
parameter QUADWORD              = 64;
parameter PRF_BITS              = $clog2(NUM_PRF_ENTRIES);
localparam NUM_ARF_ENTRIES      = 32;
localparam RAT_FREE_LIST_SIZE   = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES;
localparam RAT_FREE_LIST_BITS   = $clog2(RAT_FREE_LIST_SIZE);


module testbench;

    logic                                               clock;
    logic                                               reset;
    logic                      [1:0][4:0]               id_rda_idx;
    logic                      [1:0][4:0]               id_rdb_idx;
    logic                      [1:0][4:0]               id_dest_idx;
    logic                      [1:0]                    id_valid_inst;
    logic                      [1:0]                    nuke;
    logic      [NUM_ARF_ENTRIES-1:0][PRF_BITS-1:0]      rrat_entries;
    logic   [RAT_FREE_LIST_SIZE-1:0][PRF_BITS-1:0]      rrat_free_list;

    logic                      [1:0][PRF_BITS-1:0]      rrat_commit_new_entry;
    logic                      [1:0]                    rrat_commit_new_entry_en;
    logic                      [1:0][PRF_BITS-1:0]      rat_rda_idx;
    logic                      [1:0][PRF_BITS-1:0]      rat_rdb_idx;
    logic                      [1:0][PRF_BITS-1:0]      rat_dest_idx;
    logic                      [1:0]                    rat_dest_valid;
    logic                      [1:0]                    rat_stall;


    rat #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rat1(
           .clock(clock),
           .reset(reset),
           .id_rda_idx(id_rda_idx),
           .id_rdb_idx(id_rdb_idx),
           .id_dest_idx(id_dest_idx),
           .id_valid_inst(id_valid_inst),
           .nuke(nuke),
           .rrat_entries(rrat_entries),
           .rrat_free_list(rrat_free_list),

           .rrat_commit_new_entry(rrat_commit_new_entry),
           .rrat_commit_new_entry_en(rrat_commit_new_entry_en),
           .rat_rda_idx(rat_rda_idx),
           .rat_rdb_idx(rat_rdb_idx),
           .rat_dest_idx(rat_dest_idx),
           .rat_dest_valid(rat_dest_valid),
           .rat_stall(rat_stall)
        );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all the current RAT FREE LIST enteries
        $display("===RAT_FREE_LIST===");
        for (int i=0; i < RAT_FREE_LIST_SIZE; i++)
        begin
            $display ("Free List Entry Number: %d ", i,
                      //"Free List Entry: %d ", rat1.rat_free_list[i],
                      //"Pointer: %d", rat1.rat_pointer,
                      "Free List Empty? : %b", rat_stall);
        end

        //display all the current RAT enteries
        $display("\n\n===RAT===");
        for (int i=0; i < NUM_ARF_ENTRIES; i++)
        begin
            $display ("RAT Entry Number: %d ", i);
                      //"RAT Entry: %d ", rat1.rat_entries[i]);
        end
        $fatal("INCORRECT");
    endtask

    task dispatch;
        input                   way;
        input [4:0]             rda_idx;
        input [4:0]             rdb_idx;
        input [4:0]             dest_idx;
        input                   dest_en;

        id_rda_idx[way]     = rda_idx;
        id_rdb_idx[way]     = rdb_idx;
        id_dest_idx[way]    = dest_idx;
        id_valid_inst[way]  = dest_en;
    endtask

    task commit;
        input                   way;
        input [PRF_BITS-1:0]    entry;
        input                   en;

        rrat_commit_new_entry[way]      = entry;
        rrat_commit_new_entry_en[way]   = en;
    endtask

    task check_entry;
        input [4:0]             idx;
        input [PRF_BITS-1:0]    entry;

        //if (rat1.rat_entries[idx] != entry) exit_on_error();
    endtask

    task check_out;
        input                       way;
        input    [PRF_BITS-1:0]     rda_idx;
        input    [PRF_BITS-1:0]     rdb_idx;
        input    [PRF_BITS-1:0]     dest_idx;
        input                       valid_dest;
        if ((rat_rda_idx[way] != rda_idx) && valid_dest) exit_on_error();
        if ((rat_rdb_idx[way] != rdb_idx) && valid_dest) exit_on_error();
        if ((rat_dest_idx[way] != dest_idx) && valid_dest) exit_on_error();
        if (rat_dest_valid[way] != valid_dest) exit_on_error();
    endtask

    task check_free_list_pointer;
        input    [RAT_FREE_LIST_SIZE-1:0]       pointer;            //new pointer

        //if (rat1.rat_pointer != pointer) exit_on_error();
    endtask

    task check_free_list_entry;
        input [PRF_BITS-1:0]            entry;
        input[RAT_FREE_LIST_SIZE-1:0]   location;            //location

        //if (rat1.rat_free_list[location] != entry) exit_on_error();
    endtask


    initial
    begin
        $monitor("===WAY_0 INPUTS===\nTime= %d  rda_idx= %d rdb_idx= %d dest_idx= %d valid_inst= %b nuke= %b  commit_entry = %d commit_en = %b\n",  $time, id_rda_idx[0], id_rdb_idx[0], id_dest_idx[0], id_valid_inst[0], nuke[0], rrat_commit_new_entry[0], rrat_commit_new_entry_en[0],
                 "===WAY_0 OUTPUTS===\nTime= %d rda_out= %d rdb_out= %d dest_out= %d dest_valid= %b stall=%b ptr_next=%d\n", $time, rat_rda_idx[0], rat_rdb_idx[0], rat_dest_idx[0], rat_dest_valid[0], rat_stall[0], 0, //rat1.rat_pointer_next,
                 "===WAY_1 INPUTS===\nTime= %d  rda_idx= %d rdb_idx= %d dest_idx= %d valid_inst= %b nuke= %b  commit_entry = %d commit_en = %b\n",  $time, id_rda_idx[1], id_rdb_idx[1], id_dest_idx[1], id_valid_inst[1], nuke[1], rrat_commit_new_entry[1], rrat_commit_new_entry_en[1],
                 "===WAY_1 OUTPUTS===\nTime= %d rda_out= %d rdb_out= %d dest_out= %d dest_valid= %b stall=%b ptr_next=%d\n", $time, rat_rda_idx[1], rat_rdb_idx[1], rat_dest_idx[1], rat_dest_valid[1], rat_stall[1], 0); //rat1.rat_pointer_next);

        clock=0;
        reset=1;
        id_rda_idx=0;
        id_rdb_idx=0;
        id_dest_idx=0;
        id_valid_inst=0;
        nuke=0;
        for (int i = 0; i < 11; ++i)
        begin
            rrat_free_list[i] = i+5;
        end
        for (int i = 0; i < NUM_ARF_ENTRIES; ++i)
        begin
            rrat_entries[i] = i+16;
        end

        rrat_commit_new_entry=0;
        rrat_commit_new_entry_en=0;

        @(negedge clock);
        //CHECK reset
        reset = 0;
        for (int i=0; i< NUM_ARF_ENTRIES; ++i) begin
            check_entry(i, i);
        end
        check_out(`WAY_0, 0, 0, 0, `FALSE);
        check_out(`WAY_1, 0, 0, 0, `FALSE);
        check_free_list_pointer(7'd64);
        $display("\n@@@Passed reset\n");

        // Dispatch both ways with dest R31
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R31, `R31, `TRUE);
        dispatch(`WAY_0, `R31, `R2, `R31, `TRUE);
        @(posedge clock);
        check_out(`WAY_1, 7'd1, 7'd31, 7'd95, `TRUE);
        check_out(`WAY_0, 7'd31, 7'd2, 7'd94, `TRUE);
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R31, `R31, `FALSE);
        dispatch(`WAY_0, `R31, `R2, `R31, `FALSE);
        check_free_list_pointer(7'd62);
        check_entry(`R31, 7'd31);
        commit(`WAY_1, 7'd94, `TRUE);
        commit(`WAY_0, 7'd95, `TRUE);
        @(negedge clock);
        commit(`WAY_1, 7'd94, `FALSE);
        commit(`WAY_0, 7'd95, `FALSE);
        $display("\n@@@Passed dispatch both ways with dest R31\n");

        // writes twice consecutively
        @(negedge clock);
        dispatch(`WAY_1, `R15, `R16, `R17, `TRUE);
        @(posedge clock);
        check_out(`WAY_1, 7'd15, 7'd16, 7'd95, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd63);
        dispatch(`WAY_1, `R18, `R19, `R20, `TRUE);
        @(posedge clock);
        check_out(`WAY_1, 7'd18, 7'd19, 7'd94, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd62);
        dispatch(`WAY_1, `R18, `R19, `R17, `FALSE);
        commit(`WAY_1, 7'd94, `TRUE);
        commit(`WAY_0, 7'd95, `TRUE);
        @(negedge clock);
        commit(`WAY_1, 7'd94, `FALSE);
        commit(`WAY_0, 7'd95, `FALSE);
        $display("\n@@@Passed write twice\n");

        // Dispatch Way 0 only
        @(negedge clock);
        dispatch(`WAY_0, `R1, `R2, `R1, `TRUE);
        @(posedge clock);
        dispatch(`WAY_0, `R1, `R2, `R1, `FALSE);
        check_out(`WAY_0, 7'd1, 7'd2, 7'd95, `TRUE);
        @(negedge clock);
        check_entry(`R1, 7'd95);
        check_free_list_pointer(7'd63);
        $display("\n@@@Passed dispatch way 0\n");

        // Dispatch Way 1 only
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R2, `R1, `TRUE);
        @(posedge clock);
        dispatch(`WAY_1, `R1, `R2, `R1, `FALSE);
        check_out(`WAY_1, 7'd95, 7'd2, 7'd94, `TRUE);
        @(negedge clock);
        check_entry(`R1, 7'd94);
        check_free_list_pointer(7'd62);
        $display("\n@@@Passed dispatch way 1\n");

        // Dispatch Way 1 and 0
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R2, `R1, `TRUE);
        dispatch(`WAY_0, `R1, `R1, `R3, `TRUE);
        @(posedge clock);
        dispatch(`WAY_1, `R1, `R2, `R1, `FALSE);
        dispatch(`WAY_0, `R1, `R2, `R1, `FALSE);
        check_out(`WAY_1, 7'd94, 7'd2, 7'd93, `TRUE);
        check_out(`WAY_0, 7'd93, 7'd93, 7'd92, `TRUE);
        @(negedge clock);
        check_entry(`R1, 7'd93);
        check_entry(`R3, 7'd92);
        check_free_list_pointer(7'd60);
        $display("\n@@@Passed dispatch way 1 and 0\n");

        // Commit Way 0 only
        @(negedge clock);
        commit(`WAY_0, 7'd1, `TRUE);
        @(negedge clock);
        commit(`WAY_0, 7'd1, `FALSE);
        check_free_list_entry(7'd1, 7'd60);
        check_free_list_pointer(7'd61);
        $display("\n@@@Passed commit way 0\n");

        // Commit Way 1 only
        @(negedge clock);
        commit(`WAY_1, 7'd95, `TRUE);
        @(negedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        check_free_list_entry(7'd95, 7'd61);
        check_free_list_pointer(7'd62);
        $display("\n@@@Passed commit way 1\n");

        // Commit Way 1 and 0
        @(negedge clock);
        commit(`WAY_1, 7'd94, `TRUE);
        commit(`WAY_0, 7'd3, `TRUE);
        @(negedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        check_free_list_entry(7'd94, 7'd62);
        check_free_list_entry(7'd3, 7'd63);
        check_free_list_pointer(7'd64);
        $display("\n@@@Passed commit way 0 and 1\n");

        // RAT free list empty
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R1, `R1, `R3, `TRUE);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        dispatch(`WAY_0, `R1, `R1, `R3, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        @(negedge clock);
        //exit_on_error(); has 1 entry now
        check_free_list_pointer(7'd2);
        if (rat_stall != 2'b00) exit_on_error();
        #1;
        check_out(`WAY_1, 7'd35, 7'd35, 7'd33, `TRUE);
        check_out(`WAY_0, 7'd33, 7'd33, 7'd33, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd1);
        check_free_list_entry(7'd32, 7'd0);
        if (rat_stall != 2'b00) exit_on_error();
        dispatch(`WAY_0, `R1, `R1, `R3, `TRUE);
        @(posedge clock);
        if (rat_stall != 2'b01) exit_on_error();
        check_out(`WAY_1, 7'd33, 7'd33, 7'd32, `TRUE);
        // When stalled, should still read values
        check_out(`WAY_0, 7'd32, 7'd32, 7'd0, `FALSE);
        @(negedge clock);
        if (rat_stall != 2'b11) exit_on_error();
        check_out(`WAY_1, 7'd33, 7'd33, 7'd32, `FALSE);
        check_free_list_pointer(7'd0);
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R1, `R1, `R3, `FALSE);
        check_out(`WAY_1, 7'd32, 7'd32, 7'd31, `FALSE);
        check_out(`WAY_0, 7'd32, 7'd32, 7'd0, `FALSE);
        $display("part way");
        @(negedge clock);
        dispatch(`WAY_1, `R10, `R25, `R6, `FALSE);
        dispatch(`WAY_0, `R14, `R18, `R7, `TRUE);
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R1, `R1, `R3, `FALSE);
        check_out(`WAY_1, 7'd10, 7'd25, 7'd2, `FALSE);
        check_out(`WAY_0, 7'd31, 7'd31, 7'd0, `FALSE);
        $display("\n@@@Passed dispatch all, stall\n");

        //dispatching and committing
        @(negedge clock);
        commit(`WAY_1, 7'd31, `TRUE);
        commit(`WAY_0, 7'd32, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd2);
        check_free_list_entry(7'd31, 7'd0);
        check_free_list_entry(7'd32, 7'd1);
        commit(`WAY_1, 7'd33, `TRUE);
        commit(`WAY_0, 7'd34, `TRUE);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R1, `R1, `R3, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R1, `R1, `R3, `FALSE);
        check_free_list_pointer(7'd2);
        check_out(`WAY_1, 7'd32, 7'd32, 7'd34, `TRUE);
        check_out(`WAY_0, 7'd34, 7'd34, 7'd33, `TRUE);
        check_free_list_entry(7'd31, 7'd0);
        check_free_list_entry(7'd32, 7'd1);
        $display("\n@@@Passed commit then commit and dispatch at same time\n");

        // Empty freelist then commit and dispatch forward
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R1, `R1, `R3, `TRUE);
        #1;
        check_out(`WAY_1, 7'd34, 7'd34, 7'd32, `TRUE);
        check_out(`WAY_0, 7'd32, 7'd32, 7'd31, `TRUE);
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R1, `R1, `R3, `FALSE);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed clearing\n");
        @(negedge clock);
        commit(`WAY_1, 7'd36, `TRUE);
        commit(`WAY_0, 7'd37, `TRUE);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R1, `R1, `R3, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R1, `R1, `R3, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_1, 7'd32, 7'd32, 7'd37, `TRUE);
        check_out(`WAY_0, 7'd37, 7'd37, 7'd36, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed commit and dispatch both ways when empty\n");

        // Forwarding - commit both ways, dispatch way 1
        @(negedge clock);
        commit(`WAY_1, 7'd38, `TRUE);
        commit(`WAY_0, 7'd39, `TRUE);
        dispatch(`WAY_1, `R4, `R3, `R5, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R5, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_1, 7'd4, 7'd36, 7'd39, `TRUE);
        check_out(`WAY_0, 7'd0, 7'd0, 0, `FALSE);
        @(negedge clock);
        check_free_list_entry(7'd38, 7'd0);
        check_free_list_pointer(7'd1);
        $display("\n@@@Passed commit 2 way and dispatch way 1 when empty\n");

        // Dispatch way 0 with 1 free list entry (can happen)
        @(negedge clock);
        dispatch(`WAY_0, `R1, `R1, `R1, `TRUE);
        @(posedge clock);
        dispatch(`WAY_0, `R1, `R1, `R1, `FALSE);
        check_out(`WAY_0, 7'd37, 7'd37, 7'd38, `TRUE);
        check_out(`WAY_1, 7'd0, 7'd0, 0, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        @(negedge clock);
        check_free_list_entry(7'd38, 7'd0);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed yes way 0 dispatch\n");

        /*// Dispatch way 1 with 1 free list entry (should happen)
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        check_out(`WAY_1, 7'd37, 7'd37, 7'd38, `TRUE);
        check_out(`WAY_0, 7'd0, 7'd0, 0, `FALSE);
        if (rat_stall != 2'b11) exit_on_error();
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed way 1 dispatch with  free list entry\n");
*/
        // Forwarding - commit both ways, dispatch way 0
        @(negedge clock);
        commit(`WAY_1, 7'd40, `TRUE);
        commit(`WAY_0, 7'd41, `TRUE);
        dispatch(`WAY_0, `R1, `R8, `R3, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_0, 7'd38, 7'd8, 7'd41, `TRUE);
        check_out(`WAY_1, 7'd0, 7'd0, 0, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd1);
        check_free_list_entry(7'd40, 7'd0);
        $display("\n@@@Passed commit 2 way and dispatch way 0 when empty\n");


        // Dispatch way 1 with 1 free list entry (should happen)
        @(negedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        @(posedge clock);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        check_out(`WAY_1, 7'd38, 7'd38, 7'd40, `TRUE);
        check_out(`WAY_0, 7'd0, 7'd0, 0, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        @(negedge clock);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed way 1 dispatch with 1 free list entry\n");

        // Forwarding - Commit way 1 and Dispatch both ways
        @(negedge clock);
        commit(`WAY_1, 7'd42, `TRUE);
        dispatch(`WAY_1, `R8, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R1, `R8, `R3, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b01) exit_on_error();
        check_out(`WAY_1, 7'd8, 7'd40, 7'd42, `TRUE);
        check_out(`WAY_0, 7'd42, 7'd8, 0, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed commit way1 and dispatch both ways when empty\n");


        // Forwarding - Commit way 1 and Dispatch way 1
        @(negedge clock);
        commit(`WAY_1, 7'd43, `TRUE);
        dispatch(`WAY_1, `R8, `R1, `R1, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_1, 7'd8, 7'd42, 7'd43, `TRUE);
        check_out(`WAY_0, 7'd0, 7'd0, 0, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed commit way 1 and dispatch way 1 when empty\n");

        // Forwarding - Commit way 1 and Dispatch way 0
        @(negedge clock);
        commit(`WAY_1, 7'd44, `TRUE);
        dispatch(`WAY_0, `R8, `R1, `R1, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_0, 7'd8, 7'd43, 7'd44, `TRUE);
        check_out(`WAY_1, 7'd0, 7'd0, 0, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed commit way 1 and dispatch way 0 when empty\n");

        // Forwarding - Commit way 0 and Dispatch both ways
        @(negedge clock);
        commit(`WAY_0, 7'd45, `TRUE);
        dispatch(`WAY_1, `R8, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R1, `R8, `R3, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b01) exit_on_error();
        check_out(`WAY_1, 7'd8, 7'd44, 7'd45, `TRUE);
        check_out(`WAY_0, 7'd45, 7'd8, 0, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        dispatch(`WAY_1, `R8, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R8, `R1, `R1, `TRUE);
        @(posedge clock);
        if (rat_stall != 2'b11) exit_on_error();
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        $display("\n@@@Passed commit way0 and dispatch both ways when empty\n");


        // Forwarding - Commit way 0 and Dispatch way 1
        @(negedge clock);
        commit(`WAY_0, 7'd46, `TRUE);
        dispatch(`WAY_1, `R8, `R1, `R1, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_1, 7'd8, 7'd45, 7'd46, `TRUE);
        check_out(`WAY_0, 7'd0, 7'd0, 0, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed commit way 0 and dispatch way 1 when empty\n");

        // Forwarding - Commit way 0 and Dispatch way 0
        @(negedge clock);
        commit(`WAY_0, 7'd47, `TRUE);
        dispatch(`WAY_0, `R8, `R1, `R1, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_0, 7'd8, 7'd46, 7'd47, `TRUE);
        check_out(`WAY_1, 7'd0, 7'd0, 0, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        $display("\n@@@Passed commit way 0 and dispatch way 0 when empty\n");

        // Forwarding when stall 01- Commit and dispatch both ways
        @(negedge clock);
        commit(`WAY_0, 7'd50, `TRUE);
        @(negedge clock);
        commit(`WAY_0, 7'd1, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_free_list_pointer(7'd1);
        check_free_list_entry(7'd50, 7'd0);
        @(negedge clock);
        commit(`WAY_1, 7'd51, `TRUE);
        commit(`WAY_0, 7'd52, `TRUE);
        dispatch(`WAY_1, `R9, `R10, `R4, `TRUE);
        dispatch(`WAY_0, `R4, `R10, `R4, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_1, 7'd9, 7'd10, 7'd52, `TRUE);
        check_out(`WAY_0, 7'd52, 7'd10, 7'd51, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd1);
        check_free_list_entry(7'd50, 7'd0);
        $display("\n@@@Passed forward when stall 01-2 commits and 2 renames\n");

        // Forwarding when stall 01- commit both dispatch way 0
        @(negedge clock);
        commit(`WAY_1, 7'd53, `TRUE);
        commit(`WAY_0, 7'd54, `TRUE);
        dispatch(`WAY_0, `R4, `R10, `R4, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R4, `TRUE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_0, 7'd51, 7'd10, 7'd54, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd2);
        check_free_list_entry(7'd50, 7'd0);
        check_free_list_entry(7'd53, 7'd1);
        $display("\n@@@Passed forward when stall 01-2 commits and 1 renames\n");

        // Forwarding when stall 01- commit both dispatch way 1
        @(negedge clock);
        commit(`WAY_1, 7'd55, `TRUE);
        commit(`WAY_0, 7'd56, `TRUE);
        dispatch(`WAY_1, `R4, `R10, `R4, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R4, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R4, `TRUE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_1, 7'd53, 7'd10, 7'd56, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd2);
        check_free_list_entry(7'd50, 7'd0);
        check_free_list_entry(7'd55, 7'd1);
        $display("\n@@@Passed forward when stall 01-2 commits and 1 renames - 2\n");

        // Forwarding when stall 01- commit 1 and dispatch way 0
        @(negedge clock);
        commit(`WAY_1, 7'd57, `TRUE);
        dispatch(`WAY_0, `R4, `R10, `R4, `TRUE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_0, 7'd55, 7'd10, 7'd57, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd1);
        check_free_list_entry(7'd50, 7'd0);
        $display("\n@@@Passed forward when stall 01-1 commits and way 0 renames\n");

        // Forwarding when stall 01- commit 1 and dispatch way 1
        @(negedge clock);
        commit(`WAY_0, 7'd58, `TRUE);
        dispatch(`WAY_1, `R4, `R10, `R4, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b00) exit_on_error();
        check_out(`WAY_1, 7'd57, 7'd10, 7'd58, `TRUE);
        @(negedge clock);
        check_free_list_pointer(7'd1);
        check_free_list_entry(7'd50, 7'd0);
        $display("\n@@@Passed forward when stall 01-2 commits and 1 renames\n");

        //one commit and 2 disp when empty
        @(negedge clock);
        dispatch(`WAY_1, `R6, `R7, `R5, `FALSE);
        @(negedge clock);
        commit(`WAY_0, 7'd59, `TRUE);
        dispatch(`WAY_1, `R4, `R10, `R4, `TRUE);
        dispatch(`WAY_0, `R4, `R10, `R4, `TRUE);
        @(posedge clock);
        commit(`WAY_1, 7'd1, `FALSE);
        commit(`WAY_0, 7'd1, `FALSE);
        dispatch(`WAY_1, `R1, `R1, `R1, `TRUE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        if (rat_stall != 2'b01) exit_on_error();
        check_out(`WAY_1, 7'd58, 7'd10, 7'd59, `TRUE);
        check_out(`WAY_0, 7'd59, 7'd10, 7'd58, `FALSE);
        @(negedge clock);
        check_free_list_pointer(7'd0);
        dispatch(`WAY_1, `R1, `R1, `R1, `FALSE);
        dispatch(`WAY_0, `R6, `R7, `R5, `FALSE);
        $display("\n@@@Passed forward when stall 11-1 commits and 2 renames\n");

        // test nuke
        @(negedge clock);
        nuke = 2'b11;
        @(negedge clock);
        for (int i = 0; i < NUM_ARF_ENTRIES; ++i)
        begin
            //if (rat1.rat_entries[i] != rrat_entries[i]) exit_on_error();
        end
        for (int i = 0; i < 11; ++i)
        begin
            //if (rat1.rat_free_list[i] != rrat_free_list[i]) exit_on_error();
        end
        $display("\n@@@Passed Nuke\n");

        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

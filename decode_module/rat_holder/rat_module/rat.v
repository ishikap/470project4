// Clock period (3/5): 4.2 < t <= 4.20625
`default_nettype none
`timescale 1ns/100ps
module rat #(parameter NUM_PRF_ENTRIES = 96, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam NUM_ARF_ENTRIES = 32, localparam RAT_FREE_LIST_SIZE = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES, localparam RAT_FREE_LIST_BITS = $clog2(RAT_FREE_LIST_SIZE), localparam ARF_BITS = $clog2(NUM_ARF_ENTRIES)) (
    //INPUTS
    input   wire                                                clock,
    input   wire                                                reset,
    input   wire                       [1:0][ARF_BITS-1:0]      id_rda_idx,
    input   wire                       [1:0][ARF_BITS-1:0]      id_rdb_idx,
    input   wire                       [1:0][ARF_BITS-1:0]      id_dest_idx,
    input   wire                       [1:0]                    id_valid_inst,     // When stalled, invalid
    input   wire                       [1:0]                    nuke,

    input   wire       [NUM_ARF_ENTRIES-1:0][PRF_BITS-1:0]      rrat_entries,
    input   wire    [RAT_FREE_LIST_SIZE-1:0][PRF_BITS-1:0]      rrat_free_list,

    input   wire                       [1:0]                    rrat_commit_new_entry_en,
    input   wire                       [1:0][PRF_BITS-1:0]      rrat_commit_new_entry,

    //OUTPUTS
    output  logic                      [1:0][PRF_BITS-1:0]      rat_rda_idx,
    output  logic                      [1:0][PRF_BITS-1:0]      rat_rdb_idx,
    output  logic                      [1:0][PRF_BITS-1:0]      rat_dest_idx,
    output  logic                      [1:0]                    rat_dest_valid,     // is it needed?
    output  logic                      [1:0]                    rat_stall
);
    // Holds RAT table assignments
    logic      [NUM_ARF_ENTRIES-1:0][PRF_BITS-1:0]  rat_entries,    rat_entries_next;
    logic   [RAT_FREE_LIST_SIZE-1:0][PRF_BITS-1:0]  rat_free_list,  rat_free_list_next;
    logic     [RAT_FREE_LIST_BITS:0]                rat_pointer,    rat_pointer_next;

    always_comb begin
        rat_entries_next        = rat_entries;
        rat_free_list_next      = rat_free_list;
        rat_pointer_next        = rat_pointer;
        rat_stall               = 0;
        rat_rda_idx             = 0;
        rat_rdb_idx             = 0;
        rat_dest_idx            = 0;
        rat_dest_valid          = 0;

        // Add commited entries from RRAT to RAT's free list
        for (int i=1; i>=0; --i) begin
            if (rrat_commit_new_entry_en[i]) begin
                rat_free_list_next[rat_pointer_next]    = rrat_commit_new_entry[i];
                rat_pointer_next                        = rat_pointer_next + 1;
            end
        end

        // Read source operands, rename dest reg: remove from free list and add to RAT
        for (int i=1; i>=0; --i) begin
            // Read source operands
            rat_rda_idx[i]                              = rat_entries_next[id_rda_idx[i]];
            rat_rdb_idx[i]                              = rat_entries_next[id_rdb_idx[i]];

            // Pop a free list entry value and put in the RAT, dont if empty or way [0] stalls
            // Stall only if can not give out the value it wants in that cycle
            if (id_valid_inst[i] && rat_pointer_next == 0) begin
                rat_stall[i]                            = 1'b1;
            end
            else if (id_valid_inst[i]) begin
                rat_pointer_next                        = rat_pointer_next - 1;
                if (id_dest_idx[i] != 5'd31) begin
                    rat_entries_next[id_dest_idx[i]]    = rat_free_list_next[rat_pointer_next];
                end
                rat_dest_idx[i]                         = rat_free_list_next[rat_pointer_next];
                rat_dest_valid[i]                       = 1'b1;
            end
        end

        // Nuke
        if (nuke) begin
            rat_entries_next    = rrat_entries;
            rat_pointer_next    = RAT_FREE_LIST_SIZE;
            rat_free_list_next  = rrat_free_list;
            rat_dest_valid      = 0;
            rat_stall           = 0;
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            for (int i=0; i<32; ++i) begin
                rat_entries[i]                  <= `SD i;
            end
            for (int i=32; i<NUM_PRF_ENTRIES; ++i) begin
                rat_free_list[i-32]             <= `SD i;
            end
            rat_pointer                         <= `SD RAT_FREE_LIST_SIZE;
        end
        else begin
            rat_entries                         <= `SD rat_entries_next;
            rat_free_list                       <= `SD rat_free_list_next;
            rat_pointer                         <= `SD rat_pointer_next;
        end
    end

endmodule
`default_nettype wire

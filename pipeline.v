/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  pipeline.v                                          //
//                                                                     //
//  Description :  Top-level module of the verisimple pipeline;        //
//                 This instantiates and connects the 5 stages of the  //
//                 Verisimple pipeline togeather.                      //
//                                                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`default_nettype none
`timescale 1ns/100ps
module pipeline #(parameter NUM_BTB_ENTRIES = 32, parameter NUM_BTB_WAYS = 4, parameter NUM_BIMODAL_BRANCH_PRED_ENTRIES=32, parameter NUM_GSHARE_BRANCH_PRED_ENTRIES=64, parameter BRANCH_COUNTER_BITS = 2,
                  parameter RAS_SIZE = 16, parameter TOURNAMENT_ENTRIES = 8,
                  localparam NUM_ARF_ENTRIES = 32, localparam ARF_BITS = $clog2(NUM_ARF_ENTRIES),
                  parameter NUM_RS = 16, parameter ROB_SIZE = 64, localparam FREE_LIST_BITS = $clog2(ROB_SIZE),
                  parameter I_NUM_WAYS = 2, parameter D_NUM_WAYS = 2,
                  parameter NUM_PRF_ENTRIES = ROB_SIZE + NUM_ARF_ENTRIES, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), parameter MULT_STAGES = 4,
                  parameter FINISHED_INSTR = 6, localparam QUADWORD = 64,
                  parameter LDB_SIZE = 8, parameter STQ_SIZE = 4, localparam CDB_INPUTS = FINISHED_INSTR + 1 + LDB_SIZE) (
    // INPUTS
    input   wire                            clock,              // System clock
    input   wire                            reset,              // System reset

    input   wire         [3:0]              mem2proc_response,  // Tag from memory about current request
    input   wire         [QUADWORD-1:0]     mem2proc_data,      // Data coming back from memory
    input   wire         [3:0]              mem2proc_tag,       // Tag from memory about current reply

    // OUTPUTS
    output  logic   [1:0]                   proc2mem_command,   // command sent to memory
    output  logic        [QUADWORD-1:0]     proc2mem_addr,      // Address sent to memory
    output  logic        [QUADWORD-1:0]     proc2mem_data,      // Data sent to memory

    output  logic   [1:0][3:0]              pipeline_completed_insts,
    output  logic   [1:0][3:0]              pipeline_error_status,
    output  logic   [1:0]                   pipeline_halt_out,
    output  logic   [1:0][4:0]              pipeline_commit_wr_idx,
    output  logic   [1:0][PRF_BITS-1:0]     pipeline_commit_prf,
    output  logic   [1:0]                   pipeline_commit_wr_en,
    output  logic   [1:0][QUADWORD-1:0]     pipeline_commit_PC
                  );

	// Pipeline register enables
	logic                  [1:0]                                if_id_enable, if_id_stall, id_disp_enable, disp_enable, id_disp_stall, disp_ex_enable, ex_enable;
    logic   [FINISHED_INSTR+1:0]                                ex_wb_enable; // 6 + 2
    logic           [LDB_SIZE:0]                                lsq_wb_enable; // stq + ldb

    // Outputs from IF stage
    logic                  [1:0][QUADWORD-1:0]                  if_PC_out;
    logic                  [1:0][QUADWORD-1:0]                  if_NPC_out;
    logic                  [1:0][QUADWORD/2-1:0]                if_IR_out;
    logic                  [1:0]                                if_valid_inst_out;

    // Outputs from IF/ID Pipeline Register
    logic                  [1:0][QUADWORD-1:0]                  if_id_PC_out;
    logic                  [1:0][QUADWORD-1:0]                  if_id_NPC_out;
    logic                  [1:0][QUADWORD/2-1:0]                if_id_IR_out;
    logic                  [1:0]                                if_id_valid_inst_out;

	// Outputs from ID stage
    logic                  [1:0][1:0]                           id_opa_select_out;
    logic                  [1:0][1:0]                           id_opb_select_out;
    logic                  [1:0][ARF_BITS-1:0]                  id_arf_idx_out;
    logic                  [1:0][4:0]                           id_alu_func_out;
    logic                  [1:0]                                id_rd_mem_out;
    logic                  [1:0]                                id_wr_mem_out;
    logic                  [1:0]                                id_ldl_mem_out;
    logic                  [1:0]                                id_stc_mem_out;
    logic                  [1:0]                                id_cond_branch_out;
    logic                  [1:0]                                id_uncond_branch_out;
    logic                  [1:0]                                id_branch_call;
    logic                  [1:0]                                id_branch_ret;
    logic                  [1:0]                                id_halt_out;
    logic                  [1:0]                                id_illegal_out;
    logic                  [1:0]                                id_valid_inst_out;
    logic                  [1:0][PRF_BITS-1:0]                  id_rat_rda_idx;
    logic                  [1:0][PRF_BITS-1:0]                  id_rat_rdb_idx;
    logic                  [1:0][PRF_BITS-1:0]                  id_rat_dest_idx;
    logic                  [1:0]                                id_rat_dest_valid;
    logic                  [1:0]                                id_rat_stall;
    logic         [ROB_SIZE-1:0][PRF_BITS-1:0]                  id_rrat_free_list;

	// Outputs from ID/Disp Pipeline Register
    logic                  [1:0][QUADWORD-1:0]                  id_disp_PC_out;
    logic                  [1:0][QUADWORD-1:0]                  id_disp_NPC_out;
    logic                  [1:0][QUADWORD/2-1:0]                id_disp_IR_out;
    logic                  [1:0][1:0]                           id_disp_opa_select_out;
    logic                  [1:0][1:0]                           id_disp_opb_select_out;
    logic                  [1:0][ARF_BITS-1:0]                  id_disp_arf_idx_out;
    logic                  [1:0][4:0]                           id_disp_alu_func_out;
    logic                  [1:0]                                id_disp_rd_mem_out;
    logic                  [1:0]                                id_disp_wr_mem_out;
    logic                  [1:0]                                id_disp_ldl_mem_out;
    logic                  [1:0]                                id_disp_stc_mem_out;
    logic                  [1:0]                                id_disp_cond_branch_out;
    logic                  [1:0]                                id_disp_uncond_branch_out;
    logic                  [1:0]                                id_disp_branch_call;
    logic                  [1:0]                                id_disp_branch_ret;
    logic                  [1:0]                                id_disp_halt_out;
    logic                  [1:0]                                id_disp_illegal_out;
    logic                  [1:0]                                id_disp_valid_inst_out;
    logic                  [1:0][PRF_BITS-1:0]                  id_disp_rat_rda_idx;
    logic                  [1:0][PRF_BITS-1:0]                  id_disp_rat_rdb_idx;
    logic                  [1:0][PRF_BITS-1:0]                  id_disp_rat_dest_idx;
    logic                  [1:0]                                id_disp_rat_dest_valid;

    // Outputs from Disp stage
    logic                  [1:0]                                disp_rs_output_mem_valid;
    logic                  [1:0][QUADWORD-1:0]                  disp_rs_opb_mem_out;
    logic                  [1:0][PRF_BITS-1:0]                  disp_rs_dest_mem_out;
    logic                  [1:0][QUADWORD/2-1:0]                disp_rs_mem_IR;

    logic                  [1:0]                                disp_rs_output_no_mem_valid;
    logic                  [1:0][QUADWORD-1:0]                  disp_rs_opa_out;
    logic                  [1:0][QUADWORD-1:0]                  disp_rs_opb_out;
    logic                  [1:0][PRF_BITS-1:0]                  disp_rs_dest_out;
    logic                  [1:0][4:0]                           disp_rs_alu_func_out;
    logic                  [1:0][1:0]                           disp_rs_opa_select_out;
    logic                  [1:0][1:0]                           disp_rs_opb_select_out;
    logic                  [1:0]                                disp_rs_cond_branch_out;
    logic                  [1:0]                                disp_rs_uncond_branch_out;
    logic                  [1:0][QUADWORD-1:0]                  disp_rs_PC;
    logic                  [1:0][QUADWORD-1:0]                  disp_rs_NPC;
    logic                  [1:0][QUADWORD/2-1:0]                disp_rs_IR;
    logic                  [1:0]                                disp_rs_stall;
    logic                  [1:0]                                disp_rob_stall;
    logic                  [1:0]                                disp_rob_valid;
    logic                  [1:0][ARF_BITS-1:0]                  disp_rob_arch_reg_out;
    logic                  [1:0][PRF_BITS-1:0]                  disp_rob_prf_reg_out;
    logic                  [1:0]                                disp_rob_nuke;
    logic                  [1:0]                                disp_rob_branch_actual_out;
    logic                  [1:0][QUADWORD-1:0]                  disp_rob_pc_out;
    logic                  [1:0][QUADWORD-1:0]                  disp_rob_next_pc_out;
    logic                  [1:0]                                disp_rob_branch_conditional_out;
    logic                  [1:0]                                disp_rob_branch_unconditional_out;
    logic                  [1:0]                                disp_rob_branch_call_out;
    logic                  [1:0]                                disp_rob_branch_ret_out;
    logic                  [1:0]                                disp_rob_load_out;
    logic                  [1:0]                                disp_rob_store_out;
    logic                  [1:0]                                disp_rob_halt_out;
    logic                  [1:0]                                disp_rob_illegal_out;
    logic                                                       disp_rob_store_request_out;
    logic                                                       disp_rob_store_executed;
    logic                  [1:0][QUADWORD-1:0]                  disp_prf_rda_out;
    logic                  [1:0]                                disp_prf_rda_valid_out;

    // Outputs from EX stage
    logic   [FINISHED_INSTR-1:0]                                ex_valid;
    logic   [FINISHED_INSTR-1:0][PRF_BITS-1:0]                  ex_pr_tag;
    logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]                  ex_results;
    logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]                  ex_next_pc;
    logic   [FINISHED_INSTR-1:0]                                ex_branch_taken;
    logic                  [1:0][QUADWORD-1:0]                  ex_lsq_mem_addr;
    logic                  [1:0][PRF_BITS-1:0]                  ex_lsq_prf_tag;
    logic                  [1:0]                                ex_lsq_valid;
    logic                  [1:0]                                ex_mult_stall_out;

    // Outputs from EX/WB Pipeline Register
    logic   [FINISHED_INSTR-1:0]                                ex_wb_valid;
    logic   [FINISHED_INSTR-1:0][PRF_BITS-1:0]                  ex_wb_pr_tag;
    logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]                  ex_wb_results;
    logic   [FINISHED_INSTR-1:0][QUADWORD-1:0]                  ex_wb_next_pc;
    logic   [FINISHED_INSTR-1:0]                                ex_wb_branch_taken;
    logic                  [1:0][QUADWORD-1:0]                  ex_wb_lsq_mem_addr;
    logic                  [1:0][PRF_BITS-1:0]                  ex_wb_lsq_prf_tag;
    logic                  [1:0]                                ex_wb_lsq_valid;

    // Outputs from WB stage
    logic                  [1:0]                                wb_valid_out;
    logic                  [1:0][PRF_BITS-1:0]                  wb_pr_tag_out;
    logic                  [1:0][QUADWORD-1:0]                  wb_result_out;
    logic                  [1:0][QUADWORD-1:0]                  wb_next_pc_out;
    logic                  [1:0]                                wb_branch_taken_out;
    logic       [CDB_INPUTS-1:0]                                wb_cdb_stall;

	// Icache wires
	logic                  [1:0][QUADWORD/2-1:0]                Icache_data_out;
    logic                  [1:0][QUADWORD-1:0]                  proc2Icache_addr;
    logic                  [1:0]                                proc2Icache_addr_valid;
    logic                  [1:0][QUADWORD-1:0]                  proc2Icache_next_pc;
	logic                  [1:0]                                Icache_valid_out;
    logic                                                       cache_stq_stall;
    logic                  [2:0][QUADWORD-1:0]                  cache_ldb_data_out;
    logic                  [2:0][QUADWORD-1:0]                  cache_ldb_addr_out;
    logic                  [2:0][PRF_BITS-1:0]                  cache_ldb_prf_out;
    logic                  [2:0]                                cache_ldb_valid_out;
    logic                  [1:0]                                cache_ldb_stall_out;
    logic                                                       mshr2proc_finished;

    // Outputs from LSQ
    logic            [1:0]                                      lsq_stq_stall;
    logic            [1:0]                                      lsq_ldb_stall;
    logic                                                       lsq_stored_valid;
    logic                 [PRF_BITS-1:0]                        lsq_stored_prf;
    logic                 [QUADWORD-1:0]                        lsq_stored_result;
    logic   [LDB_SIZE-1:0]                                      lsq_load_data_valid;
    logic   [LDB_SIZE-1:0][PRF_BITS-1:0]                        lsq_load_data_prf_tag;
    logic   [LDB_SIZE-1:0][QUADWORD-1:0]                        lsq_load_data;
    logic                                                       lsq_stq_data_to_dcache_valid;
    logic                                                       lsq_stq_write_to_dcache;
    logic                 [QUADWORD-1:0]                        lsq_stq_address_to_dcache;
    logic                 [QUADWORD-1:0]                        lsq_stq_data_to_dcache;
    logic            [1:0]                                      lsq_ldb_addr_to_dcache_valid;
    logic            [1:0]                                      lsq_ldb_read_from_dcache;
    logic            [1:0][QUADWORD-1:0]                        lsq_ldb_address_to_dcache;
    logic            [1:0][PRF_BITS-1:0]                        lsq_ldb_prf_to_dcache;

    // Outputs from LSQ Pipeline to CDB
    logic                                                       lsq_wb_stored_valid;
    logic                 [PRF_BITS-1:0]                        lsq_wb_stored_prf;
    logic                 [QUADWORD-1:0]                        lsq_wb_stored_result;
    logic   [LDB_SIZE-1:0]                                      lsq_wb_load_data_valid;
    logic   [LDB_SIZE-1:0][PRF_BITS-1:0]                        lsq_wb_load_data_prf_tag;
    logic   [LDB_SIZE-1:0][QUADWORD-1:0]                        lsq_wb_load_data;


	assign pipeline_completed_insts[1]  = {3'b0, disp_rob_valid[1]};
	assign pipeline_completed_insts[0]  = {3'b0, disp_rob_valid[0]};
    assign pipeline_error_status        = mshr2proc_finished    ? `HALTED_ON_HALT
                                        : |disp_rob_illegal_out ? `HALTED_ON_ILLEGAL
								        : `NO_ERROR;

    assign pipeline_halt_out            = disp_rob_halt_out;
	assign pipeline_commit_wr_idx       = disp_rob_arch_reg_out;
	assign pipeline_commit_wr_en        = disp_rob_valid;
	assign pipeline_commit_PC           = disp_rob_pc_out;
    assign pipeline_commit_prf          = disp_rob_prf_reg_out;

    cache_holder #(.I_NUM_WAYS(I_NUM_WAYS), .D_NUM_WAYS(D_NUM_WAYS), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) cache_holder1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .proc2Icache_addr(proc2Icache_addr),
        .proc2Icache_next_pc(proc2Icache_next_pc),
        .proc2Icache_addr_valid(proc2Icache_addr_valid),
        .mem2Mshr_response(mem2proc_response),
        .mem2Mshr_tag(mem2proc_tag),
        .mem2Mshr_data(mem2proc_data),
        .stq_wr_en(lsq_stq_data_to_dcache_valid | lsq_stq_write_to_dcache),
        .stq_wr_addr(lsq_stq_address_to_dcache),
        .stq_wr_data(lsq_stq_data_to_dcache),
        .ldb_rd_en(lsq_ldb_addr_to_dcache_valid | lsq_ldb_read_from_dcache),
        .ldb_rd_addr(lsq_ldb_address_to_dcache),
        .ldb_rd_prf(lsq_ldb_prf_to_dcache),
        .halt(disp_rob_halt_out),
        .nuke(disp_rob_nuke),
        // OUTPUTS
        .icache_data_out(Icache_data_out),
        .icache_valid_out(Icache_valid_out),
        .mshr2Mem_command(proc2mem_command),
        .mshr2Mem_address(proc2mem_addr),
        .mshr2Mem_data(proc2mem_data),
        .stq_stall(cache_stq_stall),
        .ldb_data_out(cache_ldb_data_out),
        .ldb_addr_out(cache_ldb_addr_out),
        .ldb_prf_out(cache_ldb_prf_out),
        .ldb_valid_out(cache_ldb_valid_out),
        .ldb_stall_out(cache_ldb_stall_out),
        .mshr2proc_finished(mshr2proc_finished)
    );

    //////////////////////////////////////////////////
    //                                              //
    //                  IF-Stage                    //
    //                                              //
    //////////////////////////////////////////////////
    fetch #(.NUM_BTB_ENTRIES(NUM_BTB_ENTRIES), .NUM_BTB_WAYS(NUM_BTB_WAYS),
            .NUM_BIMODAL_BRANCH_PRED_ENTRIES(NUM_BIMODAL_BRANCH_PRED_ENTRIES), .NUM_GSHARE_BRANCH_PRED_ENTRIES(NUM_GSHARE_BRANCH_PRED_ENTRIES),
            .BRANCH_COUNTER_BITS(BRANCH_COUNTER_BITS), .RAS_SIZE(RAS_SIZE), .TOURNAMENT_ENTRIES(TOURNAMENT_ENTRIES)) fetch1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .update_valid(disp_rob_valid),
        .update_pc(disp_rob_pc_out),
        .update_next_pc(disp_rob_next_pc_out),
        .update_call_from_rob(disp_rob_branch_call_out),
        .update_ret_from_rob(disp_rob_branch_ret_out),
        .update_branch_actually_taken(disp_rob_branch_actual_out),
        .update_branch_unconditional(disp_rob_branch_unconditional_out),
        .update_branch_conditional(disp_rob_branch_conditional_out),
        .nuke(disp_rob_nuke),
        .Imem2proc_data(Icache_data_out),
        .Imem_valid(Icache_valid_out),
        .incoming_stall(if_id_stall),
        .id_disp_illegal_out(id_disp_illegal_out),
        // OUTPUTS
        .proc2Imem_addr(proc2Icache_addr),
        .proc2Imem_addr_valid(proc2Icache_addr_valid),
        .proc2Imem_next_pc(proc2Icache_next_pc),
        .if_PC_out(if_PC_out),
        .if_NPC_out(if_NPC_out),
        .if_IR_out(if_IR_out),
        .if_valid_inst_out(if_valid_inst_out)
    );

    //////////////////////////////////////////////////
    //                                              //
    //            IF/ID Pipeline Register           //
    //                                              //
    //////////////////////////////////////////////////
    // rat stall on way 0 only
    logic [1:0] id_enable;
    always_comb begin
        // Don't stall when we don't currently have anything stored or (no RAT stall and ID/Disp stall)
        for (int i=0; i<2; ++i) begin
            if_id_enable[i]     = (if_id_valid_inst_out[i] == 0) || ((id_rat_stall[i] == 0) && (id_disp_stall[i] == 0));
            id_enable[i]        = (if_id_valid_inst_out[i] == 0) || (id_disp_stall[i] == 0);
        end
        if_id_stall = if_id_enable == 2'b11 ? 0 : 2'b11;
    end
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        for (int i=1; i>= 0; --i) begin
            if (reset) begin
                if_id_PC_out[i]             <= `SD 0;
                if_id_NPC_out[i]            <= `SD 0;
                if_id_IR_out[i]             <= `SD `NOOP_INST;
                if_id_valid_inst_out[i]     <= `SD `FALSE;
            end
            else if (disp_rob_nuke || (if_id_enable == 2'b10 && i == 0)) begin
                if_id_PC_out[i]             <= `SD 0;
                if_id_NPC_out[i]            <= `SD 0;
                if_id_IR_out[i]             <= `SD `NOOP_INST;
                if_id_valid_inst_out[i]     <= `SD `FALSE;
            end
            else if (if_id_enable == 2'b10 && i == 1) begin
                if_id_PC_out[1]             <= `SD if_id_PC_out[0];
                if_id_NPC_out[1]            <= `SD if_id_NPC_out[0];
                if_id_IR_out[1]             <= `SD if_id_IR_out[0];
                if_id_valid_inst_out[1]     <= `SD if_id_valid_inst_out[0];
            end
            else if (if_id_enable[i]) begin
                if_id_PC_out[i]             <= `SD if_PC_out[i];
                if_id_NPC_out[i]            <= `SD if_NPC_out[i];
                if_id_IR_out[i]             <= `SD if_IR_out[i];
                if_id_valid_inst_out[i]     <= `SD if_valid_inst_out[i];
            end
        end
    end

    //////////////////////////////////////////////////
    //                                              //
    //                  ID-Stage                    //
    //                                              //
    //////////////////////////////////////////////////
    decode_holder #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) decode_holder1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .if_id_IR(if_id_IR_out),
        .if_id_valid_inst(if_id_valid_inst_out & id_enable),
        .rrat_commit_en(disp_rob_valid),
        .rrat_commit_prf_entry(disp_rob_prf_reg_out),
        .rrat_commit_arf_idx(disp_rob_arch_reg_out),
        .nuke(disp_rob_nuke),
        // OUTPUTS
        .id_opa_select_out(id_opa_select_out),
        .id_opb_select_out(id_opb_select_out),
        .id_arf_idx_out(id_arf_idx_out),
        .id_alu_func_out(id_alu_func_out),
        .id_rd_mem_out(id_rd_mem_out),
        .id_wr_mem_out(id_wr_mem_out),
        .id_ldl_mem_out(id_ldl_mem_out),
        .id_stc_mem_out(id_stc_mem_out),
        .id_cond_branch_out(id_cond_branch_out),
        .id_uncond_branch_out(id_uncond_branch_out),
        .id_branch_call(id_branch_call),
        .id_branch_ret(id_branch_ret),
        .id_halt_out(id_halt_out),
        .id_illegal_out(id_illegal_out),
        .id_valid_inst_out(id_valid_inst_out),
        .rat_rda_idx(id_rat_rda_idx),
        .rat_rdb_idx(id_rat_rdb_idx),
        .rat_dest_idx(id_rat_dest_idx),
        .rat_dest_valid(id_rat_dest_valid),
        .rat_stall(id_rat_stall),
        .rrat_free_list(id_rrat_free_list)
    );

    //////////////////////////////////////////////////
    //                                              //
    //           ID/DISP Pipeline Register          //
    //                                              //
    //////////////////////////////////////////////////
    always_comb begin
        // Don't stall when we don't currently have anything stored or (no RS stall and ROB stall and Disp/EX stall)
        for (int i=0; i<2; ++i) begin
            id_disp_enable[i] = `FALSE;
            disp_enable[i]    = `FALSE;

            if (id_disp_rd_mem_out[i]) begin        // LOAD
                if (!lsq_ldb_stall[i]) begin
                    if (!disp_rs_stall[i] && !disp_rob_stall[i]) begin
                        id_disp_enable[i]   = `TRUE;
                    end

                    disp_enable[i]      = `TRUE;
                end
            end
            else if (id_disp_wr_mem_out[i]) begin   // STORE
                if (!lsq_stq_stall[i]) begin
                    if (!disp_rs_stall[i] && !disp_rob_stall[i]) begin
                        id_disp_enable[i]   = `TRUE;
                    end

                    disp_enable[i]      = `TRUE;
                end
            end
            else begin
                if (!disp_rs_stall[i] && !disp_rob_stall[i]) begin
                    id_disp_enable[i]   = `TRUE;
                end

                disp_enable[i]          = `TRUE;
            end

            if (!id_disp_valid_inst_out[i] || !id_disp_rat_dest_valid[i]) begin
                id_disp_enable[i]   = `TRUE;
                disp_enable[i]      = `TRUE;
            end
        end

        if (!id_disp_enable[1]) begin
            id_disp_enable[0]       = `FALSE;
        end

        if (!disp_enable[1]) begin
            disp_enable[0]          = `FALSE;
        end

        id_disp_stall = id_disp_enable == 2'b11 ? 0 : 2'b11;
    end
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        for (int i=1; i>= 0; --i) begin
            if (reset) begin
                id_disp_PC_out[i]                   <= `SD 0;
                id_disp_NPC_out[i]                  <= `SD 0;
                id_disp_IR_out[i]                   <= `SD `NOOP_INST;
                id_disp_opa_select_out[i]           <= `SD 0;
                id_disp_opb_select_out[i]           <= `SD 0;
                id_disp_arf_idx_out[i]              <= `SD `ZERO_REG;
                id_disp_alu_func_out[i]             <= `SD 0;
                id_disp_rd_mem_out[i]               <= `SD 0;
                id_disp_wr_mem_out[i]               <= `SD 0;
                id_disp_ldl_mem_out[i]              <= `SD 0;
                id_disp_stc_mem_out[i]              <= `SD 0;
                id_disp_cond_branch_out[i]          <= `SD 0;
                id_disp_uncond_branch_out[i]        <= `SD 0;
                id_disp_branch_call[i]              <= `SD 0;
                id_disp_branch_ret[i]               <= `SD 0;
                id_disp_halt_out[i]                 <= `SD 0;
                id_disp_illegal_out[i]              <= `SD 0;
                id_disp_valid_inst_out[i]           <= `SD 0;
                id_disp_rat_rda_idx[i]              <= `SD 0;
                id_disp_rat_rdb_idx[i]              <= `SD 0;
                id_disp_rat_dest_idx[i]             <= `SD `ZERO_REG;
                id_disp_rat_dest_valid[i]           <= `SD 0;
            end
            else if (disp_rob_nuke || (id_disp_enable == 2'b10 && i == 0)) begin
                id_disp_PC_out[i]                   <= `SD 0;
                id_disp_NPC_out[i]                  <= `SD 0;
                id_disp_IR_out[i]                   <= `SD `NOOP_INST;
                id_disp_opa_select_out[i]           <= `SD 0;
                id_disp_opb_select_out[i]           <= `SD 0;
                id_disp_arf_idx_out[i]              <= `SD `ZERO_REG;
                id_disp_alu_func_out[i]             <= `SD 0;
                id_disp_rd_mem_out[i]               <= `SD 0;
                id_disp_wr_mem_out[i]               <= `SD 0;
                id_disp_ldl_mem_out[i]              <= `SD 0;
                id_disp_stc_mem_out[i]              <= `SD 0;
                id_disp_cond_branch_out[i]          <= `SD 0;
                id_disp_uncond_branch_out[i]        <= `SD 0;
                id_disp_branch_call[i]              <= `SD 0;
                id_disp_branch_ret[i]               <= `SD 0;
                id_disp_halt_out[i]                 <= `SD 0;
                id_disp_illegal_out[i]              <= `SD 0;
                id_disp_valid_inst_out[i]           <= `SD 0;
                id_disp_rat_rda_idx[i]              <= `SD 0;
                id_disp_rat_rdb_idx[i]              <= `SD 0;
                id_disp_rat_dest_idx[i]             <= `SD `ZERO_REG;
                id_disp_rat_dest_valid[i]           <= `SD 0;
            end
            else if (id_disp_enable == 2'b10 && i == 1) begin
                id_disp_PC_out[1]                   <= `SD id_disp_PC_out[0];
                id_disp_NPC_out[1]                  <= `SD id_disp_NPC_out[0];
                id_disp_IR_out[1]                   <= `SD id_disp_IR_out[0];
                id_disp_opa_select_out[1]           <= `SD id_disp_opa_select_out[0];
                id_disp_opb_select_out[1]           <= `SD id_disp_opb_select_out[0];
                id_disp_arf_idx_out[1]              <= `SD id_disp_arf_idx_out[0];
                id_disp_alu_func_out[1]             <= `SD id_disp_alu_func_out[0];
                id_disp_rd_mem_out[1]               <= `SD id_disp_rd_mem_out[0];
                id_disp_wr_mem_out[1]               <= `SD id_disp_wr_mem_out[0];
                id_disp_ldl_mem_out[1]              <= `SD id_disp_ldl_mem_out[0];
                id_disp_stc_mem_out[1]              <= `SD id_disp_stc_mem_out[0];
                id_disp_cond_branch_out[1]          <= `SD id_disp_cond_branch_out[0];
                id_disp_uncond_branch_out[1]        <= `SD id_disp_uncond_branch_out[0];
                id_disp_branch_call[1]              <= `SD id_disp_branch_call[0];
                id_disp_branch_ret[1]               <= `SD id_disp_branch_ret[0];
                id_disp_halt_out[1]                 <= `SD id_disp_halt_out[0];
                id_disp_illegal_out[1]              <= `SD id_disp_illegal_out[0];
                id_disp_valid_inst_out[1]           <= `SD id_disp_valid_inst_out[0];
                id_disp_rat_rda_idx[1]              <= `SD id_disp_rat_rda_idx[0];
                id_disp_rat_rdb_idx[1]              <= `SD id_disp_rat_rdb_idx[0];
                id_disp_rat_dest_idx[1]             <= `SD id_disp_rat_dest_idx[0];
                id_disp_rat_dest_valid[1]           <= `SD id_disp_rat_dest_valid[0];
            end
            else if (id_disp_enable[i]) begin
                id_disp_PC_out[i]                   <= `SD if_id_PC_out[i];
                id_disp_NPC_out[i]                  <= `SD if_id_NPC_out[i];
                id_disp_IR_out[i]                   <= `SD if_id_IR_out[i];
                id_disp_opa_select_out[i]           <= `SD id_opa_select_out[i];
                id_disp_opb_select_out[i]           <= `SD id_opb_select_out[i];
                id_disp_arf_idx_out[i]              <= `SD id_arf_idx_out[i];
                id_disp_alu_func_out[i]             <= `SD id_alu_func_out[i];
                id_disp_rd_mem_out[i]               <= `SD id_rd_mem_out[i];
                id_disp_wr_mem_out[i]               <= `SD id_wr_mem_out[i];
                id_disp_ldl_mem_out[i]              <= `SD id_ldl_mem_out[i];
                id_disp_stc_mem_out[i]              <= `SD id_stc_mem_out[i];
                id_disp_cond_branch_out[i]          <= `SD id_cond_branch_out[i];
                id_disp_uncond_branch_out[i]        <= `SD id_uncond_branch_out[i];
                id_disp_branch_call[i]              <= `SD id_branch_call[i];
                id_disp_branch_ret[i]               <= `SD id_branch_ret[i];
                id_disp_halt_out[i]                 <= `SD id_halt_out[i];
                id_disp_illegal_out[i]              <= `SD id_illegal_out[i];
                id_disp_valid_inst_out[i]           <= `SD id_valid_inst_out[i];
                id_disp_rat_rda_idx[i]              <= `SD id_rat_rda_idx[i];
                id_disp_rat_rdb_idx[i]              <= `SD id_rat_rdb_idx[i];
                id_disp_rat_dest_idx[i]             <= `SD id_rat_dest_idx[i];
                id_disp_rat_dest_valid[i]           <= `SD id_rat_dest_valid[i];
            end
        end
    end

    //////////////////////////////////////////////////
    //                                              //
    //                 DISP-Stage                   //
    //                                              //
    //////////////////////////////////////////////////
    dispatch_holder #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES), .NUM_RS(NUM_RS), .ROB_SIZE(ROB_SIZE)) dispatch_holder1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .prf_rda_idx(id_disp_rat_rda_idx),
        .prf_rdb_idx(id_disp_rat_rdb_idx),
        .prf_dest_idx(id_disp_rat_dest_idx),
        .prf_dest_valid(id_disp_rat_dest_valid),
        .rrat_overwritten(id_disp_rat_dest_idx),
        .rrat_overwritten_valid(id_disp_rat_dest_valid),
        .rrat_free_list(id_rrat_free_list),
        .opa_select_out(id_disp_opa_select_out),
        .opb_select_out(id_disp_opb_select_out),
        .arf_idx_out(id_disp_arf_idx_out),
        .alu_func_out(id_disp_alu_func_out),
        .rd_mem_out(id_disp_rd_mem_out),
        .wr_mem_out(id_disp_wr_mem_out),
        .ldl_mem_out(id_disp_ldl_mem_out),
        .stc_mem_out(id_disp_stc_mem_out),
        .cond_branch_out(id_disp_cond_branch_out),
        .uncond_branch_out(id_disp_uncond_branch_out),
        .branch_call(id_disp_branch_call),
        .branch_ret(id_disp_branch_ret),
        .halt_out(id_disp_halt_out),
        .illegal_out(id_disp_illegal_out),
        .valid_inst_out(id_disp_valid_inst_out & disp_enable),
        .cdb_valid(wb_valid_out),
        .cdb_prf_tag(wb_pr_tag_out),
        .cdb_value_in(wb_result_out),
        .cdb_branch_taken(wb_branch_taken_out),
        .cdb_actual_next_pc(wb_next_pc_out),
        .id_disp_PC(id_disp_PC_out),
        .id_disp_NPC(id_disp_NPC_out),
        .id_disp_IR(id_disp_IR_out),
        .disp_ex_enable(disp_ex_enable),
        // OUTPUTS
        .rs_output_mem_valid(disp_rs_output_mem_valid),
        .rs_opb_mem_out(disp_rs_opb_mem_out),
        .rs_dest_mem_out(disp_rs_dest_mem_out),
        .rs_mem_IR(disp_rs_mem_IR),

        .rs_output_no_mem_valid(disp_rs_output_no_mem_valid),
        .rs_opa_out(disp_rs_opa_out),
        .rs_opb_out(disp_rs_opb_out),
        .rs_dest_out(disp_rs_dest_out),
        .rs_alu_func_out(disp_rs_alu_func_out),
        .rs_opa_select_out(disp_rs_opa_select_out),
        .rs_opb_select_out(disp_rs_opb_select_out),
        .rs_cond_branch_out(disp_rs_cond_branch_out),
        .rs_uncond_branch_out(disp_rs_uncond_branch_out),
        .rs_PC(disp_rs_PC),
        .rs_NPC(disp_rs_NPC),
        .rs_IR(disp_rs_IR),
        .rs_stall(disp_rs_stall),
        .rob_stall(disp_rob_stall),
        .update_valid(disp_rob_valid),
        .update_arch_reg_out(disp_rob_arch_reg_out),
        .update_prf_reg_out(disp_rob_prf_reg_out),
        .nuke(disp_rob_nuke),
        .update_branch_actual_out(disp_rob_branch_actual_out),
        .update_pc_out(disp_rob_pc_out),
        .update_next_pc_out(disp_rob_next_pc_out),
        .update_branch_conditional_out(disp_rob_branch_conditional_out),
        .update_branch_unconditional_out(disp_rob_branch_unconditional_out),
        .update_branch_call_out(disp_rob_branch_call_out),
        .update_branch_ret_out(disp_rob_branch_ret_out),
        .update_load_out(disp_rob_load_out),
        .update_store_out(disp_rob_store_out),
        .update_halt_out(disp_rob_halt_out),
        .update_illegal_out(disp_rob_illegal_out),
        .update_store_request_out(disp_rob_store_request_out),
        .update_store_executed(disp_rob_store_executed),
        .prf_rda_out(disp_prf_rda_out),
        .prf_valid_a_out(disp_prf_rda_valid_out)
    );

    //////////////////////////////////////////////////
    //                                              //
    //           DISP/EX Pipeline Register          //
    //                                              //
    //////////////////////////////////////////////////
    always_comb begin
        // Don't stall when we don't currently have anything stored or (no ALU stall and Branch stall and Mem Addr stall and Mult stall signals)
        // TODO could make more efficient
        disp_ex_enable[1] = ((ex_wb_enable[5] == 1) && (ex_wb_enable[1] == 1) && (ex_wb_enable[7] == 1) && (ex_mult_stall_out[1] == 0));
        disp_ex_enable[0] = ((ex_wb_enable[4] == 1) && (ex_wb_enable[0] == 1) && (ex_wb_enable[6] == 1) && (ex_mult_stall_out[0] == 0));

        ex_enable[1] = ((ex_wb_enable[5] == 1) && (ex_wb_enable[1] == 1) && (ex_wb_enable[7] == 1));
        ex_enable[0] = ((ex_wb_enable[4] == 1) && (ex_wb_enable[0] == 1) && (ex_wb_enable[6] == 1));
    end

    //////////////////////////////////////////////////
    //                                              //
    //                  EX-Stage                    //
    //                                              //
    //////////////////////////////////////////////////
    execute_holder #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES), .MULT_STAGES(MULT_STAGES), .FINISHED_INSTR(FINISHED_INSTR)) execute_holder1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .mem_valid(disp_rs_output_mem_valid),
        .mem_opb_out(disp_rs_opb_mem_out),
        .mem_dest_out(disp_rs_dest_mem_out),
        .mem_IR(disp_rs_mem_IR),
        .valid_in(disp_rs_output_no_mem_valid & ex_enable),
        .rs_opa_out(disp_rs_opa_out),
        .rs_opb_out(disp_rs_opb_out),
        .rs_dest_out(disp_rs_dest_out),
        .rs_alu_func_out(disp_rs_alu_func_out),
        .rs_opa_select_out(disp_rs_opa_select_out),
        .rs_opb_select_out(disp_rs_opb_select_out),
        .rs_cond_branch_out(disp_rs_cond_branch_out),
        .rs_uncond_branch_out(disp_rs_uncond_branch_out),
        .rs_PC(disp_rs_PC),
        .rs_NPC(disp_rs_NPC),
        .rs_IR(disp_rs_IR),
        .mult_stall({!ex_wb_enable[3], !ex_wb_enable[2]}),
        .nuke(disp_rob_nuke),
        // OUTPUTS
        .valid(ex_valid),
        .pr_tag(ex_pr_tag),
        .results(ex_results),
        .next_pc(ex_next_pc),
        .branch_taken(ex_branch_taken),
        .lsq_mem_addr(ex_lsq_mem_addr),
        .lsq_prf_tag(ex_lsq_prf_tag),
        .lsq_valid(ex_lsq_valid),
        .mult_stall_out(ex_mult_stall_out)
    );

    //////////////////////////////////////////////////
    //                                              //
    //            EX/WB Pipeline Register           //
    //                                              //
    //////////////////////////////////////////////////
    always_comb begin
        // Don't stall when we don't currently have anything stored or no stall signals
        for (int i=0; i<FINISHED_INSTR; ++i) begin
            ex_wb_enable[i] = (ex_wb_valid[i] == 0) || (wb_cdb_stall[i + LDB_SIZE + 1] == 0);
        end
        ex_wb_enable[FINISHED_INSTR+1:FINISHED_INSTR] = 2'b11; // LSQ always enabled for now
    end
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if (reset) begin
            ex_wb_valid                     <= `SD 0;
            ex_wb_pr_tag                    <= `SD 0;
            ex_wb_results                   <= `SD 0;
            ex_wb_next_pc                   <= `SD 0;
            ex_wb_branch_taken              <= `SD 0;
            ex_wb_lsq_mem_addr              <= `SD 0;
            ex_wb_lsq_prf_tag               <= `SD 0;
            ex_wb_lsq_valid                 <= `SD 0;
        end
        else if (disp_rob_nuke) begin
            ex_wb_valid                     <= `SD 0;
            ex_wb_pr_tag                    <= `SD 0;
            ex_wb_results                   <= `SD 0;
            ex_wb_next_pc                   <= `SD 0;
            ex_wb_branch_taken              <= `SD 0;
            ex_wb_lsq_mem_addr              <= `SD 0;
            ex_wb_lsq_prf_tag               <= `SD 0;
            ex_wb_lsq_valid                 <= `SD 0;
        end
        else begin
            for (int i=0; i<FINISHED_INSTR; ++i) begin
                if (ex_wb_enable[i]) begin
                    ex_wb_valid[i]          <= `SD ex_valid[i];
                    ex_wb_pr_tag[i]         <= `SD ex_pr_tag[i];
                    ex_wb_results[i]        <= `SD ex_results[i];
                    ex_wb_next_pc[i]        <= `SD ex_next_pc[i];
                    ex_wb_branch_taken[i]   <= `SD ex_branch_taken[i];
                end
            end
            for (int i=0; i<2; ++i) begin
                if (ex_wb_enable[FINISHED_INSTR+i]) begin
                    ex_wb_lsq_mem_addr[i]   <= `SD ex_lsq_mem_addr[i];
                    ex_wb_lsq_prf_tag[i]    <= `SD ex_lsq_prf_tag[i];
                    ex_wb_lsq_valid[i]      <= `SD ex_lsq_valid[i];
                end
            end
        end
    end

    //////////////////////////////////////////////////
    //                                              //
    //                  WB-Stage                    //
    //                                              //
    //////////////////////////////////////////////////
    cdb_queue #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES), .NUM_EX_UNITS(CDB_INPUTS)) cdb_queue1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .valid({ex_wb_valid, lsq_wb_load_data_valid, lsq_wb_stored_valid}),
        .pr_tag({ex_wb_pr_tag, lsq_wb_load_data_prf_tag, lsq_wb_stored_prf}),
        .results({ex_wb_results, lsq_wb_load_data, lsq_wb_stored_result}),
        .next_pc({ex_wb_next_pc, {LDB_SIZE+1 {64'd0}} }),
        .branch_taken({ex_wb_branch_taken, {LDB_SIZE+1 {1'd0}} }),
        // OUTPUTS
        .valid_out(wb_valid_out),
        .pr_tag_out(wb_pr_tag_out),
        .result_out(wb_result_out),
        .next_pc_out(wb_next_pc_out),
        .branch_taken_out(wb_branch_taken_out),
        .cdb_stall(wb_cdb_stall)
    );

    lsq #(.LDB_SIZE(LDB_SIZE), .STQ_SIZE(STQ_SIZE), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) lsq1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .id_disp_valid_inst(id_disp_valid_inst_out & ~disp_rs_stall & ~disp_rob_stall),
        .id_disp_wr_mem(id_disp_wr_mem_out),
        .id_disp_rd_mem(id_disp_rd_mem_out),
        .id_disp_stc_mem(id_disp_stc_mem_out),
        .id_disp_ldl_mem(id_disp_ldl_mem_out),
        .id_disp_rat_dest(id_disp_rat_dest_idx),
        .id_disp_rat_dest_valid(id_disp_rat_dest_valid),
        .store_data(disp_prf_rda_out),
        .store_data_valid(disp_prf_rda_valid_out),
        .ex_calc_address_valid(ex_wb_lsq_valid),
        .ex_calc_address_prf(ex_wb_lsq_prf_tag),
        .ex_calc_address(ex_wb_lsq_mem_addr),
        .cdb_valid(wb_valid_out),
        .cdb_prf(wb_pr_tag_out),
        .cdb_data(wb_result_out),
        .cdb_stall(~lsq_wb_enable[LDB_SIZE:1]),
        .stq_dcache_stall(cache_stq_stall),
        .ldb_dcache_stall(cache_ldb_stall_out),
        .ldb_dcache_load_data_valid(cache_ldb_valid_out),
        .ldb_dcache_load_data(cache_ldb_data_out),
        .ldb_dcache_load_addr(cache_ldb_addr_out),
        .ldb_dcache_load_prf(cache_ldb_prf_out),
        .store_requested(disp_rob_store_request_out),
        .store_executed(disp_rob_store_executed),
        .nuke(disp_rob_nuke),
        // OUTPUTS
        .stq_stall(lsq_stq_stall),
        .ldb_stall(lsq_ldb_stall),
        .stored_valid(lsq_stored_valid),                            // pipeline to cdb
        .stored_prf(lsq_stored_prf),
        .stored_result(lsq_stored_result),
        .load_data_valid(lsq_load_data_valid),
        .load_data_prf_tag(lsq_load_data_prf_tag),
        .load_data(lsq_load_data),
        .stq_data_to_dcache_valid(lsq_stq_data_to_dcache_valid),
        .stq_write_to_dcache(lsq_stq_write_to_dcache),
        .stq_address_to_dcache(lsq_stq_address_to_dcache),
        .stq_data_to_dcache(lsq_stq_data_to_dcache),
        .ldb_addr_to_dcache_valid(lsq_ldb_addr_to_dcache_valid),
        .ldb_read_from_dcache(lsq_ldb_read_from_dcache),
        .ldb_address_to_dcache(lsq_ldb_address_to_dcache),
        .ldb_prf_to_dcache(lsq_ldb_prf_to_dcache)
    );

    //////////////////////////////////////////////////
    //                                              //
    //           LSQ/WB Pipeline Register           //
    //                                              //
    //////////////////////////////////////////////////
    always_comb begin
        // Don't stall when we don't currently have anything stored or no stall signals
        lsq_wb_enable[0] = `TRUE; // Store queue never stalls

        for (int i=1; i<LDB_SIZE+1; ++i) begin
            lsq_wb_enable[i] = (!lsq_wb_load_data_valid[i-1]) || (!wb_cdb_stall[i]);
        end
    end

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if (reset) begin
            lsq_wb_stored_valid                     <= `SD 0;
            lsq_wb_stored_prf                       <= `SD 0;
            lsq_wb_stored_result                    <= `SD 0;
            lsq_wb_load_data_valid                  <= `SD 0;
            lsq_wb_load_data_prf_tag                <= `SD 0;
            lsq_wb_load_data                        <= `SD 0;
        end
        else if (disp_rob_nuke) begin
            lsq_wb_stored_valid                     <= `SD 0;
            lsq_wb_stored_prf                       <= `SD 0;
            lsq_wb_stored_result                    <= `SD 0;
            lsq_wb_load_data_valid                  <= `SD 0;
            lsq_wb_load_data_prf_tag                <= `SD 0;
            lsq_wb_load_data                        <= `SD 0;
        end
        else begin
            lsq_wb_stored_valid                     <= `SD lsq_stored_valid;
            lsq_wb_stored_prf                       <= `SD lsq_stored_prf;
            lsq_wb_stored_result                    <= `SD lsq_stored_result;
            for (int i=0; i<LDB_SIZE; ++i) begin
                if (lsq_wb_enable[i+1]) begin
                    lsq_wb_load_data_valid[i]       <= `SD lsq_load_data_valid[i];
                    lsq_wb_load_data_prf_tag[i]     <= `SD lsq_load_data_prf_tag[i];
                    lsq_wb_load_data[i]             <= `SD lsq_load_data[i];
                end
            end
        end
    end
endmodule
`default_nettype wire

//  CLK:   6.25 < t <= 6.875
`default_nettype none
`timescale 1ns/100ps
module dispatch_holder #(parameter NUM_PRF_ENTRIES = 96, parameter NUM_RS = 16, parameter ROB_SIZE=64, localparam QUADWORD = 64, localparam PRF_BITS=$clog2(NUM_PRF_ENTRIES), localparam NUM_ARCH_REGS=32, localparam ARF_BITS=$clog2(NUM_ARCH_REGS)) (
    // INPUTS
    input   wire                                            clock,
    input   wire                                            reset,
    // From RAT
    input   wire             [1:0][PRF_BITS-1:0]            prf_rda_idx,
    input   wire             [1:0][PRF_BITS-1:0]            prf_rdb_idx,
    input   wire             [1:0][PRF_BITS-1:0]            prf_dest_idx,
    input   wire             [1:0]                          prf_dest_valid,
    input   wire             [1:0][PRF_BITS-1:0]            rrat_overwritten,
    input   wire             [1:0]                          rrat_overwritten_valid,
    input   wire    [ROB_SIZE-1:0][PRF_BITS-1:0]            rrat_free_list,
    // From Decoder
    input   wire             [1:0][1:0]                     opa_select_out,
    input   wire             [1:0][1:0]                     opb_select_out,
    input   wire             [1:0][ARF_BITS-1:0]            arf_idx_out,
    input   wire             [1:0][4:0]                     alu_func_out,
    input   wire             [1:0]                          rd_mem_out,
    input   wire             [1:0]                          wr_mem_out,
    input   wire             [1:0]                          ldl_mem_out,
    input   wire             [1:0]                          stc_mem_out,
    input   wire             [1:0]                          cond_branch_out,
    input   wire             [1:0]                          uncond_branch_out,
    input   wire             [1:0]                          branch_call,
    input   wire             [1:0]                          branch_ret,
    input   wire             [1:0]                          halt_out,
    input   wire             [1:0]                          illegal_out,
    input   wire             [1:0]                          valid_inst_out,
    // From Execution complete
    input   wire             [1:0]                          cdb_valid,
    input   wire             [1:0][PRF_BITS-1:0]            cdb_prf_tag,
    input   wire             [1:0][QUADWORD-1:0]            cdb_value_in,
    input   wire             [1:0]                          cdb_branch_taken,
    input   wire             [1:0][QUADWORD-1:0]            cdb_actual_next_pc,
    // From ID/DISP Register
    input   wire             [1:0][QUADWORD-1:0]            id_disp_PC,
    input   wire             [1:0][QUADWORD-1:0]            id_disp_NPC,
    input   wire             [1:0][QUADWORD/2-1:0]          id_disp_IR,
    input   wire             [1:0]                          disp_ex_enable,

    // OUTPUTS
    // From RS
    output  logic            [1:0]                          rs_output_mem_valid,
    output  logic            [1:0][QUADWORD-1:0]            rs_opb_mem_out,
    output  logic            [1:0][PRF_BITS-1:0]            rs_dest_mem_out,
    output  logic            [1:0][31:0]                    rs_mem_IR,

    output  logic            [1:0]                          rs_output_no_mem_valid,
    output  logic            [1:0][QUADWORD-1:0]            rs_opa_out,
    output  logic            [1:0][QUADWORD-1:0]            rs_opb_out,
    output  logic            [1:0][PRF_BITS-1:0]            rs_dest_out,
    output  logic            [1:0][4:0]                     rs_alu_func_out,
    output  logic            [1:0][1:0]                     rs_opa_select_out,
    output  logic            [1:0][1:0]                     rs_opb_select_out,
    output  logic            [1:0]                          rs_cond_branch_out,
    output  logic            [1:0]                          rs_uncond_branch_out,
    output  logic            [1:0][QUADWORD-1:0]            rs_PC,
    output  logic            [1:0][QUADWORD-1:0]            rs_NPC,
    output  logic            [1:0][QUADWORD/2-1:0]          rs_IR,
    output  logic            [1:0]                          rs_stall,
    // From ROB
    output  logic            [1:0]                          rob_stall,
    output  logic            [1:0]                          update_valid,
    output  logic            [1:0][ARF_BITS-1:0]            update_arch_reg_out,
    output  logic            [1:0][PRF_BITS-1:0]            update_prf_reg_out,
    output  logic            [1:0]                          nuke,
    output  logic            [1:0]                          update_branch_actual_out,
    output  logic            [1:0][QUADWORD-1:0]            update_pc_out,
    output  logic            [1:0][QUADWORD-1:0]            update_next_pc_out,
    output  logic            [1:0]                          update_branch_conditional_out,
    output  logic            [1:0]                          update_branch_unconditional_out,
    output  logic            [1:0]                          update_branch_call_out,
    output  logic            [1:0]                          update_branch_ret_out,
    output  logic            [1:0]                          update_load_out,
    output  logic            [1:0]                          update_store_out,
    output  logic            [1:0]                          update_halt_out,
    output  logic            [1:0]                          update_illegal_out,
    output  logic                                           update_store_request_out,
    output  logic                                           update_store_executed,
    // From PRF
    output  logic           [1:0][QUADWORD-1:0]             prf_rda_out,
    output  logic           [1:0]                           prf_valid_a_out
);

    wire    [1:0][QUADWORD-1:0]                     prf_rdb_out;
    wire    [1:0]                                   prf_valid_b_out;

    prf #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) prf1 (
        // INPUTS
        .clock(clock),
        .reset(reset),
        .rda_idx(prf_rda_idx),
        .rdb_idx(prf_rdb_idx),
        .wr_idx(cdb_prf_tag),
        .wr_data(cdb_value_in),
        .wr_en(cdb_valid),
        .nuke(nuke),
        .rrat_free_list(rrat_free_list),
        .rrat_overwritten(rrat_overwritten),
        .rrat_overwritten_valid(rrat_overwritten_valid),
        // OUTPUTS
        .rda_out(prf_rda_out),
        .rdb_out(prf_rdb_out),
        .prf_valid_a_out(prf_valid_a_out),              // if this is equal to 0, then operand value equals prf index
        .prf_valid_b_out(prf_valid_b_out)               // if this is equal to 0, then operand value equals prf index
    );

    wire    [1:0]   rs_valid = prf_dest_valid & valid_inst_out & ~rob_stall;

    rs #(.NUM_RS(NUM_RS), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rs1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .nuke(nuke),
        .rs_dest_in(prf_dest_idx),
        .rs_opa_in(prf_rda_out),
        .rs_opa_valid_in(prf_valid_a_out | rd_mem_out | wr_mem_out),        // if store, then lsq handles getting reg A values
        .rs_opb_in(prf_rdb_out),
        .rs_opb_valid_in(prf_valid_b_out),
        .alu_func_in(alu_func_out),
        .id_rd_in(rd_mem_out),
        .id_wr_in(wr_mem_out),
        .cdb_value_in(cdb_value_in),
        .cdb_tag(cdb_prf_tag),
        .cdb_valid(cdb_valid),
        .id_opa_select_out(opa_select_out),
        .id_opb_select_out(opb_select_out),
        .id_cond_branch_out(cond_branch_out),
        .id_uncond_branch_out(uncond_branch_out),
        .id_valid_inst_out(rs_valid),
        .if_id_PC(id_disp_PC),
        .if_id_NPC(id_disp_NPC),
        .if_id_IR(id_disp_IR),
        .disp_ex_enable(disp_ex_enable),
        // OUTPUTS
        .rs_output_mem_valid(rs_output_mem_valid),
        .rs_opb_mem_out(rs_opb_mem_out),
        .rs_dest_mem_out(rs_dest_mem_out),
        .id_rs_mem_IR(rs_mem_IR),

        .rs_output_no_mem_valid(rs_output_no_mem_valid),
        .rs_opa_out(rs_opa_out),
        .rs_opb_out(rs_opb_out),
        .rs_dest_out(rs_dest_out),
        .rs_alu_func_out(rs_alu_func_out),
        .id_rs_opa_select_out(rs_opa_select_out),
        .id_rs_opb_select_out(rs_opb_select_out),
        .id_rs_cond_branch_out(rs_cond_branch_out),
        .id_rs_uncond_branch_out(rs_uncond_branch_out),
        .id_rs_PC(rs_PC),
        .id_rs_NPC(rs_NPC),
        .id_rs_IR(rs_IR),
        .rs_stall(rs_stall)
    );

    wire    [1:0]   rob_valid = prf_dest_valid & valid_inst_out & ~rs_stall;

    rob #(.ROB_SIZE(ROB_SIZE), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rob1(
        // INPUTS
        .clock(clock),
        .reset(reset),
        .valid_in(rob_valid),
        .pc_in(id_disp_PC),
        .next_pc_in(id_disp_NPC),
        .dest_arch_reg_in(arf_idx_out),
        .dest_prf_reg_in(prf_dest_idx),
        .branch_conditional_in(cond_branch_out),
        .branch_unconditional_in(uncond_branch_out),
        .branch_call_in(branch_call),
        .branch_return_in(branch_ret),
        .load_in(rd_mem_out),
        .store_in(wr_mem_out),
        .halt_in(halt_out),
        .illegal_in(illegal_out),
        .cdb_valid(cdb_valid),
        .cdb_tag(cdb_prf_tag),
        .cdb_branch_taken(cdb_branch_taken),
        .cdb_actual_next_pc(cdb_actual_next_pc),
        // OUTPUTS
        .rob_stall(rob_stall),
        .valid(update_valid),
        .arch_reg_out(update_arch_reg_out),
        .prf_reg_out(update_prf_reg_out),
        .nuke(nuke),
        .branch_actual_out(update_branch_actual_out),
        .pc_out(update_pc_out),
        .next_pc_out(update_next_pc_out),
        .branch_conditional_out(update_branch_conditional_out),
        .branch_unconditional_out(update_branch_unconditional_out),
        .branch_call_out(update_branch_call_out),
        .branch_return_out(update_branch_ret_out),
        .load_out(update_load_out),
        .store_out(update_store_out),
        .halt_out(update_halt_out),
        .illegal_out(update_illegal_out),
        .store_request_out(update_store_request_out),
        .store_executed(update_store_executed)
    );
endmodule
`default_nettype wire

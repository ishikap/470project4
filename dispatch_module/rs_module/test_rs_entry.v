//For: Testing Single RS module
//By: Sidhaarth and Scott
//Date 2/15/18

`timescale 1ns/100ps
`define NOT_CHECK 	2'b10
`define TRUE_2 		2'b01
`define FALSE_2 	2'b00
`define WAY_0   	1'b0
`define WAY_1 	    1'b1

parameter NUM_PRF_ENTRIES   = 64;
parameter PRF_BITS          = $clog2(NUM_PRF_ENTRIES);
parameter QUADWORD          = 64;

module testbench;

    logic                               clock;
    logic                               reset;
    logic		[PRF_BITS-1:0]          rs_dest_in;         //input logic from RAT
    logic   	[QUADWORD-1:0]          rs_opa_in;          //if invalid; 5 bits
    logic                               rs_opa_valid_in;
    logic   	[QUADWORD-1:0]          rs_opb_in;          //if invalid; 5 bits
    logic                               rs_opb_valid_in;
    logic   	[4:0]                   alu_func_in;
    logic                               id_rd_mem;          //for LSQ
    logic                               id_wr_mem;
    logic   	[1:0][QUADWORD-1:0]     cdb_value_in;
    logic   	[1:0][PRF_BITS-1:0]  	cdb_tag;
    logic       [1:0]                   cdb_valid;
    logic                               grant;              //decide name later
    logic                               rs_input_valid;
    logic   	[1:0]                   id_opa_select_out;
    logic   	[1:0]                   id_opb_select_out;
    logic                               id_cond_branch_out;
    logic                               id_uncond_branch_out;
    logic                               id_halt_out;
    logic                               id_illegal_out;
    logic                               id_valid_inst_out;
    logic       [QUADWORD-1:0]          if_id_NPC;
    logic       [31:0]                  if_id_IR;
    logic                               id_thread_id;
    logic                               rs_wake_up_mult;
    logic                               rs_wake_up_alu;
    logic  	    [QUADWORD-1:0]          rs_opa_out;
    logic   	[QUADWORD-1:0]          rs_opb_out;
    logic		[PRF_BITS-1:0]          rs_dest_out;        //output to ex unit
    logic                               rs_free;
    logic                               rs_lsq_en;
    logic       [4:0]                   id_rs_alu_func_out;
    logic       [1:0]                   id_rs_opa_select_out;
    logic   	[1:0]                   id_rs_opb_select_out;
    logic                               id_rs_cond_branch_out;
    logic                               id_rs_uncond_branch_out;
    logic                               id_rs_halt_out;
    logic                               id_rs_illegal_out;
    logic                               id_rs_valid_inst_out;
    logic                               id_rs_wr_mem;
    logic                               id_rs_rd_mem;
    logic       [QUADWORD-1:0]          id_rs_NPC;
    logic       [31:0]                  id_rs_IR;
    logic                               id_rs_thread;


    rs_entry #(NUM_PRF_ENTRIES) rs1(
            .clock(clock),
            .reset(reset),
            .rs_dest_in(rs_dest_in),
            .rs_opa_in(rs_opa_in),
            .rs_opa_valid_in(rs_opa_valid_in),
            .rs_opb_in(rs_opb_in),
            .rs_opb_valid_in(rs_opb_valid_in),
            .alu_func_in(alu_func_in),
            .id_rd_mem(id_rd_mem),                              //for LSQ
            .id_wr_mem(id_wr_mem),
            .cdb_value_in(cdb_value_in),
            .cdb_tag(cdb_tag),
            .cdb_valid(cdb_valid),
            .grant(grant),                                      //decide name later
            .rs_input_valid(rs_input_valid),
            .id_opa_select_out(id_opa_select_out),
            .id_opb_select_out(id_opb_select_out),
            .id_cond_branch_out(id_cond_branch_out),
            .id_uncond_branch_out(id_uncond_branch_out),
            .id_halt_out(id_halt_out),
            .id_illegal_out(id_illegal_out),
            .id_valid_inst_out(id_valid_inst_out),
            .if_id_NPC(if_id_NPC),
            .if_id_IR(if_id_IR),
            .id_thread_id(id_thread_id),
            .rs_wake_up_mult(rs_wake_up_mult),
            .rs_wake_up_alu(rs_wake_up_alu),
            .rs_opa_out(rs_opa_out),
            .rs_opb_out(rs_opb_out),
            .rs_dest_out(rs_dest_out),                          //output to ex unit
            .rs_free(rs_free),
            .rs_lsq_en(rs_lsq_en),
            .id_rs_alu_func_out(id_rs_alu_func_out),
            .id_rs_opa_select_out(id_rs_opa_select_out),
            .id_rs_opb_select_out(id_rs_opb_select_out),
            .id_rs_cond_branch_out(id_rs_cond_branch_out),
            .id_rs_uncond_branch_out(id_rs_uncond_branch_out),
            .id_rs_halt_out(id_rs_halt_out),
            .id_rs_illegal_out(id_rs_illegal_out),
            .id_rs_valid_inst_out(id_rs_valid_inst_out),
            .id_rs_wr_mem(id_rs_wr_mem),
            .id_rs_rd_mem(id_rs_rd_mem),
            .id_rs_NPC(id_rs_NPC),
            .id_rs_IR(id_rs_IR),
            .id_rs_thread(id_rs_thread)
        );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $fatal("Incorrect");
    endtask

    task dispatch;
        input [4:0] alu_func;
        input rs_opa_valid;
        input [63:0] rs_opa;
        input rs_opb_valid;
        input [63:0] rs_opb;
        input [4:0] rs_dest;
        input rd_mem, wr_mem;

        rs_dest_in = rs_dest;
        rs_opa_valid_in = rs_opa_valid;
        rs_opa_in = rs_opa;
        rs_opb_valid_in = rs_opb_valid;
        rs_opb_in = rs_opb;
        alu_func_in = alu_func;
        id_rd_mem = rd_mem;
        id_wr_mem = wr_mem;

		rs_input_valid = `TRUE;

	endtask

	task dispatch_check;
		@(negedge clock);
		rs_input_valid = `FALSE;

        if (rs_free != 0) exit_on_error();
        if (rs_opa_valid_in && rs_opb_valid_in)
        begin
            if (alu_func_in == `ALU_ADDQ)
            begin
                if (rs_wake_up_alu != 1) exit_on_error();
                if (rs_wake_up_mult != 0) exit_on_error();
            end
            else if (alu_func_in == `ALU_MULQ)
            begin
                if (rs_wake_up_alu != 0) exit_on_error();
                if (rs_wake_up_mult != 1) exit_on_error();
            end
        end

        if (rs_opa_valid_in && rs_opb_valid_in)
        begin
            if (id_rd_mem || id_wr_mem)
            begin
                if (rs_lsq_en != 1) exit_on_error();
            end
            else
            begin
                if (rs_lsq_en != 0) exit_on_error();
            end
        end

        id_rd_mem = `FALSE;
        id_rd_mem = `FALSE;
    endtask

	task cdb;
		input way;
		input cdb_valid_in;
		input [63:0] cdb_value;
		input [4:0] cdb_tag_in;

		cdb_valid[way] = cdb_valid_in;
		cdb_value_in[way] = cdb_value;
		cdb_tag[way] = cdb_tag_in;

	endtask

	task grantToRS;
		grant = 1;
		@(negedge clock);
		grant = 0;
	endtask

	task check;
		input [1:0] alu;
		input [1:0] mult;
		input [1:0] free;
		input [1:0] lsq;

		if ((rs_wake_up_alu != alu[0]) && (alu != `NOT_CHECK)) exit_on_error();
    		if ((rs_wake_up_mult != mult[0]) && (mult != `NOT_CHECK)) exit_on_error();
    		if ((rs_free != free[0]) && (free != `NOT_CHECK)) exit_on_error();
		if ((rs_lsq_en != lsq) && (lsq != `NOT_CHECK)) exit_on_error();
	endtask

	initial
	begin
		 $monitor("Time= %d  wake_up_alu= %b wake_up_mult= %b resfree= %b rs_lsq= %b opa= %d  opb= %d", $time, rs_wake_up_alu, rs_wake_up_mult, rs_free, rs_lsq_en, rs_opa_out, rs_opb_out);

		clock = 0;
		reset = 0;
		rs_input_valid = `FALSE;
		rs_dest_in = 0;
		rs_opa_valid_in = `FALSE;
		rs_opa_in = 0;
		rs_opb_valid_in = `FALSE;
		rs_opb_in = 0;
		alu_func_in = 0;
		id_rd_mem = `FALSE;
		id_wr_mem = `FALSE;
		cdb_valid = `FALSE;
		cdb_value_in = 0;
		cdb_tag = 0;
		grant = `FALSE;

		@(negedge clock);
		reset = 1;
		@(negedge clock);
		reset = 0;
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd666, 6'd0);
		cdb(`WAY_1, `TRUE, 64'd666, 6'd0);
		@(negedge clock);
		cdb(`WAY_0, `FALSE, 64'd666, 6'd2);
		cdb(`WAY_1, `FALSE, 64'd666, 6'd2);
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
        if(rs_opa_out != 0) exit_on_error();
        if(rs_opb_out != 0) exit_on_error();
		//Dispatch, both opa and opb are ready
		dispatch(`ALU_ADDQ, `TRUE, 64'd55, `TRUE, 64'd78, 6'd1, `FALSE, `FALSE);
		dispatch_check();
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed both opa and opb ready\n");
		@(negedge clock);

		//Dispatch, valid RS but CDB trying to change
		cdb(`WAY_0, `TRUE, 64'd666, 6'd2);
		@(negedge clock);
		dispatch(`ALU_ADDQ, `TRUE, 64'd02, `TRUE, 64'd02, 6'd1, `FALSE, `FALSE);
		dispatch_check();
		@(negedge clock);
        if(rs_opa_out != 2) exit_on_error();
        if(rs_opb_out != 2) exit_on_error();
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed valid RS but CDB trying to change\n");
		@(negedge clock);

		//Dispatch, but opa has to wait for CDB
		dispatch(`ALU_ADDQ, `FALSE, 6'd2, `TRUE, 64'd102, 6'd1, `FALSE, `FALSE);
		dispatch_check();
		cdb(`WAY_0, `TRUE, 64'd666, 6'd2);
		@(negedge clock);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed opa wait for CDB\n");
		@(negedge clock);

        //Dispatch, but opb has to wait for CDB
		dispatch(`ALU_MULQ, `TRUE, 64'd84, `FALSE, 6'd4, 6'd1, `FALSE, `FALSE);
		dispatch_check();
		cdb(`WAY_0, `TRUE, 64'd752, 6'd4);
		@(negedge clock);
		check(`FALSE_2, `TRUE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed opb wait for CDB\n");
		@(negedge clock);

        //Dispatch, both opa and opb wait for CDB WAY_1 (LOAD)
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `FALSE, 6'd7, 6'd1, `TRUE, `FALSE);
		dispatch_check();
		cdb(`WAY_0, `TRUE, 64'd945, 6'd5);
		@(negedge clock);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		cdb(`WAY_0, `FALSE, 64'd256, 6'd7);
		cdb(`WAY_1, `TRUE, 64'd222, 6'd7);
        if(rs_opa_out != 945) exit_on_error();
		@(negedge clock);
        if(rs_opb_out != 222) exit_on_error();
		@(negedge clock);
		cdb(`WAY_1, `FALSE, 64'd945, 6'd5);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `TRUE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed both opa and opb wait for CDB WAY_1 (LOAD)\n");
		@(negedge clock);

        //Dispatch, both opa and opb wait for CDB WAY_0 (LOAD)
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `FALSE, 6'd7, 6'd1, `TRUE, `FALSE);
		dispatch_check();
		cdb(`WAY_0, `TRUE, 64'd945, 6'd5);
		@(negedge clock);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		cdb(`WAY_0, `TRUE, 64'd256, 6'd7);
		cdb(`WAY_1, `FALSE, 64'd222, 6'd7);
		@(negedge clock);
		cdb(`WAY_0, `FALSE, 64'd256, 6'd7);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `TRUE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed both opa and opb wait for CDB WAY_0 (LOAD)\n");
		@(negedge clock);

        //Dispatch, both opa and opb wait for CDB WAY_0 and WAY_1 (STORE)
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `FALSE, 6'd7, 6'd1, `FALSE, `TRUE);
		dispatch_check();
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd5);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd7);
		@(negedge clock);
		cdb(`WAY_0, `FALSE, 64'd1024, 6'd5);
		cdb(`WAY_1, `FALSE, 64'd64, 6'd7);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `TRUE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed both opa and opb wait for CDB WAY_0 and WAY_1 (STORE)\n");
		@(negedge clock);

        //Dispatch, both opa wait for CDB WAY_0 and WAY_1 wrong
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `TRUE, 64'd764, 6'd1, `FALSE, `FALSE);
		dispatch_check();
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd10);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd4);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd11);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd6);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd12);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd13);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd15);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd20);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd50);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd14);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd19);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd23);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd249, 6'd5);
		cdb(`WAY_1, `FALSE, 64'd64, 6'd7);
		@(negedge clock);
		cdb(`WAY_0, `FALSE, 64'd249, 6'd5);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed both opa wait for CDB WAY_0 and WAY_1 wrong\n");
		@(negedge clock);

        //Dispatch, opb wait for CDB WAY_0 and WAY_1 wrong
		dispatch(`ALU_ADDQ, `TRUE, 64'd975, `FALSE, 6'd5, 6'd1, `FALSE, `FALSE);
		dispatch_check();
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd10);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd4);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd11);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd6);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd12);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd13);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd15);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd20);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd50);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd14);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd1024, 6'd19);
		cdb(`WAY_1, `TRUE, 64'd64, 6'd23);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		cdb(`WAY_0, `TRUE, 64'd249, 6'd5);
		cdb(`WAY_1, `FALSE, 64'd64, 6'd7);
		@(negedge clock);
        if(rs_opa_out != 975) exit_on_error();
        if(rs_opb_out != 249) exit_on_error();
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed opb wait for CDB WAY_0 and WAY_1 wrong\n");
		@(negedge clock);

        //Ships passing in night
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `FALSE, 6'd5, 6'd1, `FALSE, `FALSE);
		cdb(`WAY_0, `TRUE, 64'd94, 6'd5);
		cdb(`WAY_1, `FALSE, 64'd45, 6'd5);
		@(negedge clock);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `NOT_CHECK);
        if(rs_opa_out != 94) exit_on_error();
        if(rs_opb_out != 94) exit_on_error();
		dispatch_check();
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed Ships passing in night\n");
		@(negedge clock);

        //Ships passing in night WAY_1
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `FALSE, 6'd5, 6'd1, `FALSE, `FALSE);
		cdb(`WAY_1, `TRUE, 64'd56, 6'd5);
		cdb(`WAY_0, `FALSE, 64'd46, 6'd5);
		@(negedge clock);
		dispatch_check();
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
        if(rs_opa_out != 56) exit_on_error();
        if(rs_opb_out != 56) exit_on_error();
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed Ships passing in night WAY_1\n");
		@(negedge clock);

        //Ships passing in night both CDB shouting values
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `FALSE, 6'd4, 6'd1, `FALSE, `FALSE);
		cdb(`WAY_0, `TRUE, 64'd04, 6'd5);
		cdb(`WAY_1, `TRUE, 64'd05, 6'd4);
		@(negedge clock);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `NOT_CHECK);
        if(rs_opa_out != 4) exit_on_error();
        if(rs_opb_out != 5) exit_on_error();
		dispatch_check();
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed Ships passing in night both CDB shouting values\n");
		@(negedge clock);

        //Ships passing in night, CDB invalid
		dispatch(`ALU_ADDQ, `FALSE, 6'd5, `TRUE, 64'd5087, 6'd1, `FALSE, `FALSE);
		cdb(`WAY_0, `FALSE, 64'd945, 6'd5);
		cdb(`WAY_1, `FALSE, 64'd945, 6'd5);
		@(negedge clock);
		check(`FALSE_2, `FALSE_2, `FALSE_2, `NOT_CHECK);
		cdb(`WAY_0, `TRUE, 64'd945, 6'd5);
		cdb(`WAY_1, `FALSE, 64'd523, 6'd5);
		@(negedge clock);
		check(`TRUE_2, `FALSE_2, `FALSE_2, `NOT_CHECK);
		dispatch_check();
		check(`TRUE_2, `FALSE_2, `FALSE_2, `FALSE_2);
		@(negedge clock);
		grantToRS();
		check(`NOT_CHECK, `NOT_CHECK, `TRUE_2, `NOT_CHECK);
		$display("\n@@@Passed Ships passing in night, CDB invalid\n");
		@(negedge clock);

        //Dispatch, and issue same clock cycle
		dispatch(`ALU_ADDQ, `TRUE, 64'd55, `TRUE, 64'd78, 6'd1, `FALSE, `FALSE);
		dispatch_check();
		check(`TRUE_2, `FALSE_2, `FALSE_2, `NOT_CHECK);
		@(negedge clock);
		grant = 1;
		dispatch(`ALU_ADDQ, `FALSE, 6'd3, `FALSE, 6'd4, 6'd1, `FALSE, `FALSE);
		@(negedge clock);
		grant = 0;
		check(`FALSE_2, `FALSE_2, `FALSE_2, `NOT_CHECK);
		$display("\n@@@Passed Dispatch, and issue same clock cycle\n");
		@(negedge clock);



		@(negedge clock);
		@(negedge clock);
		$display("\n@@@Passed\n");
		$finish;
	end
endmodule

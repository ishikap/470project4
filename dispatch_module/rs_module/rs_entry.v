// TODO:-rs_wake_up_alu should depend on all ALU instructions
//      -check when isUsed is true and getting an invalid "ships passing in the night" with an invalid instruction

// Fastest Clock Periods:
//      -2/17/18 2.1


`default_nettype none
`timescale 1ns/100ps
module rs_entry #(parameter NUM_PRF_ENTRIES = 64, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES)) (
    // INPUTS
    input   wire                                    clock,
    input   wire                                    reset,
    input   wire    [PRF_BITS-1:0]                  rs_dest_in,         // Input wire from RAT, renamed to PRF
    input   wire    [QUADWORD-1:0]                  rs_opa_in,          // If invalid, identifies a waiting PRF
                                                                        // If valid, has an immediate
    input   wire                                    rs_opa_valid_in,
    input   wire    [QUADWORD-1:0]                  rs_opb_in,          // If invalid, identifies a waiting PRF
                                                                        // If valid, has an immediate
    input   wire                                    rs_opb_valid_in,
    input   wire    [4:0]                           alu_func_in,        // Which EX unit to use?
    input   wire                                    id_rd_in,
    input   wire                                    id_wr_in,
    input   wire    [1:0][QUADWORD-1:0]             cdb_value_in,       // Value from the CDB
    input   wire    [1:0][PRF_BITS-1:0]             cdb_tag,            // Which PRF the CDB is using
    input   wire    [1:0]                           cdb_valid,          // Is the value on the CDB valid (could
                                                                        // special case this w/$r31, maybe not)
    input   wire                                    grant,              // Decide name later (which elements(?)
                                                                        // have been selected and are
                                                                        // going to be issued)
    input   wire                                    rs_input_valid,     // Is this RS selected to have new instr.?

    input   wire    [1:0]                           id_opa_select_out,
    input   wire    [1:0]                           id_opb_select_out,
    input   wire                                    id_cond_branch_out,
    input   wire                                    id_uncond_branch_out,
    input   wire    [QUADWORD-1:0]                  if_id_PC,
    input   wire    [QUADWORD-1:0]                  if_id_NPC,
    input   wire    [31:0]                          if_id_IR,
    input   wire     [1:0]                          nuke,

    // OUTPUTS
    output  logic                                   rs_wake_up,         // Is this RS "woke"?
    output  logic   [QUADWORD-1:0]                  rs_opa_out,         // Value of the operands (possibly a PRF)
    output  logic   [QUADWORD-1:0]                  rs_opb_out,
    output  logic   [PRF_BITS-1:0]                  rs_dest_out,        // Output to ex unit for where to store
    output  logic                                   rs_free,            // Is this RS free for a new instr.?
    output  logic   [4:0]                           id_rs_alu_func_out,
    output  logic   [1:0]                           id_rs_opa_select_out,
    output  logic   [1:0]                           id_rs_opb_select_out,
    output  logic                                   id_rs_cond_branch_out,
    output  logic                                   id_rs_uncond_branch_out,
    output  logic                                   id_rs_rd_out,
    output  logic                                   id_rs_wr_out,
    output  logic   [QUADWORD-1:0]                  id_rs_PC,
    output  logic   [QUADWORD-1:0]                  id_rs_NPC,
    output  logic   [31:0]                          id_rs_IR
);


    logic   [QUADWORD-1:0]                  opA;            // Operand A
    logic   [QUADWORD-1:0]                  opB;            // Operand B
    logic                                   opAValid;       // Operand a Tag/Value
    logic                                   opBValid;       // Operand B Tag/Value
    logic                                   inUse;          // InUse bit
    logic   [PRF_BITS-1:0]                  destTag;        // Destination Tag bit

    logic [1:0] loadAFromCDB;  // signal to load from the CDB
    logic [1:0] loadBFromCDB;  // signal to load from the CDB


    assign rs_free      = ~inUse;

    // Determine whether this RS is "woke" for an EX unit
    assign rs_wake_up   = inUse && opAValid && opBValid;
    assign rs_opa_out   = opA;
    assign rs_opb_out   = opB;
    assign rs_dest_out  = destTag;

    // Has the tag we are waiting for shown up on the CDB
    // Includes the "ships passing in the night" case

    //got rid of inUse
    always_comb begin
        for (int i=0; i<2; ++i) begin
            loadAFromCDB[i] = (cdb_tag[i] == opA && !opAValid && cdb_valid[i] && inUse) ||
                              (cdb_tag[i] == rs_opa_in && !rs_opa_valid_in && cdb_valid[i] && rs_input_valid);
            loadBFromCDB[i] = (cdb_tag[i] == opB && !opBValid && cdb_valid[i] && inUse) ||
                              (cdb_tag[i] == rs_opb_in && !rs_opb_valid_in && cdb_valid[i] && rs_input_valid);
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            opA                     <= `SD 0;
            opB                     <= `SD 0;
            opAValid                <= `SD 0;
            opBValid                <= `SD 0;
            inUse                   <= `SD 0;
            destTag                 <= `SD 0;
            id_rs_alu_func_out      <= `SD 0;
            id_rs_opa_select_out    <= `SD 0;
            id_rs_opb_select_out    <= `SD 0;
            id_rs_cond_branch_out   <= `SD 0;
            id_rs_uncond_branch_out <= `SD 0;
            id_rs_PC                <= `SD 0;
            id_rs_NPC               <= `SD 0;
            id_rs_IR                <= `SD 0;
            id_rs_rd_out            <= `SD 0;
            id_rs_wr_out            <= `SD 0;
        end
        else if (nuke) begin
            opA                     <= `SD 0;
            opB                     <= `SD 0;
            opAValid                <= `SD 0;
            opBValid                <= `SD 0;
            inUse                   <= `SD 0;
            destTag                 <= `SD 0;
            id_rs_alu_func_out      <= `SD 0;
            id_rs_opa_select_out    <= `SD 0;
            id_rs_opb_select_out    <= `SD 0;
            id_rs_cond_branch_out   <= `SD 0;
            id_rs_uncond_branch_out <= `SD 0;
            id_rs_PC                <= `SD 0;
            id_rs_NPC               <= `SD 0;
            id_rs_IR                <= `SD 0;
            id_rs_rd_out            <= `SD 0;
            id_rs_wr_out            <= `SD 0;
        end
        // When being written in, if grant = 1 then prev data left
        else if (rs_input_valid) begin
            // Handle ships passing in the night
            opA                     <= `SD loadAFromCDB[0] ? cdb_value_in[0] :
                                           loadAFromCDB[1] ? cdb_value_in[1] : rs_opa_in;
            opB                     <= `SD loadBFromCDB[0] ? cdb_value_in[0] :
                                           loadBFromCDB[1] ? cdb_value_in[1] : rs_opb_in;
            opAValid                <= `SD (loadAFromCDB[1] || loadAFromCDB[0]) ? 1'b1 : rs_opa_valid_in;
            opBValid                <= `SD (loadBFromCDB[1] || loadBFromCDB[0]) ? 1'b1 : rs_opb_valid_in;
            inUse                   <= `SD 1'b1;
            destTag                 <= `SD rs_dest_in;
            id_rs_alu_func_out      <= `SD alu_func_in;
            id_rs_opa_select_out    <= `SD id_opa_select_out;
            id_rs_opb_select_out    <= `SD id_opb_select_out;
            id_rs_cond_branch_out   <= `SD id_cond_branch_out;
            id_rs_uncond_branch_out <= `SD id_uncond_branch_out;
            id_rs_PC                <= `SD if_id_PC;
            id_rs_NPC               <= `SD if_id_NPC;
            id_rs_IR                <= `SD if_id_IR;
            id_rs_rd_out            <= `SD id_rd_in;
            id_rs_wr_out            <= `SD id_wr_in;
        end
        // Not sure if safe
        else if (grant) begin
            opA                     <= `SD 0;
            opB                     <= `SD 0;
            opAValid                <= `SD 0;
            opBValid                <= `SD 0;
            inUse                   <= `SD 0;
            destTag                 <= `SD 0;
            id_rs_alu_func_out      <= `SD 0;
            id_rs_opa_select_out    <= `SD 0;
            id_rs_opb_select_out    <= `SD 0;
            id_rs_cond_branch_out   <= `SD 0;
            id_rs_uncond_branch_out <= `SD 0;
            id_rs_PC                <= `SD 0;
            id_rs_NPC               <= `SD 0;
            id_rs_IR                <= `SD 0;
            id_rs_rd_out            <= `SD 0;
            id_rs_wr_out            <= `SD 0;
        end
        else if (inUse) begin
            opA                     <= `SD loadAFromCDB[0] ? cdb_value_in[0] :
                                           loadAFromCDB[1] ? cdb_value_in[1] : opA;
            opB                     <= `SD loadBFromCDB[0] ? cdb_value_in[0] :
                                           loadBFromCDB[1] ? cdb_value_in[1] : opB;
            opAValid                <= `SD (loadAFromCDB[1] || loadAFromCDB[0]) ? 1'b1 : opAValid;
            opBValid                <= `SD (loadBFromCDB[1] || loadBFromCDB[0]) ? 1'b1 : opBValid;
        end
    end // always @
endmodule

`default_nettype wire


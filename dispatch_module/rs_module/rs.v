// TODO:    Parameterize number of outputs? (depndings on number of issues)
//          Make lsq_en true within RS not RS_entry

// CLOCK PERIOD: With 16 entries and 64 PRFs, 4.5ns < x <= 4.625ns (high effort)
//                    32 entries and 64 PRFS, 5.25ns < x <= 5.5ns
//                    2/18/18: 16 entries and 64 PRFs, 4.7ns < x <= 4.9ns

`default_nettype none
`timescale 1ns/100ps
module rs #(parameter NUM_RS = 16, parameter NUM_PRF_ENTRIES = 64, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES)) (
    // INPUTS
    input   wire                                            clock,
    input   wire                                            reset,

    input   wire      [1:0][PRF_BITS-1:0]                   rs_dest_in,
    input   wire      [1:0][QUADWORD-1:0]                   rs_opa_in,
    input   wire      [1:0]                                 rs_opa_valid_in,
    input   wire      [1:0][QUADWORD-1:0]                   rs_opb_in,
    input   wire      [1:0]                                 rs_opb_valid_in,
    input   wire      [1:0][4:0]                            alu_func_in,
    input   wire      [1:0]                                 id_rd_in,
    input   wire      [1:0]                                 id_wr_in,
    input   wire      [1:0][QUADWORD-1:0]                   cdb_value_in,
    input   wire      [1:0][PRF_BITS-1:0]                   cdb_tag,
    input   wire      [1:0]                                 cdb_valid,

    input   wire      [1:0][1:0]                            id_opa_select_out,
    input   wire      [1:0][1:0]                            id_opb_select_out,
    input   wire      [1:0]                                 id_cond_branch_out,
    input   wire      [1:0]                                 id_uncond_branch_out,
    input   wire      [1:0]                                 id_valid_inst_out,
    input   wire      [1:0][QUADWORD-1:0]                   if_id_PC,               // required for ex_branch (jsr)
    input   wire      [1:0][QUADWORD-1:0]                   if_id_NPC,
    input   wire      [1:0][31:0]                           if_id_IR,

    input   wire      [1:0]                                 disp_ex_enable,
    input   wire      [1:0]                                 nuke,

    // OUTPUTS
    // to memory ex unit
    output  logic     [1:0]                                 rs_output_mem_valid,
    output  logic     [1:0][QUADWORD-1:0]                   rs_opb_mem_out,
    output  logic     [1:0][PRF_BITS-1:0]                   rs_dest_mem_out,
    output  logic     [1:0][31:0]                           id_rs_mem_IR,
    // to non-memory ex units
    output  logic     [1:0]                                 rs_output_no_mem_valid,
    output  logic     [1:0][QUADWORD-1:0]                   rs_opa_out,
    output  logic     [1:0][QUADWORD-1:0]                   rs_opb_out,
    output  logic     [1:0][PRF_BITS-1:0]                   rs_dest_out,
    output  logic     [1:0][4:0]                            rs_alu_func_out,
    output  logic     [1:0][1:0]                            id_rs_opa_select_out,
    output  logic     [1:0][1:0]                            id_rs_opb_select_out,
    output  logic     [1:0]                                 id_rs_cond_branch_out,
    output  logic     [1:0]                                 id_rs_uncond_branch_out,
    output  logic     [1:0][QUADWORD-1:0]                   id_rs_PC,
    output  logic     [1:0][QUADWORD-1:0]                   id_rs_NPC,
    output  logic     [1:0][31:0]                           id_rs_IR,

    output  logic     [1:0]                                 rs_stall         // True if RS is full
);

    wire    [NUM_RS-1:0]                            grant;      // 2 hot internal signal determined by prioriy selector
                                                                // Determines which RSes have been selected and are going to be issued

    wire           [1:0][NUM_RS-1:0]                grants_no_mem, grants_mem;

    wire    [NUM_RS-1:0]                            woke;
    wire    [NUM_RS-1:0]                            rs_free;
    wire           [1:0][NUM_RS-1:0]                rs_sel_dispatch;
    wire    [NUM_RS-1:0]                            rs_input_valid;

    // Internal Output signals: 16 of them, choose 2 to be outputs
    logic   [NUM_RS-1:0][QUADWORD-1:0]              rs_opa_out_int;
    logic   [NUM_RS-1:0][QUADWORD-1:0]              rs_opb_out_int;
    logic   [NUM_RS-1:0][PRF_BITS-1:0]              rs_dest_out_int;
    logic   [NUM_RS-1:0][4:0]                       rs_alu_func_out_int;
    logic   [NUM_RS-1:0][1:0]                       id_rs_opa_select_out_int;
    logic   [NUM_RS-1:0][1:0]                       id_rs_opb_select_out_int;
    logic   [NUM_RS-1:0]                            id_rs_cond_branch_out_int;
    logic   [NUM_RS-1:0]                            id_rs_uncond_branch_out_int;
    logic   [NUM_RS-1:0]                            id_rs_rd_out_int;
    logic   [NUM_RS-1:0]                            id_rs_wr_out_int;
    logic   [NUM_RS-1:0][QUADWORD-1:0]              id_rs_PC_int;
    logic   [NUM_RS-1:0][QUADWORD-1:0]              id_rs_NPC_int;
    logic   [NUM_RS-1:0][31:0]                      id_rs_IR_int;

    // Internal input signals: 2 of them, 16 outputs
    logic   [NUM_RS-1:0][PRF_BITS-1:0]              rs_dest_in_int;
    logic   [NUM_RS-1:0][QUADWORD-1:0]              rs_opa_in_int;
    logic   [NUM_RS-1:0]                            rs_opa_valid_in_int;
    logic   [NUM_RS-1:0][QUADWORD-1:0]              rs_opb_in_int;
    logic   [NUM_RS-1:0]                            rs_opb_valid_in_int;
    logic   [NUM_RS-1:0][4:0]                       alu_func_in_int;
    logic   [NUM_RS-1:0]                            id_rd_in_int;
    logic   [NUM_RS-1:0]                            id_wr_in_int;
    logic   [NUM_RS-1:0][1:0]                       id_opa_select_out_int;
    logic   [NUM_RS-1:0][1:0]                       id_opb_select_out_int;
    logic   [NUM_RS-1:0]                            id_cond_branch_out_int;
    logic   [NUM_RS-1:0]                            id_uncond_branch_out_int;
    logic   [NUM_RS-1:0][QUADWORD-1:0]              if_id_PC_int;
    logic   [NUM_RS-1:0][QUADWORD-1:0]              if_id_NPC_int;
    logic   [NUM_RS-1:0][31:0]                      if_id_IR_int;


    // Determine which RSes we can select, based on whether the mult is ready
    // Why not fetch multiple instr at fill RS faster and issue to two ex
    // units at a time

    // Issue 2 at a time, should do 4 (if the timings allow)
    ps_select_n #(.SEL(2), .REQ_BITS(NUM_RS)) woke_select_no_mem(
        .req({2{woke & ~id_rs_rd_out_int & ~id_rs_wr_out_int}}),
        .en(2'b11),
        .grant(grants_no_mem)
    );

    // Issue 2 at a time, should do 4 (if the timings allow)
    ps_select_n #(.SEL(2), .REQ_BITS(NUM_RS)) woke_select_mem(
        .req({2{woke & (id_rs_rd_out_int | id_rs_wr_out_int)}}),
        .en(2'b11),
        .grant(grants_mem)
    );

    // Get the selected ones to wake
    assign rs_output_mem_valid[0] = |grants_mem[0];
    assign rs_output_mem_valid[1] = |grants_mem[1];
    assign rs_output_no_mem_valid[0] = |grants_no_mem[0];
    assign rs_output_no_mem_valid[1] = |grants_no_mem[1];

    // Get the zero, one, or two-hot representation of which
    // RSes should be granted
    assign grant = (grants_no_mem[0] & {NUM_RS{disp_ex_enable[0]}}) | (grants_no_mem[1] & {NUM_RS{disp_ex_enable[1]}}) | grants_mem[0] | grants_mem[1];

    // Dispatch 2 at a time
    // passing backwards because ps selects way 0 first
    ps_select_n #(.SEL(2), .REQ_BITS(NUM_RS)) rs_select(
        .req({rs_free, rs_free}),
        .en({id_valid_inst_out[0], id_valid_inst_out[1]}),
        .grant({rs_sel_dispatch[0], rs_sel_dispatch[1]})
    );

    // Valid (two hot) if no memory stalls
    assign rs_input_valid = rs_sel_dispatch[0] | rs_sel_dispatch[1];

    // Stall if all rs_sel_dispatch's are 0
    // rs_stall[0], should go high when exactly 1 bit high of rs_free, or
    // when rs_free is all 0's, algorithm taken from stackoverflow...
    assign rs_stall[0] = ((|rs_free) & (~|(rs_free & (rs_free - 1)))) || (rs_free == 0);
    assign rs_stall[1] = (rs_free == 0);

    always_comb begin
        // Default everything to the input 1
        for (int i=0; i<NUM_RS; ++i) begin
            rs_dest_in_int[i]               = rs_dest_in[1];
            rs_opa_in_int[i]                = rs_opa_in[1];
            rs_opa_valid_in_int[i]          = rs_opa_valid_in[1];
            rs_opb_in_int[i]                = rs_opb_in[1];
            rs_opb_valid_in_int[i]          = rs_opb_valid_in[1];
            alu_func_in_int[i]              = alu_func_in[1];
            id_rd_in_int[i]                 = id_rd_in[1];
            id_wr_in_int[i]                 = id_wr_in[1];
            id_opa_select_out_int[i]        = id_opa_select_out[1];
            id_opb_select_out_int[i]        = id_opb_select_out[1];
            id_cond_branch_out_int[i]       = id_cond_branch_out[1];
            id_uncond_branch_out_int[i]     = id_uncond_branch_out[1];
            if_id_PC_int[i]                 = if_id_PC[1];
            if_id_NPC_int[i]                = if_id_NPC[1];
            if_id_IR_int[i]                 = if_id_IR[1];
        end
        // Set the one value to input 0
        for (int i=0; i<NUM_RS; ++i) begin
            if (rs_sel_dispatch[0][i] & { NUM_RS{id_valid_inst_out[0]} }) begin
                rs_dest_in_int[i]               = rs_dest_in[0];
                rs_opa_in_int[i]                = rs_opa_in[0];
                rs_opa_valid_in_int[i]          = rs_opa_valid_in[0];
                rs_opb_in_int[i]                = rs_opb_in[0];
                rs_opb_valid_in_int[i]          = rs_opb_valid_in[0];
                alu_func_in_int[i]              = alu_func_in[0];
                id_rd_in_int[i]                 = id_rd_in[0];
                id_wr_in_int[i]                 = id_wr_in[0];
                id_opa_select_out_int[i]        = id_opa_select_out[0];
                id_opb_select_out_int[i]        = id_opb_select_out[0];
                id_cond_branch_out_int[i]       = id_cond_branch_out[0];
                id_uncond_branch_out_int[i]     = id_uncond_branch_out[0];
                if_id_PC_int[i]                 = if_id_PC[0];
                if_id_NPC_int[i]                = if_id_NPC[0];
                if_id_IR_int[i]                 = if_id_IR[0];
            end
        end
    end

    rs_entry #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) reservation_stations [NUM_RS-1:0] (
        // Inputs
        .clock(clock),
        .reset(reset),
        .rs_dest_in(rs_dest_in_int),
        .rs_opa_in(rs_opa_in_int),
        .rs_opa_valid_in(rs_opa_valid_in_int),
        .rs_opb_in(rs_opb_in_int),
        .rs_opb_valid_in(rs_opb_valid_in_int),
        .alu_func_in(alu_func_in_int),
        .id_rd_in(id_rd_in_int),
        .id_wr_in(id_wr_in_int),
        .cdb_value_in(cdb_value_in),
        .cdb_tag(cdb_tag),
        .cdb_valid(cdb_valid),
        .grant(grant),
        .rs_input_valid(rs_input_valid),

        .id_opa_select_out(id_opa_select_out_int),
        .id_opb_select_out(id_opb_select_out_int),
        .id_cond_branch_out(id_cond_branch_out_int),
        .id_uncond_branch_out(id_uncond_branch_out_int),
        .if_id_PC(if_id_PC_int),
        .if_id_NPC(if_id_NPC_int),
        .if_id_IR(if_id_IR_int),
        .nuke(nuke),

        // Outputs
        .rs_wake_up(woke),
        .id_rs_alu_func_out(rs_alu_func_out_int),
        .rs_opa_out(rs_opa_out_int),
        .rs_opb_out(rs_opb_out_int),
        .rs_dest_out(rs_dest_out_int),
        .rs_free(rs_free),
        .id_rs_opa_select_out(id_rs_opa_select_out_int),
        .id_rs_opb_select_out(id_rs_opb_select_out_int),
        .id_rs_cond_branch_out(id_rs_cond_branch_out_int),
        .id_rs_uncond_branch_out(id_rs_uncond_branch_out_int),
        .id_rs_rd_out(id_rs_rd_out_int),
        .id_rs_wr_out(id_rs_wr_out_int),
        .id_rs_PC(id_rs_PC_int),
        .id_rs_NPC(id_rs_NPC_int),
        .id_rs_IR(id_rs_IR_int)
    );

    // Output muxing: choose 1 of 16 outputs twice
    always_comb begin
        // Initializing
        rs_opa_out                  = 0;
        rs_opb_out                  = 0;
        rs_dest_out                 = 0;
        rs_alu_func_out             = 0;
        id_rs_opa_select_out        = 0;
        id_rs_opb_select_out        = 0;
        id_rs_cond_branch_out       = 0;
        id_rs_uncond_branch_out     = 0;
        id_rs_PC                    = 0;
        id_rs_NPC                   = 0;
        id_rs_IR                    = 0;

        rs_opb_mem_out              = 0;
        rs_dest_mem_out             = 0;
        id_rs_mem_IR                = 0;

        for (int i = 0; i < 2; ++i) begin
            for (int j = 0; j < NUM_RS; ++j) begin
                if (grants_no_mem[i][j]) begin
                    rs_opa_out[i]                   = rs_opa_out_int[j];
                    rs_opb_out[i]                   = rs_opb_out_int[j];
                    rs_dest_out[i]                  = rs_dest_out_int[j];
                    rs_alu_func_out[i]              = rs_alu_func_out_int[j];
                    id_rs_opa_select_out[i]         = id_rs_opa_select_out_int[j];
                    id_rs_opb_select_out[i]         = id_rs_opb_select_out_int[j];
                    id_rs_cond_branch_out[i]        = id_rs_cond_branch_out_int[j];
                    id_rs_uncond_branch_out[i]      = id_rs_uncond_branch_out_int[j];
                    id_rs_PC[i]                     = id_rs_PC_int[j];
                    id_rs_NPC[i]                    = id_rs_NPC_int[j];
                    id_rs_IR[i]                     = id_rs_IR_int[j];
                end
            end
        end


        for (int i = 0; i < 2; ++i) begin
            for (int j = 0; j < NUM_RS; ++j) begin
                if (grants_mem[i][j]) begin
                    rs_opb_mem_out[i]               = rs_opb_out_int[j];
                    rs_dest_mem_out[i]              = rs_dest_out_int[j];
                    id_rs_mem_IR[i]                 = id_rs_IR_int[j];
                end
            end
        end
    end

endmodule
`default_nettype wire


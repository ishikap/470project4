//For: Testing Full RS Wrapper
//By: Sidhaarth and Scott
//Date: 2/15/18

`timescale 1ns/100ps
`define NOT_CHECK 	2'b10
`define TRUE_2 		2'b01
`define FALSE_2 	2'b00

`define WAY_0	1'b0
`define WAY_1	1'b1


parameter NUM_PRF_ENTRIES = 64;
parameter PRF_BITS = $clog2(NUM_PRF_ENTRIES);
parameter NUM_RS = 16;
parameter QUADWORD = 64;

module testbench;

    logic                                            clock;
    logic                                            reset;

    logic      [1:0][PRF_BITS-1:0]                   rs_dest_in;
    logic      [1:0][QUADWORD-1:0]                   rs_opa_in;
    logic      [1:0]                                 rs_opa_valid_in;
    logic      [1:0][QUADWORD-1:0]                   rs_opb_in;
    logic      [1:0]                                 rs_opb_valid_in;
    logic      [1:0][4:0]                            alu_func_in;
    logic      [1:0]                                 id_rd_mem;
    logic      [1:0]                                 id_wr_mem;
    logic      [1:0][QUADWORD-1:0]                   cdb_value_in;
    logic      [1:0][PRF_BITS-1:0]                   cdb_tag;
    logic      [1:0]                                 cdb_valid;

    logic      [1:0][1:0]                            id_opa_select_out;
    logic      [1:0][1:0]                            id_opb_select_out;
    logic      [1:0]                                 id_cond_branch_out;
    logic      [1:0]                                 id_uncond_branch_out;
    logic      [1:0]                                 id_halt_out;
    logic      [1:0]                                 id_illegal_out;
    logic      [1:0]                                 id_valid_inst_out;
    logic      [1:0][QUADWORD-1:0]                   if_id_NPC;
    logic      [1:0][31:0]                           if_id_IR;
    logic      [1:0]                                 id_thread_id;

    logic      [1:0]                                 mult_ready;
    logic      [1:0]                                 alu_ready;

    // OUTPUTS
    wire      [1:0]                                 rs_output_valid;
    wire      [1:0][QUADWORD-1:0]                   rs_opa_out;
    wire      [1:0][QUADWORD-1:0]                   rs_opb_out;
    wire      [1:0][PRF_BITS-1:0]                   rs_dest_out;
    wire      [1:0][4:0]                            rs_alu_func_out;
    wire      [1:0]                                 rs_lsq_en;
    wire      [1:0][1:0]                            id_rs_opa_select_out;
    wire      [1:0][1:0]                            id_rs_opb_select_out;
    wire      [1:0]                                 id_rs_cond_branch_out;
    wire      [1:0]                                 id_rs_uncond_branch_out;
    wire      [1:0]                                 id_rs_halt_out;
    wire      [1:0]                                 id_rs_illegal_out;
    wire      [1:0]                                 id_rs_wr_mem;
    wire      [1:0]                                 id_rs_rd_mem;
    wire      [1:0]                                 id_rs_valid_inst_out;
    wire      [1:0][QUADWORD-1:0]                   id_rs_NPC;
    wire      [1:0][31:0]                           id_rs_IR;
    wire      [1:0]                                 id_rs_thread;

    wire      [1:0]                                 rs_stall;         // True if RS is full



rs #(.NUM_RS(NUM_RS), .NUM_PRF_ENTRIES(NUM_PRF_ENTRIES)) rs_full(
		.clock(clock),
		.reset(reset),
		.rs_dest_in(rs_dest_in),
		.rs_opa_in(rs_opa_in),
		.rs_opa_valid_in(rs_opa_valid_in),
		.rs_opb_in(rs_opb_in),
		.rs_opb_valid_in(rs_opb_valid_in),
		.alu_func_in(alu_func_in),
		.id_rd_mem(id_rd_mem),
		.id_wr_mem(id_wr_mem),
		.cdb_value_in(cdb_value_in),
		.cdb_tag(cdb_tag),
		.cdb_valid(cdb_valid),
		.id_opa_select_out(id_opa_select_out),
		.id_opb_select_out(id_opb_select_out),
		.id_cond_branch_out(id_cond_branch_out),
		.id_uncond_branch_out(id_uncond_branch_out),
		.id_halt_out(id_halt_out),
		.id_illegal_out(id_illegal_out),
		.id_valid_inst_out(id_valid_inst_out),
		.if_id_NPC(if_id_NPC),
		.if_id_IR(if_id_IR),
		.id_thread_id(id_thread_id),
		.mult_ready(mult_ready),
		.alu_ready(alu_ready),
		.rs_output_valid(rs_output_valid),
		.rs_opa_out(rs_opa_out),
		.rs_opb_out(rs_opb_out),
		.rs_dest_out(rs_dest_out),        //ou
        .rs_alu_func_out(rs_alu_func_out),
		.rs_lsq_en(rs_lsq_en),
		.id_rs_opa_select_out(id_rs_opa_select_out),
		.id_rs_opb_select_out(id_rs_opb_select_out),
		.id_rs_cond_branch_out(id_rs_cond_branch_out),
		.id_rs_uncond_branch_out(id_rs_uncond_branch_out),
		.id_rs_halt_out(id_rs_halt_out),
		.id_rs_illegal_out(id_rs_illegal_out),
		.id_rs_valid_inst_out(id_rs_valid_inst_out),
        .id_rs_wr_mem(id_rs_wr_mem),
        .id_rs_rd_mem(id_rs_rd_mem),
		.id_rs_NPC(id_rs_NPC),
		.id_rs_IR(id_rs_IR),
		.id_rs_thread(id_rs_thread),
		.rs_stall(rs_stall)
	);

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("Incorrect");
        $finish;
    endtask

    task dispatch;
			input way;
        input [4:0] alu_func;
        input rs_opa_valid;
        input [63:0] rs_opa;
        input rs_opb_valid;
        input [63:0] rs_opb;
        input [4:0] rs_dest;
        input rd_mem, wr_mem;

        rs_dest_in[way] = rs_dest;
        rs_opa_valid_in[way] = rs_opa_valid;
        rs_opa_in[way] = rs_opa;
        rs_opb_valid_in[way] = rs_opb_valid;
        rs_opb_in[way] = rs_opb;
        alu_func_in[way] = alu_func;
        id_rd_mem[way] = rd_mem;
        id_wr_mem[way] = wr_mem;

	endtask



	task cdb;
        input way;
		input cdb_valid_in;
		input [63:0] cdb_value;
		input [4:0] cdb_tag_in;

		cdb_valid[way] = cdb_valid_in;
		cdb_value_in[way] = cdb_value;
		cdb_tag[way] = cdb_tag_in;

	endtask
	initial
	begin
        /*
        $monitor("==CDB 0===\nCDB_TAG: %d, CDB_VALUE: %d, CDB_VALID: %d\n===CDB 1===\nCDB_TAG: %d, CDB_VALUE: %d, CDB_VALID: %d\n===IN 0===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, ID_VALID_INSTR: %b\n===IN 1===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, ID_VALID_INSTR: %b\n===OUT 0===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, RS_OUTPUT_VALID: %b, RS_STALL: %b\n===OUT 1===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, RS_OUTPUT_VALID: %b, RS_STALL: %b\n\n",
            cdb_tag[0], cdb_value_in[0], cdb_tag[0],
            cdb_tag[1], cdb_value_in[1], cdb_tag[1],
            $time, rs_dest_in[0], rs_opa_in[0], rs_opb_in[0], alu_func_in[0], id_valid_inst_out[0],
            $time, rs_dest_in[1], rs_opa_in[1], rs_opb_in[1], alu_func_in[1], id_valid_inst_out[1],
            $time, rs_dest_out[0], rs_opa_out[0], rs_opb_out[0], rs_alu_func_out[0], rs_output_valid[0], rs_stall[0],
            $time, rs_dest_out[1], rs_opa_out[1], rs_opb_out[1], rs_alu_func_out[1], rs_output_valid[1], rs_stall[1]);*/



        $monitor("==CDB 0===\nCDB_TAG: %d, CDB_VALUE: %d, CDB_VALID: %d\n",
                cdb_tag[0], cdb_value_in[0], cdb_tag[0],
                "===CDB 1===\nCDB_TAG: %d, CDB_VALUE: %d, CDB_VALID: %d\n",
                cdb_tag[1], cdb_value_in[1], cdb_tag[1],
                "===IN 0===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, ID_VALID_INSTR: %b\n",
                $time, rs_dest_in[0], rs_opa_in[0], rs_opb_in[0], alu_func_in[0], id_valid_inst_out[0],
                "===IN 1===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, ID_VALID_INSTR: %b\n",
                $time, rs_dest_in[1], rs_opa_in[1], rs_opb_in[1], alu_func_in[1], id_valid_inst_out[1],
                "===OUT 0===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, RS_OUTPUT_VALID: %b, RS_STALL: %b\n",
                $time, rs_dest_out[0], rs_opa_out[0], rs_opb_out[0], rs_alu_func_out[0], rs_output_valid[0], rs_stall[0],
                "===OUT 1===\nTime: %d, RS_DEST: %d, RS_OPA: %d, RS_OPB: %d, ALU_FUNC: %d, RS_OUTPUT_VALID: %b, RS_STALL: %b\n\n",
                $time, rs_dest_out[1], rs_opa_out[1], rs_opb_out[1], rs_alu_func_out[1], rs_output_valid[1], rs_stall[1]);

        id_opa_select_out = 0;
        id_opb_select_out = 0;
        id_cond_branch_out = 0;
        id_uncond_branch_out = 0;
        id_halt_out = 0;
        id_illegal_out = 0;
        id_valid_inst_out = 0;
        if_id_NPC = 0;
        if_id_IR = 0;
        id_thread_id = 0;

		clock = 0;
		reset = 0;

		rs_dest_in[0] = 0;
		rs_opa_valid_in[0] = `FALSE;
		rs_opa_in[0] = 0;
		rs_opb_valid_in[0] = `FALSE;
		rs_opb_in[0] = 0;
		alu_func_in[0] = 0;
		id_rd_mem[0] = `FALSE;
		id_wr_mem[0] = `FALSE;
		cdb_valid[0] = `FALSE;
		cdb_value_in[0] = 0;
		cdb_tag[0] = 0;
        //alu_ready = 2'b0;
		//mult_ready = 2'b0;
		alu_ready = 2'b11;
		mult_ready = 2'b11;

		rs_dest_in[1] = 0;
		rs_opa_valid_in[1] = `FALSE;
		rs_opa_in[1] = 0;
		rs_opb_valid_in[1] = `FALSE;
		rs_opb_in[1] = 0;
		alu_func_in[1] = 0;
		id_rd_mem[1] = `FALSE;
		id_wr_mem[1] = `FALSE;
		cdb_valid[1] = `FALSE;
		cdb_value_in[1] = 0;
		cdb_tag[1] = 0;

		@(negedge clock);
		reset = 1;
		@(negedge clock);
		reset = 0;
        id_valid_inst_out = 2'b11;

		//Dispatch two ALU instructions in the same cycle, Both instructions have
		//all operands ready
		dispatch(`WAY_0, `ALU_ADDQ, `TRUE, 64'd55, `TRUE, 64'd78, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_ADDQ, `TRUE, 64'd44, `TRUE, 64'd97, 5'd2, `FALSE, `FALSE);
        @(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd55) exit_on_error();
		if(rs_opb_out[0] !== 64'd78) exit_on_error();
		if(rs_opa_out[1] !== 64'd44) exit_on_error();
		if(rs_opb_out[1] !== 64'd97) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		$display("\n@@@Passed\n");

		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_ADDQ, `FALSE, 5'd30, `TRUE, 64'd78, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_ADDQ, `FALSE, 5'd29, `TRUE, 64'd97, 5'd2, `FALSE, `FALSE);
		alu_ready = 2'b11;
		mult_ready = 2'b11;
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_ADDQ, `FALSE, 5'd30, `TRUE, 64'd88, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_ADDQ, `FALSE, 5'd29, `TRUE, 64'd34, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `FALSE, 5'd30, `TRUE, 64'd56, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `FALSE, 5'd29, `TRUE, 64'd33, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `FALSE, 5'd30, `TRUE, 64'd66, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `FALSE, 5'd29, `TRUE, 64'd23, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_ADDQ, `FALSE, 5'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `FALSE, 5'd29, `TRUE, 64'd53, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_ADDQ, `FALSE, 5'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `FALSE, 5'd29, `TRUE, 64'd53, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `FALSE, 5'd30, `TRUE, 64'd96, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_ADDQ, `FALSE, 5'd29, `TRUE, 64'd03, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle	----THE RS IS FULL
		dispatch(`WAY_0, `ALU_MULQ, `FALSE, 5'd30, `TRUE, 64'd46, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_ADDQ, `FALSE, 5'd29, `TRUE, 64'd73, 5'd2, `FALSE, `FALSE);
		$display("\n@@@Passed, all 16 RS dispatched, ie RS is now full.\n");
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b11) exit_on_error();
		//Dispatch two instructions in the same cycle	//Shouldnt have dispatched into RS
		dispatch(`WAY_0, `ALU_MULQ, `FALSE, 5'd28, `TRUE, 64'd46, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_ADDQ, `FALSE, 5'd27, `TRUE, 64'd73, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b11) exit_on_error();
		cdb(`WAY_0, `TRUE, 64'd666, 5'd29);
		cdb(`WAY_1, `TRUE, 64'd777, 5'd30);
        id_valid_inst_out = 0;
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd78) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd97) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		cdb(`WAY_1, `TRUE, 64'd888, 5'd28);
		cdb(`WAY_0, `TRUE, 64'd999, 5'd27);
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd88) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd34) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd56) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd33) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd66) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd23) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd53) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd53) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd96) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd03) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd777) exit_on_error();
		if(rs_opb_out[0] !== 64'd46) exit_on_error();
		if(rs_opa_out[1] !== 64'd666) exit_on_error();
		if(rs_opb_out[1] !== 64'd73) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		$display("\n@@@Passed, All 16 RS are issued\n");


		//Dispatch an ADD and MULT, issue ADD in Way1 followed by MULT in Way0
		dispatch(`WAY_0, `ALU_ADDQ, `TRUE, 64'd55, `TRUE, 64'd78, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd44, `TRUE, 64'd97, 5'd2, `FALSE, `FALSE);
		alu_ready = 2'b10;
		mult_ready = 2'b10;
        id_valid_inst_out = 2'b11;
        @(negedge clock);
        id_valid_inst_out = 2'b00;
		if(rs_output_valid !== 2'b10) exit_on_error();
		if(rs_opa_out[1] !== 64'd55) exit_on_error();
		if(rs_opb_out[1] !== 64'd78) exit_on_error();
		if(rs_dest_out[1] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();

        @(negedge clock);
		if(rs_output_valid !== 2'b10) exit_on_error();
		if(rs_opa_out[1] !== 64'd44) exit_on_error();
		if(rs_opb_out[1] !== 64'd97) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		$display("\n@@@Passed, ADD issued then next cycle a MULT\n");


        //Dispatch an ADD and MULT, then MULT issued in Way1 and ADD issued in
        //Way0
		dispatch(`WAY_0, `ALU_ADDQ, `TRUE, 64'd33, `TRUE, 64'd26, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd104, `TRUE, 64'd789, 5'd2, `FALSE, `FALSE);
		alu_ready = 2'b10;
		mult_ready = 2'b01;
        id_valid_inst_out = 2'b11;
        @(negedge clock);
        id_valid_inst_out = 2'b00;
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[1] !== 64'd33) exit_on_error();
		if(rs_opb_out[1] !== 64'd26) exit_on_error();
		if(rs_dest_out[1] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_opa_out[0] !== 64'd104) exit_on_error();
		if(rs_opb_out[0] !== 64'd789) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		$display("\n@@@Passed\n");


        //Dispatch an ADD and MULT, then MULT issued in Way1 and ADD issued in
        //Way0 (dipatch Way_0 and Way_1 switched)
        @(negedge clock);
		dispatch(`WAY_1, `ALU_ADDQ, `TRUE, 64'd33, `TRUE, 64'd26, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd104, `TRUE, 64'd789, 5'd2, `FALSE, `FALSE);
		alu_ready = 2'b10;
		mult_ready = 2'b01;
        id_valid_inst_out = 2'b11;
        @(negedge clock);
        id_valid_inst_out = 2'b00;
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[1] !== 64'd33) exit_on_error();
		if(rs_opb_out[1] !== 64'd26) exit_on_error();
		if(rs_dest_out[1] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_opa_out[0] !== 64'd104) exit_on_error();
		if(rs_opb_out[0] !== 64'd789) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		$display("\n@@@Passed Way_0 and Way_1 switched\n");


        //Dispatch an ADD and MULT, then ADD issued in Way1 and MULT issued
        //after a few cycles
        @(negedge clock);
		dispatch(`WAY_0, `ALU_ADDQ, `TRUE, 64'd33, `TRUE, 64'd26, 5'd1, `TRUE, `TRUE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd104, `TRUE, 64'd789, 5'd2, `FALSE, `FALSE);
		alu_ready = 2'b10;
		mult_ready = 2'b00;
        id_valid_inst_out = 2'b11;
        @(negedge clock);
        id_valid_inst_out = 2'b00;
		if(rs_output_valid !== 2'b10) exit_on_error();
		if(rs_opa_out[1] !== 64'd33) exit_on_error();
		if(rs_opb_out[1] !== 64'd26) exit_on_error();
		if(rs_dest_out[1] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b10) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
		mult_ready = 2'b01;
		#1; //Comb delay
        if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd104) exit_on_error();
		if(rs_opb_out[0] !== 64'd789) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		$display("\n@@@Passed MULT waiting\n");


        //Fill all 16 with MULTs and then issue one at a time
		//Dispatch two instructions in the same cycle
		@(negedge clock);
		alu_ready = 2'b00;
		mult_ready = 2'b00;
		@(negedge clock);
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd78, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd97, 5'd2, `FALSE, `FALSE);
        id_valid_inst_out = 2'b11;
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd88, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd34, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd56, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd33, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd66, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd23, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd53, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd53, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd96, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd03, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle	----THE RS IS FULL
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd46, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd73, 5'd2, `FALSE, `FALSE);
		$display("\n@@@Passed, all 16 RS dispatched, ie RS is now full.\n");
		@(negedge clock);
        id_valid_inst_out = 2'b00;
		mult_ready = 2'b01;
		#1; //Comb delay
        if(rs_stall !== 2'b00) exit_on_error();
        if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd78) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd97) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd88) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd34) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd56) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd33) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd66) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd23) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd53) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd53) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd96) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd03) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd46) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd29) exit_on_error();
		if(rs_opb_out[0] !== 64'd73) exit_on_error();
		if(rs_dest_out[0] !== 5'd2) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		$display("\n@@@Passed All RS multiplys issued.\n");


		// Fill all 16 with MULTs and then issue two at a time, and dispatch while full and issuing
		@(negedge clock);
		alu_ready = 2'b00;
		mult_ready = 2'b00;
		@(negedge clock);
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd78, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd97, 5'd2, `FALSE, `FALSE);
		id_valid_inst_out = 2'b11;
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd88, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd34, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd56, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd33, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd66, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd23, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd53, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd53, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd96, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd03, 5'd2, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch two instructions in the same cycle	----THE RS IS FULL
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd46, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd29, `TRUE, 64'd73, 5'd2, `FALSE, `FALSE);
		$display("\n@@@Passed, all 16 RS dispatched, ie RS is now full.\n");

		// Dispatch into RS while issuing while RS is full
		@(negedge clock);
		alu_ready = 2'b00;
		mult_ready = 2'b11;
		#1; //Comb delay
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd78) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd97) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		dispatch(`WAY_0, `ALU_ADDQ, `TRUE, 64'd0, `TRUE, 64'd2, 5'd0, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_ADDQ, `TRUE, 64'd1, `TRUE, 64'd3, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		id_valid_inst_out = 2'b00;
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd88) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd34) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		@(negedge clock);
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd56) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd33) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		@(negedge clock);
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd66) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd23) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		@(negedge clock);
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd53) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		@(negedge clock);
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd53) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		@(negedge clock);
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd96) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd3) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		@(negedge clock);
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd46) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_opa_out[1] !== 64'd29) exit_on_error();
		if(rs_opb_out[1] !== 64'd73) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		@(negedge clock);
		if(rs_stall !== 2'b00) exit_on_error();
		if(rs_output_valid !== 2'b00) exit_on_error();
		@(negedge clock);
		alu_ready = 2'b11;
		mult_ready = 2'b00;
		#1; // Comb delay
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd0) exit_on_error();
		if(rs_opb_out[0] !== 64'd2) exit_on_error();
		if(rs_dest_out[0] !== 5'd0) exit_on_error();
		if(rs_opa_out[1] !== 64'd1) exit_on_error();
		if(rs_opb_out[1] !== 64'd3) exit_on_error();
		if(rs_dest_out[1] !== 5'd1) exit_on_error();
		$display("\n@@@Passed dispatching when RS is full and issuing.\n");


        // Test that other outputs are passed on, two alu instr: both issued
        // to ALU FU
		@(negedge clock);
		dispatch(`WAY_0, `ALU_SUBQ, `TRUE, 64'd3, `TRUE, 64'd4, 5'd1, `FALSE, `TRUE);
		dispatch(`WAY_1, `ALU_AND, `TRUE, 64'd5, `TRUE, 64'd6, 5'd2, `TRUE, `FALSE);
		alu_ready = 2'b11;
		mult_ready = 2'b11;
        id_valid_inst_out = 2'b11;
        id_opa_select_out[0] = 2'b10;
        id_opa_select_out[1] = 2'b01;
        id_opb_select_out[0] = 2'b01;
        id_opb_select_out[1] = 2'b10;
        id_cond_branch_out[0] = 0;
        id_cond_branch_out[1] = 1;
        id_uncond_branch_out[0] = 1;
        id_uncond_branch_out[1] = 0;
        id_halt_out = 2'b11;
        id_illegal_out = 2'b11;
        if_id_NPC[0] = 64'hFFFF_0000_FFFF_0000;
        if_id_NPC[1] = 64'h0000_FFFF_0000_FFFF;
        if_id_IR[0] = 32'hFF00_FF00;
        if_id_IR[1] = 32'h00FF_00FF;
        id_thread_id = 2'b11;
		@(negedge clock);
        id_valid_inst_out = 0;
		if(rs_output_valid !== 2'b11) exit_on_error();
		if(rs_opa_out[0] !== 64'd3) exit_on_error();
		if(rs_opb_out[0] !== 64'd4) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_alu_func_out[0] !== `ALU_SUBQ) exit_on_error();
		if(rs_lsq_en !== 2'b11) exit_on_error();
		if(id_rs_opa_select_out[0] !== 2'b10) exit_on_error();
		if(id_rs_opb_select_out[0] !== 2'b01) exit_on_error();
		if(id_rs_cond_branch_out !== 2'b10) exit_on_error();
		if(id_rs_uncond_branch_out !== 2'b01) exit_on_error();
		if(id_rs_wr_mem !== 2'b01) exit_on_error();
		if(id_rs_rd_mem !== 2'b10) exit_on_error();
		if(id_rs_halt_out !== 2'b11) exit_on_error();
		if(id_rs_illegal_out !== 2'b11) exit_on_error();
		if(id_rs_valid_inst_out !== 2'b11) exit_on_error();
		if(id_rs_NPC[0] !== 64'hFFFF_0000_FFFF_0000) exit_on_error();
		if(id_rs_IR[0] !== 32'hFF00_FF00) exit_on_error();
		if(rs_opa_out[1] !== 64'd5) exit_on_error();
		if(rs_opb_out[1] !== 64'd6) exit_on_error();
		if(rs_dest_out[1] !== 5'd2) exit_on_error();
		if(rs_alu_func_out[1] !== `ALU_AND) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		if(id_rs_opa_select_out[1] !== 2'b01) exit_on_error();
		if(id_rs_opb_select_out[1] !== 2'b10) exit_on_error();
		if(id_rs_NPC[1] !== 64'h0000_FFFF_0000_FFFF) exit_on_error();
		if(id_rs_IR[1] !== 32'h00FF_00FF) exit_on_error();
        if(id_rs_thread !== 2'b11) exit_on_error();
		$display("\n@@@Passed All outputs correct.\n");

        //Dispatch 1 at a time (WAY_0), WAY_1 fetch stalled
		@(negedge clock);
		alu_ready = 2'b11;  //Ready, but never used
		mult_ready = 2'b00;
		@(negedge clock);
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd78, 5'd1, `FALSE, `FALSE);
        id_valid_inst_out = 2'b01;
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd88, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd56, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd66, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd36, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd30, `TRUE, 64'd96, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction	    ----THE RS IS 1/2 FULL
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd56, `TRUE, 64'd97, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd120, `TRUE, 64'd24, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd50, `TRUE, 64'd32, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd122, `TRUE, 64'd19, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd78, `TRUE, 64'd64, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd47, `TRUE, 64'd18, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd61, `TRUE, 64'd12, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd48, `TRUE, 64'd789, 5'd1, `FALSE, `FALSE);
		@(negedge clock);
		if(rs_output_valid !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		//Dispatch one instruction      ----THE RS IS FULL
		dispatch(`WAY_0, `ALU_MULQ, `TRUE, 64'd03, `TRUE, 64'd159, 5'd1, `FALSE, `FALSE);
		dispatch(`WAY_1, `ALU_MULQ, `TRUE, 64'd47, `TRUE, 64'd222, 5'd1, `FALSE, `FALSE);
		$display("\n@@@Passed, all 16 RS dispatched (one at a time), ie RS is now full.\n");
        id_valid_inst_out = 2'b01;
		@(negedge clock);
        if(rs_stall !== 2'b01) exit_on_error();
        id_valid_inst_out = 2'b00;
		@(negedge clock);
		mult_ready = 2'b01;
		#1; //Comb delay
        if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd78) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd88) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd56) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd66) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd36) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd30) exit_on_error();
		if(rs_opb_out[0] !== 64'd96) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd56) exit_on_error();
		if(rs_opb_out[0] !== 64'd97) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd120) exit_on_error();
		if(rs_opb_out[0] !== 64'd24) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd50) exit_on_error();
		if(rs_opb_out[0] !== 64'd32) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd122) exit_on_error();
		if(rs_opb_out[0] !== 64'd19) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd78) exit_on_error();
		if(rs_opb_out[0] !== 64'd64) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd47) exit_on_error();
		if(rs_opb_out[0] !== 64'd18) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd61) exit_on_error();
		if(rs_opb_out[0] !== 64'd12) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd48) exit_on_error();
		if(rs_opb_out[0] !== 64'd789) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		@(negedge clock);
		if(rs_output_valid !== 2'b01) exit_on_error();
		if(rs_opa_out[0] !== 64'd03) exit_on_error();
		if(rs_opb_out[0] !== 64'd159) exit_on_error();
		if(rs_dest_out[0] !== 5'd1) exit_on_error();
		if(rs_lsq_en !== 2'b00) exit_on_error();
		if(rs_stall !== 2'b00) exit_on_error();
		$display("\n@@@Passed All RS multiplys issued (dispatched 1 at a time).\n");


		@(negedge clock);
		@(negedge clock);
		$display("\n@@@Passed ALLLL!!!!!!!!!!!!\n");
		$finish;
    end
endmodule

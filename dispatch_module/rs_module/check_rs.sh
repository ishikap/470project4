#!/bin/bash

make | grep 'Incorrect' &> /dev/null

if [ $? != 0 ]; then
    echo "Both testbenches PASSED"
else
    echo "FAIL, at least one testbench failed"
fi


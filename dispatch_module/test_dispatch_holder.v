//For: Testing Dispatch Holder
//Date: 3/9/17
`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define TRUE_2          2'b01
`define FALSE_2         2'b00
`define WAY_0           1'b0
`define WAY_1           1'b1
`define R0              5'd0
`define R1              5'd1
`define R2              5'd2
`define R3              5'd3
`define R4              5'd4
`define R5              5'd5
`define R6              5'd6
`define R7              5'd7
`define R8              5'd8
`define R9              5'd9
`define R10             5'd10
`define R11             5'd11
`define R12             5'd12
`define R13             5'd13
`define R14             5'd14
`define R15             5'd15
`define R16             5'd16
`define R17             5'd17
`define R18             5'd18
`define R19             5'd19
`define R20             5'd20
`define R21             5'd21
`define R22             5'd22
`define R23             5'd23
`define R24             5'd24
`define R25             5'd25
`define R26             5'd26
`define R27             5'd27
`define R28             5'd28
`define R29             5'd29
`define R30             5'd30
`define R31             5'd31

parameter NUM_PRF_ENTRIES       = 96;
parameter NUM_RS                = 16;
parameter ROB_SIZE              = 64;
localparam ROB_BITS             = $clog2(ROB_SIZE);
localparam QUADWORD             = 64;
localparam PRF_BITS             = $clog2(NUM_PRF_ENTRIES);
localparam NUM_ARF_ENTRIES      = 32;
localparam ARF_BITS             = $clog2(NUM_ARF_ENTRIES);


module testbench;

    //INPUTS
    logic                                               clock;
    logic                                               reset;
    // From RAT
    logic             [1:0][PRF_BITS-1:0]               prf_rda_idx;
    logic             [1:0][PRF_BITS-1:0]               prf_rdb_idx;
    logic             [1:0][PRF_BITS-1:0]               prf_dest_idx;
    logic             [1:0]                             prf_dest_valid;
    logic             [1:0][PRF_BITS-1:0]               rrat_overwritten;
    logic             [1:0]                             rrat_overwritten_valid;
    logic    [ROB_SIZE-1:0][PRF_BITS-1:0]               rrat_free_list;
    // From Decoder
    logic             [1:0][1:0]                        opa_select_out;
    logic             [1:0][1:0]                        opb_select_out;
    logic             [1:0][ARF_BITS-1:0]               arf_idx_out;
    logic             [1:0][4:0]                        alu_func_out;
    logic             [1:0]                             rd_mem_out;
    logic             [1:0]                             wr_mem_out;
    logic             [1:0]                             ldl_mem_out;
    logic             [1:0]                             stc_mem_out;
    logic             [1:0]                             cond_branch_out;
    logic             [1:0]                             uncond_branch_out;
    logic             [1:0]                             branch_call;
    logic             [1:0]                             branch_ret;
    logic             [1:0]                             halt_out;
    logic             [1:0]                             illegal_out;
    logic             [1:0]                             valid_inst_out;
    // From Execution complete
    logic             [1:0]                             cdb_valid;
    logic             [1:0][PRF_BITS-1:0]               cdb_prf_tag;
    logic             [1:0][QUADWORD-1:0]               cdb_value_in;
    logic             [1:0]                             cdb_branch_taken;
    logic             [1:0][QUADWORD-1:0]               cdb_actual_next_pc;
    // From IF/ID Register & Thread ID
    logic             [1:0][QUADWORD-1:0]               id_disp_PC;           // MUST ADD TO FETCH
    logic             [1:0][QUADWORD-1:0]               id_disp_NPC;
    logic             [1:0][QUADWORD/2-1:0]             id_disp_IR;
    logic             [1:0]                             thread_id;

    // OUTPUTS
    // From RS
    logic            [1:0]                              rs_output_valid;
    logic            [1:0][QUADWORD-1:0]                rs_opa_out;
    logic            [1:0][QUADWORD-1:0]                rs_opb_out;
    logic            [1:0][PRF_BITS-1:0]                rs_dest_out;
    logic            [1:0][4:0]                         rs_alu_func_out;
    logic            [1:0][1:0]                         rs_opa_select_out;
    logic            [1:0][1:0]                         rs_opb_select_out;
    logic            [1:0]                              rs_cond_branch_out;
    logic            [1:0]                              rs_uncond_branch_out;
    logic            [1:0]                              rs_halt_out;
    logic            [1:0]                              rs_illegal_out;
    logic            [1:0]                              rs_mem_op; ///888
    logic            [1:0][QUADWORD-1:0]                rs_PC;
    logic            [1:0][QUADWORD-1:0]                rs_NPC;
    logic            [1:0][31:0]                        rs_IR;
    logic            [1:0]                              rs_thread_id;
    logic            [1:0]                              rs_stall;
    // From ROB
    logic            [1:0]                              rob_stall;
    logic            [1:0]                              update_valid;
    logic            [1:0][ARF_BITS-1:0]                update_arch_reg_out;
    logic            [1:0][PRF_BITS-1:0]                update_prf_reg_out;
    logic            [1:0]                              nuke;
    logic            [1:0]                              update_branch_actual_out;
    logic            [1:0][QUADWORD-1:0]                update_pc_out;
    logic            [1:0][QUADWORD-1:0]                update_next_pc_out;
    logic            [1:0]                              update_branch_conditional_out;
    logic            [1:0]                              update_branch_unconditional_out;
    logic            [1:0]                              update_branch_call_out;
    logic            [1:0]                              update_branch_ret_out;
    logic            [1:0]                              update_load_out;
    logic            [1:0]                              update_store_out;
    logic            [1:0]                              update_halt_out;


    dispatch_holder #(.NUM_PRF_ENTRIES(NUM_PRF_ENTRIES), .NUM_RS(NUM_RS), .ROB_SIZE(ROB_SIZE)) dispatch_holder1(
        //INPUTS
        .clock(clock),
        .reset(reset),
        .prf_rda_idx(prf_rda_idx),
        .prf_rdb_idx(prf_rdb_idx),
        .prf_dest_idx(prf_dest_idx),
        .prf_dest_valid(prf_dest_valid),
        .rrat_overwritten(rrat_overwritten),
        .rrat_overwritten_valid(rrat_overwritten_valid),
        .rrat_free_list(rrat_free_list),

        .opa_select_out(opa_select_out),
        .opb_select_out(opb_select_out),
        .arf_idx_out(arf_idx_out),
        .alu_func_out(alu_func_out),
        .rd_mem_out(rd_mem_out),
        .wr_mem_out(wr_mem_out),
        .ldl_mem_out(ldl_mem_out),
        .stc_mem_out(stc_mem_out),
        .cond_branch_out(cond_branch_out),
        .uncond_branch_out(uncond_branch_out),
        .branch_call(branch_call),
        .branch_ret(branch_ret),
        .halt_out(halt_out),
        .illegal_out(illegal_out),
        .valid_inst_out(valid_inst_out),

        .cdb_valid(cdb_valid),
        .cdb_prf_tag(cdb_prf_tag),
        .cdb_value_in(cdb_value_in),
        .cdb_branch_taken(cdb_branch_taken),
        .cdb_actual_next_pc(cdb_actual_next_pc),

        .id_disp_PC(id_disp_PC),
        .id_disp_NPC(id_disp_NPC),
        .id_disp_IR(id_disp_IR),
        .thread_id(thread_id),

        //OUTPUTS
        .rs_output_valid(rs_output_valid),
        .rs_opa_out(rs_opa_out),
        .rs_opb_out(rs_opb_out),
        .rs_dest_out(rs_dest_out),
        .rs_alu_func_out(rs_alu_func_out),
        .rs_opa_select_out(rs_opa_select_out),
        .rs_opb_select_out(rs_opb_select_out),
        .rs_cond_branch_out(rs_cond_branch_out),
        .rs_uncond_branch_out(rs_uncond_branch_out),
        .rs_halt_out(rs_halt_out),
        .rs_illegal_out(rs_illegal_out),
        .rs_mem_op(rs_mem_op),
        .rs_PC(rs_PC),
        .rs_NPC(rs_NPC),
        .rs_IR(rs_IR),
        .rs_thread_id(rs_thread_id),
        .rs_stall(rs_stall),

        .rob_stall(rob_stall),
        .update_valid(update_valid),
        .update_arch_reg_out(update_arch_reg_out),
        .update_prf_reg_out(update_prf_reg_out),
        .nuke(nuke),
        .update_branch_actual_out(update_branch_actual_out),
        .update_pc_out(update_pc_out),
        .update_next_pc_out(update_next_pc_out),
        .update_branch_conditional_out(update_branch_conditional_out),
        .update_branch_unconditional_out(update_branch_unconditional_out),
        .update_branch_call_out(update_branch_call_out),
        .update_branch_ret_out(update_branch_ret_out),
        .update_load_out(update_load_out),
        .update_store_out(update_store_out),
        .update_halt_out(update_halt_out)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");

        //display all ROB enteries
        $display("===RoB===");
        $display("\n\n\nRob full_rob: %d ", dispatch_holder1.rob1.full_rob);
        $display("Rob stall: %b ", rob_stall);
        $display("Rob head: %d, Rob tail: %d ", dispatch_holder1.rob1.head, dispatch_holder1.rob1.tail);
        for (int i = 0; i < ROB_SIZE; i++)
        begin
            if (i == dispatch_holder1.rob1.head)
                $display("\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---  <--What rob is pointing to! HEAD");

            $display("Rob Entry Number: %d ", i,
                      "\n     ARF Reg:            %10d ", dispatch_holder1.rob1.arch_reg[i],
                      "\n     PRF Reg:            %10d ", dispatch_holder1.rob1.prf_reg[i],
                      "\n     PC:                 %10h ", dispatch_holder1.rob1.pc[i],
                      "\n     Next PC:            %10h ", dispatch_holder1.rob1.next_pc[i],
                      "\n     Branch Actual:      %b ", dispatch_holder1.rob1.branch_actual[i],
                      "\n     Branch Addr Wrong:  %b ", dispatch_holder1.rob1.branch_addr_wrong[i],
                      "\n     Exe Complete:       %b ", dispatch_holder1.rob1.execution_complete[i],
                      "\n     Branch Cond:        %b ", dispatch_holder1.rob1.branch_conditional[i],
                      "\n     Branch Uncond:      %b ", dispatch_holder1.rob1.branch_unconditional[i],
                      "\n     Load:               %b ", dispatch_holder1.rob1.load[i],
                      "\n     Store:              %b ", dispatch_holder1.rob1.store[i],
                      "\n     Branch Call:        %b ", dispatch_holder1.rob1.branch_call[i],
                      "\n     Branch Return:      %b ", dispatch_holder1.rob1.branch_return[i]);

             if (i == (dispatch_holder1.rob1.tail - 1))
                $display("/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---  <--What rob is pointing to! TAIL");
        end

        //display all the current PRF enteries
        $display("\n\n===PRF===");
        $display("Outputs WAY_0: ",
                    "   prf_rda_out:        %10d ", dispatch_holder1.prf_rda_out[0],
                    "   prf_rdb_out:        %10d ", dispatch_holder1.prf_rdb_out[0],
                    "   prf_valid_a_out:    %b   ", dispatch_holder1.prf_valid_a_out[0],
                    "   prf_valid_b_out:    %b   ", dispatch_holder1.prf_valid_b_out[0]);
        $display("Outputs WAY_1: ",
                    "   prf_rda_out:        %10d ", dispatch_holder1.prf_rda_out[1],
                    "   prf_rdb_out:        %10d ", dispatch_holder1.prf_rdb_out[1],
                    "   prf_valid_a_out:    %b   ", dispatch_holder1.prf_valid_a_out[1],
                    "   prf_valid_b_out:    %b   \n", dispatch_holder1.prf_valid_b_out[1]);
        for (int i=0; i<NUM_PRF_ENTRIES; i++)
        begin
            $display("PRF Entry #: %2d   ", i,
                     "PRF Data : %5d   ", dispatch_holder1.prf1.prfs[i],
                     "PRF Valid : %b   ", dispatch_holder1.prf1.valid[i]);

        end


        //display all the current RS enteries
        $display("\n\n===RS===");
        $display("RS Entry Number: %2d ", 0,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[0].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[0].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[0].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[0].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[0].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[0].destTag);
        $display("RS Entry Number: %2d ", 1,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[1].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[1].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[1].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[1].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[1].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[1].destTag);
        $display("RS Entry Number: %2d ", 2,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[2].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[2].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[2].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[2].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[2].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[2].destTag);
        $display("RS Entry Number: %2d ", 3,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[3].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[3].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[3].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[3].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[3].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[3].destTag);
        $display("RS Entry Number: %2d ", 4,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[4].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[4].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[4].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[4].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[4].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[4].destTag);
        $display("RS Entry Number: %2d ", 5,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[5].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[5].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[5].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[5].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[5].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[5].destTag);
        $display("RS Entry Number: %2d ", 6,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[6].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[6].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[6].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[6].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[6].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[6].destTag);
        $display("RS Entry Number: %2d ", 7,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[7].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[7].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[7].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[7].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[7].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[7].destTag);
        $display("RS Entry Number: %2d ", 8,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[8].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[8].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[8].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[8].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[8].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[8].destTag);
        $display("RS Entry Number: %2d ", 9,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[9].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[9].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[9].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[9].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[9].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[9].destTag);
        $display("RS Entry Number: %2d ", 10,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[10].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[10].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[10].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[10].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[10].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[10].destTag);
        $display("RS Entry Number: %2d ", 11,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[11].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[11].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[11].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[11].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[11].inUse,
                  "\n     destTag:      %10d ", dispatch_holder1.rs1.reservation_stations[11].destTag);
        $display("RS Entry Number: %2d ", 12,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[12].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[12].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[12].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[12].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[12].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[12].destTag);
        $display("RS Entry Number: %2d ", 13,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[13].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[13].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[13].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[13].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[13].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[13].destTag);
        $display("RS Entry Number: %2d ", 14,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[14].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[14].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[14].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[14].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[14].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[14].destTag);
        $display("RS Entry Number: %2d ", 15,
                  "\n     opA:          %10d ", dispatch_holder1.rs1.reservation_stations[15].opA,
                  "\n     opB:          %10d ", dispatch_holder1.rs1.reservation_stations[15].opB,
                  "\n     opAValid:     %b ",   dispatch_holder1.rs1.reservation_stations[15].opAValid,
                  "\n     opBValid:     %b ",   dispatch_holder1.rs1.reservation_stations[15].opBValid,
                  "\n     inUse:        %b ",   dispatch_holder1.rs1.reservation_stations[15].inUse,
                  "\n     destTag:      %10d ",   dispatch_holder1.rs1.reservation_stations[15].destTag);

        $fatal("\n\nINCORRECT");
    endtask


    task from_decoder1;
        input                           way;
        input   [1:0]                   opa_select_out_tmp;
        input   [1:0]                   opb_select_out_tmp;
        input   [ARF_BITS-1:0]          arf_idx_out_tmp;
        input   [4:0]                   alu_func_out_tmp;
        input                           rd_mem_out_tmp;
        input                           wr_mem_out_tmp;

        opa_select_out[way]             = opa_select_out_tmp;
        opb_select_out[way]             = opb_select_out_tmp;
        arf_idx_out[way]                = arf_idx_out_tmp;
        alu_func_out[way]               = alu_func_out_tmp;
        rd_mem_out[way]                 = rd_mem_out_tmp;
        wr_mem_out[way]                 = wr_mem_out_tmp;
    endtask

    task from_decoder2;
        input                           way;
        input                           ldl_mem_out_tmp;
        input                           stc_mem_out_tmp;
        input                           cond_branch_out_tmp;
        input                           uncond_branch_out_tmp;
        input                           branch_call_tmp;
        input                           branch_ret_tmp;

        ldl_mem_out[way]                = ldl_mem_out_tmp;
        stc_mem_out[way]                = stc_mem_out_tmp;
        cond_branch_out[way]            = cond_branch_out_tmp;
        uncond_branch_out[way]          = uncond_branch_out_tmp;
        branch_call[way]                = branch_call_tmp;
        branch_ret[way]                 = branch_ret_tmp;
    endtask

    task from_decoder3;
        input                           way;
        input                           halt_out_tmp;
        input                           illegal_out_tmp;
        input                           valid_inst_out_tmp;

        halt_out[way]                   = halt_out_tmp;
        illegal_out[way]                = illegal_out_tmp;
        valid_inst_out[way]             = valid_inst_out_tmp;
    endtask

    task invalid_inst;
        input                           way;

        valid_inst_out[way]             = `FALSE;
    endtask

    task from_rat;
        input                           way;
        input   [PRF_BITS-1:0]          prf_rda_idx_tmp;
        input   [PRF_BITS-1:0]          prf_rdb_idx_tmp;
        input   [PRF_BITS-1:0]          prf_dest_idx_tmp;
        input                           prf_dest_valid_tmp;

        prf_rda_idx[way]                = prf_rda_idx_tmp;
        prf_rdb_idx[way]                = prf_rdb_idx_tmp;
        prf_dest_idx[way]               = prf_dest_idx_tmp;
        prf_dest_valid[way]             = prf_dest_valid_tmp;
    endtask

    task from_rrat;
        input                           way;
        input   [PRF_BITS-1:0]          rrat_overwritten_tmp;
        input                           rrat_overwritten_valid_tmp;

        rrat_overwritten[way]           = rrat_overwritten_tmp;
        rrat_overwritten_valid[way]     = rrat_overwritten_valid_tmp;
    endtask

    task invalid_prf_dest;
        input                           way;

        prf_dest_valid[way]             = `FALSE;
    endtask

    task invalid_rrat_overwritten;
        input                           way;

        rrat_overwritten_valid[way]     = `FALSE;
    endtask

    task from_cdb;
        input                           way;
        input                           cdb_valid_tmp;
        input   [PRF_BITS-1:0]          cdb_prf_tag_tmp;
        input   [QUADWORD-1:0]          cdb_value_in_tmp;
        input                           cdb_branch_taken_tmp;
        input   [QUADWORD-1:0]          cdb_actual_next_pc_tmp;

        cdb_valid[way]                  = cdb_valid_tmp;
        cdb_prf_tag[way]                = cdb_prf_tag_tmp;
        cdb_value_in[way]               = cdb_value_in_tmp;
        cdb_branch_taken[way]           = cdb_branch_taken_tmp;
        cdb_actual_next_pc[way]         = cdb_actual_next_pc_tmp;
    endtask

    task invalid_cdb;
        input                           way;

        cdb_valid[way]                  = `FALSE;
    endtask

    task id_disp_pipe;
        input                           way;
        input   [QUADWORD-1:0]          id_disp_PC_tmp;
        input   [QUADWORD-1:0]          id_disp_NPC_tmp;
        input   [QUADWORD/2-1:0]        id_disp_IR_tmp;
        input                           thread_id_tmp;

        id_disp_PC[way]                 = id_disp_PC_tmp;
        id_disp_NPC[way]                = id_disp_NPC_tmp;
        id_disp_IR[way]                 = id_disp_IR_tmp;
        thread_id[way]                  = thread_id_tmp;
    endtask

    task check_rs1;
        input                           way;
        input                           rs_output_valid_tmp;
        input  [QUADWORD-1:0]           rs_opa_out_tmp;
        input  [QUADWORD-1:0]           rs_opb_out_tmp;
        input  [PRF_BITS-1:0]           rs_dest_out_tmp;
        input  [4:0]                    rs_alu_func_out_tmp;

        if (rs_output_valid[way]        != rs_output_valid_tmp)                                 exit_on_error();
        if (rs_opa_out[way]             != rs_opa_out_tmp && rs_output_valid_tmp)               exit_on_error();
        if (rs_opb_out[way]             != rs_opb_out_tmp && rs_output_valid_tmp)               exit_on_error();
        if (rs_dest_out[way]            != rs_dest_out_tmp && rs_output_valid_tmp)              exit_on_error();
        if (rs_alu_func_out[way]        != rs_alu_func_out_tmp && rs_output_valid_tmp)          exit_on_error();
    endtask

    task check_rs2;
        input                           way;
        input                           rs_output_valid_tmp;
        input  [1:0]                    rs_opa_select_out_tmp;
        input  [1:0]                    rs_opb_select_out_tmp;
        input                           rs_cond_branch_out_tmp;
        input                           rs_uncond_branch_out_tmp;
        input                           rs_halt_out_tmp;
        input                           rs_illegal_out_tmp;

        if (rs_output_valid[way]        != rs_output_valid_tmp)                                 exit_on_error();
        if (rs_opa_select_out[way]      != rs_opa_select_out_tmp && rs_output_valid_tmp)        exit_on_error();
        if (rs_opb_select_out[way]      != rs_opb_select_out_tmp && rs_output_valid_tmp)        exit_on_error();
        if (rs_cond_branch_out[way]     != rs_cond_branch_out_tmp && rs_output_valid_tmp)       exit_on_error();
        if (rs_uncond_branch_out[way]   != rs_uncond_branch_out_tmp && rs_output_valid_tmp)     exit_on_error();
        if (rs_halt_out[way]            != rs_halt_out_tmp && rs_output_valid_tmp)              exit_on_error();
        if (rs_illegal_out[way]         != rs_illegal_out_tmp && rs_output_valid_tmp)           exit_on_error();
    endtask

    task check_rs3;
        input                           way;
        input                           rs_output_valid_tmp;
        input                           rs_mem_op_tmp;
        input  [QUADWORD-1:0]           rs_NPC_tmp;
        input  [31:0]                   rs_IR_tmp;
        input                           rs_thread_id_tmp;
        input                           rs_stall_tmp;

        if (rs_output_valid[way]        != rs_output_valid_tmp)                                 exit_on_error();
        if (rs_mem_op[way]              != rs_mem_op_tmp && rs_output_valid_tmp)                exit_on_error();
        if (rs_NPC[way]                 != rs_NPC_tmp && rs_output_valid_tmp)                   exit_on_error();
        if (rs_IR[way]                  != rs_IR_tmp && rs_output_valid_tmp)                    exit_on_error();
        if (rs_thread_id[way]           != rs_thread_id_tmp && rs_output_valid_tmp)             exit_on_error();
        if (rs_stall[way]               != rs_stall_tmp && rs_output_valid_tmp)                 exit_on_error();
    endtask

    task check_rs_entry_free_status;
        input                           idx;
        input                           status;

        if (dispatch_holder1.rs1.rs_free[idx] != status)                                    exit_on_error();
    endtask

    task check_rs_output_invalid;
        input                           way;

        if (rs_output_valid[way]         != `FALSE)                                         exit_on_error();
    endtask

    task check_rob1;
        input                           way;
        input                           rob_stall_tmp;
        input                           update_valid_tmp;
        input       [ARF_BITS-1:0]      update_arch_reg_out_tmp;
        input       [PRF_BITS-1:0]      update_prf_reg_out_tmp;
        input                           nuke_tmp;

        if (rob_stall[way]              != rob_stall_tmp)                                           exit_on_error();
        if (update_valid[way]           != update_valid_tmp)                                        exit_on_error();
        if (update_arch_reg_out[way]    != update_arch_reg_out_tmp && update_valid_tmp)             exit_on_error();
        if (update_prf_reg_out[way]     != update_prf_reg_out_tmp && update_valid_tmp)              exit_on_error();
        if (nuke[way]                   != nuke_tmp)                                                exit_on_error();
    endtask

    task check_rob2;
        input                           way;
        input                           update_valid_tmp;
        input                           update_branch_actual_out_tmp;
        input       [QUADWORD-1:0]      update_pc_out_tmp;
        input       [QUADWORD-1:0]      update_next_pc_out_tmp;
        input                           update_branch_conditional_out_tmp;
        input                           update_branch_unconditional_out_tmp;

        if (update_valid[way]                       != update_valid_tmp)                                        exit_on_error();
        if (update_branch_actual_out[way]           != update_branch_actual_out_tmp && update_valid_tmp)        exit_on_error();
        if (update_pc_out[way]                      != update_pc_out_tmp && update_valid_tmp)                   exit_on_error();
        if (update_next_pc_out[way]                 != update_next_pc_out_tmp && update_valid_tmp)              exit_on_error();
        if (update_branch_conditional_out[way]      != update_branch_conditional_out_tmp && update_valid_tmp)   exit_on_error();
        if (update_branch_unconditional_out[way]    != update_branch_unconditional_out_tmp && update_valid_tmp) exit_on_error();
    endtask

    task check_rob3;
        input                           way;
        input                           update_valid_tmp;
        input                           update_branch_call_out_tmp;
        input                           update_branch_ret_out_tmp;
        input                           update_load_out_tmp;
        input                           update_store_out_tmp;

        if (update_valid[way]                        != update_valid_tmp)                                        exit_on_error();
        if (update_branch_call_out[way]              != update_branch_call_out_tmp && update_valid_tmp)          exit_on_error();
        if (update_branch_ret_out[way]               != update_branch_ret_out_tmp && update_valid_tmp)           exit_on_error();
        if (update_load_out[way]                     != update_load_out_tmp && update_valid_tmp)                 exit_on_error();
        if (update_store_out[way]                    != update_store_out_tmp && update_valid_tmp)                exit_on_error();
    endtask

    task check_rob_invalid_out;
        input                           way;

        if (update_valid[way]   != `FALSE)  exit_on_error();
    endtask

    task check_rob_head;
        input   [ROB_SIZE-1:0]  head_ptr;

        if (dispatch_holder1.rob1.head != head_ptr) exit_on_error();
    endtask

    task check_rob_tail;
        input   [ROB_SIZE-1:0]  tail_ptr;

        if (dispatch_holder1.rob1.tail != tail_ptr) exit_on_error();
    endtask

    task check_prf;
        input                           way;
        input   [QUADWORD-1:0]          prf_rda_in;
        input   [QUADWORD-1:0]          prf_rdb_in;
        input                           prf_valid_a_in;
        input                           prf_valid_b_in;

        if (dispatch_holder1.prf_rda_out[way]           != prf_rda_in && prf_valid_a_in)    exit_on_error();
        if (dispatch_holder1.prf_rdb_out[way]           != prf_rdb_in && prf_valid_b_in)    exit_on_error();
        if (dispatch_holder1.prf_valid_a_out[way]       != prf_valid_a_in)                  exit_on_error();
        if (dispatch_holder1.prf_valid_b_out[way]       != prf_valid_b_in)                  exit_on_error();
    endtask

    task check_prf_entry;
        input   [PRF_BITS-1:0]          idx;
        input   [QUADWORD-1:0]          entry;
        input                           valid;

        if (dispatch_holder1.prf1.valid[idx]    != valid)                           exit_on_error();
        if (dispatch_holder1.prf1.prfs[idx]     != entry && valid)                  exit_on_error();
    endtask


    initial
    begin
        $monitor("\n\n===============WAY_0 INPUTS===============\n",
                 "===WAY_0 RAT===\nTime= %d  prf_rda_idx= %d prf_rdb_idx= %d prf_dest_idx= %d prf_dest_valid= %b arf_idx= %d rrat_overwritten= %d  rrat_overwritten_valid = %b\n", $time, prf_rda_idx[0], prf_rdb_idx[0], prf_dest_idx[0], prf_dest_valid[0], arf_idx_out[0], rrat_overwritten[0], rrat_overwritten_valid[0],
                 "===WAY_0 ID 1===\nTime= %d opa_sel_out= %d opb_sel_out= %d alu_func_out= %d rd_mem_out= %b wr_mem_out= %b\n", $time, opa_select_out[0], opb_select_out[0], alu_func_out[0], rd_mem_out[0], wr_mem_out[0],
                 "===WAY_0 ID 2===\nTime= %d ldl_mem_out= %b stc_mem_out= %b cond_branch_out= %b uncond_branch_out= %b\n", $time, ldl_mem_out[0], stc_mem_out[0], cond_branch_out[0], uncond_branch_out[0],
                 "===WAY_0 ID 3===\nTime= %d branch_call= %b branch_ret= %b halt_out= %b illegal_out= %b valid_inst_out= %b\n", $time, branch_call[0], branch_ret[0], halt_out[0], illegal_out[0], valid_inst_out[0],
                 "===WAY_0 CDB===\nTime= %d cdb_valid= %b cdb_prf_tag= %d cdb_value_in= %d cdb_branch_taken= %b\n", $time, cdb_valid[0], cdb_prf_tag[0], cdb_value_in[0], cdb_branch_taken[0],
                 "===WAY_0 Pipe===\nTime= %d id_disp_PC= %h id_disp_NPC= %h id_disp_IR= %h thread_id= %b\n", $time, id_disp_PC[0], id_disp_NPC[0], id_disp_IR[0], thread_id[0],
                 "=============WAY_0 OUTPUTS=============\n",
                 "===WAY_0 RS 1===\nTime= %d rs_output_valid= %b rs_opa_out= %d rs_opb_out= %d rs_dest_out= %d rs_stall= %b rs_alu_func_out= %d\n", $time, rs_output_valid[0], rs_opa_out[0], rs_opb_out[0], rs_dest_out[0], rs_stall[0], rs_alu_func_out[0],
                 "===WAY_0 RS 2===\nTime= %d rs_opa_select_out= %d rs_opb_select_out= %d rs_cond_branch_out= %b rs_uncond_branch_out= %b rs_halt_out= %b rs_illegal_out= %b\n", $time, rs_opa_select_out[0], rs_opb_select_out[0], rs_cond_branch_out[0], rs_uncond_branch_out[0], rs_halt_out[0], rs_illegal_out[0],
                 "===WAY_0 RS 3===\nTime= %d rs_mem_op= %b rs_NPC= %h rs_IR= %h rs_thread_id= %b\n", $time, rs_mem_op[0], rs_NPC[0], rs_IR[0], rs_thread_id[0],
                 "===WAY_0 ROB 1===\nTime= %d rob_stall= %b update_valid= %b update_arch_reg_out= %d update_prf_reg_out= %d nuke=%d\n", $time, rob_stall[0], update_valid[0], update_arch_reg_out[0], update_prf_reg_out[0], nuke[0],
                 "===WAY_0 ROB 2===\nTime= %d update_branch_actual_out= %b update_pc_out= %d update_next_pc_out= %d update_branch_conditional_out= %b update_branch_unconditional_out= %b\n", $time, update_branch_actual_out[0], update_pc_out[0], update_next_pc_out[0], update_branch_conditional_out[0], update_branch_unconditional_out[0],
                 "===WAY_0 ROB 3===\nTime= %d update_branch_call_out= %b update_branch_ret_out= %b update_load_out= %b update_store_out= %b\n", $time, update_branch_call_out[0], update_branch_ret_out[0], update_load_out[0], update_store_out[0],
                 "===============WAY_1 INPUTS===============\n",
                 "===WAY_1 RAT===\nTime= %d  prf_rda_idx= %d prf_rdb_idx= %d prf_dest_idx= %d prf_dest_valid= %b arf_idx= %d rrat_overwritten= %d  rrat_overwritten_valid = %b\n", $time, prf_rda_idx[1], prf_rdb_idx[1], prf_dest_idx[1], prf_dest_valid[1], arf_idx_out[1], rrat_overwritten[1], rrat_overwritten_valid[1],
                 "===WAY_1 ID 1===\nTime= %d opa_sel_out= %d opb_sel_out= %d alu_func_out= %d rd_mem_out= %b wr_mem_out= %b\n", $time, opa_select_out[1], opb_select_out[1], alu_func_out[1], rd_mem_out[1], wr_mem_out[1],
                 "===WAY_1 ID 2===\nTime= %d ldl_mem_out= %b stc_mem_out= %b cond_branch_out= %b uncond_branch_out= %b\n", $time, ldl_mem_out[1], stc_mem_out[1], cond_branch_out[1], uncond_branch_out[1],
                 "===WAY_1 ID 3===\nTime= %d branch_call= %b branch_ret= %b halt_out= %b illegal_out= %b valid_inst_out= %b\n", $time, branch_call[1], branch_ret[1], halt_out[1], illegal_out[1], valid_inst_out[1],
                 "===WAY_1 CDB===\nTime= %d cdb_valid= %b cdb_prf_tag= %d cdb_value_in= %d cdb_branch_taken= %b\n", $time, cdb_valid[1], cdb_prf_tag[1], cdb_value_in[1], cdb_branch_taken[1],
                 "===WAY_1 Pipe===\nTime= %d id_disp_PC= %h id_disp_NPC= %h id_disp_IR= %h thread_id= %b\n", $time, id_disp_PC[1], id_disp_NPC[1], id_disp_IR[1], thread_id[1],
                 "=============WAY_1 OUTPUTS=============\n",
                 "===WAY_1 RS 1===\nTime= %d rs_output_valid= %b rs_opa_out= %d rs_opb_out= %d rs_dest_out= %d rs_stall= %b rs_alu_func_out= %d\n", $time, rs_output_valid[1], rs_opa_out[1], rs_opb_out[1], rs_dest_out[1], rs_stall[1], rs_alu_func_out[1],
                 "===WAY_1 RS 2===\nTime= %d rs_opa_select_out= %d rs_opb_select_out= %d rs_cond_branch_out= %b rs_uncond_branch_out= %b rs_halt_out= %b rs_illegal_out= %b\n", $time, rs_opa_select_out[1], rs_opb_select_out[1], rs_cond_branch_out[1], rs_uncond_branch_out[1], rs_halt_out[1], rs_illegal_out[1],
                 "===WAY_1 RS 3===\nTime= %d rs_mem_op= %b rs_NPC= %h rs_IR= %h rs_thread_id= %b\n", $time, rs_mem_op[1], rs_NPC[1], rs_IR[1], rs_thread_id[1],
                 "===WAY_1 ROB 1===\nTime= %d rob_stall= %b update_valid= %b update_arch_reg_out= %d update_prf_reg_out= %d nuke=%d\n", $time, rob_stall[1], update_valid[1], update_arch_reg_out[1], update_prf_reg_out[1], nuke[1],
                 "===WAY_1 ROB 2===\nTime= %d update_branch_actual_out= %b update_pc_out= %d update_next_pc_out= %d update_branch_conditional_out= %b update_branch_unconditional_out= %b\n", $time, update_branch_actual_out[1], update_pc_out[1], update_next_pc_out[1], update_branch_conditional_out[1], update_branch_unconditional_out[1],
                 "===WAY_1 ROB 3===\nTime= %d update_branch_call_out= %b update_branch_ret_out= %b update_load_out= %b update_store_out= %b\n", $time, update_branch_call_out[1], update_branch_ret_out[1], update_load_out[1], update_store_out[1]);



        //Initialize
        clock                   = 0;
        reset                   = 1;
        prf_rda_idx             = 0;
        prf_rdb_idx             = 0;
        prf_dest_idx            = 0;
        prf_dest_valid          = 0;
        rrat_overwritten        = 0;
        rrat_overwritten_valid  = 0;
        rrat_free_list          = 0;
        opa_select_out          = 0;
        opb_select_out          = 0;
        arf_idx_out             = 0;
        alu_func_out            = 0;
        rd_mem_out              = 0;
        wr_mem_out              = 0;
        ldl_mem_out             = 0;
        stc_mem_out             = 0;
        cond_branch_out         = 0;
        uncond_branch_out       = 0;
        branch_call             = 0;
        branch_ret              = 0;
        halt_out                = 0;
        illegal_out             = 0;
        valid_inst_out          = 0;
        cdb_valid               = 0;
        cdb_prf_tag             = 0;
        cdb_value_in            = 0;
        cdb_branch_taken        = 0;
        id_disp_PC              = 0;
        id_disp_NPC             = 0;
        id_disp_IR              = 0;
        thread_id               = 0;


        //CHECK reset
        @(negedge clock);
        reset = 0;
        //PRFs are invalid
        for (int i=0; i< NUM_PRF_ENTRIES; ++i) begin
            if (i != 31)    //31 is always valid
              check_prf_entry(i, i, `FALSE);
        end
        //head == tail
        check_rob_head(6'd0);
        check_rob_tail(6'd0);
        //All RSs are free
        for (int i=0; i< NUM_RS ; ++i) begin
            check_rs_entry_free_status(i, `TRUE);
        end
        $display("\n@@@Passed reset\n");
        //exit_on_error();

        //LOAD VALUES TO USE FOR LATTER INSTRUCTIONS!!!
        //Dispatch invalid instr, RoB and RS don't take it
        @(negedge clock);
        //Decoder
        from_decoder1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `R1, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_1, `FALSE, `FALSE, `FALSE);   //invalid is false
        from_decoder1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `R0, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_0, `FALSE, `FALSE, `FALSE);   //invalid is false
        //CDB
        invalid_cdb(`WAY_1);
        invalid_cdb(`WAY_0);
        //Rat and RRat
        invalid_prf_dest(`WAY_1);
        invalid_prf_dest(`WAY_0);
        invalid_rrat_overwritten(`WAY_1);
        invalid_rrat_overwritten(`WAY_0);
        @(posedge clock);
        //Check outputs
        if (rs_output_valid != 2'b00) exit_on_error;
        @(negedge clock);
        //PRFs are invalid
        for (int i=0; i< NUM_PRF_ENTRIES; ++i) begin
            if (i != 31)    //31 is always valid
                check_prf_entry(i, i, `FALSE);
        end
        //head == tail
        check_rob_head(6'd0);
        check_rob_tail(6'd0);
        //All RSs are free
        for (int i=0; i< NUM_RS ; ++i) begin
            check_rs_entry_free_status(i, `TRUE);
        end
        $display("\n@@@Passed dispatch invalid instr\n");
        //exit_on_error();

        //Dispatch 2 instrs to load immediate some values
        // lda 0 into r0
        // lda 1 into r1
        @(negedge clock);
        //Decoder
        from_decoder1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `R1, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_1, `FALSE, `FALSE, `TRUE);   //valid is true
        from_decoder1(`WAY_0, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `R0, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_0, `FALSE, `FALSE, `TRUE);   //valid is true
        //CDB
        invalid_cdb(`WAY_1);
        invalid_cdb(`WAY_0);
        //Rat
        from_rat(`WAY_1, 7'd31, 7'd31, 7'd95, `TRUE);
        from_rat(`WAY_0, 7'd31, 7'd31, 7'd94, `TRUE);
        //RRat
        from_rrat(`WAY_1, 7'd0, `FALSE);
        from_rrat(`WAY_0, 7'd0, `FALSE);
        //From pipe reg
        id_disp_pipe(`WAY_1, 64'd00, 64'd04, 32'h00000000, 1'b0);   //don't care what the instr is
        id_disp_pipe(`WAY_0, 64'd04, 64'd08, 32'h00000000, 1'b0);   //don't care what the instr is
        @(posedge clock);
        //exit_on_error();
        //Check outputs
        //RS
        if (rs_output_valid != 2'b00) exit_on_error; //not ready until latched in RS
        //RoB
        check_rob_invalid_out(`WAY_1);
        check_rob_invalid_out(`WAY_0);
        //PRF
        check_prf(`WAY_1, 64'd00, 64'd00, `TRUE, `TRUE);    //R31 is valid and 0
        check_prf(`WAY_0, 64'd00, 64'd00, `TRUE, `TRUE);    //R31 is valid and 0
        @(negedge clock);
        //exit_on_error();
        //Check outputs from last time---------------------
        //RS, actually issues
        check_rs1(`WAY_1, `TRUE, 64'd00, 64'd00, 7'd95, `ALU_ADDQ);
        check_rs2(`WAY_1, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_1, `TRUE, `FALSE, 64'd04, 64'h00000000, 1'b0, `FALSE);
        check_rs1(`WAY_0, `TRUE, 64'd00, 64'd00, 7'd94, `ALU_ADDQ);
        check_rs2(`WAY_0, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_0, `TRUE, `FALSE, 64'd08, 64'h00000000, 1'b0, `FALSE);
        //RoB
        check_rob_head(6'd0);
        check_rob_tail(6'd2);
        $display("\n@@@Passed issue 2 load immediate from RS\n");
        //New Inputs for nex time--------------------------
        //Decoder
        from_decoder1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `R2, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_1, `FALSE, `FALSE, `TRUE);   //valid is true
        from_decoder1(`WAY_0, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `R1, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_0, `FALSE, `FALSE, `TRUE);   //valid is true
        //CDB
        from_cdb(`WAY_1, `TRUE, 7'd95, 64'd0, `FALSE, 64'd04);
        from_cdb(`WAY_0, `TRUE, 7'd94, 64'd1, `FALSE, 64'd08);
        //Rat
        from_rat(`WAY_1, 7'd31, 7'd31, 7'd93, `TRUE);
        from_rat(`WAY_0, 7'd31, 7'd31, 7'd92, `TRUE);
        //RRat
        from_rrat(`WAY_1, 7'd0, `FALSE);
        from_rrat(`WAY_0, 7'd0, `FALSE);
        //From pipe reg
        id_disp_pipe(`WAY_1, 64'h08, 64'h0c, 32'h00000000, 1'b0);   //don't care what the instr is
        id_disp_pipe(`WAY_0, 64'h0c, 64'h10, 32'h00000000, 1'b0);   //don't care what the instr is
        @(posedge clock)
        check_rob1(`WAY_1, `FALSE, `TRUE, `R1, 7'd95, `FALSE) ;
        check_rob2(`WAY_1, `TRUE, `FALSE, 64'h00, 64'h04, `FALSE, `FALSE);
        check_rob3(`WAY_1, `TRUE, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rob1(`WAY_0, `FALSE, `TRUE, `R0, 7'd94, `FALSE);
        check_rob2(`WAY_0, `TRUE, `FALSE, 64'h04, 64'h08, `FALSE, `FALSE);
        check_rob3(`WAY_0, `TRUE, `FALSE, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        //Check outputs from last time---------------------
        //RS, actually issues
        check_rs1(`WAY_1, `TRUE, 64'd00, 64'd00, 7'd93, `ALU_ADDQ);
        check_rs2(`WAY_1, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_1, `TRUE, `FALSE, 64'h0c, 64'h00000000, 1'b0, `FALSE);
        check_rs1(`WAY_0, `TRUE, 64'd00, 64'd00, 7'd92, `ALU_ADDQ);
        check_rs2(`WAY_0, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_0, `TRUE, `FALSE, 64'h10, 64'h00000000, 1'b0, `FALSE);
        //RoB
        check_rob_head(6'd2);
        check_rob_tail(6'd4);
        //PRF --finially some valid values in the PRF
        check_prf_entry(7'd95, 64'd00, `TRUE);
        check_prf_entry(7'd94, 64'd01, `TRUE);
        $display("\n@@@Passed commited 2 load immediate, and issued 2 load immediate from RS\n");
        //New Inputs for next time--------------------------
        //Decoder
        from_decoder1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `R4, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_1, `FALSE, `FALSE, `TRUE);   //valid is true
        from_decoder1(`WAY_0, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `R3, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_0, `FALSE, `FALSE, `TRUE);   //valid is true
        //CDB
        from_cdb(`WAY_1, `TRUE, 7'd93, 64'd2, `FALSE, 64'h0c);
        from_cdb(`WAY_0, `TRUE, 7'd92, 64'd3, `FALSE, 64'h10);
        //Rat
        from_rat(`WAY_1, 7'd31, 7'd31, 7'd91, `TRUE);
        from_rat(`WAY_0, 7'd31, 7'd31, 7'd90, `TRUE);
        //RRat --finially RRat has gone though pipe and said free
        from_rrat(`WAY_1, 7'd0, `TRUE);
        from_rrat(`WAY_0, 7'd1, `TRUE);
        //From pipe reg
        id_disp_pipe(`WAY_1, 64'h10, 64'h14, 32'h00000000, 1'b0);   //don't care what the instr is
        id_disp_pipe(`WAY_0, 64'h14, 64'h18, 32'h00000000, 1'b0);   //don't care what the instr is
        @(posedge clock)
        check_rob1(`WAY_1, `FALSE, `TRUE, `R2, 7'd93, `FALSE) ;
        check_rob2(`WAY_1, `TRUE, `FALSE, 64'h08, 64'h0c, `FALSE, `FALSE);
        check_rob3(`WAY_1, `TRUE, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rob1(`WAY_0, `FALSE, `TRUE, `R1, 7'd92, `FALSE);
        check_rob2(`WAY_0, `TRUE, `FALSE, 64'h0c, 64'h10, `FALSE, `FALSE);
        check_rob3(`WAY_0, `TRUE, `FALSE, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        //Check outputs from last time---------------------
        //RS, actually issues
        check_rs1(`WAY_1, `TRUE, 64'd00, 64'd00, 7'd91, `ALU_ADDQ);
        check_rs2(`WAY_1, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_1, `TRUE, `FALSE, 64'h14, 64'h00000000, 1'b0, `FALSE);
        check_rs1(`WAY_0, `TRUE, 64'd00, 64'd00, 7'd90, `ALU_ADDQ);
        check_rs2(`WAY_0, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_0, `TRUE, `FALSE, 64'h18, 64'h00000000, 1'b0, `FALSE);
        //RoB
        check_rob_head(6'd4);
        check_rob_tail(6'd6);
        //PRF --finially some valid values in the PRF
        //New vaild
        check_prf_entry(7'd95, 64'd00, `TRUE);
        check_prf_entry(7'd94, 64'd01, `TRUE);
        check_prf_entry(7'd93, 64'd02, `TRUE);
        check_prf_entry(7'd92, 64'd03, `TRUE);
        //New invalid
        check_prf_entry(7'd00, 64'd00, `FALSE); //These are newly freed from RRAT, but
        check_prf_entry(7'd01, 64'd00, `FALSE); //they are not that exciting because invalid already
        $display("\n@@@Passed commited 2 load immediate, and issued 2 load immediate from RS again\n");
        //exit_on_error();
        //Run a whole bunch of loads
        for (int i = 1, j = 1; i < 23; i+=2, j++) begin
            //New Inputs for next time--------------------------
            //Decoder
            from_decoder1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, 4 + i + 1, `ALU_ADDQ, `FALSE, `FALSE);
            from_decoder2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
            from_decoder3(`WAY_1, `FALSE, `FALSE, `TRUE);   //valid is true
            from_decoder1(`WAY_0, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, 4 + i, `ALU_ADDQ, `FALSE, `FALSE);
            from_decoder2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
            from_decoder3(`WAY_0, `FALSE, `FALSE, `TRUE);   //valid is true
            //CDB
            from_cdb(`WAY_1, `TRUE, 7'd92 - i, 3 + i, `FALSE, 64'h0c + j*8);
            from_cdb(`WAY_0, `TRUE, 7'd92 - i - 1, 3 + i + 1, `FALSE, 64'h10 + j*8);
            //Rat
            from_rat(`WAY_1, 7'd31, 7'd31, 7'd90 - i, `TRUE);
            from_rat(`WAY_0, 7'd31, 7'd31, 7'd90 - i - 1, `TRUE);
            //RRat --finially RRat has gone though pipe and said free
            from_rrat(`WAY_1, 7'd1 + i, `TRUE);
            from_rrat(`WAY_0, 7'd1 + i + 1, `TRUE);
            //From pipe reg
            id_disp_pipe(`WAY_1, 64'h10 + j*8, 64'h14 + j*8, 32'h00000000, 1'b0);   //don't care what the instr is
            id_disp_pipe(`WAY_0, 64'h14 + j*8, 64'h18 + j*8, 32'h00000000, 1'b0);   //don't care what the instr is
            @(negedge clock);
            //Check outputs from last time---------------------
            //RS, actually issues
            //$display("\n\nTEST, NPC[1]: %h", rs_NPC[1]);
            //$display("TEST, NPC[0]: %h", rs_NPC[0]);
            //$display("TEST, i: %d", i);
            check_rs1(`WAY_1, `TRUE, 64'd00, 64'd00, 7'd90 - i, `ALU_ADDQ);
            check_rs2(`WAY_1, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
            check_rs3(`WAY_1, `TRUE, `FALSE, 64'h14 + j*8, 64'h00000000, 1'b0, `FALSE);
            check_rs1(`WAY_0, `TRUE, 64'd00, 64'd00, 7'd90 - i - 1, `ALU_ADDQ);
            check_rs2(`WAY_0, `TRUE, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
            check_rs3(`WAY_0, `TRUE, `FALSE, 64'h18 + j*8, 64'h00000000, 1'b0, `FALSE);
            //RoB
            check_rob_head(6'd4 + j*2);
            check_rob_tail(6'd6 + j*2);
            //PRF --finially some valid values in the PRF
            //New vaild
            check_prf_entry(7'd92 - i, 64'd03 + i, `TRUE);
            check_prf_entry(7'd92 - i - 1, 64'd03 + i + 1, `TRUE);
            //New invalid
            check_prf_entry(7'd01 + i, 64'd00, `FALSE); //These are newly freed from RRAT, but
            check_prf_entry(7'd01 + i + 1, 64'd00, `FALSE); //they are not that exciting because invalid already
        end
        $display("\n@@@Passed a bunch of lda's\n");
        //exit_on_error();
        //Commit whats left of get rid of it
        //CDB
        from_cdb(`WAY_1, `TRUE, 7'd69, 64'd27, `FALSE, 64'h6c);
        from_cdb(`WAY_0, `TRUE, 7'd68, 64'd26, `FALSE, 64'h70);
        //pipe reg
        invalid_inst(`WAY_1);
        invalid_inst(`WAY_0);
        //Rat
        invalid_prf_dest(`WAY_1);
        invalid_prf_dest(`WAY_0);
        //RRat
        invalid_rrat_overwritten(`WAY_1);
        invalid_rrat_overwritten(`WAY_0);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        //exit_on_error();

        //Now real testing--------------------------------------------
        //Dispatch invalid instr, RoB and RS don't take it
        //Decoder
        from_decoder1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `R1, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_1, `FALSE, `FALSE, `TRUE);   //valid is true
        from_decoder1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `R0, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_0, `FALSE, `FALSE, `TRUE);   //valid is true
        //CDB
        invalid_cdb(`WAY_1);
        invalid_cdb(`WAY_0);
        //Rat
        from_rat(`WAY_1, 7'd94, 7'd94, 7'd67, `TRUE);
        from_rat(`WAY_0, 7'd94, 7'd93, 7'd66, `TRUE);
        //RRat
        from_rrat(`WAY_1, 7'd0, `FALSE);
        from_rrat(`WAY_0, 7'd0, `FALSE);
        //From pipe reg
        id_disp_pipe(`WAY_1, 64'd00, 64'd04, 32'h40210403, 1'b0);
        id_disp_pipe(`WAY_0, 64'd04, 64'd08, 32'h40220403, 1'b0);
        //Check outputs
        @(negedge clock);
        //RS
        check_rs1(`WAY_1, `TRUE, 64'd01, 64'd01, 7'd67, `ALU_ADDQ);
        check_rs2(`WAY_1, `TRUE, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_1, `TRUE, `FALSE, 64'h04, 32'h40210403, 1'b0, `FALSE);
        check_rs1(`WAY_0, `TRUE, 64'd01, 64'd02, 7'd66, `ALU_ADDQ);
        check_rs2(`WAY_0, `TRUE, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        check_rs3(`WAY_0, `TRUE, `FALSE, 64'h08, 32'h40220403, 1'b0, `FALSE);
        //RoB
        check_rob_head(6'd28);
        check_rob_tail(6'd30);
        //PRF
        check_prf(`WAY_1, 64'd01, 64'd01, `TRUE, `TRUE);
        check_prf(`WAY_0, 64'd01, 64'd02, `TRUE, `TRUE);
        $display("\n@@@Passed ADDQ of two values\n");
        exit_on_error();




       /*
        //exit_on_error();
        //PRF
        //check_prf_entry(7'd1, 64'd00, `TRUE);    //IDK what the actual value is
        //check_prf_entry(7'd2, 64'd00, `TRUE);    //IDK what the actual value is
        //RoB
        //check_rob_head(6'd2);
        //check_rob_tail(6'd4);
        //RS
        //check_rs1(`WAY_1, `TRUE, 64'd01, 64'd01, 7'd95, `ALU_ADDQ);
        //check_rs2(`WAY_1, `TRUE, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        //check_rs3(`WAY_1, `TRUE, `FALSE, 64'd04, 64'h40210403, 1'b0, `FALSE);
        //check_rs1(`WAY_0, `TRUE, 64'd01, 64'd02, 7'd94, `ALU_ADDQ);
        //check_rs2(`WAY_0, `TRUE, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        //check_rs3(`WAY_0, `TRUE, `FALSE, 64'd08, 64'h40220403, 1'b0, `FALSE);
        //check_rs_entry_free_status(4'd00, `FALSE);
        //check_rs_entry_free_status(4'd01, `FALSE);
        for (int i=2; i< NUM_RS ; ++i) begin
            //check_rs_entry_free_status(i, `FALSE);
        end
        $display("\n@@@Passed dispatch 2 instrs to load immediate some values\n");
        //exit_on_error();




*/




































/* I need to load values first, before I can start to use them for a different instrustions!!!!!!!!!!!!!!!!

        //Dispatch invalid instr, RoB and RS don't take it
        @(negedge clock);
        //Decoder
        from_decoder1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `R1, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_1, `FALSE, `FALSE, `TRUE);   //valid is true
        from_decoder1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `R0, `ALU_ADDQ, `FALSE, `FALSE);
        from_decoder2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE);
        from_decoder3(`WAY_0, `FALSE, `FALSE, `TRUE);   //valid is true
        //CDB
        invalid_cdb(`WAY_1);
        invalid_cdb(`WAY_0);
        //Rat
        from_rat(`WAY_1, 7'd1, 7'd1, 7'd95, `TRUE);
        from_rat(`WAY_0, 7'd1, 7'd2, 7'd94, `TRUE);
        //RRat
        from_rrat(`WAY_1, 7'd0, `FALSE);
        from_rrat(`WAY_0, 7'd0, `FALSE);
        //From pipe reg
        id_disp_pipe(`WAY_1, 64'd00, 64'd04, 32'h40210403, 1'b0);
        id_disp_pipe(`WAY_1, 64'd04, 64'd08, 32'h40220403, 1'b0);
        @(posedge clock);
        //Check outputs
        //RS
        if (rs_output_valid != 2'b00) exit_on_error; //not ready until latched in RS
        //RoB
        check_rob_invalid_out(`WAY_1);
        check_rob_invalid_out(`WAY_0);
        //PRF
        check_prf(`WAY_1, 64'd00, 64'd00, `FALSE, `FALSE);    //Nothing written so not valid yet
        check_prf(`WAY_0, 64'd00, 64'd00, `FALSE, `FALSE);    //Nothing written so not valid yet
        @(negedge clock);
        //PRF
        //check_prf_entry(7'd1, 64'd00, `TRUE);    //IDK what the actual value is
        //check_prf_entry(7'd2, 64'd00, `TRUE);    //IDK what the actual value is
        //RoB
        //check_rob_head(6'd0);
        //check_rob_tail(6'd2);
        //RS
        //check_rs1(`WAY_1, `TRUE, 64'd01, 64'd01, 7'd95, `ALU_ADDQ);
        //check_rs2(`WAY_1, `TRUE, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        //check_rs3(`WAY_1, `TRUE, `FALSE, 64'd04, 64'h40210403, 1'b0, `FALSE);
        //check_rs1(`WAY_0, `TRUE, 64'd01, 64'd02, 7'd94, `ALU_ADDQ);
        //check_rs2(`WAY_0, `TRUE, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `FALSE, `FALSE, `FALSE, `FALSE);
        //check_rs3(`WAY_0, `TRUE, `FALSE, 64'd08, 64'h40220403, 1'b0, `FALSE);
        //check_rs_entry_free_status(4'd00, `FALSE);
        //check_rs_entry_free_status(4'd01, `FALSE);
        for (int i=2; i< NUM_RS ; ++i) begin
            //check_rs_entry_free_status(i, `FALSE);
        end
        $display("\n@@@Passed dispatch two valid independent instrs\n");
        //exit_on_error();


*/






































/*

        //id combinational, don't check its outputs on reset!
        check_rat(`WAY_1, 7'd0, 7'd0, 7'd0, `FALSE, `FALSE);
        check_rat(`WAY_0, 7'd0, 7'd0, 7'd0, `FALSE, `FALSE);
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        $display("\n@@@Passed reset\n");

        //Dispatch  WAY_1: ADDQ $r1, $r1, $r3
        //          WAY_0: ADDQ $r1, $r2, $r3
        @(negedge clock);
        if_id_pipe(`WAY_1, 32'h40210403, `TRUE);
        if_id_pipe(`WAY_0, 32'h40220403, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd1, 7'd1, 7'd95, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd1, 7'd2, 7'd94, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R3, 7'd94);         //got renamed twice! 3->95->94
        $display("\n@@@Passed dispatch double rewrite R3\n");

        //Dispatch  WAY_1: ADDQ $r3, $r3, $r3
        //          WAY_0: SUBQ $r3, $r1, $r2
        if_id_pipe(`WAY_1, 32'h40630403, `TRUE);
        if_id_pipe(`WAY_0, 32'h40610522, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_SUBQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd94, 7'd94, 7'd93, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd93, 7'd01, 7'd92, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R3, 7'd93);
        check_rat_entry(`R2, 7'd92);
        $display("\n@@@Passed dispatch double rename R3 and then use it right away\n");

        //Commit    WAY_1: ADDQ $r1, $r1, $r3
        rob(`WAY_1, `TRUE, 7'd95, `R3, `FALSE);
        inv_inst(`WAY_1); //Don't dispatch and commit same cycle for now...
        inv_inst(`WAY_0);
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd3, `TRUE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        $display("\n@@@Passed commit only WAY_1\n");

        //Commit    WAY_1: ADDQ $r1, $r2, $r3
        rob(`WAY_1, `TRUE, 7'd94, `R3, `FALSE);
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd95, `TRUE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        $display("\n@@@Passed commit only WAY_1 again\n");

        //Commit    WAY_1: ADDQ $r03, $r03, $r03
        //          WAY_0: SUBQ $r03, $r01, $r02
        //Dispatch  WAY_1: ADDQ $r10, $r03, $r11
        //          WAY_0: ADDQ $r02, $r11, $r3
        rob(`WAY_1, `TRUE, 7'd93, `R3, `FALSE);
        rob(`WAY_0, `TRUE, 7'd92, `R2, `FALSE);
        if_id_pipe(`WAY_1, 32'h4143040b, `TRUE);
        if_id_pipe(`WAY_0, 32'h404b0403, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd10, 7'd93, 7'd02, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd92, 7'd02, 7'd94, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd94, `TRUE);
        check_rrat(`WAY_0, 7'd02, `TRUE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R3, 7'd94);
        check_rat_entry(`R11, 7'd2);
        //Disable Inputs (Comment out if you don't want to)
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed commit double and dispatch double\n");

        //NUKE
        nuke = 2'b10;        //NOTE: Instructions are invalid on nuke!
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        nuke = 2'b00;
        check_rat_entry(`R2, 7'd92);
        check_rat_entry(`R3, 7'd93);
        check_rat_entry(`R11, 7'd11);
        $display("\n@@@Passed NUKE, but no updating RRAT\n");
        //exit_on_error();

        //Dispatch some more instructions to get ready for a NUKE and commit
        //at same time
        //Dispatch  WAY_1: ADDQ $r05, $r06, $r07
        //          WAY_0: ADDQ $r03, $r03, $r02
        if_id_pipe(`WAY_1, 32'h40a60407, `TRUE);
        if_id_pipe(`WAY_0, 32'h40630402, `TRUE);
        @(posedge clock);
        //check id outputs
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs
        check_rat(`WAY_1, 7'd05, 7'd06, 7'd03, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd93, 7'd93, 7'd95, `TRUE, `FALSE);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated
        check_rat_entry(`R2, 7'd95);
        check_rat_entry(`R7, 7'd03);
        //Disable Inputs (Comment out if you don't want to)
        //inv_inst(`WAY_1);
        //inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed double dispatch again\n");

        //Dispatch  WAY_1: lda $r20, data (data = 0x1000)
        //          WAY_0: ldq $r21, 0($r20)
        if_id_pipe(`WAY_1, 32'h229f1000, `TRUE);
        if_id_pipe(`WAY_0, 32'ha6b40000, `TRUE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `ALU_ADDQ, `TRUE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd20, 7'd31, 7'd94, `TRUE, `FALSE);
        check_rat(`WAY_0, 7'd21, 7'd94, 7'd02, `TRUE, `FALSE);
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R20, 7'd94);
        check_rat_entry(`R21, 7'd02);
        //Disable Inputs (Comment out if you don't want to)
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch load immediate, and regular load\n");

        //Dispatch  WAY_1: stq $r21, 0($r20)
        //          WAY_0: nop
        if_id_pipe(`WAY_1, 32'hb6b40000, `TRUE);
        if_id_pipe(`WAY_0, 32'h47ff041f, `TRUE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_MEM_DISP, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `TRUE);
        check_id_1(`WAY_0, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_BIS, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd02, 7'd94, 7'd91, `TRUE, `FALSE); //popped out but not renamed
        check_rat(`WAY_0, 7'd31, 7'd31, 7'd90, `TRUE, `FALSE); //popped out but not renamed
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R31, 7'd31);       //not renamed
        //Disable Inputs (Comment out if you don't want to)
        //inv_inst(`WAY_1);
        //inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch stq, and nop\n");

        //Dispatch  WAY_1: call_pal (HALT)
        //          WAY_0: INVALID
        if_id_pipe(`WAY_1, 32'h00000555, `TRUE);
        if_id_pipe(`WAY_0, 32'h47ff041f, `FALSE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_REGA, `ALU_OPB_IS_REGB, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `TRUE, `FALSE, `FALSE, `TRUE);
        check_invalid_inst(`WAY_0);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd00, 7'd00, 7'd89, `TRUE, `FALSE);  //popped out but not renamed
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R31, 7'd31);         //not renamed
        //Disable Inputs (Comment out if you don't want to)
        //inv_inst(`WAY_1);
        //inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch call_pal (HALT) and invalid instr\n");

        //Dispatch  WAY_1: beq $r20 loop
        //          WAY_0: br  loop
        if_id_pipe(`WAY_1, 32'he69ffffe, `TRUE);
        if_id_pipe(`WAY_0, 32'hc3fffffd, `TRUE);
        @(posedge clock);
        //check id outputs---------------
        check_id_1(`WAY_1, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_1(`WAY_0, `ALU_OPA_IS_NPC, `ALU_OPB_IS_BR_DISP, `ALU_ADDQ, `FALSE, `FALSE);
        check_id_2(`WAY_1, `FALSE, `FALSE, `TRUE, `FALSE);
        check_id_2(`WAY_0, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_1, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        check_id_3(`WAY_0, `FALSE, `FALSE, `FALSE, `FALSE, `FALSE, `TRUE);
        //check rat outputs--------------
        check_rat(`WAY_1, 7'd94, 7'd31, 7'd88, `TRUE, `FALSE); //popped out but not renamed
        check_rat(`WAY_0, 7'd31, 7'd31, 7'd87, `TRUE, `FALSE); //popped out but not renamed
        //check rrat outputs-------------
        check_rrat(`WAY_1, 7'd0, `FALSE);
        check_rrat(`WAY_0, 7'd0, `FALSE);
        @(negedge clock);
        //rat should now be updated------
        check_rat_entry(`R31, 7'd31);       //not renamed
        //Disable Inputs (Comment out if you don't want to)
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        $display("\n@@@Passed dispatch beq, and br\n");

        //NUKE and Commit
        rob(`WAY_1, `TRUE, 7'd94, 7'd20, `TRUE);
        rob(`WAY_0, `TRUE, 7'd02, 7'd21, `FALSE);
        @(posedge clock);
        //check id and rat outputs are invalid
        check_invalid_inst(`WAY_1);
        check_invalid_inst(`WAY_0);
        check_rat_invalid_out(`WAY_1);
        check_rat_invalid_out(`WAY_0);
        //check rrat outputs
        check_rrat(`WAY_1, 7'd20, `TRUE);
        check_rrat(`WAY_0, 7'd21, `FALSE); //Does not get sent
        @(negedge clock);
        //rat should now be updated
        nuke = 2'b00;
        inv_inst(`WAY_1);
        inv_inst(`WAY_0);
        disable_rob(`WAY_1);
        disable_rob(`WAY_0);
        check_rat_entry(`R20, 7'd94);
        check_rat_entry(`R21, 7'd21);
        check_rat_entry(`R2, 7'd92);
        check_rat_entry(`R3, 7'd93);
        check_rat_entry(`R7, 7'd7);
        $display("\n@@@Passed NUKE, and commit same time\n");
        //exit_on_error();
        */
        @(negedge clock);
        @(negedge clock);
        $display("\n@@@Passed ALLLLLL\n");
        $finish;
    end
endmodule

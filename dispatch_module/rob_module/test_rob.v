//For: Testing rob.v
//Date 2/24/18

//TODO:
`timescale 1ns/100ps
`define NOT_CHECK       2'b10
`define NOT_CHECK_ARF   6'b100000
`define TRUE_2          2'b01
`define FALSE_2         2'b00
`define WAY_0           1'b0
`define WAY_1           1'b1
`define R0              5'd0
`define R1              5'd1
`define R2              5'd2
`define R3              5'd3
`define R4              5'd4
`define R5              5'd5
`define R6              5'd6
`define R7              5'd7
`define R8              5'd8
`define R9              5'd9
`define R10             5'd10
`define R11             5'd11
`define R12             5'd12
`define R13             5'd13
`define R14             5'd14
`define R15             5'd15
`define R16             5'd16
`define R17             5'd17
`define R18             5'd18
`define R19             5'd19
`define R20             5'd20
`define R21             5'd21
`define R22             5'd22
`define R23             5'd23
`define R24             5'd24
`define R25             5'd25
`define R26             5'd26
`define R27             5'd27
`define R28             5'd28
`define R29             5'd29
`define R30             5'd30
`define R31             5'd31


parameter NUM_PRF_ENTRIES   = 96;
parameter ROB_SIZE          = 64;
parameter QUADWORD          = 64;
parameter PRF_BITS          = $clog2(NUM_PRF_ENTRIES);
localparam NUM_ARCH_REGS    = 32;
localparam ARCH_BITS        = $clog2(NUM_ARCH_REGS);
localparam ROB_BITS         = $clog2(ROB_SIZE);

module testbench;

    logic                           clock;
    logic                           reset;

    logic   [1:0]                   valid_in;
    logic   [1:0][QUADWORD-1:0]     pc_in;
    logic   [1:0][QUADWORD-1:0]     next_pc_in;         // if branch taken, then is branch addr
    logic   [1:0][ARCH_BITS-1:0]    dest_arch_reg_in;
    logic   [1:0][PRF_BITS-1:0]     dest_prf_reg_in;
    logic   [1:0]                   branch_conditional_in;
    logic   [1:0]                   branch_unconditional_in;
    logic   [1:0]                   branch_call_in;
    logic   [1:0]                   branch_return_in;
    logic   [1:0]                   load_in;
    logic   [1:0]                   store_in;

    logic   [1:0]                   cdb_valid;
    logic   [1:0][PRF_BITS-1:0]     cdb_tag;            // PRF tag
    logic   [1:0]                   cdb_branch_taken;
    logic   [1:0][QUADWORD-1:0]     cdb_actual_next_pc;

    logic   [1:0]                   rob_stall;
    logic   [1:0]                   valid;              // Commit: tell RRAT to update
    logic   [1:0][ARCH_BITS-1:0]    arch_reg_out;       // Committing architected reg
    logic   [1:0][PRF_BITS-1:0]     prf_reg_out;        // Committing PRF reg
    logic   [1:0]                   n_nuke;
    logic   [1:0]                   branch_actual_out;
    logic   [1:0][QUADWORD-1:0]     pc_out;
    logic   [1:0][QUADWORD-1:0]     next_pc_out;
    logic   [1:0]                   branch_conditional_out;
    logic   [1:0]                   branch_unconditional_out;
    logic   [1:0]                   branch_call_out;
    logic   [1:0]                   branch_return_out;
    logic   [1:0]                   load_out;
    logic   [1:0]                   store_out;



    rob #(.ROB_SIZE(ROB_SIZE)) rob1(
        .clock(clock),
        .reset(reset),
        .valid_in(valid_in),
        .pc_in(pc_in),
        .next_pc_in(next_pc_in),
        .dest_arch_reg_in(dest_arch_reg_in),
        .dest_prf_reg_in(dest_prf_reg_in),
        .branch_conditional_in(branch_conditional_in),
        .branch_unconditional_in(branch_unconditional_in),
        .branch_call_in(branch_call_in),
        .branch_return_in(branch_return_in),
        .load_in(load_in),
        .store_in(store_in),
        .cdb_valid(cdb_valid),
        .cdb_tag(cdb_tag),
        .cdb_branch_taken(cdb_branch_taken),
        .cdb_actual_next_pc(cdb_actual_next_pc),
        .rob_stall(rob_stall),
        .valid(valid),
        .arch_reg_out(arch_reg_out),
        .prf_reg_out(prf_reg_out),
        .nuke(n_nuke),
        .branch_actual_out(branch_actual_out),
        .pc_out(pc_out),
        .next_pc_out(next_pc_out),
        .branch_conditional_out(branch_conditional_out),
        .branch_unconditional_out(branch_unconditional_out),
        .branch_call_out(branch_call_out),
        .branch_return_out(branch_return_out),
        .load_out(load_out),
        .store_out(store_out)
    );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
        $display("!!!FATAL ERROR!!!");/*
        //display all the current ROB entries
        $display("===ROB===");
        $display("\n\n\n===WAY_0 INPUTS===\nTime= %10d  valid_in= %b pc_in= %16h next_pc_in= %16h dest_arch_reg_in= %d dest_prf_reg_in= %d\n", $time, valid_in[0], pc_in[0], next_pc_in[0], dest_arch_reg_in[0], dest_prf_reg_in[0],
                 "===WAY_0 OUTPUTS===\nTime= %10d rob_stall= %b valid= %b arch_reg_out= %d prf_reg_out= %d n_nuke= %b branch_actual_out= %b pc_out= %16h next_pc_out= %16h\n", $time, rob_stall[0], valid[0], arch_reg_out[0], prf_reg_out[0], n_nuke[0], branch_actual_out[0], pc_out[0], next_pc_out[0],
                 "===WAY_1 INPUTS===\nTime= %10d  valid_in= %b pc_in= %16h next_pc_in= %16h dest_arch_reg_in= %d dest_prf_reg_in= %d\n", $time, valid_in[1], pc_in[1], next_pc_in[1], dest_arch_reg_in[1], dest_prf_reg_in[1],
                 "===WAY_1 OUTPUTS===\nTime= %10d rob_stall= %b valid= %b arch_reg_out= %d prf_reg_out= %d n_nuke= %b branch_actual_out= %b pc_out= %16h next_pc_out= %16h\n", $time, rob_stall[1], valid[1], arch_reg_out[1], prf_reg_out[1], n_nuke[1], branch_actual_out[1], pc_out[1], next_pc_out[1]);
        $display("\n\n\nRob full_rob: %d ", rob1.full_rob);
        $display("Rob stall: %b ", rob_stall);
        $display("Rob head: %d, Rob tail: %d ", rob1.head, rob1.tail);
        for (int i = 0; i < ROB_SIZE; i++)
        begin
            if (i == rob1.head)
                $display("\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---\\/---  <--What rob is pointing to! HEAD");

            $display ("Rob Entry Number: %d ", i,
                      "\n     ARF Reg:            %10d ", rob1.arch_reg[i],
                      "\n     PRF Reg:            %10d ", rob1.prf_reg[i],
                      "\n     PC:                 %10h ", rob1.pc[i],
                      "\n     Next PC:            %10h ", rob1.next_pc[i],
                      "\n     Branch Actual:      %b ", rob1.branch_actual[i],
                      "\n     Branch Addr Wrong:  %b ", rob1.branch_addr_wrong[i],
                      "\n     Exe Complete:       %b ", rob1.execution_complete[i],
                      "\n     Branch Cond:        %b ", rob1.branch_conditional[i],
                      "\n     Branch Uncond:      %b ", rob1.branch_unconditional[i],
                      "\n     Load:               %b ", rob1.load[i],
                      "\n     Store:              %b ", rob1.store[i],
                      "\n     Branch Call:        %b ", rob1.branch_call[i],
                      "\n     Branch Return:      %b ", rob1.branch_return[i]);

             if (i == (rob1.tail - 1))
                $display("/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---/\\---  <--What rob is pointing to! TAIL");
        end
*/
        $fatal("INCORRECT");
    endtask

    task dispatch;
        input                       way;
        input                       valid;
        input     [QUADWORD-1:0]    pc;
        input     [QUADWORD-1:0]    next_pc;
        input     [ARCH_BITS-1:0]   dest_arch_reg;
        input     [PRF_BITS-1:0]    dest_prf_reg;
        input                       branch_conditional;
        input                       branch_unconditional;

        valid_in[way]                   = valid;
        pc_in[way]                      = pc;
        next_pc_in[way]                 = next_pc;
        dest_arch_reg_in[way]           = dest_arch_reg;
        dest_prf_reg_in[way]            = dest_prf_reg;
        branch_conditional_in[way]      = branch_conditional;
        branch_unconditional_in[way]    = branch_unconditional;
    endtask

    task stop_dispatch;
        input                   way;

        valid_in[way]           = `FALSE;
    endtask

    task cdb;
        input                   way;
        input                   cdb_v;
        input   [PRF_BITS-1:0]  cdb_t;
        input                   cdb_br_tkn;
        input   [QUADWORD-1:0]  cdb_act_n_pc;

        cdb_valid[way]          = cdb_v;
        cdb_tag[way]            = cdb_t;
        cdb_branch_taken[way]   = cdb_br_tkn;
        cdb_actual_next_pc[way] = cdb_act_n_pc;
    endtask

    task stop_cdb;
        input                   way;

        cdb_valid[way]          = `FALSE;
    endtask

    task check_head;
        input   [ROB_BITS-1:0]  entry;

        //if (rob1.head != entry) exit_on_error();
    endtask

    task check_tail;
        input   [ROB_BITS-1:0]  entry;

        //if (rob1.tail != entry) exit_on_error();
    endtask

    task check_head_tail_eq;
        input                   equal;

        //if (rob1.head != rob1.tail && equal == `TRUE ) exit_on_error();
        //if (rob1.head == rob1.tail && equal == `FALSE ) exit_on_error();
    endtask

    task check_entry;
        input                   full_rob;
        input   [ROB_BITS-1:0]  offset; //offset from head, if you want tail, SOL
        input   [ARCH_BITS:0]   arch_reg; //Not minus one to include 1 hot for `NOT_CHECK_ARF
        input   [PRF_BITS-1:0]  prf_reg;
        input   [QUADWORD-1:0]  pc;
        input   [QUADWORD-1:0]  next_pc;
        input                   branch_addr_wrong;
        input   [1:0]           branch_actual;
        input                   execution_complete;
        input                   branch_conditional;
        input                   branch_unconditional;
/*
        if (rob1.full_rob                                   != full_rob)                                                exit_on_error();
        if (rob1.arch_reg[rob1.head + offset]               != arch_reg         && arch_reg         != `NOT_CHECK_ARF)  exit_on_error();
        if (rob1.prf_reg[rob1.head + offset]                != prf_reg)                                                 exit_on_error();
        if (rob1.pc[rob1.head + offset]                     != pc)                                                      exit_on_error();
        if (rob1.next_pc[rob1.head + offset]                != next_pc)                                                 exit_on_error();
        if (rob1.branch_addr_wrong[rob1.head + offset]      != branch_addr_wrong)                                       exit_on_error();
        if (rob1.branch_actual[rob1.head + offset]          != branch_actual    && branch_actual    != `NOT_CHECK)      exit_on_error();
        if (rob1.execution_complete[rob1.head + offset]     != execution_complete)                                      exit_on_error();
        if (rob1.branch_conditional[rob1.head + offset]     != branch_conditional)                                      exit_on_error();
        if (rob1.branch_unconditional[rob1.head + offset]   != branch_unconditional)                                    exit_on_error();*/
    endtask

    task check_outputs; //TODO: Check outputs on posedge or after some delay after CDB valid
        input                   way;
        input                   valid_in;
        input   [ARCH_BITS:0]   arch_reg; //Not minus one to include 1 hot for `NOT_CHECK_ARF
        input   [PRF_BITS-1:0]  prf_reg;
        input                   nuke;
        input   [1:0]           branch_actual;
        input   [QUADWORD-1:0]  pc;
        input   [QUADWORD-1:0]  next_pc;

        if (valid[way]                  != valid_in)                                                exit_on_error();
        if (arch_reg_out[way]           != arch_reg         && arch_reg         != `NOT_CHECK_ARF)  exit_on_error();
        if (prf_reg_out[way]            != prf_reg)                                                 exit_on_error();
        if (n_nuke[way]                 != nuke)                                                    exit_on_error();  //Why did I comment this out?
        if (branch_actual_out[way]      != branch_actual    && branch_actual    != `NOT_CHECK)      exit_on_error();
        if (pc_out[way]                 != pc)                                                      exit_on_error();
        if (next_pc_out[way]            != next_pc)                                                 exit_on_error();
    endtask

    task check_output_invalid;
        input                   way;

        if (valid[way]  != `FALSE)    exit_on_error();
    endtask

    initial
    begin
        $monitor("===WAY_0 INPUTS===\nTime= %10d  valid_in= %b pc_in= %16h next_pc_in= %16h dest_arch_reg_in= %d dest_prf_reg_in= %d\n", $time, valid_in[0], pc_in[0], next_pc_in[0], dest_arch_reg_in[0], dest_prf_reg_in[0],
                 "===WAY_0 OUTPUTS===\nTime= %10d rob_stall= %b valid= %b arch_reg_out= %d prf_reg_out= %d n_nuke= %b branch_actual_out= %b pc_out= %16h next_pc_out= %16h\n", $time, rob_stall[0], valid[0], arch_reg_out[0], prf_reg_out[0], n_nuke[0], branch_actual_out[0], pc_out[0], next_pc_out[0],
                 "===WAY_1 INPUTS===\nTime= %10d  valid_in= %b pc_in= %16h next_pc_in= %16h dest_arch_reg_in= %d dest_prf_reg_in= %d\n", $time, valid_in[1], pc_in[1], next_pc_in[1], dest_arch_reg_in[1], dest_prf_reg_in[1],
                 "===WAY_1 OUTPUTS===\nTime= %10d rob_stall= %b valid= %b arch_reg_out= %d prf_reg_out= %d n_nuke= %b branch_actual_out= %b pc_out= %16h next_pc_out= %16h\n", $time, rob_stall[1], valid[1], arch_reg_out[1], prf_reg_out[1], n_nuke[1], branch_actual_out[1], pc_out[1], next_pc_out[1]);

        //Intalize Inputs
        clock                       = 0;
        reset                       = 1;
        valid_in                    = 0;
        pc_in                       = 0;
        next_pc_in                  = 0;
        dest_arch_reg_in            = 0;
        dest_prf_reg_in             = 0;
        branch_conditional_in       = 0;
        branch_unconditional_in     = 0;
        branch_call_in              = 0;
        branch_return_in            = 0;
        load_in                     = 0;
        store_in                    = 0;

        cdb_valid                   = 0;
        cdb_tag                     = 0;
        cdb_branch_taken            = 0;
        cdb_actual_next_pc          = 0;

        @(negedge clock);
        reset = 0;
        check_head(0);
        check_tail(0);
        //if (rob1.execution_complete != 0) exit_on_error();
        $display("\n@@@Passed Reset\n");

        //Dispatch one valid value in ROB
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h8, 64'hc, `R1, 7'd10, `FALSE, `FALSE);
        @(negedge clock);
        stop_dispatch(`WAY_1);
        check_entry(`FALSE, 0, `R1, 7'd10, 64'h8, 64'hc, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        $display("\n@@@Passed dispatch one valid value in ROB\n");

        //Try commit on that value in ROB, but invalid CDB
        @(negedge clock);
        cdb(`WAY_1, `FALSE, 7'd10, `FALSE, 64'h0);
        @(negedge clock);
        check_entry(`FALSE, 0, `R1, 7'd10, 64'h8, 64'hc, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_head_tail_eq(`FALSE);
        $display("\n@@@Passed CDB is INvalid\n");

        //Commit on that value in ROB
        @(negedge clock);
        cdb(`WAY_1, `TRUE, 7'd10, `FALSE, 64'hc);
        //#1 //comb delay for outputs (could do posedge?)
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R1, 7'd10, `FALSE, `FALSE, 64'h8, 64'hc);
        @(negedge clock);//head gets updated the next cycle TODO: issue when both WAY_0 and WAY_1 trying to commit same time, head will move once
        stop_cdb(`WAY_1);
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed commit one valid value in ROB\n");

        //Dispatch two valid values in ROB
        @(negedge clock);
		dispatch(`WAY_1, `TRUE, 64'h64, 64'h68, `R3, 7'd11, `FALSE, `FALSE);
		dispatch(`WAY_0, `TRUE, 64'h42, 64'h46, `R7, 7'd12, `FALSE, `FALSE);
        @(negedge clock);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        check_entry(`FALSE, 0, `R3, 7'd11, 64'h64, 64'h68, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 1, `R7, 7'd12, 64'h42, 64'h46, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        $display("\n@@@Passed dispatch two valid values in ROB\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Dispatch one INvalid value WAY_1 and valid WAY_0
        @(negedge clock);
		dispatch(`WAY_1, `FALSE, 64'h24, 64'h28, `R4, 7'd13, `FALSE, `FALSE);
		dispatch(`WAY_0, `TRUE, 64'h78, 64'h82, `R8, 7'd14, `FALSE, `FALSE);
        @(negedge clock);
        stop_dispatch(`WAY_0);
        check_entry(`FALSE, 2, `R8, 7'd14, 64'h78, 64'h82, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        $display("\n@@@Passed dispatch one INvalid value WAY_1 and valid WAY_0\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Dispatch one valid value WAY_1 and INvalid WAY_0
        @(negedge clock);
		dispatch(`WAY_1, `TRUE, 64'h24, 64'h28, `R4, 7'd13, `FALSE, `FALSE);
		dispatch(`WAY_0, `FALSE, 64'h52, 64'h56, `R9, 7'd15, `FALSE, `FALSE);
        @(negedge clock);
        stop_dispatch(`WAY_1);
        check_entry(`FALSE, 3, `R4, 7'd13, 64'h24, 64'h28, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        $display("\n@@@Passed dispatch one valid value WAY_1 and INvalid WAY_0\n");

        //Complete execution on tail of RoB
        @(negedge clock);
        cdb(`WAY_1, `TRUE, 7'd13, `FALSE, 64'h28);
        //#1 //comb delay for outputs (could do posedge?)
        @(posedge clock);
        check_output_invalid(`WAY_1);
        @(negedge clock);//head doesn't get updated
        stop_cdb(`WAY_1);
        check_head(1);   //head hasn't moved
        check_entry(`FALSE, 3, `R4, 7'd13, 64'h24, 64'h28, `FALSE, `NOT_CHECK, `TRUE, `FALSE, `FALSE);
        $display("\n@@@Passed complete execution on tail of RoB\n");

        //Complete execution on two, no commit!
        @(negedge clock);
        cdb(`WAY_1, `TRUE, 7'd12, `FALSE, 64'h46);
        cdb(`WAY_0, `TRUE, 7'd14, `FALSE, 64'h82);
        //#1 //comb delay for outputs (could do posedge?)
        @(posedge clock);
        check_output_invalid(`WAY_1);
        check_output_invalid(`WAY_0);
        @(negedge clock);//head doesn't get updated
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        check_head(1);   //head hasn't moved
        check_entry(`FALSE, 1, `R7, 7'd12, 64'h42, 64'h46, `FALSE, `NOT_CHECK, `TRUE, `FALSE, `FALSE);
        check_entry(`FALSE, 2, `R8, 7'd14, 64'h78, 64'h82, `FALSE, `NOT_CHECK, `TRUE, `FALSE, `FALSE);
        $display("\n@@@Passed complete execution on two, no commit!\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Commit unitil RoB empty
        @(negedge clock);
        cdb(`WAY_0, `TRUE, 7'd11, `FALSE, 64'h68);
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R3, 7'd11, `FALSE, `FALSE, 64'h64, 64'h68);
        check_outputs(`WAY_0, `TRUE, `R7, 7'd12, `FALSE, `FALSE, 64'h42, 64'h46);
        @(negedge clock);
        stop_cdb(`WAY_0);
        check_head(3);
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R8, 7'd14, `FALSE, `FALSE, 64'h78, 64'h82);
        check_outputs(`WAY_0, `TRUE, `R4, 7'd13, `FALSE, `FALSE, 64'h24, 64'h28);
        @(negedge clock);
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed commit until RoB empty\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Commit and dispatch same cycle
        //Dispatch some more shit
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h0, 64'h4, `R0, 7'd0, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h4, 64'h8, `R1, 7'd1, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'hc, 64'h10, `R2, 7'd2, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h10, 64'h14, `R3, 7'd3, `FALSE, `FALSE);
        check_entry(`FALSE, 0, `R0, 7'd0, 64'h0, 64'h4, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 1, `R1, 7'd1, 64'h4, 64'h8, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R4, 7'd4, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R5, 7'd5, `FALSE, `FALSE);
        check_entry(`FALSE, 2, `R2, 7'd2, 64'hc, 64'h10, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 3, `R3, 7'd3, 64'h10, 64'h14, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        check_entry(`FALSE, 4, `R4, 7'd4, 64'h14, 64'h18, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 5, `R5, 7'd5, 64'h18, 64'h1c, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        @(negedge clock);
        cdb(`WAY_1, `TRUE, 7'd0, `FALSE, 64'h04);
        cdb(`WAY_0, `TRUE, 7'd1, `FALSE, 64'h08);
        dispatch(`WAY_1, `TRUE, 64'h20, 64'h24, `R6, 7'd6, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h24, 64'h28, `R7, 7'd7, `FALSE, `FALSE);
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R0, 7'd0, `FALSE, `FALSE, 64'h0, 64'h4);
        check_outputs(`WAY_0, `TRUE, `R1, 7'd1, `FALSE, `FALSE, 64'h4, 64'h8);
        @(negedge clock);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        check_entry(`FALSE, 4, `R6, 7'd6, 64'h20, 64'h24, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 5, `R7, 7'd7, 64'h24, 64'h28, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        stop_cdb(`WAY_0);
        check_head(7);
        $display("\n@@@Passed commit and dispatch same cycle\n");
        //exit_on_error(); //<-- for checking RoB contents

        //NUKE WAY_1 dont commit WAY_0
        @(negedge clock);
        cdb(`WAY_0, `TRUE, 7'd2, `FALSE, 64'h14); //Misspredict -next_pc wrong
        cdb(`WAY_1, `TRUE, 7'd3, `FALSE, 64'h14);
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R2, 7'd2, `TRUE, `FALSE, 64'hc, 64'h10);
        check_output_invalid(`WAY_0);
        @(negedge clock);
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed NUKE WAY_1 dont commit WAY_0\n");
        //exit_on_error(); //<-- for checking RoB contents

        //NUKE WAY_0 commit WAY_1
        //Dispatch some more shit
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h0, 64'h4, `R0, 7'd0, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h4, 64'h8, `R1, 7'd1, `TRUE, `FALSE);    //Predicted true //gonna have to think about this shit TODO!!!
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'hc, 64'h10, `R2, 7'd2, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h10, 64'h14, `R3, 7'd3, `FALSE, `FALSE);
        check_entry(`FALSE, 0, `R0, 7'd0, 64'h0, 64'h4, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 1, `R1, 7'd1, 64'h4, 64'h8, `FALSE, `NOT_CHECK, `FALSE, `TRUE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R4, 7'd4, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R5, 7'd5, `FALSE, `FALSE);
        check_entry(`FALSE, 2, `R2, 7'd2, 64'hc, 64'h10, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 3, `R3, 7'd3, 64'h10, 64'h14, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        check_entry(`FALSE, 4, `R4, 7'd4, 64'h14, 64'h18, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 5, `R5, 7'd5, 64'h18, 64'h1c, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        cdb(`WAY_1, `TRUE, 7'd1, `FALSE, 64'h10); //Misspredict
        cdb(`WAY_0, `TRUE, 7'd0, `FALSE, 64'h4);
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R0, 7'd0, `FALSE, `FALSE, 64'h0, 64'h4);
        check_outputs(`WAY_0, `TRUE, `R1, 7'd1, `TRUE, `FALSE, 64'h4, 64'h8);
        @(negedge clock);
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed NUKE WAY_0 commit WAY_1\n");

        //Wait to NUKE
        //Dispatch some more shit
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h0, 64'h4, `R0, 7'd0, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h4, 64'h8, `R1, 7'd1, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'hc, 64'h10, `R2, 7'd2, `TRUE, `FALSE); //Predicted true //TODO, think about this....
        dispatch(`WAY_0, `TRUE, 64'h10, 64'h14, `R3, 7'd3, `FALSE, `FALSE);
        check_entry(`FALSE, 0, `R0, 7'd0, 64'h0, 64'h4, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 1, `R1, 7'd1, 64'h4, 64'h8, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R4, 7'd4, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R5, 7'd5, `FALSE, `FALSE);
        check_entry(`FALSE, 2, `R2, 7'd2, 64'hc, 64'h10, `FALSE, `NOT_CHECK, `FALSE, `TRUE, `FALSE);
        check_entry(`FALSE, 3, `R3, 7'd3, 64'h10, 64'h14, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        check_entry(`FALSE, 4, `R4, 7'd4, 64'h14, 64'h18, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        check_entry(`FALSE, 5, `R5, 7'd5, 64'h18, 64'h1c, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        @(negedge clock);
        cdb(`WAY_1, `TRUE, 7'd2, `FALSE, 64'hc); //Misspredict
        cdb(`WAY_0, `TRUE, 7'd3, `FALSE, 64'h14);
        @(posedge clock);
        check_output_invalid(`WAY_1);
        check_output_invalid(`WAY_0);
        @(negedge clock);
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        check_head(0);
        @(negedge clock);
        cdb(`WAY_1, `TRUE, 7'd0, `FALSE, 64'h4);
        cdb(`WAY_0, `TRUE, 7'd1, `FALSE, 64'h8);
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R0, 7'd0, `FALSE, `FALSE, 64'h0, 64'h4);
        check_outputs(`WAY_0, `TRUE, `R1, 7'd1, `FALSE, `FALSE, 64'h4, 64'h8);
        @(negedge clock);
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        @(posedge clock); //Now NUKE
        check_outputs(`WAY_1, `TRUE, `R2, 7'd2, `TRUE, `FALSE, 64'hc, 64'hc); //Changes the next pc!
        check_output_invalid(`WAY_0);
        @(negedge clock);
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed wait to NUKE\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Disptach and commit to get head near in middle
        //Dispatch
        @(negedge clock);
        for (int i = 0; i < 40; i += 2)
        begin
            dispatch(`WAY_1, `TRUE, i + 0, i + 4, i, i, `FALSE, `FALSE);
            dispatch(`WAY_0, `TRUE, i + 4, i + 8, i + 1, i + 1, `FALSE, `FALSE);
            @(negedge clock);
            check_entry(`FALSE, i, `NOT_CHECK_ARF, i, i + 0, i + 4, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
            check_entry(`FALSE, i + 1, `NOT_CHECK_ARF, i + 1, i + 4, i + 8, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        end
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        //exit_on_error(); //<-- for checking RoB contents
        //Commit
        for (int i = 0; i < 38; i += 2)
        begin
            @(negedge clock);
            cdb(`WAY_1, `TRUE, i, `FALSE, i + 4);
            cdb(`WAY_0, `TRUE, i + 1, `FALSE, i + 8);
            @(posedge clock);
            check_outputs(`WAY_1, `TRUE, `NOT_CHECK_ARF, i, `FALSE, `FALSE, i + 0, i + 4);
            check_outputs(`WAY_0, `TRUE, `NOT_CHECK_ARF, i + 1, `FALSE, `FALSE, i + 4, i + 8);
        end
        @(negedge clock);
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        check_head(38);
        $display("\n@@@Passed dispatch and commit alot\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Make RoB tail wrap around! and check it is full (Strutural Hazard)
        //Dispatch
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h0, 64'h4, `R0, 7'd0, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h4, 64'h8, `R1, 7'd1, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'hc, 64'h10, `R2, 7'd2, `TRUE, `FALSE); //Predicted true  //TODO: gonna have to think about this
        dispatch(`WAY_0, `TRUE, 64'h10, 64'h14, `R3, 7'd3, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R4, 7'd4, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R5, 7'd5, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R6, 7'd6, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R7, 7'd7, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R8, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R9, 7'd9, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R10, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R11, 7'd9, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R12, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R13, 7'd9, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R14, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R15, 7'd9, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R16, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R17, 7'd9, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R18, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R19, 7'd9, `FALSE, `FALSE);
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R20, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R21, 7'd9, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R21, 7'd8, `FALSE, `FALSE); //Tail should wrap now
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R22, 7'd9, `FALSE, `FALSE);
        //exit_on_error(); //<-- for checking RoB contents
        @(negedge clock);
        check_tail(0);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R23, 7'd8, `FALSE, `FALSE); //Still should dispatch
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R24, 7'd9, `FALSE, `FALSE);
        $display("\n@@@Passed tail wrapped around\n");

        @(negedge clock);
        //exit_on_error(); //<-- for checking RoB contents
        //Fill until RoB nearly full (Only two more entries left in RoB)
        for (int i = 0; i < 34; i += 2)
        begin
            dispatch(`WAY_1, `TRUE, i + 0, i + 4, i, i, `FALSE, `FALSE);
            dispatch(`WAY_0, `TRUE, i + 4, i + 8, i + 1, i + 1, `FALSE, `FALSE);
            @(negedge clock);
        end
        //exit_on_error(); //<-- for checking RoB contents
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R23, 7'd8, `FALSE, `FALSE); //Fill last two slots
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R24, 7'd9, `FALSE, `FALSE);
        @(negedge clock);
        if (rob_stall != 2'b11) exit_on_error();
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        $display("\n@@@Passed full RoB and stall\n");

        //Try to dispatch, should fail
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R10, 7'd8, `FALSE, `FALSE);
        dispatch(`WAY_0, `TRUE, 64'h18, 64'h1c, `R11, 7'd9, `FALSE, `FALSE);
        @(negedge clock);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        if (rob_stall != 2'b11) exit_on_error();
        check_tail(38);
        check_head(38);
        $display("\n@@@Passed dispatch failed\n");

        //Commit one, and make sure Rob not full
        //exit_on_error();
        cdb(`WAY_1, `TRUE, 7'd38, `FALSE, 64'h2a);
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R6, 7'd38, `FALSE, `FALSE, 64'h26, 64'h2a);
        @(negedge clock);
        dispatch(`WAY_0, `TRUE, 64'h14, 64'h18, `R12, 7'd8, `FALSE, `FALSE);    //fill again
        if (rob_stall != 2'b00) exit_on_error();
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        $display("\n@@@Passed commited and not full\n");
        //exit_on_error();

        //Some stuff
        @(negedge clock);
        check_head_tail_eq(`TRUE);
        if (rob_stall != 2'b01) exit_on_error(); //valid_in for WAY_1 is still invalid, WAY_0 is valid
        stop_dispatch(`WAY_0);
        @(negedge clock);
        check_head_tail_eq(`TRUE);
        if (rob_stall != 2'b00) exit_on_error(); //both dispatches invalid
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R12, 7'd8, `FALSE, `FALSE);    //Try to fill
        @(negedge clock);
        check_head_tail_eq(`TRUE);
        if (rob_stall != 2'b10) exit_on_error(); //valid_in for WAY_1 is valid, WAY_0 is invalid
        stop_dispatch(`WAY_1);
        @(negedge clock);
        //exit_on_error();
        cdb(`WAY_0, `TRUE, 7'd39, `FALSE, 64'h2e);  //Commit some stuff to get ready for commit and dispatch
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R12, 7'd8, `FALSE, `FALSE);    //Try to fill
        dispatch(`WAY_0, `TRUE, 64'h14, 64'h18, `R13, 7'd9, `FALSE, `FALSE);    //Try to fill
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R7, 7'd39, `FALSE, `FALSE, 64'h2a, 64'h2e);
        @(negedge clock);
        stop_cdb(`WAY_0);
        @(negedge clock);
        if (rob_stall != 2'b11) exit_on_error(); //Should still stall, dispatch still valid
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed some stuff\n");
        //exit_on_error();

        //Commit one and dispatch 2 same cycle
        if (rob_stall != 2'b11) exit_on_error(); //Should still stall
        cdb(`WAY_1, `TRUE, 7'd0, `FALSE, 64'h4);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R12, 7'd8, `FALSE, `FALSE);    //Fill
        dispatch(`WAY_0, `TRUE, 64'h14, 64'h18, `R13, 7'd9, `FALSE, `FALSE);    //Try to fill
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R0, 7'd0, `FALSE, `FALSE, 64'h0, 64'h4);
        @(negedge clock);
        if (rob_stall != 2'b11) exit_on_error(); //Should still stall
        stop_cdb(`WAY_1);
        @(negedge clock);
        if (rob_stall != 2'b11) exit_on_error(); //Should still stall, dispatch still valid
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed commit one and dispatch 2 same cycle\n");
        //exit_on_error();

        //Commit one and dispatch 1 same cycle
        cdb(`WAY_0, `TRUE, 7'd1, `FALSE, 64'h8);
        dispatch(`WAY_0, `TRUE, 64'h14, 64'h18, `R14, 7'd10, `FALSE, `FALSE);    //Fill
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R1, 7'd1, `FALSE, `FALSE, 64'h4, 64'h8);
        @(negedge clock);
        if (rob_stall != 2'b11) exit_on_error(); //Should still stall
        stop_cdb(`WAY_0);
        @(negedge clock);
        if (rob_stall != 2'b11) exit_on_error(); //Should still stall, dispatch still valid
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed commit one and dispatch 1 same cycle\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Commit 2 and dispatch 2 same cycle
        cdb(`WAY_1, `TRUE, 7'd2, `FALSE, 64'h10);
        cdb(`WAY_0, `TRUE, 7'd3, `FALSE, 64'h14);
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R15, 7'd11, `FALSE, `FALSE);    //Fill
        dispatch(`WAY_0, `TRUE, 64'h14, 64'h18, `R16, 7'd12, `FALSE, `FALSE);    //Fill
        @(posedge clock);
        check_outputs(`WAY_1, `TRUE, `R2, 7'd2, `FALSE, `FALSE, 64'hc, 64'h10);
        check_outputs(`WAY_0, `TRUE, `R3, 7'd3, `FALSE, `FALSE, 64'h10, 64'h14);
        @(negedge clock);
        if (rob_stall != 2'b11) exit_on_error(); //Should still stall
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        @(negedge clock);
        if (rob_stall != 2'b00) exit_on_error(); //Should not stall, dispatch invalid
        check_head_tail_eq(`TRUE);
        $display("\n@@@Passed commit 2 and dispatch 2 same cycle\n");
        //exit_on_error(); //<-- for checking RoB contents

        //Make sure head wraps around
        //Commit
        for (int i = 4; i < 22; i += 2)
        begin
            @(negedge clock);
            cdb(`WAY_1, `TRUE, i, `FALSE, 64'h18);
            cdb(`WAY_0, `TRUE, i + 1, `FALSE, 64'h1c);
        end
        @(negedge clock); //Just two more until head wraps around (already completed exe)
        //exit_on_error(); //<-- for checking RoB contents
        stop_cdb(`WAY_1);
        stop_cdb(`WAY_0);
        @(negedge clock);
        check_head(0);
        @(negedge clock); //Head still proceeds forward
        check_head(2);
        $display("\n@@@Passed head wrapped around\n");
        //exit_on_error(); //<-- for checking RoB contents

        //It ends up NUKING on mispredict, Entry 4
        @(negedge clock);
        @(negedge clock);
        //exit_on_error(); //<-- for checking RoB contents

        //MAKE SURE ENABLE SIGNAL WORKS
        //Both can't dispatch
        dispatch(`WAY_1, `FALSE, 64'h14, 64'h18, `R15, 7'd11, `FALSE, `FALSE);    //Can't fill
        dispatch(`WAY_0, `FALSE, 64'h14, 64'h18, `R16, 7'd12, `FALSE, `FALSE);    //Can't fill
        @(negedge clock);
        //exit_on_error(); //<-- for checking RoB contents
        check_tail(0);  //tail didn't move
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        @(negedge clock);
        $display("\n@@@Passed valid_in, both can't dispatch\n");
        //exit_on_error(); //<-- for checking RoB contents


        // THIS COULD NEVER HAPPEN!!!!!
        /*
        //WAY_0 can dispatch
        valid_in = 2'b01;
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R15, 7'd11, `FALSE, `FALSE);    //Can't fill
        dispatch(`WAY_0, `TRUE, 64'h14, 64'h18, `R16, 7'd12, `FALSE, `FALSE);    //Fill
        @(negedge clock);
        check_tail(1);  //tail moved once
        check_entry(`FALSE, 0, `R16, 7'd12, 64'h14, 64'h18, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        @(negedge clock);
        $display("\n@@@Passed valid_in, WAY_0 can dispatch\n");
        */

        //WAY_1 can dispatch
        dispatch(`WAY_1, `TRUE, 64'h14, 64'h18, `R15, 7'd11, `FALSE, `FALSE);    //Can't fill
        dispatch(`WAY_0, `FALSE, 64'h14, 64'h18, `R16, 7'd12, `FALSE, `FALSE);    //Fill
        @(negedge clock);
        check_tail(1);  //tail moved once
        check_entry(`FALSE, 0, `R15, 7'd11, 64'h14, 64'h18, `FALSE, `NOT_CHECK, `FALSE, `FALSE, `FALSE);
        stop_dispatch(`WAY_1);
        stop_dispatch(`WAY_0);
        @(negedge clock);
        $display("\n@@@Passed valid_in, WAY_1 can dispatch\n");

        @(negedge clock);
        @(negedge clock);
        $display("\n@@@ PASSED ALL TEST CASES @@@\n");
        $finish;
    end
endmodule

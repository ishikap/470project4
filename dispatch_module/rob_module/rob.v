// TODO: handle dependency of ROB_STALL in RS
//
//       2/26/18: CLK: 5.1562 < x < 5.53125
//       2/28/18: CLK: 6.3125 < x <= 6.40625
//       3/6/18: CLK: 5.875 < x <= 6.25

`default_nettype none
`timescale 1ns/100ps
module rob #(localparam QUADWORD=64, parameter ROB_SIZE=64, parameter NUM_PRF_ENTRIES=96, localparam PRF_BITS=$clog2(NUM_PRF_ENTRIES), localparam NUM_ARCH_REGS=32, localparam ARF_BITS=$clog2(NUM_ARCH_REGS), localparam ROB_BITS = $clog2(ROB_SIZE)) (
    // INPUTS
    input   wire                            clock,
    input   wire                            reset,

    // First instruction in program order must be in valid_in[1]
    // (i.e., one should never have valid_in[0] be true but not valid_in[1])
    input   wire    [1:0]                   valid_in,
    input   wire    [1:0][QUADWORD-1:0]     pc_in,
    input   wire    [1:0][QUADWORD-1:0]     next_pc_in,         // if branch taken, then is branch addr
    input   wire    [1:0][ARF_BITS-1:0]     dest_arch_reg_in,
    input   wire    [1:0][PRF_BITS-1:0]     dest_prf_reg_in,
    input   wire    [1:0]                   branch_conditional_in,
    input   wire    [1:0]                   branch_unconditional_in,
    input   wire    [1:0]                   branch_call_in,
    input   wire    [1:0]                   branch_return_in,
    input   wire    [1:0]                   load_in,
    input   wire    [1:0]                   store_in,
    input   wire    [1:0]                   halt_in,
    input   wire    [1:0]                   illegal_in,

    // We need some way to update a given ROB entry about whether or not the branch was actually taken
    // POSSIBLE SOLUTIONS: use a PRF entry and update the ROB entry that uses that PRF (that is what this assumes)
    input   wire    [1:0]                   cdb_valid,
    input   wire    [1:0][PRF_BITS-1:0]     cdb_tag,            // PRF tag
    input   wire    [1:0]                   cdb_branch_taken,
    input   wire    [1:0][QUADWORD-1:0]     cdb_actual_next_pc,

    // OUTPUTS
    output  logic   [1:0]                   rob_stall,
    output  logic   [1:0]                   valid,              // Commit: tell RRAT to update
    output  logic   [1:0][ARF_BITS-1:0]     arch_reg_out,       // Committing architected reg
    output  logic   [1:0][PRF_BITS-1:0]     prf_reg_out,        // Committing PRF reg
    output  logic   [1:0]                   nuke,
    output  logic   [1:0]                   branch_actual_out,      // Needed for updating predictor
    output  logic   [1:0][QUADWORD-1:0]     pc_out,
    output  logic   [1:0][QUADWORD-1:0]     next_pc_out,
    output  logic   [1:0]                   branch_conditional_out,
    output  logic   [1:0]                   branch_unconditional_out,
    output  logic   [1:0]                   branch_call_out,
    output  logic   [1:0]                   branch_return_out,
    output  logic   [1:0]                   load_out,
    output  logic   [1:0]                   store_out,
    output  logic   [1:0]                   halt_out,
    output  logic   [1:0]                   illegal_out,
    output  logic                           store_request_out,
    output  logic                           store_executed
);

    logic [ROB_BITS-1:0]                    head, tail;
    // Circular Buffer contents
    logic [ROB_SIZE-1:0][ARF_BITS-1:0]      arch_reg;
    logic [ROB_SIZE-1:0][PRF_BITS-1:0]      prf_reg;
    logic [ROB_SIZE-1:0][QUADWORD-1:0]      pc;
    logic [ROB_SIZE-1:0][QUADWORD-1:0]      next_pc;
    logic [ROB_SIZE-1:0]                    branch_actual;
    logic [ROB_SIZE-1:0]                    branch_addr_wrong;
    logic [ROB_SIZE-1:0]                    execution_complete;
    logic [ROB_SIZE-1:0]                    branch_conditional;
    logic [ROB_SIZE-1:0]                    branch_unconditional;
    logic [ROB_SIZE-1:0]                    branch_call;
    logic [ROB_SIZE-1:0]                    branch_return;
    logic [ROB_SIZE-1:0]                    load;
    logic [ROB_SIZE-1:0]                    store;
    logic [ROB_SIZE-1:0]                    halt;
    logic [ROB_SIZE-1:0]                    illegal;
    logic [ROB_SIZE-1:0]                    valid_rob_entry;

    logic                                   full_rob;   // Used to distinguish between completely empty and completely full ROB

    logic [ROB_BITS-1:0]                    n_head, n_tail;
    logic                                   n_full_rob; // Used to distinguish between completely empty and completely full ROB
    logic [ROB_SIZE-1:0][ARF_BITS-1:0]      n_arch_reg;
    logic [ROB_SIZE-1:0][PRF_BITS-1:0]      n_prf_reg;
    logic [ROB_SIZE-1:0][QUADWORD-1:0]      n_pc;
    logic [ROB_SIZE-1:0][QUADWORD-1:0]      n_next_pc;
    logic [ROB_SIZE-1:0]                    n_branch_predict;
    logic [ROB_SIZE-1:0]                    n_branch_actual;
    logic [ROB_SIZE-1:0]                    n_branch_addr_wrong;
    logic [ROB_SIZE-1:0]                    n_execution_complete;
    logic [ROB_SIZE-1:0]                    n_branch_conditional;
    logic [ROB_SIZE-1:0]                    n_branch_unconditional;
    logic [ROB_SIZE-1:0]                    n_branch_call;
    logic [ROB_SIZE-1:0]                    n_branch_return;
    logic [ROB_SIZE-1:0]                    n_load;
    logic [ROB_SIZE-1:0]                    n_store;
    logic [ROB_SIZE-1:0]                    n_halt;
    logic [ROB_SIZE-1:0]                    n_illegal;
    logic [ROB_SIZE-1:0]                    n_valid_rob_entry;


    logic previous_taken;
    logic halt_done;
    logic nuked;

    always_comb begin
        //Intialize Outputs
        rob_stall                   = 0;
        valid                       = 0;
        arch_reg_out                = 0;
        prf_reg_out                 = 0;
        nuke                        = 0;
        branch_actual_out           = 0;
        pc_out                      = 0;
        branch_conditional_out      = 0;
        branch_unconditional_out    = 0;
        next_pc_out                 = 0;
        branch_call_out             = 0;
        branch_return_out           = 0;
        load_out                    = 0;
        store_out                   = 0;
        halt_out                    = 0;
        illegal_out                 = 0;
        store_request_out           = 0;
        store_executed              = 0;
        nuked                       = 0;

        //Intialize Latch values
        n_full_rob              = full_rob;
        n_arch_reg              = arch_reg;
        n_prf_reg               = prf_reg;
        n_pc                    = pc;
        n_next_pc               = next_pc;
        n_branch_addr_wrong     = branch_addr_wrong;
        n_branch_actual         = branch_actual;
        n_execution_complete    = execution_complete;
        n_head                  = head;
        n_tail                  = tail;
        valid                   = 0;
        rob_stall               = 0;
        n_branch_conditional    = branch_conditional;
        n_branch_unconditional  = branch_unconditional;
        n_branch_call           = branch_call;
        n_branch_return         = branch_return;
        n_load                  = load;
        n_store                 = store;
        n_halt                  = halt;
        n_illegal               = illegal;
        n_valid_rob_entry       = valid_rob_entry;

        // if store at head of rob (not exec complete) send request to lsq to commit it
        if (n_store[n_head]) begin
            store_request_out                   = `TRUE;
            store_executed                      = n_execution_complete[n_head];
        end

        // Handle stalls based only on saved state
        rob_stall[1] = (n_head == n_tail) && n_full_rob;
        rob_stall[0] = (n_head - n_tail == 1) || (n_head == 0 && n_tail == ROB_SIZE - 1) || rob_stall[1];

        // Commit [1] first then [0]
        for (int i=1; i>=0; --i) begin
            illegal_out[i]                      = illegal[n_head]; // doesnt check for execution complete
            if (n_execution_complete[n_head] && !halt_done && !halt_out[1] && !nuke[1]) begin
                arch_reg_out[i]                 = n_arch_reg[n_head];
                prf_reg_out[i]                  = n_prf_reg[n_head];
                pc_out[i]                       = n_pc[n_head];
                branch_conditional_out[i]       = n_branch_conditional[n_head];
                branch_unconditional_out[i]     = n_branch_unconditional[n_head];
                branch_call_out[i]              = n_branch_call[n_head];
                branch_return_out[i]            = n_branch_return[n_head];
                load_out[i]                     = n_load[n_head];
                store_out[i]                    = n_store[n_head];
                halt_out[i]                     = n_halt[n_head];

                next_pc_out[i]                  = n_next_pc[n_head];
                valid[i]                        = `TRUE;
                branch_actual_out[i]            = n_branch_actual[n_head];
                nuke[i]                         = n_branch_addr_wrong[n_head];

                n_execution_complete[n_head]    = `FALSE;

                n_valid_rob_entry[n_head]       = 1'b0;
                n_head                          = n_head + 1;

                n_full_rob                      = 0;
            end
        end

        // Dispatch [1] first then [0]
        // Forward from CDB if being put into ROB while CDB matches
        for (int i=1; i>=0; --i) begin
            if (valid_in[i] && !n_full_rob) begin
                n_arch_reg[n_tail]              = dest_arch_reg_in[i];
                n_prf_reg[n_tail]               = dest_prf_reg_in[i];
                n_pc[n_tail]                    = pc_in[i];
                n_branch_conditional[n_tail]    = branch_conditional_in[i];
                n_branch_unconditional[n_tail]  = branch_unconditional_in[i];
                n_branch_call[n_tail]           = branch_call_in[i];
                n_branch_return[n_tail]         = branch_return_in[i];
                n_load[n_tail]                  = load_in[i];
                n_store[n_tail]                 = store_in[i];
                n_halt[n_tail]                  = halt_in[i];
                n_illegal[n_tail]               = illegal_in[i];

                n_next_pc[n_tail]               = next_pc_in[i];
                n_branch_addr_wrong[n_tail]     = `FALSE;
                n_execution_complete[n_tail]    = `FALSE;

                n_valid_rob_entry[n_tail]       = 1'b1;
                n_tail                          = n_tail + 1;

                n_full_rob                      = (n_tail == n_head);

            end
        end

        // CDB
        for (int i=0; i<2; ++i) begin
            for (int j=0; j<ROB_SIZE; ++j) begin
                if (cdb_valid[i] && cdb_tag[i] == n_prf_reg[j] && n_valid_rob_entry[j]) begin
                    n_execution_complete[j] = `TRUE;
                    n_branch_actual[j]      = cdb_branch_taken[i];

                    // Just compare next pc with actual (special case for loads and stores)
                    if (n_branch_conditional[j] || n_branch_unconditional[j]) begin
                        if (n_next_pc[j] != cdb_actual_next_pc[i]) begin
                            n_next_pc[j]            = cdb_actual_next_pc[i];
                            n_branch_addr_wrong[j]  = `TRUE;
                        end
                    end
                end
            end
        end

        if (nuke) begin
            n_head                    = 0;
            n_tail                    = 0;
            n_full_rob                = 0;
            n_arch_reg                = 0;
            n_prf_reg                 = 0;
            n_pc                      = 0;
            n_branch_conditional      = 0;
            n_branch_unconditional    = 0;
            n_next_pc                 = 0;
            n_branch_addr_wrong       = 0;
            n_branch_actual           = 0;
            n_execution_complete      = 0;
            n_branch_call             = 0;
            n_branch_return           = 0;
            n_load                    = 0;
            n_store                   = 0;
            n_halt                    = 0;
            n_illegal                 = 0;
            n_valid_rob_entry         = 0;
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            head                    <= `SD 0;
            tail                    <= `SD 0;
            full_rob                <= `SD 0;

            arch_reg                <= `SD 0;
            prf_reg                 <= `SD 0;
            pc                      <= `SD 0;
            branch_conditional      <= `SD 0;
            branch_unconditional    <= `SD 0;
            next_pc                 <= `SD 0;
            branch_addr_wrong       <= `SD 0;
            branch_actual           <= `SD 0;
            execution_complete      <= `SD 0;
            branch_call             <= `SD 0;
            branch_return           <= `SD 0;
            load                    <= `SD 0;
            store                   <= `SD 0;
            halt                    <= `SD 0;
            illegal                 <= `SD 0;
            valid_rob_entry         <= `SD 0;
            halt_done               <= `SD 0;
        end
        else begin
            head                    <= `SD n_head;
            tail                    <= `SD n_tail;
            full_rob                <= `SD n_full_rob;

            arch_reg                <= `SD n_arch_reg;
            prf_reg                 <= `SD n_prf_reg;
            pc                      <= `SD n_pc;
            branch_conditional      <= `SD n_branch_conditional;
            branch_unconditional    <= `SD n_branch_unconditional;
            next_pc                 <= `SD n_next_pc;
            branch_addr_wrong       <= `SD n_branch_addr_wrong;
            branch_actual           <= `SD n_branch_actual;
            execution_complete      <= `SD n_execution_complete;
            branch_call             <= `SD n_branch_call;
            branch_return           <= `SD n_branch_return;
            load                    <= `SD n_load;
            store                   <= `SD n_store;
            halt                    <= `SD n_halt;
            illegal                 <= `SD n_illegal;
            valid_rob_entry         <= `SD n_valid_rob_entry;
            halt_done               <= `SD halt_out ? `TRUE : halt_done;
        end
    end
endmodule

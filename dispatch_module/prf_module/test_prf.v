//For: Testing PRF module
//By: Scott
//Date 2/19/18

`timescale 1ns/100ps
`define NOT_CHECK 	2'b10
`define TRUE_2 		2'b01
`define FALSE_2 	2'b00
`define WAY_0   	1'b0
`define WAY_1 	    1'b1

parameter NUM_PRF_ENTRIES   = 96;
parameter QUADWORD          = 64;
parameter PRF_BITS          = $clog2(NUM_PRF_ENTRIES);
parameter NUM_ARF_ENTRIES   = 32;
parameter FREE_LIST_SIZE    = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES;

module testbench;

    logic                                           clock;
    logic                                           reset;
    logic   [1:0][PRF_BITS-1:0]						rda_idx;
    logic   [1:0][PRF_BITS-1:0]						rdb_idx;
    logic   [1:0][PRF_BITS-1:0]						wr_idx;
    logic   [1:0][QUADWORD-1:0]						wr_data;					// write data
    logic   [1:0]									wr_en;
    logic	[1:0]									nuke;						//for branch mispredict
    logic	[FREE_LIST_SIZE-1:0][PRF_BITS-1:0]		rrat_free_list;
    logic   [1:0][PRF_BITS-1:0]                     rrat_overwritten;
    logic   [1:0]                                   rrat_overwritten_valid;
    logic	[1:0][QUADWORD-1:0]                		rda_out;
  	logic	[1:0][QUADWORD-1:0]                		rdb_out;
    logic   [1:0]                                   prf_valid_a_out;
    logic   [1:0]                                   prf_valid_b_out;


    prf #(NUM_PRF_ENTRIES) prf1(
            .clock(clock),
            .reset(reset),
            .rda_idx(rda_idx),
            .rdb_idx(rdb_idx),
            .wr_idx(wr_idx),
            .wr_data(wr_data),					// write data
            .wr_en(wr_en),
            .nuke(nuke),						//for branch mispredict
            .rrat_free_list(rrat_free_list),
            .rrat_overwritten(rrat_overwritten),
            .rrat_overwritten_valid(rrat_overwritten_valid),
            .rda_out(rda_out),
            .rdb_out(rdb_out),
            .prf_valid_a_out(prf_valid_a_out),
            .prf_valid_b_out(prf_valid_b_out)
        );

    always
    begin
        #5;
        clock = ~ clock;
    end

    task exit_on_error;
	    $display("!!!FATAL ERROR!!!");
        /*
        for (int i=0; i < NUM_PRF_ENTRIES; i++)
        begin
            $display ("PRF Entry #: %d ", i,
                      "PRF Data: %d ", prf1.prfs[i],
                      "PRF Valid: %b", prf1.valid[i]);
        end
        */
        $fatal("Incorrect");
    endtask

    task write;
        input                  way;
        input [PRF_BITS-1:0]   wr_idx_in;
        input [QUADWORD-1:0]   wr_data_in;
        input                  wr_en_in;

        wr_idx[way] = wr_idx_in;
        wr_data[way] = wr_data_in;
        wr_en[way] = wr_en_in;
    endtask

    task read;
        input                  way;
        input [PRF_BITS-1:0]   rda_idx_in;
        input [PRF_BITS-1:0]   rdb_idx_in;

        rda_idx[way] = rda_idx_in;
        rdb_idx[way] = rdb_idx_in;
    endtask

    task check;
        input                  way;
        input [QUADWORD-1:0]   rda_out_in;
        input [QUADWORD-1:0]   rdb_out_in;
        input                  prf_valid_a_out_in;
        input                  prf_valid_b_out_in;

        if (rda_out_in != rda_out[way]) exit_on_error();
        if (rdb_out_in != rdb_out[way]) exit_on_error();
        if (prf_valid_a_out_in != prf_valid_a_out[way]) exit_on_error();
        if (prf_valid_b_out_in != prf_valid_b_out[way]) exit_on_error();
    endtask

    task check_prfs;
        input [PRF_BITS-1:0]    index;
        input [QUADWORD-1:0]    data;
        input                   valid;

        //Commented out for synthis
        //if (prf1.prfs[index] != data && data != `NOT_CHECK) exit_on_error();
        //if (prf1.valid[index] != valid) exit_on_error();
    endtask


	initial
	begin
	    $monitor("===WAY_0===\nTime= %d  rda_idx= %d rdb_idx= %d wr_idx= %d wr_data= %10d rda_out= %10d  rdb_out= %10d", $time, rda_idx[0], rdb_idx[0], wr_idx[0], wr_data[0], rda_out[0], rdb_out[0],
	             "\n===WAY_1===\nTime= %d  rda_idx= %d rdb_idx= %d wr_idx= %d wr_data= %10d rda_out= %10d  rdb_out= %10d\n\n", $time, rda_idx[1], rdb_idx[1], wr_idx[1], wr_data[1], rda_out[1], rdb_out[1]);

		clock = 0;
		reset = 0;
        rda_idx = 0;
        rdb_idx = 0;
        wr_idx = 0;
        wr_data = 0;
        wr_en = 0;
        nuke = 0;
        rrat_overwritten_valid = 0;
        rrat_overwritten = 0;

        //Intialize free list
        for (int i = 0; i < 9; ++i)
        begin
            rrat_free_list[i] = i;
        end

		@(negedge clock);
		reset = 1;
		@(negedge clock);
		reset = 0;

        //Check if data valid
		@(negedge clock);
        write(`WAY_0, 6'd5, 64'd948, `FALSE);
		@(negedge clock);
        check_prfs(6'd5, `NOT_CHECK, `FALSE);
		$display("\n@@@Passed Check for valid data\n");

        //Check if data inputed
		@(negedge clock);
        write(`WAY_1, 6'd8, 64'd222, `TRUE);
		@(negedge clock);
        check_prfs(6'd8, 64'd222, `TRUE);
		$display("\n@@@Passed Check if data written\n");

        //Write data, but write is not enabled
		@(negedge clock);
        write(`WAY_0, 6'd8, 64'd948, `FALSE);
		@(negedge clock);
        check_prfs(6'd8, 64'd222, `TRUE);
		$display("\n@@@Passed Check data was not written\n");

        //Read data simple
		@(negedge clock);
        read(`WAY_0, 6'd8, 6'd8);
        read(`WAY_1, 6'd8, 6'd8);
		@(posedge clock);
        check(`WAY_0, 64'd222, 64'd222,`TRUE, `TRUE);
        check(`WAY_1, 64'd222, 64'd222,`TRUE, `TRUE);
		$display("\n@@@Passed read data simple\n");

        //Read from invalid data
		@(negedge clock);
        read(`WAY_0, 6'd5, 6'd8);
        read(`WAY_1, 6'd31, 6'd13);
		@(posedge clock);
        check(`WAY_0, 64'd5, 64'd222,`FALSE, `TRUE);
        check(`WAY_1, 64'd31, 64'd13,`FALSE, `FALSE);
		$display("\n@@@Passed read from invalid data\n");

        //Write data, and forward to read
		@(negedge clock);
        write(`WAY_0, 6'd18, 64'd9, `TRUE);
        write(`WAY_1, 6'd10, 64'd48, `TRUE);
        read(`WAY_0, 6'd18, 6'd10);
        read(`WAY_1, 6'd10, 6'd18);
		@(posedge clock);
        check(`WAY_0, 64'd9, 64'd48,`TRUE, `TRUE);
        check(`WAY_1, 64'd48, 64'd9,`TRUE, `TRUE);
		@(negedge clock);
        check_prfs(6'd18, 64'd9, `TRUE);
        check_prfs(6'd10, 64'd48, `TRUE);
		$display("\n@@@Passed Write data and forward to read\n");

        //Write data, and forward to read all parts
		@(negedge clock);
        write(`WAY_0, 6'd0, 64'd10, `TRUE);
        write(`WAY_1, 6'd1, 64'd11, `TRUE);
        read(`WAY_0, 6'd0, 6'd0);
        read(`WAY_1, 6'd0, 6'd0);
		@(posedge clock);
        write(`WAY_0, 6'd0, 64'd10, `FALSE);
        write(`WAY_1, 6'd1, 64'd11, `FALSE);
        check(`WAY_0, 64'd10, 64'd10,`TRUE, `TRUE);
        check(`WAY_1, 64'd10, 64'd10,`TRUE, `TRUE);
		@(negedge clock);
        check_prfs(6'd0, 64'd10, `TRUE);
        check_prfs(6'd1, 64'd11, `TRUE);
		$display("\n@@@Passed Write data and forward to read all parts\n");

        // Normal read
        @(negedge clock);
        read(`WAY_0, 6'd1, 6'd8);
        read(`WAY_1, 6'd10, 6'd11);
		@(posedge clock);
        check(`WAY_0, 64'd11, 64'd222,`TRUE, `TRUE);
        check(`WAY_1, 64'd48, 64'd11,`TRUE, `FALSE);
		$display("\n@@@Passed normal read\n");

        //Nuke 2
        @(negedge clock);
        @(negedge clock);
        nuke = 2;
		@(negedge clock);
        nuke = 0;
        check_prfs(6'd0, `NOT_CHECK, `FALSE);
        check_prfs(6'd1, `NOT_CHECK, `FALSE);
        check_prfs(6'd8, `NOT_CHECK, `FALSE);
        check_prfs(6'd10, 64'd48, `TRUE);
        check_prfs(6'd18, 64'd9, `TRUE);
		$display("\n@@@Passed Nuke and free some PRFs\n");

        // do rrat_overwritten
		@(negedge clock);
        rrat_overwritten_valid = 1;
        rrat_overwritten = 10;
		@(negedge clock);
        check_prfs(6'd10, `NOT_CHECK, `FALSE);
        $display("\n@@@Passed rrat overwrite PRFs\n");


		@(negedge clock);
		$display("\n@@@Passed ALLLLLL\n");
		$finish;
	end
endmodule

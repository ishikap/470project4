//         -(3/5) CLK_PERIOD = 2.6875 < x <= 2.75

`default_nettype none
`timescale 1ns/100ps
module prf #(parameter NUM_PRF_ENTRIES = 96, localparam QUADWORD = 64, localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES), localparam NUM_ARF_ENTRIES = 32, localparam FREE_LIST_SIZE = NUM_PRF_ENTRIES - NUM_ARF_ENTRIES) (
    // INPUTS
    input   wire                                                            clock,
    input   wire                                                            reset,
    input   wire                   [1:0][PRF_BITS-1:0]                      rda_idx,
    input   wire                   [1:0][PRF_BITS-1:0]                      rdb_idx,
    input   wire                   [1:0][PRF_BITS-1:0]                      wr_idx,
    input   wire                   [1:0][QUADWORD-1:0]                      wr_data,                    // write data
    input   wire                   [1:0]                                    wr_en,
    input   wire                   [1:0]                                    nuke,                       // for branch mispredict
    input   wire    [FREE_LIST_SIZE-1:0][PRF_BITS-1:0]                      rrat_free_list,
    input   wire                   [1:0][PRF_BITS-1:0]                      rrat_overwritten,
    input   wire                   [1:0]                                    rrat_overwritten_valid,

    // OUTPUTS
    output  logic                   [1:0][QUADWORD-1:0]                     rda_out,
    output  logic                   [1:0][QUADWORD-1:0]                     rdb_out,
    output  logic                   [1:0]                                   prf_valid_a_out,            // if this is equal to 0, then operand value equals prf index
    output  logic                   [1:0]                                   prf_valid_b_out             // if this is equal to 0, then operand value equals prf index
);

    // PRF state
    logic   [NUM_PRF_ENTRIES-1:0][QUADWORD-1:0] prfs, prfs_next;
    logic     [NUM_PRF_ENTRIES-1:0]             valid, valid_next;

    always_comb begin
        prfs_next       = prfs;
        valid_next      = valid;
        prf_valid_a_out = 0;
        prf_valid_b_out = 0;

        // When RRAT commits, the overwritten RRAT entry is freed in PRF (invalidate)
        for (int i=1; i>=0; --i) begin
            if (rrat_overwritten_valid[i]) begin
                valid_next[rrat_overwritten[i]] = 0;
            end
        end

        // Read
        for (int i=1; i>=0; --i) begin
            // Read port A
            if (wr_en[0] && (wr_idx[0] == rda_idx[i])) begin
                rda_out[i]              = wr_data[0];
                prf_valid_a_out[i]      = `TRUE;
            end
            else if (wr_en[1] && (wr_idx[1] == rda_idx[i])) begin
                rda_out[i]              = wr_data[1];
                prf_valid_a_out[i]      = `TRUE;
            end
            else if (valid_next[rda_idx[i]]) begin
                rda_out[i]              = prfs_next[rda_idx[i]];
                prf_valid_a_out[i]      = `TRUE;
            end
            else begin
                rda_out[i]              = rda_idx[i];
            end
            // Read port B
            if (wr_en[0] && (wr_idx[0] == rdb_idx[i])) begin
                rdb_out[i]              = wr_data[0];
                prf_valid_b_out[i]      = `TRUE;
            end
            else if (wr_en[1] && (wr_idx[1] == rdb_idx[i])) begin
                rdb_out[i]              = wr_data[1];
                prf_valid_b_out[i]      = `TRUE;
            end
            else if (valid_next[rdb_idx[i]]) begin
                rdb_out[i]              = prfs_next[rdb_idx[i]];
                prf_valid_b_out[i]      = `TRUE;
            end
            else begin
                rdb_out[i]              = rdb_idx[i];
            end
        end

        // Write to PRF
        for (int i=1; i>=0; --i) begin
            if (wr_en[i]) begin
                prfs_next[wr_idx[i]]    = wr_data[i];
                valid_next[wr_idx[i]]   = 1;
            end
        end

        // Nuke
        if (nuke) begin
            for (int i=0; i<FREE_LIST_SIZE; ++i) begin
                valid_next[rrat_free_list[i]] = 0;
            end
        end
    end


    always_ff @(posedge clock) begin
        if (reset) begin
            for (int i=0; i<NUM_PRF_ENTRIES; ++i) begin
                if (i == `ZERO_REG) begin
                    valid[i]    <= `SD 1'b1;
                end
                else begin
                    valid[i]    <= `SD 0;
                end
            end
            prfs                <= `SD 0;
        end
        else begin
            valid               <= `SD valid_next;
            prfs                <= `SD prfs_next;
        end
    end
endmodule
`default_nettype wire

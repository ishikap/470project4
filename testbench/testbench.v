/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  testbench.v                                         //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline;       //
//                                                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

//`timescale 1ns/100ps

`include "sys_defs.vh"
`timescale 1ns/100ps

extern void print_header(string str);
extern void print_cycles();
extern void print_stage(string div, int inst, int npc, int valid_inst);
extern void print_reg(int wb_reg_wr_data_out_hi, int wb_reg_wr_data_out_lo,
                      int wb_reg_wr_idx_out, int wb_reg_wr_en_out);
extern void print_membus(int proc2mem_command, int mem2proc_response,
                         int proc2mem_addr_hi, int proc2mem_addr_lo,
                         int proc2mem_data_hi, int proc2mem_data_lo);
extern void print_close();

parameter NUM_RS = 16;
parameter ROB_SIZE = 64;

parameter NUM_BTB_ENTRIES = 32;
parameter NUM_BTB_WAYS = 4;
parameter NUM_BRANCH_PRED_ENTRIES = 32;
parameter BRANCH_COUNTER_BITS = 2;
parameter RAS_SIZE = 16;
localparam NUM_ARF_ENTRIES = 32;
localparam ARF_BITS = $clog2(NUM_ARF_ENTRIES);
localparam FREE_LIST_BITS = $clog2(ROB_SIZE);
parameter I_NUM_WAYS = 2;
parameter D_NUM_WAYS = 2;
parameter NUM_PRF_ENTRIES = ROB_SIZE + NUM_ARF_ENTRIES;
localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES);
parameter MULT_STAGES = 4;
parameter FINISHED_INSTR = 6;
localparam QUADWORD = 64;
parameter LDB_SIZE = 8;
parameter STQ_SIZE = 4;
localparam CDB_INPUTS = FINISHED_INSTR + 1 + LDB_SIZE;

module testbench;

	// Registers and wires used in the testbench
	logic                       clock;
	logic                       reset;
	logic [QUADWORD/2-1:0]      clock_count;
	logic [QUADWORD/2-1:0]      instr_count;
	logic [QUADWORD/2-1:0]      instr_count_prev;
	logic [QUADWORD/2-1:0]      instr_count_prev_next;
	int                         wb_fileno;
    int                         wb_fileno_time;
	int							analysis_file;
	int							analysis_file_read;

	logic  [1:0]                proc2mem_command;
	logic  [QUADWORD-1:0]       proc2mem_addr;
	logic  [QUADWORD-1:0]       proc2mem_data;
	logic  [3:0]                mem2proc_response;
	logic  [QUADWORD-1:0]       mem2proc_data;
	logic  [3:0]                mem2proc_tag;

	logic [1:0][3:0]            pipeline_completed_insts;
	logic [1:0][3:0]            pipeline_error_status;
	logic [1:0]            		pipeline_halt_out;
	logic [1:0][ARF_BITS-1:0]   pipeline_commit_wr_idx;
	logic [1:0][QUADWORD-1:0]   pipeline_commit_wr_data;
	logic [1:0]                 pipeline_commit_wr_en;
	logic [1:0][QUADWORD-1:0]   pipeline_commit_PC;
    logic [1:0][PRF_BITS-1:0]   pipeline_commit_prf;

	logic 						halted = 0;

	int							cycles_delayed_from_store = 0;

    `ifndef SYNTH_TEST
    assign pipeline_commit_wr_data[1] = pipeline_0.dispatch_holder1.prf1.prfs_next[pipeline_commit_prf[1]];
    assign pipeline_commit_wr_data[0] = pipeline_0.dispatch_holder1.prf1.prfs_next[pipeline_commit_prf[0]];
	`endif

	// Instantiate the Pipeline
	`DUT(pipeline) pipeline_0 (
		// INPUTS
		.clock             (clock),
		.reset             (reset),
		.mem2proc_response (mem2proc_response),
		.mem2proc_data     (mem2proc_data),
		.mem2proc_tag      (mem2proc_tag),

		// OUTPUTS
		.proc2mem_command  (proc2mem_command),
		.proc2mem_addr     (proc2mem_addr),
		.proc2mem_data     (proc2mem_data),

		.pipeline_completed_insts(pipeline_completed_insts),
		.pipeline_error_status(pipeline_error_status),
		.pipeline_halt_out(pipeline_halt_out),
		.pipeline_commit_wr_idx(pipeline_commit_wr_idx),
		.pipeline_commit_prf(pipeline_commit_prf),
		.pipeline_commit_wr_en(pipeline_commit_wr_en),
		.pipeline_commit_PC(pipeline_commit_PC)

	);


	// Instantiate the Data Memory
	mem memory (
		// INPUTS
		.clk               (clock),
		.proc2mem_command  (proc2mem_command),
		.proc2mem_addr     (proc2mem_addr),
		.proc2mem_data     (proc2mem_data),
		// OUTPUTS
		.mem2proc_response (mem2proc_response),
		.mem2proc_data     (mem2proc_data),
		.mem2proc_tag      (mem2proc_tag)
	);

	// Generate System Clock
	always
	begin
		#(`VERILOG_CLOCK_PERIOD/2.0);
		clock = ~clock;
        // Print PRF values

        if (!clock && pipeline_error_status==`NO_ERROR) begin // && instr_count != instr_count_prev) begin
            // $display("%d", instr_count);
            instr_count_prev_next = instr_count;

			/*
			if (pipeline.disp_rob_store_request_out && !pipeline.lsq1.store_queue1.already_stored &&
				!pipeline.lsq1.store_queue1.data_valid[pipeline.lsq1.store_queue1.head]) begin

				cycles_delayed_from_store++;
			end
			*/

        end
	end

	// Task to display # of elapsed clock edges
	task show_clk_count;
		real cpi;

		begin
			cpi = (clock_count + 1.0) / (instr_count-1);
			$display("@@  %0d cycles / %0d instrs = %f CPI\n@@",
			clock_count+1, instr_count-1, cpi);
			$display("@@  %4.2f ns total time to execute\n@@\n",
			clock_count*`VIRTUAL_CLOCK_PERIOD);
		end

	endtask  // task show_clk_count

	// Show contents of a range of Unified Memory, in both hex and decimal
	task show_mem_with_decimal;
		input [31:0] start_addr;
		input [31:0] end_addr;
		int showing_data;
		begin
			$display("@@@");
			showing_data=0;
			for(int k=start_addr;k<=end_addr; k=k+1)
				if (memory.unified_memory[k] != 0)
				begin
					//$display("@@@ mem[%5d] = %x : %0d, Time: %d", k*8,	memory.unified_memory[k],
					//											memory.unified_memory[k], $time);
					$display("@@@ mem[%5d] = %x : %0d", k*8,	memory.unified_memory[k],
																memory.unified_memory[k]);
					showing_data=1;
				end
				else if(showing_data!=0)
				begin
					$display("@@@");
					showing_data=0;
				end
			$display("@@@");
		end
	endtask  // task show_mem_with_decimal

	initial
	begin
		`ifdef DUMP
		  $vcdplusdeltacycleon;
		  $vcdpluson();
		  $vcdplusmemon(memory.unified_memory);
		`endif

		clock = 1'b0;
		reset = 1'b0;

		// Pulse the reset signal
		$display("@@\n@@\n@@  %t  Asserting System reset......", $realtime);
		reset = 1'b1;
		@(posedge clock);
		@(posedge clock);

		$readmemh("program.mem", memory.unified_memory);

		@(posedge clock);
		@(posedge clock);
		`SD;
		// This reset is at an odd time to avoid the pos & neg clock edges

		reset = 1'b0;
		$display("@@  %t  Deasserting System reset......\n@@\n@@", $realtime);

		wb_fileno = $fopen("writeback.out");
        wb_fileno_time = $fopen("writeback_time.out");
	end

	`ifndef SYNTH_TEST
	// Branch Analysis Data
	integer branch_mispredict_count = 0;

	always @(posedge clock) begin
		if(pipeline_0.disp_rob_nuke[1] && pipeline_0.disp_rob_valid[1] && !reset) begin
			branch_mispredict_count++;
		end
		else if(pipeline_0.disp_rob_nuke[0] && pipeline_0.disp_rob_valid[0] && !reset) begin
			branch_mispredict_count++;
		end
	end

	integer branch_conditional_count = 0;
	integer branch_unconditional_count = 0;
	integer branch_call_count = 0;
	integer branch_ret_count = 0;

	always @(posedge clock) begin
		if (pipeline_0.disp_rob_valid[1] && !reset) begin
			if (pipeline_0.disp_rob_branch_conditional_out[1]) begin
				branch_conditional_count++;
			end
			if (pipeline_0.disp_rob_branch_unconditional_out[1]) begin
				branch_unconditional_count++;
			end
			if (pipeline_0.disp_rob_branch_call_out[1]) begin
				branch_call_count++;
			end
			if (pipeline_0.disp_rob_branch_ret_out[1]) begin
				branch_ret_count++;
			end
		end
		if (pipeline_0.disp_rob_valid[0] && !pipeline_0.disp_rob_nuke[1] && !reset) begin
			if (pipeline_0.disp_rob_branch_conditional_out[0]) begin
				branch_conditional_count++;
			end
			if (pipeline_0.disp_rob_branch_unconditional_out[0]) begin
				branch_unconditional_count++;
			end
			if (pipeline_0.disp_rob_branch_call_out[0]) begin
				branch_call_count++;
			end
			if (pipeline_0.disp_rob_branch_ret_out[0]) begin
				branch_ret_count++;
			end
		end
	end

	// ICache Analysis data
	integer total_request_to_icache_count = 0;
	integer icache_misses_count = 0;
	logic [1:0][QUADWORD-1:3] proc2Icache_addr_splice;

	always_comb begin
		for (int i =0; i<2; ++i) begin
			proc2Icache_addr_splice[i] = pipeline_0.proc2Icache_addr[i][63:3];
		end
	end
	logic [1:0][QUADWORD-1:3] proc2Icache_addr_splice_prev;
	logic flag1, flag2;
	always @(posedge clock) begin

		if(pipeline_0.proc2Icache_addr_valid[1] &&
		(proc2Icache_addr_splice[1] != proc2Icache_addr_splice_prev[1]) && !reset) begin
			total_request_to_icache_count++;
			flag1 = 1'b0;
			if(pipeline_0.proc2Icache_addr_valid[0] &&
			(proc2Icache_addr_splice[1] != proc2Icache_addr_splice_prev[0])) begin
				total_request_to_icache_count++;
				flag2 = 1'b0;
			end
			else if (!flag2) begin
				icache_misses_count++;
				flag2 = 1'b1;
			end
		end
		else if (!flag1 && !reset) begin
			icache_misses_count++;
			flag1 = 1'b1;
		end

		if(pipeline_0.proc2Icache_addr_valid && !reset) begin
			proc2Icache_addr_splice_prev <= proc2Icache_addr_splice;
		end
	end

	// DCache Analysis Data
	integer total_request_to_dcache_count = 0;
	integer total_dcache_hits = 0;

	always @(posedge clock) begin
		for (int i = 0; i < 2; ++i) begin
			if (pipeline_0.lsq_ldb_read_from_dcache[i] && !pipeline_0.cache_ldb_stall_out[i] && !reset) begin
				total_request_to_dcache_count++;
				for (int j = 0; j < 3; ++j) begin
					if (pipeline_0.cache_ldb_valid_out[j] &&
					(pipeline_0.lsq_ldb_prf_to_dcache[i] == pipeline_0.cache_ldb_prf_out[j])) begin
						total_dcache_hits++;
					end
				end
			end
		end
	end

	// LSQ Analysis Data

	`define TRUE_DEPENDENCY         2'b10
	integer total_lsq_forwarding = 0;

	always @(posedge clock) begin
		for (int i=0; i<LDB_SIZE; ++i) begin
			if(pipeline_0.lsq1.ldb.next_state[i] == `TRUE_DEPENDENCY
			&& !pipeline_0.lsq1.ldb.cdb_stall[i] && !reset) begin
				total_lsq_forwarding++;
			end
		end
	end


	// Stall Analysis Data
	integer disp_rs_full_count = 0;
	integer disp_rob_full_count = 0;
	integer lsq_stq_full_count = 0;
	integer lsq_ldb_full_count = 0;

	always @(posedge clock) begin
		if(&pipeline_0.disp_rs_stall && !reset) begin
			disp_rs_full_count++;
		end
		if(&pipeline_0.disp_rob_stall && !reset) begin
			disp_rob_full_count++;
		end
		if(&pipeline_0.lsq_stq_stall && !reset) begin
			lsq_stq_full_count++;
		end
		if(&pipeline_0.lsq_ldb_stall && !reset) begin
			lsq_ldb_full_count++;
		end
	end

	// MSHR Analysis Data

	integer mshr_dcache_full_count = 0;
	integer mshr_icache_full_count = 0;

	always @(posedge clock) begin
		if(&pipeline_0.cache_holder1.mshr2Icache_contr_stall && !reset) begin
			mshr_icache_full_count++;
		end
		if(&pipeline_0.cache_holder1.mshr2Dcache_contr_stall && !reset) begin
			mshr_dcache_full_count++;
		end
	end
	`endif



	// Count the number of posedges and number of instructions completed
	// till simulation ends
	always @(posedge clock or posedge reset) begin
		if (reset) begin
			clock_count         <= `SD 0;
			instr_count         <= `SD 0;
            instr_count_prev    <= `SD -1;
		end
		else begin
			if (pipeline_halt_out) begin
				halted <= `SD 1;
			end
			if (!halted) begin
				clock_count <= `SD (clock_count + 1);
			end
			instr_count <= `SD (instr_count + pipeline_completed_insts[1] + pipeline_completed_insts[0]);
            instr_count_prev <= instr_count_prev_next;
		end
	end


        always @(negedge clock)
        begin
            if(reset)
                $display(	"@@\n@@  %t : System STILL at reset, can't show anything\n@@",
                            $realtime);
            else
            begin
              `SD;
              `SD;

               // print the piepline stuff via c code to the pipeline.out
               print_cycles();

                // print the writeback information to writeback.out
                `ifndef SYNTH_TEST
                if (pipeline.lsq_stq_data_to_dcache_valid) begin
                    $fdisplay(wb_fileno_time, "STORE_ADDR=%d, STORE_DATA=%d, Time=%d",
							  pipeline.lsq_stq_address_to_dcache,
							  pipeline.lsq_stq_data_to_dcache,
							  $time);
                end

                for (int i = 0; i < 3; ++i) begin
                    if (pipeline.cache_ldb_valid_out[i]) begin
                        $fdisplay(wb_fileno_time, "LW_ADDR=%d, PRF=%d, LW_DATA=%d, WAY=%d, Time=%d",
                                        pipeline.cache_ldb_addr_out[i],
										pipeline.cache_ldb_prf_out[i],
                                        pipeline.cache_ldb_data_out[i],
										i,
                                        $time);
                    end
                end

                for (int i = 0; i < 2; ++i) begin
                    if (pipeline.lsq_ldb_addr_to_dcache_valid[i]) begin
                        $fdisplay(wb_fileno_time, "REQ_LW_ADDR=%d, PRF=%d, STALL=%d, WAY=%d, Time=%d",
                                        pipeline.lsq_ldb_address_to_dcache[i],
										pipeline.lsq_ldb_prf_to_dcache[i],
                                        pipeline.cache_ldb_stall_out[i],
										i,
                                        $time);
                    end
                end

                if (pipeline_completed_insts>0) begin
					for (int i = 1; i >= 0; --i) begin
						if (pipeline_commit_wr_en[i] && pipeline_commit_wr_idx[i] != 5'd31) begin
							$fdisplay(	wb_fileno_time, "PC=%x, REG[%d]=%x, PRF=%d, Time=%d",
										pipeline_commit_PC[i],
										pipeline_commit_wr_idx[i],
										pipeline_commit_wr_data[i],
										pipeline_commit_prf[i],
										$time);

							$fdisplay(	wb_fileno, "PC=%x, REG[%d]=%x",
                                    pipeline_commit_PC[i],
                                    pipeline_commit_wr_idx[i],
                                    pipeline_commit_wr_data[i]);
						end
						else if (pipeline_commit_wr_en[i]) begin
							$fdisplay(wb_fileno_time, "PC=%x, ---",pipeline_commit_PC[i]);
							$fdisplay(wb_fileno, "PC=%x, ---",pipeline_commit_PC[i]);
						end
					end
                end
                `endif
			// deal with any halting conditions
			if(pipeline_error_status!=`NO_ERROR)
			begin
				$display(	"@@@ Unified Memory contents hex on left, decimal on right: ");
							show_mem_with_decimal(0,`MEM_64BIT_LINES - 1);
				// 8Bytes per line, 16kB total

				$display("@@  %t : System halted\n@@", $realtime);

				case(pipeline_error_status)
					`HALTED_ON_MEMORY_ERROR:
						$display(	"@@@ System halted on memory error");
					`HALTED_ON_HALT:
						$display(	"@@@ System halted on HALT instruction");
					`HALTED_ON_ILLEGAL:
						$display(	"@@@ System halted on illegal instruction");
					default:
						$display(	"@@@ System halted on unknown error code %x",
									pipeline_error_status);
				endcase
				$display("@@@\n@@");
				show_clk_count;
				$display("Cycles delayed due to store: %d", cycles_delayed_from_store);
				// print_close(); // close the pipe_print output file
                // Wait one clock cycles because last display was right before halt
/*
                $display ("\ntime=%4d\n", $time);
                $display("\n\n===PRF===");
                for (int i=0; i < 96; i++)
                begin
                    $display ("PRF Entry #: %4d ", i,
                            "PRF Data: %4h ", pipeline_0.dispatch_holder1.prf1.prfs[i],
                            "PRF Valid: %4b", pipeline_0.dispatch_holder1.prf1.valid[i]);
                end
                //display all the current RAT enteries
                $display("\n\n===rRAT===");
                for (int i=0; i < 32; i++)
                begin
                    $display ("rRAT Entry Number: %d ", i,
                            "rRAT Entry: %d ", pipeline_0.decode_holder1.rat_holder1.rrat1.rrat_entries[i]);
                end
*/
                //@(negedge clock);
                //@(negedge clock);
				`ifndef SYNTH_TEST
				analysis_file_read =  $fopen("analysis.txt");
				analysis_file =  $fopen("analysis.csv");
				// For reading purposes
				$fdisplay(analysis_file_read, "%-30s = %d", "Instruction count", instr_count-1);
				$fdisplay(analysis_file_read, "%-30s = %f", "CPI", ((clock_count + 1.0) / (instr_count-1)));
				// Cache
				$fdisplay(analysis_file_read, "%-30s = %d", "total_request_to_icache_count", total_request_to_icache_count+2);
				$fdisplay(analysis_file_read, "%-30s = %d", "icache_misses_count", icache_misses_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "total_request_to_dcache_count", total_request_to_dcache_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "total_dcache_hits", total_dcache_hits);
				// Branch
				$fdisplay(analysis_file_read, "%-30s = %d", "branch_conditional_count", branch_conditional_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "branch_unconditional_count", branch_unconditional_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "branch_call_count", branch_call_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "branch_ret_count", branch_ret_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "Number of branch mispredicts", branch_mispredict_count);
				// LSQ
				$fdisplay(analysis_file_read, "%-30s = %d", "total_loads_in_ldb", total_request_to_dcache_count + total_lsq_forwarding);
				$fdisplay(analysis_file_read, "%-30s = %d", "total_lsq_forwarding", total_lsq_forwarding);
				// Full
				$fdisplay(analysis_file_read, "%-30s = %d", "disp_rs_full_count", disp_rs_full_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "disp_rob_full_count", disp_rob_full_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "lsq_stq_full_count", lsq_stq_full_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "lsq_ldb_full_count", lsq_ldb_full_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "mshr_dcache_full_count", mshr_dcache_full_count);
				$fdisplay(analysis_file_read, "%-30s = %d", "mshr_icache_full_count", mshr_icache_full_count);
				//***************************************************************************//
				// For data gathering
				$fdisplay(analysis_file, "%d", instr_count-1);
				$fdisplay(analysis_file, "%f", ((clock_count + 1.0) / (instr_count-1)));
				// Cache
				$fdisplay(analysis_file, "%d", total_request_to_icache_count+2);
				$fdisplay(analysis_file, "%d", icache_misses_count);
				$fdisplay(analysis_file, "%d", total_request_to_dcache_count);
				$fdisplay(analysis_file, "%d", total_dcache_hits);
				// Branch
				$fdisplay(analysis_file, "%d", branch_conditional_count);
				$fdisplay(analysis_file, "%d", branch_unconditional_count);
				$fdisplay(analysis_file, "%d", branch_call_count);
				$fdisplay(analysis_file, "%d", branch_ret_count);
				$fdisplay(analysis_file, "%d", branch_mispredict_count);
				// LSQ
				$fdisplay(analysis_file, "%d", total_request_to_dcache_count + total_lsq_forwarding);
				$fdisplay(analysis_file, "%d", total_lsq_forwarding);
				// Full
				$fdisplay(analysis_file, "%d", disp_rs_full_count);
				$fdisplay(analysis_file, "%d", disp_rob_full_count);
				$fdisplay(analysis_file, "%d", lsq_stq_full_count);
				$fdisplay(analysis_file, "%d", lsq_ldb_full_count);
				$fdisplay(analysis_file, "%d", mshr_dcache_full_count);
				$fdisplay(analysis_file, "%d", mshr_icache_full_count);
                $fclose(analysis_file_read);
				$fclose(analysis_file);
				`endif
				$fclose(wb_fileno);
				$fclose(wb_fileno_time);
				#100 $finish;
			end

		end  // if(reset)
	end

	// Translate IRs into strings for opcodes (for waveform viewer)
	/* For testbench, will need it later
    always_comb begin
		if_instr_str  = get_instr_string(if_IR_out, if_valid_inst_out);
		id_instr_str  = get_instr_string(if_id_IR, if_id_valid_inst);
		ex_instr_str  = get_instr_string(id_ex_IR, id_ex_valid_inst);
		mem_instr_str = get_instr_string(ex_mem_IR, ex_mem_valid_inst);
		wb_instr_str  = get_instr_string(mem_wb_IR, mem_wb_valid_inst);
	end
    */
	function [8*7:0] get_instr_string;
	input [31:0] IR;
	input        instr_valid;
	begin
		if (!instr_valid)
			get_instr_string = "-";
		else if (IR==`NOOP_INST)
			get_instr_string = "nop";
		else
			case (IR[31:26])
				6'h00: get_instr_string = (IR == 32'h555) ? "halt" : "call_pal";
				6'h08: get_instr_string = "lda";
				6'h09: get_instr_string = "ldah";
				6'h0a: get_instr_string = "ldbu";
				6'h0b: get_instr_string = "ldqu";
				6'h0c: get_instr_string = "ldwu";
				6'h0d: get_instr_string = "stw";
				6'h0e: get_instr_string = "stb";
				6'h0f: get_instr_string = "stqu";
				6'h10: // INTA_GRP
				begin
					case (IR[11:5])
						7'h00: get_instr_string = "addl";
						7'h02: get_instr_string = "s4addl";
						7'h09: get_instr_string = "subl";
						7'h0b: get_instr_string = "s4subl";
						7'h0f: get_instr_string = "cmpbge";
						7'h12: get_instr_string = "s8addl";
						7'h1b: get_instr_string = "s8subl";
						7'h1d: get_instr_string = "cmpult";
						7'h20: get_instr_string = "addq";
						7'h22: get_instr_string = "s4addq";
						7'h29: get_instr_string = "subq";
						7'h2b: get_instr_string = "s4subq";
						7'h2d: get_instr_string = "cmpeq";
						7'h32: get_instr_string = "s8addq";
						7'h3b: get_instr_string = "s8subq";
						7'h3d: get_instr_string = "cmpule";
						7'h40: get_instr_string = "addlv";
						7'h49: get_instr_string = "sublv";
						7'h4d: get_instr_string = "cmplt";
						7'h60: get_instr_string = "addqv";
						7'h69: get_instr_string = "subqv";
						7'h6d: get_instr_string = "cmple";
						default: get_instr_string = "invalid";
					endcase
				end
				6'h11: // INTL_GRP
				begin
					case (IR[11:5])
						7'h00: get_instr_string = "and";
						7'h08: get_instr_string = "bic";
						7'h14: get_instr_string = "cmovlbs";
						7'h16: get_instr_string = "cmovlbc";
						7'h20: get_instr_string = "bis";
						7'h24: get_instr_string = "cmoveq";
						7'h26: get_instr_string = "cmovne";
						7'h28: get_instr_string = "ornot";
						7'h40: get_instr_string = "xor";
						7'h44: get_instr_string = "cmovlt";
						7'h46: get_instr_string = "cmovge";
						7'h48: get_instr_string = "eqv";
						7'h61: get_instr_string = "amask";
						7'h64: get_instr_string = "cmovle";
						7'h66: get_instr_string = "cmovgt";
						7'h6c: get_instr_string = "implver";
						default: get_instr_string = "invalid";
					endcase
				end
				6'h12: // INTS_GRP
				begin
					case(IR[11:5])
						7'h02: get_instr_string = "mskbl";
						7'h06: get_instr_string = "extbl";
						7'h0b: get_instr_string = "insbl";
						7'h12: get_instr_string = "mskwl";
						7'h16: get_instr_string = "extwl";
						7'h1b: get_instr_string = "inswl";
						7'h22: get_instr_string = "mskll";
						7'h26: get_instr_string = "extll";
						7'h2b: get_instr_string = "insll";
						7'h30: get_instr_string = "zap";
						7'h31: get_instr_string = "zapnot";
						7'h32: get_instr_string = "mskql";
						7'h34: get_instr_string = "srl";
						7'h36: get_instr_string = "extql";
						7'h39: get_instr_string = "sll";
						7'h3b: get_instr_string = "insql";
						7'h3c: get_instr_string = "sra";
						7'h52: get_instr_string = "mskwh";
						7'h57: get_instr_string = "inswh";
						7'h5a: get_instr_string = "extwh";
						7'h62: get_instr_string = "msklh";
						7'h67: get_instr_string = "inslh";
						7'h6a: get_instr_string = "extlh";
						7'h72: get_instr_string = "mskqh";
						7'h77: get_instr_string = "insqh";
						7'h7a: get_instr_string = "extqh";
						default: get_instr_string = "invalid";
					endcase
				end
				6'h13: // INTM_GRP
				begin
					case (IR[11:5])
						7'h01: get_instr_string = "mull";
						7'h20: get_instr_string = "mulq";
						7'h30: get_instr_string = "umulh";
						7'h40: get_instr_string = "mullv";
						7'h60: get_instr_string = "mulqv";
						default: get_instr_string = "invalid";
					endcase
				end
				6'h14: get_instr_string = "itfp"; // unimplemented
				6'h15: get_instr_string = "fltv"; // unimplemented
				6'h16: get_instr_string = "flti"; // unimplemented
				6'h17: get_instr_string = "fltl"; // unimplemented
				6'h1a: get_instr_string = "jsr";
				6'h1c: get_instr_string = "ftpi";
				6'h20: get_instr_string = "ldf";
				6'h21: get_instr_string = "ldg";
				6'h22: get_instr_string = "lds";
				6'h23: get_instr_string = "ldt";
				6'h24: get_instr_string = "stf";
				6'h25: get_instr_string = "stg";
				6'h26: get_instr_string = "sts";
				6'h27: get_instr_string = "stt";
				6'h28: get_instr_string = "ldl";
				6'h29: get_instr_string = "ldq";
				6'h2a: get_instr_string = "ldll";
				6'h2b: get_instr_string = "ldql";
				6'h2c: get_instr_string = "stl";
				6'h2d: get_instr_string = "stq";
				6'h2e: get_instr_string = "stlc";
				6'h2f: get_instr_string = "stqc";
				6'h30: get_instr_string = "br";
				6'h31: get_instr_string = "fbeq";
				6'h32: get_instr_string = "fblt";
				6'h33: get_instr_string = "fble";
				6'h34: get_instr_string = "bsr";
				6'h35: get_instr_string = "fbne";
				6'h36: get_instr_string = "fbge";
				6'h37: get_instr_string = "fbgt";
				6'h38: get_instr_string = "blbc";
				6'h39: get_instr_string = "beq";
				6'h3a: get_instr_string = "blt";
				6'h3b: get_instr_string = "ble";
				6'h3c: get_instr_string = "blbs";
				6'h3d: get_instr_string = "bne";
				6'h3e: get_instr_string = "bge";
				6'h3f: get_instr_string = "bgt";
				default: get_instr_string = "invalid";
			endcase
		end
	endfunction

endmodule  // module testbench
`default_nettype wire

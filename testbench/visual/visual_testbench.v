/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  visual_testbench.v                                  //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline        //
//                   for the visual debugger                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

extern void initcurses(int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int);
extern void flushpipe();
extern void waitforresponse();
extern void initmem();
extern int get_instr_at_pc(int);
extern int not_valid_pc(int);

parameter ROB_SIZE = 64;
localparam NUM_ARF_ENTRIES = 32;
parameter NUM_PRF_ENTRIES = ROB_SIZE + NUM_ARF_ENTRIES;
localparam PRF_BITS = $clog2(NUM_PRF_ENTRIES);


module testbench();

  // Registers and wires used in the testbench
  reg        clock;
  reg        reset;
  reg [31:0] clock_count;
  reg [31:0] instr_count;

  wire [1:0]  proc2mem_command;
  wire [63:0] proc2mem_addr;
  wire [63:0] proc2mem_data;
  wire [3:0]  mem2proc_response;
  wire [63:0] mem2proc_data;
  wire [3:0]  mem2proc_tag;

  wire [1:0][3:0]               pipeline_completed_insts;
  wire [1:0][3:0]               pipeline_error_status;
  wire [1:0][4:0]               pipeline_commit_wr_idx;
  wire [1:0][63:0]              pipeline_commit_wr_data;
  wire [1:0]                    pipeline_commit_wr_en;
  wire [1:0][63:0]              pipeline_commit_PC;
  wire [1:0][PRF_BITS-1:0]      pipeline_commit_prf;
  wire [1:0]                    pipeline_halt_out;


  wire [63:0] if_NPC_out;
  wire [31:0] if_IR_out;
  wire        if_valid_inst_out;
  wire [63:0] if_id_NPC;
  wire [31:0] if_id_IR;
  wire        if_id_valid_inst;
  wire [63:0] id_ex_NPC;
  wire [31:0] id_ex_IR;
  wire        id_ex_valid_inst;
  wire [63:0] ex_mem_NPC;
  wire [31:0] ex_mem_IR;
  wire        ex_mem_valid_inst;
  wire [63:0] mem_wb_NPC;
  wire [31:0] mem_wb_IR;
  wire        mem_wb_valid_inst;

  //integer i;


	// Instantiate the Pipeline
	pipeline pipeline_0 (// Inputs
					   .clock             (clock),
					   .reset             (reset),
					   .mem2proc_response (mem2proc_response),
					   .mem2proc_data     (mem2proc_data),
					   .mem2proc_tag      (mem2proc_tag),

						// Outputs
					   .proc2mem_command  (proc2mem_command),
					   .proc2mem_addr     (proc2mem_addr),
					   .proc2mem_data     (proc2mem_data),

					   .pipeline_completed_insts(pipeline_completed_insts),
					   .pipeline_error_status(pipeline_error_status),
					   .pipeline_halt_out(pipeline_halt_out),
					   .pipeline_commit_wr_idx(pipeline_commit_wr_idx),
					   .pipeline_commit_wr_en(pipeline_commit_wr_en),
					   .pipeline_commit_PC(pipeline_commit_PC),
                       .pipeline_commit_prf(pipeline_commit_prf)
					  );




                      // Instantiate the Data Memory
  mem memory (// Inputs
            .clk               (clock),
            .proc2mem_command  (proc2mem_command),
            .proc2mem_addr     (proc2mem_addr),
            .proc2mem_data     (proc2mem_data),

             // Outputs

            .mem2proc_response (mem2proc_response),
            .mem2proc_data     (mem2proc_data),
            .mem2proc_tag      (mem2proc_tag)
           );

  // Generate System Clock
  always
  begin
    #(`VERILOG_CLOCK_PERIOD/2.0);
    clock = ~clock;
  end

  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock)
  begin
    if(reset)
    begin
      clock_count <= `SD 0;
      instr_count <= `SD 0;
    end
    else
    begin
      clock_count <= `SD (clock_count + 1);
      instr_count <= `SD (instr_count + pipeline_completed_insts);
    end
  end

  initial
  begin
    clock = 0;
    reset = 0;

    // Call to initialize visual debugger
    // *Note that after this, all stdout output goes to visual debugger*
    // each argument is number of registers/signals for the group
    // (IF, PRED, IF/ID, ID, ID/DISP, DISP, EX/WB, LSQ/WB, CDB, LDB, STQ, Stall, 
        // ROBHEAD, rob2, arf,rat,prf_valid icache,ic_controller,mshr,dcache,dc_cont,Misc)
    initcurses(15,13,9,27,32,19,30,27,11,30,23,21,15,15,32,32,32,6,12,13,12,22,0);

    // Pulse the reset signal
    reset = 1'b1;
    @(posedge clock);
    @(posedge clock);

    // Read program contents into memory array
    $readmemh("program.mem", memory.unified_memory);

    @(posedge clock);
    @(posedge clock);
    `SD;
    // This reset is at an odd time to avoid the pos & neg clock edges
    reset = 1'b0;
  end

  always @(negedge clock)
  begin
    if(!reset)
    begin
      `SD;
      `SD;

      // deal with any halting conditions
      if(pipeline_error_status!=`NO_ERROR)
      begin
        #100
        $display("\nDONE\n");
        waitforresponse();
        flushpipe();
        $finish;
      end

    end
  end

  // This block is where we dump all of the signals that we care about to
  // the visual debugger.  Notice this happens at *every* clock edge.
  integer i;
  always @(clock) begin
    #2;

    // Dump clock and time onto stdout
    $display("c%h%7.0d",clock,clock_count);
    $display("t%8.0f",$time);
    $display("z%h",reset);

    // dump ARF contents
    $write("a");
    for(i = 0; i < 32; i=i+1)
    begin
      //$write("%h", pipeline_0.id_stage_0.regf_0.registers[i]);
    end
    $display("");

    // dump IR information so we can see which instruction
    // is in each stage
    $write("p");

    $write("%h%h ",
            pipeline_0.if_IR_out[1], pipeline_0.if_valid_inst_out[1],
            //pipeline_0.if_id_IR,  pipeline_0.if_id_valid_inst,
            //pipeline_0.id_ex_IR,  pipeline_0.id_ex_valid_inst,
            //pipeline_0.ex_mem_IR, pipeline_0.ex_mem_valid_inst,
            //pipeline_0.mem_wb_IR, pipeline_0.mem_wb_valid_inst
            );

    $display("");

    // Dump interesting register/signal contents onto stdout
    // format is "<reg group prefix><name> <width in hex chars>:<data>"
    // Current register groups (and prefixes) are:
    // f: IF   d: ID   r: DISP   e: EX   m: MEM    w: WB  v: misc. reg
    // g: IF/ID   h: ID/DISP  z: DISP/EX   i: EX/MEM  j: MEM/WB

    // IF signals (15) - prefix 'f'
    $display("f[1]PC 16:%h",           pipeline_0.if_PC_out[1]);
    $display("f[1]NPC 16:%h",          pipeline_0.if_NPC_out[1]);
    $display("f[1]IR 8:%h",            pipeline_0.if_IR_out[1]);
    $display("f[1]if_valid 1:%h",      pipeline_0.if_valid_inst_out[1]);
    $display("f[1]Imem_addr 16:%h",    pipeline_0.proc2Icache_addr[1]);
    $display("f[1]Imem_valid 1:%h",    pipeline_0.proc2Icache_addr_valid[1]);
    $display("f[1]Imem_npc 16:%h",     pipeline_0.proc2Icache_next_pc[1]);
    $display("f");
    $display("f[0]PC 16:%h",           pipeline_0.if_PC_out[0]);
    $display("f[0]NPC 16:%h",          pipeline_0.if_NPC_out[0]);
    $display("f[0]IR 8:%h",            pipeline_0.if_IR_out[0]);
    $display("f[0]if_valid 1:%h",      pipeline_0.if_valid_inst_out[0]);
    $display("f[0]Imem_addr 16:%h",    pipeline_0.proc2Icache_addr[0]);
    $display("f[0]Imem_valid 1:%h",    pipeline_0.proc2Icache_addr_valid[0]);
    $display("f[0]Imem_npc 16:%h",     pipeline_0.proc2Icache_next_pc[0]);

    // PREDCITOR signals (13) - prefix 'k'
    $display("K[1]taken 1:%h",           pipeline_0.fetch1.taken[1]);
    $display("K[1]pred_NPC 16:%h",       pipeline_0.fetch1.pred_next_pc[1]);
    $display("K[1]btb_pred_taken 1:%h",  pipeline_0.fetch1.ph1.btb_predict_taken[1]);
    $display("K[1]btb_NPC 16:%h",        pipeline_0.fetch1.ph1.btb_next_pc[1]);
    $display("K[1]ras_valid 1:%h",       pipeline_0.fetch1.ph1.ras_return_addr_out_valid[1]);
    $display("K[1]ras_addr 16:%h",       pipeline_0.fetch1.ph1.ras_return_addr_out[1]);
    $display("K");
    $display("K[0]taken 1:%h",           pipeline_0.fetch1.taken[0]);
    $display("K[0]pred_NPC 16:%h",       pipeline_0.fetch1.pred_next_pc[0]);
    $display("K[0]btb_pred_taken 1:%h",  pipeline_0.fetch1.ph1.btb_predict_taken[0]);
    $display("K[0]btb_NPC 16:%h",        pipeline_0.fetch1.ph1.btb_next_pc[0]);
    $display("K[0]ras_valid 1:%h",       pipeline_0.fetch1.ph1.ras_return_addr_out_valid[0]);
    $display("K[0]ras_addr 16:%h",       pipeline_0.fetch1.ph1.ras_return_addr_out[0]);


    // IF/ID signals (9) - prefix 'g'
    $display("g[1]PC 16:%h",           pipeline_0.if_id_PC_out[1]);
    $display("g[1]NPC 16:%h",          pipeline_0.if_id_NPC_out[1]);
    $display("g[1]IR 8:%h",            pipeline_0.if_id_IR_out[1]);
    $display("g[1]valid 1:%h",         pipeline_0.if_id_valid_inst_out[1]);
    $display("g");
    $display("g[0]PC 16:%h",           pipeline_0.if_id_PC_out[0]);
    $display("g[0]NPC 16:%h",          pipeline_0.if_id_NPC_out[0]);
    $display("g[0]IR 8:%h",            pipeline_0.if_id_IR_out[0]);
    $display("g[0]valid 1:%h",         pipeline_0.if_id_valid_inst_out[0]);

    // ID signals (27) - prefix 'd'
    $display("d[1]opa_sel 1:%h",       pipeline_0.id_opa_select_out[1]);
    $display("d[1]opb_sel 1:%h",       pipeline_0.id_opb_select_out[1]);
    $display("d[1]dest_reg 2:%h",      pipeline_0.id_arf_idx_out[1]);
    $display("d[1]alu_func 2:%h",      pipeline_0.id_alu_func_out[1]);
    $display("d[1]rd_mem 1:%h",        pipeline_0.id_rd_mem_out[1]);
    $display("d[1]wr_mem 1:%h",        pipeline_0.id_wr_mem_out[1]);
    $display("d[1]cond_br 1:%h",       pipeline_0.id_cond_branch_out[1]);
    $display("d[1]uncond_br 1:%h",     pipeline_0.id_uncond_branch_out[1]);
    $display("d[1]br_call 1:%h",       pipeline_0.id_branch_call[1]);
    $display("d[1]br_ret 1:%h",        pipeline_0.id_branch_ret[1]);
    $display("d[1]halt 1:%h",          pipeline_0.id_halt_out[1]);
    $display("d[1]illegal 1:%h",       pipeline_0.id_illegal_out[1]);
    $display("d[1]valid 1:%h",         pipeline_0.id_valid_inst_out[1]);
    $display("d");
    $display("d[0]opa_sel 1:%h",       pipeline_0.id_opa_select_out[0]);
    $display("d[0]opb_sel 1:%h",       pipeline_0.id_opb_select_out[0]);
    $display("d[0]dest_reg 2:%h",      pipeline_0.id_arf_idx_out[0]);
    $display("d[0]alu_func 2:%h",      pipeline_0.id_alu_func_out[0]);
    $display("d[0]rd_mem 1:%h",        pipeline_0.id_rd_mem_out[0]);
    $display("d[0]wr_mem 1:%h",        pipeline_0.id_wr_mem_out[0]);
    $display("d[0]cond_br 1:%h",       pipeline_0.id_cond_branch_out[0]);
    $display("d[0]uncond_br 1:%h",     pipeline_0.id_uncond_branch_out[0]);
    $display("d[0]br_call 1:%h",       pipeline_0.id_branch_call[0]);
    $display("d[0]br_ret 1:%h",        pipeline_0.id_branch_ret[0]);
    $display("d[0]halt 1:%h",          pipeline_0.id_halt_out[0]);
    $display("d[0]illegal 1:%h",       pipeline_0.id_illegal_out[0]);
    $display("d[0]valid 1:%h",         pipeline_0.id_valid_inst_out[0]);

    // ID/DISP signals (32) - prefix 'h'
    $display("h[1]PC 16:%h",           pipeline_0.id_disp_PC_out[1]);
    $display("h[1]NPC 16:%h",          pipeline_0.id_disp_NPC_out[1]);
    $display("h[1]IR 8:%h",            pipeline_0.id_disp_IR_out[1]);
    $display("h[1]opa_sel 1:%h",       pipeline_0.id_disp_opa_select_out[1]);
    $display("h[1]opb_sel 1:%h",       pipeline_0.id_disp_opb_select_out[1]);
    $display("h[1]dest_reg 2:%h",      pipeline_0.id_disp_arf_idx_out[1]);
    $display("h[1]alu_func 2:%h",      pipeline_0.id_disp_alu_func_out[1]);
    $display("h[1]rd_mem 1:%h",        pipeline_0.id_disp_rd_mem_out[1]);
    $display("h[1]wr_mem 1:%h",        pipeline_0.id_disp_wr_mem_out[1]);
    $display("h[1]cond_br 1:%h",       pipeline_0.id_disp_cond_branch_out[1]);
    $display("h[1]uncond_br 1:%h",     pipeline_0.id_disp_uncond_branch_out[1]);
    $display("h[1]br_call 1:%h",       pipeline_0.id_disp_branch_call[1]);
    $display("h[1]br_ret 1:%h",        pipeline_0.id_disp_branch_ret[1]);
    $display("h[1]halt 1:%h",          pipeline_0.id_disp_halt_out[1]);
    $display("h[1]illegal 1:%h",       pipeline_0.id_disp_illegal_out[1]);
    $display("h[1]valid 1:%h",         pipeline_0.id_disp_valid_inst_out[1]);
    $display("h[0]PC 16:%h",           pipeline_0.id_disp_PC_out[0]);
    $display("h[0]NPC 16:%h",          pipeline_0.id_disp_NPC_out[0]);
    $display("h[0]IR 8:%h",            pipeline_0.id_disp_IR_out[0]);
    $display("h[0]opa_sel 1:%h",       pipeline_0.id_disp_opa_select_out[0]);
    $display("h[0]opb_sel 1:%h",       pipeline_0.id_disp_opb_select_out[0]);
    $display("h[0]dest_reg 2:%h",      pipeline_0.id_disp_arf_idx_out[0]);
    $display("h[0]alu_func 2:%h",      pipeline_0.id_disp_alu_func_out[0]);
    $display("h[0]rd_mem 1:%h",        pipeline_0.id_disp_rd_mem_out[0]);
    $display("h[0]wr_mem 1:%h",        pipeline_0.id_disp_wr_mem_out[0]);
    $display("h[0]cond_br 1:%h",       pipeline_0.id_disp_cond_branch_out[0]);
    $display("h[0]uncond_br 1:%h",     pipeline_0.id_disp_uncond_branch_out[0]);
    $display("h[0]br_call 1:%h",       pipeline_0.id_disp_branch_call[0]);
    $display("h[0]br_ret 1:%h",        pipeline_0.id_disp_branch_ret[0]);
    $display("h[0]halt 1:%h",          pipeline_0.id_disp_halt_out[0]);
    $display("h[0]illegal 1:%h",       pipeline_0.id_disp_illegal_out[0]);
    $display("h[0]valid 1:%h",         pipeline_0.id_disp_valid_inst_out[0]);


    // DISP signals (25) - prefix 'r'
    $display("r[1]PC 16:%h",           pipeline_0.disp_rs_PC[1]);
    $display("r[1]NPC 16:%h",          pipeline_0.disp_rs_NPC[1]);
    $display("r[1]IR 8:%h",            pipeline_0.disp_rs_IR[1]);
    $display("r[1]opa 16:%h",          pipeline_0.disp_rs_opa_out[1]);
    $display("r[1]opb 16:%h",          pipeline_0.disp_rs_opb_out[1]);
    $display("r[1]dest_reg 2:%h",      pipeline_0.disp_rs_dest_out[1]);
    $display("r[1]alu_func 2:%h",      pipeline_0.disp_rs_alu_func_out[1]);
    $display("r[1]cond_br 1:%h",       pipeline_0.disp_rs_cond_branch_out[1]);
    $display("r[1]uncond_br 1:%h",     pipeline_0.disp_rs_uncond_branch_out[1]);
    $display("r");
    $display("r[0]PC 16:%h",           pipeline_0.disp_rs_PC[0]);
    $display("r[0]NPC 16:%h",          pipeline_0.disp_rs_NPC[0]);
    $display("r[0]IR 8:%h",            pipeline_0.disp_rs_IR[0]);
    $display("r[0]opa 16:%h",          pipeline_0.disp_rs_opa_out[0]);
    $display("r[0]opb 16:%h",          pipeline_0.disp_rs_opb_out[0]);
    $display("r[0]dest_reg 2:%h",      pipeline_0.disp_rs_dest_out[0]);
    $display("r[0]alu_func 2:%h",      pipeline_0.disp_rs_alu_func_out[0]);
    $display("r[0]cond_br 1:%h",       pipeline_0.disp_rs_cond_branch_out[0]);
    $display("r[0]uncond_br 1:%h",     pipeline_0.disp_rs_uncond_branch_out[0]);

    // EX/WB signals (5) - prefix 'e'
    $display("e[5]valid 1:%h",             pipeline_0.ex_wb_valid[5]);
    $display("e[5]prf_tag 7:%h",           pipeline_0.ex_wb_pr_tag[5]);
    $display("e[5]results 16:%h",          pipeline_0.ex_wb_results[5]);
    $display("e[5]next_pc 1:%h",           pipeline_0.ex_wb_next_pc[5]);
    $display("e[5]branch_taken 1:%h",      pipeline_0.ex_wb_branch_taken[5]);
    $display("e[4]valid 1:%h",             pipeline_0.ex_wb_valid[4]);
    $display("e[4]prf_tag 7:%h",           pipeline_0.ex_wb_pr_tag[4]);
    $display("e[4]results 16:%h",          pipeline_0.ex_wb_results[4]);
    $display("e[4]next_pc 1:%h",           pipeline_0.ex_wb_next_pc[4]);
    $display("e[4]branch_taken 1:%h",      pipeline_0.ex_wb_branch_taken[4]);
    $display("e[3]valid 1:%h",             pipeline_0.ex_wb_valid[3]);
    $display("e[3]prf_tag 7:%h",           pipeline_0.ex_wb_pr_tag[3]);
    $display("e[3]results 16:%h",          pipeline_0.ex_wb_results[3]);
    $display("e[3]next_pc 1:%h",           pipeline_0.ex_wb_next_pc[3]);
    $display("e[3]branch_taken 1:%h",      pipeline_0.ex_wb_branch_taken[3]);
    $display("e[2]valid 1:%h",             pipeline_0.ex_wb_valid[2]);
    $display("e[2]prf_tag 7:%h",           pipeline_0.ex_wb_pr_tag[2]);
    $display("e[2]results 16:%h",          pipeline_0.ex_wb_results[2]);
    $display("e[2]next_pc 1:%h",           pipeline_0.ex_wb_next_pc[2]);
    $display("e[2]branch_taken 1:%h",      pipeline_0.ex_wb_branch_taken[2]);
    $display("e[1]valid 1:%h",             pipeline_0.ex_wb_valid[1]);
    $display("e[1]prf_tag 7:%h",           pipeline_0.ex_wb_pr_tag[1]);
    $display("e[1]results 16:%h",          pipeline_0.ex_wb_results[1]);
    $display("e[1]next_pc 1:%h",           pipeline_0.ex_wb_next_pc[1]);
    $display("e[1]branch_taken 1:%h",      pipeline_0.ex_wb_branch_taken[1]);
    $display("e[0]valid 1:%h",             pipeline_0.ex_wb_valid[0]);
    $display("e[0]prf_tag 7:%h",           pipeline_0.ex_wb_pr_tag[0]);
    $display("e[0]results 16:%h",          pipeline_0.ex_wb_results[0]);
    $display("e[0]next_pc 1:%h",           pipeline_0.ex_wb_next_pc[0]);
    $display("e[0]branch_taken 1:%h",      pipeline_0.ex_wb_branch_taken[0]);

    // LSQ/WB signals (27) - prefix 'l'
    $display("lstr_valid 1:%h",         pipeline_0.lsq_wb_stored_valid);
    $display("lstr_prf 7:%h",           pipeline_0.lsq_wb_stored_prf);
    $display("lstr_res 16:%h",          pipeline_0.lsq_wb_stored_result);
    $display("l[7]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[7]);
    $display("l[7]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[7]);
    $display("l[7]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[7]);
    $display("l[6]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[6]);
    $display("l[6]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[6]);
    $display("l[6]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[6]);
    $display("l[5]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[5]);
    $display("l[5]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[5]);
    $display("l[5]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[5]);
    $display("l[4]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[4]);
    $display("l[4]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[4]);
    $display("l[4]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[4]);
    $display("l[3]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[3]);
    $display("l[3]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[3]);
    $display("l[3]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[3]);
    $display("l[2]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[2]);
    $display("l[2]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[2]);
    $display("l[2]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[2]);
    $display("l[1]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[1]);
    $display("l[1]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[1]);
    $display("l[1]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[1]);
    $display("l[0]load_valid 1:%h",     pipeline_0.lsq_wb_load_data_valid[0]);
    $display("l[0]load_prf 7:%h",       pipeline_0.lsq_wb_load_data_prf_tag[0]);
    $display("l[0]ld_dt 16:%h",     pipeline_0.lsq_wb_load_data[0]);



    // CDB signals (12) - prefix 'i'
    $display("i[1]valid 1:%h",             pipeline_0.wb_valid_out[1]);
    $display("i[1]prf_tag 7:%h",           pipeline_0.wb_pr_tag_out[1]);
    $display("i[1]results 16:%h",          pipeline_0.wb_result_out[1]);
    $display("i[1]next_pc 1:%h",           pipeline_0.wb_next_pc_out[1]);
    $display("i[1]branch_taken 1:%h",      pipeline_0.wb_branch_taken_out[1]);
    $display("i");
    $display("i[0]valid 1:%h",             pipeline_0.wb_valid_out[0]);
    $display("i[0]prf_tag 7:%h",           pipeline_0.wb_pr_tag_out[0]);
    $display("i[0]results 16:%h",          pipeline_0.wb_result_out[0]);
    $display("i[0]next_pc 1:%h",           pipeline_0.wb_next_pc_out[0]);
    $display("i[0]branch_taken 1:%h",      pipeline_0.wb_branch_taken_out[0]);




    // LDB signals (30) - prefix 'm'
    $display("m[7]valid 1:%h",          pipeline_0.lsq1.load_data_valid[7]);
    $display("m[7]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[7]);
    $display("m[7]data 16:%h",          pipeline_0.lsq1.load_data[7]);
    $display("m[6]valid 1:%h",          pipeline_0.lsq1.load_data_valid[6]);
    $display("m[6]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[6]);
    $display("m[6]data 16:%h",          pipeline_0.lsq1.load_data[6]);
    $display("m[5]valid 1:%h",          pipeline_0.lsq1.load_data_valid[5]);
    $display("m[5]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[5]);
    $display("m[5]data 16:%h",          pipeline_0.lsq1.load_data[5]);
    $display("m[4]valid 1:%h",          pipeline_0.lsq1.load_data_valid[4]);
    $display("m[4]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[4]);
    $display("m[4]data 16:%h",          pipeline_0.lsq1.load_data[4]);
    $display("m[3]valid 1:%h",          pipeline_0.lsq1.load_data_valid[3]);
    $display("m[3]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[3]);
    $display("m[3]data 16:%h",          pipeline_0.lsq1.load_data[3]);
    $display("m[2]valid 1:%h",          pipeline_0.lsq1.load_data_valid[2]);
    $display("m[2]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[2]);
    $display("m[2]data 16:%h",          pipeline_0.lsq1.load_data[2]);
    $display("m[1]valid 1:%h",          pipeline_0.lsq1.load_data_valid[1]);
    $display("m[1]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[1]);
    $display("m[1]data 16:%h",          pipeline_0.lsq1.load_data[1]);
    $display("m[0]valid 1:%h",          pipeline_0.lsq1.load_data_valid[0]);
    $display("m[0]prf_tag 7:%h",        pipeline_0.lsq1.load_data_prf_tag[0]);
    $display("m[0]data 16:%h",          pipeline_0.lsq1.load_data[0]);
    $display("m[1]read_dcache 1:%h",    pipeline_0.lsq1.ldb_read_from_dcache[1]);
    $display("m[0]read_dcache 1:%h",    pipeline_0.lsq1.ldb_read_from_dcache[0]);
    $display("m[1]adr_cac 16:%h",       pipeline_0.lsq1.ldb_address_to_dcache[1]);
    $display("m[1]prf_dcache 7:%h",     pipeline_0.lsq1.ldb_prf_to_dcache[1]);
    $display("m[0]adr_cac 16:%h",       pipeline_0.lsq1.ldb_address_to_dcache[0]);
    $display("m[0]prf_dcache 7:%h",     pipeline_0.lsq1.ldb_prf_to_dcache[0]);


    // STQ signals (23) - prefix 'j'
    $display("jvalid 1:%h",                 pipeline_0.lsq1.stored_valid);
    $display("jstored_prf 7:%h",            pipeline_0.lsq1.stored_prf);
    $display("jstrd_data 16:%h",            pipeline_0.lsq1.stored_result);
    $display("j[1]valid_disp 3:%h",         pipeline_0.lsq1.stq_valid_on_disp[1]);
    $display("j[0]valid_disp 3:%h",         pipeline_0.lsq1.stq_valid_on_disp[0]);
    $display("j[2]addr 13:%h",              pipeline_0.lsq1.stq_address[2]);
    $display("j[2]addr_valid 1:%h",         pipeline_0.lsq1.stq_address_valid[2]);
    $display("j[2]st_dat 16:%h",            pipeline_0.lsq1.stq_data[2]);
    $display("j[2]data_valid 1:%h",         pipeline_0.lsq1.stq_data_valid[2]);
    $display("j[1]addr 13:%h",              pipeline_0.lsq1.stq_address[1]);
    $display("j[1]addr_valid 1:%h",         pipeline_0.lsq1.stq_address_valid[1]);
    $display("j[1]st_dat 16:%h",            pipeline_0.lsq1.stq_data[1]);
    $display("j[0]addr 13:%h",              pipeline_0.lsq1.stq_address[0]);
    $display("j[0]addr_valid 1:%h",         pipeline_0.lsq1.stq_address_valid[0]);
    $display("j[0]st_dat 16:%h",            pipeline_0.lsq1.stq_data[0]);
    $display("j[1]data_valid 1:%h",         pipeline_0.lsq1.stq_data_valid[1]);
    $display("j[0]data_valid 1:%h",         pipeline_0.lsq1.stq_data_valid[0]);
    $display("jhead 3:%h",                  pipeline_0.lsq1.stq_head);
    $display("jtail 3:%h",                  pipeline_0.lsq1.stq_tail);
    $display("jdata_dcache_valid 1:%h",     pipeline_0.lsq1.stq_data_to_dcache_valid);
    $display("jwrite_dcache 1:%h",          pipeline_0.lsq1.stq_write_to_dcache);
    $display("jadr_cac 16:%h",              pipeline_0.lsq1.stq_address_to_dcache);
    $display("jdata_ca 16:%h",              pipeline_0.lsq1.stq_data_to_dcache);


// STALL signals (21) - prefix 'w'
    $display("w[1]if_id 1:%h",     pipeline_0.if_id_stall[1]);
    $display("w[0]if_id 1:%h",     pipeline_0.if_id_stall[0]);
    $display("w[1]id_disp 1:%h",   pipeline_0.id_disp_stall[1]);
    $display("w[0]id_disp 1:%h",   pipeline_0.id_disp_stall[0]);
    $display("w[1]id_rat 1:%h",    pipeline_0.id_rat_stall[1]);
    $display("w[0]id_rat 1:%h",    pipeline_0.id_rat_stall[0]);
    $display("w[1]disp_rs 1:%h",   pipeline_0.disp_rs_stall[1]);
    $display("w[0]disp_rs 1:%h",   pipeline_0.disp_rs_stall[0]);
    $display("w[1]disp_rob 1:%h",  pipeline_0.disp_rob_stall[1]);
    $display("w[0]disp_rob 1:%h",  pipeline_0.disp_rob_stall[0]);
    $display("w[1]ex_mult 1:%h",   pipeline_0.ex_mult_stall_out[1]);
    $display("w[0]ex_mult 1:%h",   pipeline_0.ex_mult_stall_out[0]);
    $display("w[1]wb_cdb 1:%h",    pipeline_0.wb_cdb_stall[1]);
    $display("w[0]wb_cdb 1:%h",    pipeline_0.wb_cdb_stall[0]);
    $display("wcache_stq 1:%h",    pipeline_0.cache_stq_stall);
    $display("w[1]cache_ldb 1:%h", pipeline_0.cache_ldb_stall_out[1]);
    $display("w[0]cache_ldb 1:%h", pipeline_0.cache_ldb_stall_out[0]);
    $display("w[1]lsq_stq 1:%h",   pipeline_0.lsq_stq_stall[1]);
    $display("w[0]lsq_stq 1:%h",   pipeline_0.lsq_stq_stall[0]);
    $display("w[1]lsq_ldb 1:%h",   pipeline_0.lsq_ldb_stall[1]);
    $display("w[0]lsq_ldb 1:%h",   pipeline_0.lsq_ldb_stall[0]);


// ROB HEAD signals (21) - prefix 'o'
    $display("oarch_reg 5:%h",      pipeline_0.dispatch_holder1.rob1.arch_reg[pipeline_0.dispatch_holder1.rob1.head]);
    $display("oprf_reg 7:%h",       pipeline_0.dispatch_holder1.rob1.prf_reg[pipeline_0.dispatch_holder1.rob1.head]);
    $display("opc 16:%h",           pipeline_0.dispatch_holder1.rob1.pc[pipeline_0.dispatch_holder1.rob1.head]);
    $display("onpc 16:%h",          pipeline_0.dispatch_holder1.rob1.next_pc[pipeline_0.dispatch_holder1.rob1.head]);
    $display("obr_actual 1:%h",     pipeline_0.dispatch_holder1.rob1.branch_actual[pipeline_0.dispatch_holder1.rob1.head]);
    $display("obr_addr_wrong 1:%h", pipeline_0.dispatch_holder1.rob1.branch_addr_wrong[pipeline_0.dispatch_holder1.rob1.head]);
    $display("obr_cond 1:%h",       pipeline_0.dispatch_holder1.rob1.branch_conditional[pipeline_0.dispatch_holder1.rob1.head]);
    $display("obr_uncond 1:%h",     pipeline_0.dispatch_holder1.rob1.branch_unconditional[pipeline_0.dispatch_holder1.rob1.head]);
    $display("obr_call 1:%h",       pipeline_0.dispatch_holder1.rob1.branch_call[pipeline_0.dispatch_holder1.rob1.head]);
    $display("obr_return 1:%h",     pipeline_0.dispatch_holder1.rob1.branch_return[pipeline_0.dispatch_holder1.rob1.head]);
    $display("oex_comp 1:%h",       pipeline_0.dispatch_holder1.rob1.execution_complete[pipeline_0.dispatch_holder1.rob1.head]);
    $display("oload 1:%h",          pipeline_0.dispatch_holder1.rob1.load[pipeline_0.dispatch_holder1.rob1.head]);
    $display("ohalt 1:%h",          pipeline_0.dispatch_holder1.rob1.halt[pipeline_0.dispatch_holder1.rob1.head]);
    $display("ostore 1:%h",         pipeline_0.dispatch_holder1.rob1.store[pipeline_0.dispatch_holder1.rob1.head]);
    $display("oillegal 1:%h",       pipeline_0.dispatch_holder1.rob1.illegal[pipeline_0.dispatch_holder1.rob1.head]);

// ROB head+1 signals (21) - prefix 's'
    $display("sarch_reg 5:%h",      pipeline_0.dispatch_holder1.rob1.arch_reg[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sprf_reg 7:%h",       pipeline_0.dispatch_holder1.rob1.prf_reg[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("spc 16:%h",           pipeline_0.dispatch_holder1.rob1.pc[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("snpc 16:%h",          pipeline_0.dispatch_holder1.rob1.next_pc[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sbr_actual 1:%h",     pipeline_0.dispatch_holder1.rob1.branch_actual[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sbr_addr_wrong 1:%h", pipeline_0.dispatch_holder1.rob1.branch_addr_wrong[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sbr_cond 1:%h",       pipeline_0.dispatch_holder1.rob1.branch_conditional[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sbr_uncond 1:%h",     pipeline_0.dispatch_holder1.rob1.branch_unconditional[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sbr_call 1:%h",       pipeline_0.dispatch_holder1.rob1.branch_call[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sbr_return 1:%h",     pipeline_0.dispatch_holder1.rob1.branch_return[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sex_comp 1:%h",       pipeline_0.dispatch_holder1.rob1.execution_complete[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sload 1:%h",          pipeline_0.dispatch_holder1.rob1.load[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("shalt 1:%h",          pipeline_0.dispatch_holder1.rob1.halt[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sstore 1:%h",         pipeline_0.dispatch_holder1.rob1.store[pipeline_0.dispatch_holder1.rob1.head+1]);
    $display("sillegal 1:%h",       pipeline_0.dispatch_holder1.rob1.illegal[pipeline_0.dispatch_holder1.rob1.head+1]);

// ARF (32) - prefix 'u'
   

    for(int i = 0; i < 32; ++i) begin
        $display("u- 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[i]]);
    end

/*
    $display("u00 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[0]]);
    $display("u01 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[1]]);
    $display("u02 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[2]]);
    $display("u03 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[3]]);
    $display("u04 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[4]]);
    $display("u05 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[5]]);
    $display("u06 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[6]]);
    $display("u07 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[7]]);
    $display("u08 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[8]]);
    $display("u09 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[9]]);
    $display("u10 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[10]]);
    $display("u11 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[11]]);
    $display("u12 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[12]]);
    $display("u13 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[13]]);
    $display("u14 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[14]]);
    $display("u15 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[15]]);
    $display("u16 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[16]]);
    $display("u17 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[17]]);
    $display("u18 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[18]]);
    $display("u19 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[19]]);
    $display("u20 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[20]]);
    $display("u21 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[21]]);
    $display("u22 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[22]]);
    $display("u23 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[23]]);
    $display("u24 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[24]]);
    $display("u25 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[25]]);
    $display("u26 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[26]]);
    $display("u27 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[27]]);
    $display("u28 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[28]]);
    $display("u29 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[29]]);
    $display("u30 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[30]]);
    $display("u31 16:%h",   pipeline_0.dispatch_holder1.prf1.prfs[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[31]]);
*/
    
    // ARF (32) - prefix 'x'
    $display("x00 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[0]);
    $display("x01 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[1]);
    $display("x02 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[2]);
    $display("x03 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[3]);
    $display("x04 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[4]);
    $display("x05 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[5]);
    $display("x06 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[6]);
    $display("x07 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[7]);
    $display("x08 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[8]);
    $display("x09 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[9]);
    $display("x10 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[10]);
    $display("x11 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[11]);
    $display("x12 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[12]);
    $display("x13 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[13]);
    $display("x14 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[14]);
    $display("x15 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[15]);
    $display("x16 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[16]);
    $display("x17 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[17]);
    $display("x18 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[18]);
    $display("x19 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[19]);
    $display("x20 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[20]);
    $display("x21 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[21]);
    $display("x22 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[22]);
    $display("x23 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[23]);
    $display("x24 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[24]);
    $display("x25 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[25]);
    $display("x26 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[26]);
    $display("x27 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[27]);
    $display("x28 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[28]);
    $display("x29 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[29]);
    $display("x30 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[30]);
    $display("x31 6:%d",   pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[31]);
    //why won't you work you freakin for loop

    for(int i = 0; i < 32; ++i) begin
        $display("U- 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[i]]);
    end
/*

    $display("U00 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[0]]);
    $display("U01 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[1]]);
    $display("U02 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[2]]);
    $display("U03 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[3]]);
    $display("U04 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[4]]);
    $display("U05 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[5]]);
    $display("U06 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[6]]);
    $display("U07 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[7]]);
    $display("U08 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[8]]);
    $display("U09 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[9]]);
    $display("U10 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[10]]);
    $display("U11 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[11]]);
    $display("U12 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[12]]);
    $display("U13 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[13]]);
    $display("U14 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[14]]);
    $display("U15 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[15]]);
    $display("U16 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[16]]);
    $display("U17 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[17]]);
    $display("U18 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[18]]);
    $display("U19 1:%h",  pipeline_0.dispatch_holder1.prf1.valid[pipeline_0.decode_holder1.rat_holder1.rat1.rat_entries[19]]);
*/
    //Icache (6)
    $display("A[2]mem_dt 16:%h",    pipeline_0.cache_holder1.icachemem_data[2]);
    $display("A[2]valid 1:%h",      pipeline_0.cache_holder1.icachemem_valid[2]);
    $display("A[1]valid 1:%h",      pipeline_0.cache_holder1.icachemem_valid[1]);
    $display("A[1]mem_dt 16:%h",    pipeline_0.cache_holder1.icachemem_data[0]);
    $display("A[0]mem_dt 16:%h",    pipeline_0.cache_holder1.icachemem_data[1]);
    $display("A[0]valid 1:%h",      pipeline_0.cache_holder1.icachemem_valid[0]);
    
    //Icache Controller (12)
    $display("B[2]tag 9:%h",        pipeline_0.cache_holder1.icache_tag[2]);
    $display("B[2]index 4:%h",      pipeline_0.cache_holder1.icache_index[2]);
    $display("B[1]tag 9:%h",        pipeline_0.cache_holder1.icache_tag[1]);
    $display("B[1]index 4:%h",      pipeline_0.cache_holder1.icache_index[1]);
    $display("B[0]tag 9:%h",        pipeline_0.cache_holder1.icache_tag[0]);
    $display("B[0]index 4:%h",      pipeline_0.cache_holder1.icache_index[0]);
    $display("B[1]command 2:%h",    pipeline_0.cache_holder1.icache_cont2Mshr_command[1]);
    $display("B[1]addr 16:%h",      pipeline_0.cache_holder1.icache_cont2Mshr_addr[1]);
    $display("B[1]pref 1:%h",       pipeline_0.cache_holder1.icache_cont2Mshr_pref[1]);
    $display("B[0]command 2:%h",    pipeline_0.cache_holder1.icache_cont2Mshr_command[0]);
    $display("B[0]addr 16:%h",      pipeline_0.cache_holder1.icache_cont2Mshr_addr[0]);
    $display("B[0]pref 1:%h",       pipeline_0.cache_holder1.icache_cont2Mshr_pref[0]);

    
    //MSHR (13)
    $display("Ci_tag 9:%h",        pipeline_0.cache_holder1.mshr2Icache_tag);
    $display("Ci_index 4:%h",      pipeline_0.cache_holder1.mshr2Icache_index);
    $display("Ci_en 1:%h",         pipeline_0.cache_holder1.mshr2Icache_en);
    $display("Ci_data 16:%h",      pipeline_0.cache_holder1.mshr2Icache_data);
    $display("Cd_tag 9:%h",        pipeline_0.cache_holder1.mshr2Dcache_tag);
    $display("Cd_index 4:%h",      pipeline_0.cache_holder1.mshr2Dcache_index);
    $display("Cd_en 1:%h",         pipeline_0.cache_holder1.mshr2Dcache_en);
    $display("Cd_data 16:%h",      pipeline_0.cache_holder1.mshr2Dcache_data);
    $display("Cd_prf 7:%h",        pipeline_0.cache_holder1.mshr2Dcache_prf);
    $display("Cmem_com 2:%h",      pipeline_0.proc2mem_command);
    $display("Cmem_adr 16:%h",     pipeline_0.proc2mem_addr);
    $display("Cmem_dt 16:%h",      pipeline_0.proc2mem_data);
    $display("Cfin 16:%h",         pipeline_0.mshr2proc_finished);
    
    //DCACHE (13)
    $display("D[1]rd_data 16:%h",      pipeline_0.cache_holder1.d_cachemem_data[1]);
    $display("D[1]rd_valid 1:%h",      pipeline_0.cache_holder1.d_cachemem_valid[1]);
    $display("D[1]ev_dt 16:%h",        pipeline_0.cache_holder1.evicted_data[1]);
    $display("D[1]ev_idx 16:%h",       pipeline_0.cache_holder1.evicted_idx[1]);
    $display("D[1]ev_valid 1:%h",      pipeline_0.cache_holder1.evicted_valid[1]);
    $display("D[1]ev_tag 16:%h",       pipeline_0.cache_holder1.evicted_tag[1]);
    $display("D[0]rd_data 16:%h",      pipeline_0.cache_holder1.d_cachemem_data[0]);
    $display("D[0]rd_valid 1:%h",      pipeline_0.cache_holder1.d_cachemem_valid[0]);
    $display("D[0]ev_dt 16:%h",        pipeline_0.cache_holder1.evicted_data[0]);
    $display("D[0]ev_idx 16:%h",       pipeline_0.cache_holder1.evicted_idx[0]);
    $display("D[0]ev_valid 1:%h",      pipeline_0.cache_holder1.evicted_valid[0]);
    $display("D[0]ev_tag 16:%h",       pipeline_0.cache_holder1.evicted_tag[0]);
    
    //DCACHE Controller (13)
    $display("E[3]command 2:%h",       pipeline_0.cache_holder1.dcache_cont2Mshr_command[3]);
    $display("E[3]addr 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_addr[3]);
    $display("E[3]data 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_data[3]);
    $display("E[3]prf 7:%h",           pipeline_0.cache_holder1.dcache_cont2Mshr_prf[3]);
    $display("E[2]command 2:%h",       pipeline_0.cache_holder1.dcache_cont2Mshr_command[2]);
    $display("E[2]addr 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_addr[2]);
    $display("E[2]data 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_data[2]);
    $display("E[2]prf 7:%h",           pipeline_0.cache_holder1.dcache_cont2Mshr_prf[2]);
    $display("E[1]command 2:%h",       pipeline_0.cache_holder1.dcache_cont2Mshr_command[1]);
    $display("E[1]addr 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_addr[1]);
    $display("E[1]data 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_data[1]);
    $display("E[1]prf 7:%h",           pipeline_0.cache_holder1.dcache_cont2Mshr_prf[1]);
    $display("E[0]command 2:%h",       pipeline_0.cache_holder1.dcache_cont2Mshr_command[0]);
    $display("E[0]addr 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_addr[0]);
    $display("E[0]data 16:%h",         pipeline_0.cache_holder1.dcache_cont2Mshr_data[0]);
    $display("E[0]prf 7:%h",           pipeline_0.cache_holder1.dcache_cont2Mshr_prf[0]);
    $display("E[1]en 1:%h",            pipeline_0.cache_holder1.dcache_en[1]);
    $display("E[1]index 4:%h",         pipeline_0.cache_holder1.dcache_index[1]);
    $display("E[1]tag 9:%h",           pipeline_0.cache_holder1.dcache_tag[1]);
    $display("E[0]en 1:%h",            pipeline_0.cache_holder1.dcache_en[0]);
    $display("E[0]index 4:%h",         pipeline_0.cache_holder1.dcache_index[0]);
    $display("E[0]tag 9:%h",           pipeline_0.cache_holder1.dcache_tag[0]);
    
    /*
    // Misc signals (9) - prefix 'v'
    $display("vcompleted 1:%h",     pipeline_0.pipeline_completed_insts);
    $display("vpipe_err 1:%h",      pipeline_error_status);
    $display("vI$_data 16:%h",      pipeline_0.Icache_data_out);
    $display("vI$_valid 1:%h",      pipeline_0.Icache_valid_out);
    $display("vI$_rd_idx 2:%h",     pipeline_0.Icache_rd_idx);
    $display("vI$_rd_tag 6:%h",     pipeline_0.Icache_rd_tag);
    $display("vI$_wr_idx 2:%h",     pipeline_0.Icache_wr_idx);
    $display("vI$_wr_tag 6:%h",     pipeline_0.Icache_wr_tag);
    $display("vI$_wr_en 1:%h",      pipeline_0.Icache_wr_en);
*/

    // must come last
    $display("break");

    // This is a blocking call to allow the debugger to control when we
    // advance the simulation
    waitforresponse();
  end
endmodule

#include "DirectC.h"
#include <curses.h>
#include <stdio.h>
#include <signal.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <time.h>
#include <string.h>

#define PARENT_READ     readpipe[0]
#define CHILD_WRITE     readpipe[1]
#define CHILD_READ      writepipe[0]
#define PARENT_WRITE    writepipe[1]
#define NUM_HISTORY     256
#define NUM_ARF         32
#define NUM_STAGES      5
#define NOOP_INST       0x47ff041f
#define NUM_REG_GROUPS  8

// random variables/stuff
int fd[2], writepipe[2], readpipe[2];
int stdout_save;
int stdout_open;
void signal_handler_IO (int status);
int wait_flag=0;
char done_state;
char echo_data;
FILE *fp;
FILE *fp2;
int setup_registers = 0;
int stop_time;
int done_time = -1;
char time_wrapped = 0;

// Structs to hold information about each register/signal group
typedef struct win_info {
  int height;
  int width;
  int starty;
  int startx;
  int color;
} win_info_t;

typedef struct reg_group {
  WINDOW *reg_win;
  char ***reg_contents;
  char **reg_names;
  int num_regs;
  win_info_t reg_win_info;
} reg_group_t;

// Window pointers for ncurses windows
WINDOW *title_win;
WINDOW *comment_win;
WINDOW *time_win;
WINDOW *sim_time_win;
WINDOW *instr_win;
WINDOW *clock_win;
WINDOW *pipe_win;
WINDOW *if_win;
WINDOW *pred_win;
WINDOW *if_id_win;
WINDOW *id_win;
WINDOW *id_disp_win;
WINDOW *disp_win;
WINDOW *disp_ex_win;
WINDOW *ex_wb_win;
WINDOW *lsq_wb_win;
WINDOW *cdb_win;
WINDOW *ldb_win;
WINDOW *stq_win;
WINDOW *ic_win;
WINDOW *dc_win;
WINDOW *dcc_win;
WINDOW *icc_win;
WINDOW *mshr_win;
WINDOW *stall_win;
WINDOW *rob_win;
WINDOW *rob2_win;
WINDOW *arf_win;
WINDOW *prf_win;
WINDOW *rat_win;
WINDOW *misc_win;

// arrays for register contents and names
int history_num=0;
int num_misc_regs;
int num_if_regs = 0;
int num_pred_regs = 0;
int num_if_id_regs = 0;
int num_id_regs = 0;
int num_id_disp_regs = 0;
int num_disp_regs = 0;
int num_disp_ex_regs = 0;
int num_ex_wb_regs = 0;
int num_lsq_wb_regs = 0;
int num_cdb_regs = 0;
int num_ldb_regs = 0;
int num_stq_regs = 0;
int num_ic_regs = 0;
int num_dc_regs = 0;
int num_dcc_regs = 0;
int num_icc_regs = 0;
int num_mshr_regs = 0;
int num_stall_regs = 0;
int num_rob_regs = 0;
int num_rob2_regs = 0;
int num_arf_regs = 0;
int num_prf_regs = 0;
int num_rat_regs = 0;
char readbuffer[1024];
char **timebuffer;
char **cycles;
char *clocks;
char *resets;
char **inst_contents;
char ***if_contents;
char ***pred_contents;
char ***if_id_contents;
char ***id_contents;
char ***id_disp_contents;
char ***disp_contents;
char ***disp_ex_contents;
char ***ex_wb_contents;
char ***lsq_wb_contents;
char ***cdb_contents;
char ***ldb_contents;
char ***stq_contents;
char ***ic_contents;
char ***dc_contents;
char ***dcc_contents;
char ***icc_contents;
char ***mshr_contents;
char ***stall_contents;
char ***rob_contents;
char ***rob2_contents;
char ***arf_contents;
char ***prf_contents;
char ***rat_contents;
char ***misc_contents;
char **if_reg_names;
char **pred_reg_names;
char **if_id_reg_names;
char **id_reg_names;
char **id_disp_reg_names;
char **disp_reg_names;
char **disp_ex_reg_names;
char **ex_wb_reg_names;
char **lsq_wb_reg_names;
char **cdb_reg_names;
char **ldb_reg_names;
char **stq_reg_names;
char **ic_reg_names;
char **dc_reg_names;
char **dcc_reg_names;
char **icc_reg_names;
char **mshr_reg_names;
char **stall_reg_names;
char **rob_reg_names;
char **rob2_reg_names;
char **arf_reg_names;
char **prf_reg_names;
char **rat_reg_names;
char **misc_reg_names;

char *get_opcode_str(int inst, int valid_inst);
void parse_register(char* readbuf, int reg_num, char*** contents, char** reg_names);
int get_time();


// Helper function for ncurses gui setup
WINDOW *create_newwin(int height, int width, int starty, int startx, int color){
  WINDOW *local_win;
  local_win = newwin(height, width, starty, startx);
  wbkgd(local_win,COLOR_PAIR(color));
  wattron(local_win,COLOR_PAIR(color));
  box(local_win,0,0);
  wrefresh(local_win);
  return local_win;
}

// Function to draw positive edge or negative edge in clock window
void update_clock(char clock_val){
  static char cur_clock_val = 0;
  // Adding extra check on cycles because:
  //  - if the user, right at the beginning of the simulation, jumps to a new
  //    time right after a negative clock edge, the clock won't be drawn
  if((clock_val != cur_clock_val) || strncmp(cycles[history_num],"      0",7) == 1){
    mvwaddch(clock_win,3,7,ACS_VLINE | A_BOLD);
    if(clock_val == 1){

      //we have a posedge
      mvwaddch(clock_win,2,1,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,ACS_ULCORNER | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      mvwaddch(clock_win,4,1,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_LRCORNER | A_BOLD);
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
    } else {

      //we have a negedge
      mvwaddch(clock_win,4,1,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,ACS_LLCORNER | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      mvwaddch(clock_win,2,1,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_URCORNER | A_BOLD);
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
    }
  }
  cur_clock_val = clock_val;
  wrefresh(clock_win);
}

// Function to create and initialize the gui
// Color pairs are (foreground color, background color)
// If you don't like the dark backgrounds, a safe bet is to have
//   COLOR_BLUE/BLACK foreground and COLOR_WHITE background
void setup_gui(FILE *fp){
  initscr();
  if(has_colors()){
    start_color();
    init_pair(1,COLOR_CYAN,COLOR_BLACK);    // shell background
    init_pair(2,COLOR_YELLOW,COLOR_RED);
    init_pair(3,COLOR_RED,COLOR_BLACK);
    init_pair(4,COLOR_YELLOW,COLOR_BLUE);   // title window
    init_pair(5,COLOR_YELLOW,COLOR_BLACK);  // register/signal windows
    init_pair(6,COLOR_RED,COLOR_BLACK);
    init_pair(7,COLOR_MAGENTA,COLOR_BLACK); // pipeline window
  }
  curs_set(0);
  noecho();
  cbreak();
  keypad(stdscr,TRUE);
  wbkgd(stdscr,COLOR_PAIR(1));
  wrefresh(stdscr);
  int pipe_width=0;

  //instantiate the title window at top of screen
  title_win = create_newwin(3,COLS,0,0,4);
  mvwprintw(title_win,1,1,"PIPELINE SIMULATION");
  mvwprintw(title_win,1,COLS-22,"GROUP 1");
  wrefresh(title_win);

  //instantiate time window at right hand side of screen
  time_win = create_newwin(3,10,8,COLS-10,5);
  mvwprintw(time_win,0,3,"TIME");
  wrefresh(time_win);

  //instantiate a sim time window which states the actual simlator time
  sim_time_win = create_newwin(3,10,11,COLS-10,5);
  mvwprintw(sim_time_win,0,1,"SIM TIME");
  wrefresh(sim_time_win);

  //instantiate a window to show which clock edge this is
  clock_win = create_newwin(6,15,8,COLS-25,5);
  mvwprintw(clock_win,0,5,"CLOCK");
  mvwprintw(clock_win,1,1,"cycle:");
  update_clock(0);
  wrefresh(clock_win);

  /*
  // instantiate a window for the ARF on the right side
  arf_win = create_newwin(34,25,14,COLS-25,5);
  mvwprintw(arf_win,0,13,"ARF");
  int i=0;
  char tmp_buf[32];
  for (; i < NUM_ARF; i++) {
    sprintf(tmp_buf, "r%02d: ", i);
    mvwprintw(arf_win,i+1,1,tmp_buf);
  }
  wrefresh(arf_win);

  //instantiate window to visualize instructions in pipeline below title
  pipe_win = create_newwin(5,COLS,3,0,7);
  pipe_width = COLS/6;
  mvwprintw(pipe_win,0,(COLS-8)/2,"PIPELINE");
  wattron(pipe_win,A_UNDERLINE);
  mvwprintw(pipe_win,1,pipe_width-2,"IF");
  mvwprintw(pipe_win,1,2*pipe_width-2,"ID");
  mvwprintw(pipe_win,1,3*pipe_width-2,"EX");
  mvwprintw(pipe_win,1,4*pipe_width-3,"MEM");
  mvwprintw(pipe_win,1,5*pipe_width-3,"WB");
  wattroff(pipe_win,A_UNDERLINE);
  wrefresh(pipe_win);
*/
  //instantiate window to visualize IF stage (including IF/ID)
  if_win = create_newwin((num_if_regs+2),32,8,0,1);
  mvwprintw(if_win,0,10,"IF STAGE");
  wrefresh(if_win);

  //instantiate window to visualize predictor
  pred_win = create_newwin((num_pred_regs+2),32,8+(num_if_regs+2),0,1);
  mvwprintw(pred_win,0,10,"PREDICTOR");
  wrefresh(pred_win);
  
  //instantiate window to visualize IF/ID signals
  if_id_win = create_newwin((num_if_id_regs+2),30,8+(num_if_regs+num_pred_regs+4),0,1);
  mvwprintw(if_id_win,0,12,"IF/ID");
  wrefresh(if_id_win);

  //instantiate a window to visualize ID stage
  id_win = create_newwin((num_id_regs+2),30,8,32,1);
  mvwprintw(id_win,0,10,"ID STAGE");
  wrefresh(id_win);

  //instantiate a window to visualize ID/DISP signals
  id_disp_win = create_newwin((num_id_disp_regs+2),30,8,62,1);
  mvwprintw(id_disp_win,0,12,"ID/DISP");
  wrefresh(id_disp_win);

  //instantiate a window to visualize DISP signals
  disp_win = create_newwin((num_disp_regs+2),30,8+(num_id_regs+2),32,1);
  mvwprintw(disp_win,0,12,"DISP STAGE");
  wrefresh(disp_win);
  
  //instantiate a window to visualize EX/WB 
  ex_wb_win = create_newwin((num_ex_wb_regs+2),30,8,122,1);
  mvwprintw(ex_wb_win,0,10,"EX/WB");
  wrefresh(ex_wb_win);

  //instantiate a window to visualize LSQ/WB 
  lsq_wb_win = create_newwin((num_lsq_wb_regs+2),30,8+(num_ex_wb_regs+2),122,1);
  mvwprintw(lsq_wb_win,0,10,"LSQ/WB");
  wrefresh(lsq_wb_win);
  
  //instantiate a window to visualize CDB
  cdb_win = create_newwin((num_cdb_regs+2),30,8+(num_if_regs+num_pred_regs+num_if_id_regs+6),0,5);
  mvwprintw(cdb_win,0,12,"CDB");
  wrefresh(cdb_win);

  //instantiate a window to visualize LDB
  ldb_win = create_newwin((num_ldb_regs+2),30,8,92,1);
  mvwprintw(ldb_win,0,10,"LDB");
  wrefresh(ldb_win);

  //instantiate a window to visualize STQ
  stq_win = create_newwin((num_stq_regs+2),30,8+(num_ldb_regs+2),92,1);
  mvwprintw(stq_win,0,12,"STQ");
  wrefresh(stq_win);

  //instantiate a window to visualize Icache
  ic_win = create_newwin((num_ic_regs+2),30,8,152,1);
  mvwprintw(ic_win,0,12,"I-CACHE");
  wrefresh(ic_win);
  
  //instantiate a window to visualize Icache
  icc_win = create_newwin((num_icc_regs+2),30,8+(num_ic_regs+2),152,1);
  mvwprintw(icc_win,0,12,"ICACHE CONTROLLER");
  wrefresh(icc_win);
  
  //instantiate a window to visualize MSHR stage
  mshr_win = create_newwin((num_mshr_regs+2),30,8+(num_ic_regs+num_icc_regs+4),152,1);
  mvwprintw(mshr_win,0,10,"MSHR");
  wrefresh(mshr_win);
  
  //instantiate a window to visualize Icache
  dc_win = create_newwin((num_dc_regs+2),30,8+(num_ic_regs+num_icc_regs+num_mshr_regs+6),152,1);
  mvwprintw(dc_win,0,12,"D-CACHE");
  wrefresh(dc_win);
  
  //instantiate a window to visualize Icache
  dcc_win = create_newwin((num_dcc_regs+2),30,8,182,1);
  mvwprintw(dcc_win,0,12,"DCACHE CONTROLLER");
  wrefresh(dcc_win);
  
  //instantiate a window to visualize STALL stage
  stall_win = create_newwin((num_stall_regs+2),30,LINES-5-(num_stall_regs+2),62,5);
  mvwprintw(stall_win,0,10,"STALLS");
  wrefresh(stall_win);

  //instantiate a window to visualize ROB HEAD
  rob_win = create_newwin((num_rob_regs+2),30,LINES-5-(num_rob_regs+2),COLS-60,5);
  mvwprintw(rob_win,0,10,"ROB HEAD");
  wrefresh(rob_win);
  
  //instantiate a window to visualize ROB2 HEAD
  rob2_win = create_newwin((num_rob2_regs+2),30,LINES-5-(num_rob2_regs+2),COLS-30,5);
  mvwprintw(rob2_win,0,10,"ROB HEAD 2");
  wrefresh(rob2_win);
  
  //instantiate a window to visualize ARF
  arf_win = create_newwin((num_arf_regs+2),30,14,COLS-30,5);
  mvwprintw(arf_win,0,10,"ARF");
  wrefresh(arf_win);
  
  //instantiate a window to visualize PRF window
  prf_win = create_newwin((num_prf_regs+2),7,14,COLS-37,5);
  mvwprintw(prf_win,0,1,"valid");
  wrefresh(prf_win);
  
  //instantiate a window to visualize RAT
  rat_win = create_newwin((num_rat_regs+2),8,14,COLS-45,5);
  mvwprintw(rat_win,0,2,"RAT");
  wrefresh(rat_win);
  
  //instantiate an instructional window to help out the user some
  instr_win = create_newwin(7,30,8+(num_if_regs+num_pred_regs+num_if_id_regs+num_cdb_regs+8),0,7);
  mvwprintw(instr_win,0,9,"INSTRUCTIONS");
  wattron(instr_win,COLOR_PAIR(7));
  mvwaddstr(instr_win,1,1,"'n'   -> Next clock edge");
  mvwaddstr(instr_win,2,1,"'b'   -> Previous clock edge");
  mvwaddstr(instr_win,3,1,"'c/g' -> Goto specified time");
  mvwaddstr(instr_win,4,1,"'r'   -> Run to end of sim");
  mvwaddstr(instr_win,5,1,"'q'   -> Quit Simulator");
  wrefresh(instr_win);
 /* 
  // instantiate window to visualize misc regs/wires
  misc_win = create_newwin(7,COLS-25-30,LINES-7,30,5);
  mvwprintw(misc_win,0,(COLS-30-30)/2-6,"MISC SIGNALS");
  wrefresh(misc_win);
*/
  refresh();
}

// This function updates all of the signals being displayed with the values
// from time history_num_in (this is the index into all of the data arrays).
// If the value changed from what was previously display, the signal has its
// display color inverted to make it pop out.
void parsedata(int history_num_in){
  static int old_history_num_in=0;
  static int old_head_position=0;
  static int old_tail_position=0;
  int i=0;
  int data_counter=0;
  char *opcode;
  int tmp=0;
  int tmp_val=0;
  char tmp_buf[32];
  int pipe_width = COLS/6;

  // Handle updating resets
  if (resets[history_num_in]) {
    wattron(title_win,A_REVERSE);
    mvwprintw(title_win,1,(COLS/2)-3,"RESET");
    wattroff(title_win,A_REVERSE);
  }
  else if (done_time != 0 && (history_num_in == done_time)) {
    wattron(title_win,A_REVERSE);
    mvwprintw(title_win,1,(COLS/2)-3,"DONE ");
    wattroff(title_win,A_REVERSE);
  }
  else
    mvwprintw(title_win,1,(COLS/2)-3,"     ");
  wrefresh(title_win);

  // Handle updating the pipeline window
  for(i=0; i < NUM_STAGES; i++) {
    strncpy(tmp_buf,inst_contents[history_num_in]+i*9,8);
    tmp_buf[9] = '\0';
    sscanf(tmp_buf,"%8x", &tmp_val);
    tmp = (int)inst_contents[history_num_in][8+(i*9)] - (int)'0';
    opcode = get_opcode_str(tmp_val, tmp);

    // clear string and overwrite
    mvwprintw(pipe_win,2,pipe_width*(i+1)-2-5,"          ");
    if (strncmp(tmp_buf,"xxxxxxxx",8) == 0)
      mvwaddnstr(pipe_win,2,pipe_width*(i+1)-2-4,tmp_buf,8);
    else
      mvwaddstr(pipe_win,2,pipe_width*(i+1)-2-(strlen(opcode)/2),opcode);
    if (tmp==0 || tmp==((int)'x'-(int)'0'))
      mvwprintw(pipe_win,3,pipe_width*(i+1)-2,"I");
    else
      mvwprintw(pipe_win,3,pipe_width*(i+1)-2,"V");

  }
  wrefresh(pipe_win);
/*
  // Handle updating the ARF window
  for (i=0; i < NUM_ARF; i++) {
    if (strncmp(arf_contents[history_num_in]+i*16,
                arf_contents[old_history_num_in]+i*16,16))
      wattron(arf_win, A_REVERSE);
    else
      wattroff(arf_win, A_REVERSE);
    mvwaddnstr(arf_win,i+1,6,arf_contents[history_num_in]+i*16,16);
  }
  wrefresh(arf_win);

  // Handle updating the RAT window
  for (i=0; i < NUM_RAT; i++) {
    if (strncmp(rat_contents[history_num_in]+i*16,
                rat_contents[old_history_num_in]+i*16,16))
      wattron(rat_win, A_REVERSE);
    else
      wattroff(rat_win, A_REVERSE);
    mvwaddnstr(rat_win,i+1,6,rat_contents[history_num_in]+i*16,16);
  }
  wrefresh(rat_win);
*/
  // Handle updating the IF window
  for(i=0;i<num_if_regs;i++){
    if (strcmp(if_contents[history_num_in][i],
                if_contents[old_history_num_in][i]))
      wattron(if_win, A_REVERSE);
    else
      wattroff(if_win, A_REVERSE);
    mvwaddstr(if_win,i+1,strlen(if_reg_names[i])+3,if_contents[history_num_in][i]);
  }
  wrefresh(if_win);

  // Handle updating the PREDICTOR window
  for(i=0;i<num_pred_regs;i++){
    if (strcmp(pred_contents[history_num_in][i],
                pred_contents[old_history_num_in][i]))
      wattron(pred_win, A_REVERSE);
    else
      wattroff(pred_win, A_REVERSE);
    mvwaddstr(pred_win,i+1,strlen(pred_reg_names[i])+3,pred_contents[history_num_in][i]);
  }
  wrefresh(pred_win);
  
  // Handle updating the IF/ID window
  for(i=0;i<num_if_id_regs;i++){
    if (strcmp(if_id_contents[history_num_in][i],
                if_id_contents[old_history_num_in][i]))
      wattron(if_id_win, A_REVERSE);
    else
      wattroff(if_id_win, A_REVERSE);
    mvwaddstr(if_id_win,i+1,strlen(if_id_reg_names[i])+3,if_id_contents[history_num_in][i]);
  }
  wrefresh(if_id_win);

  // Handle updating the ID window
  for(i=0;i<num_id_regs;i++){
    if (strcmp(id_contents[history_num_in][i],
                id_contents[old_history_num_in][i]))
      wattron(id_win, A_REVERSE);
    else
      wattroff(id_win, A_REVERSE);
    mvwaddstr(id_win,i+1,strlen(id_reg_names[i])+3,id_contents[history_num_in][i]);
  }
  wrefresh(id_win);


  // Handle updating the ID/DISP window
  for(i=0;i<num_id_disp_regs;i++){
    if (strcmp(id_disp_contents[history_num_in][i],
                id_disp_contents[old_history_num_in][i]))
      wattron(id_disp_win, A_REVERSE);
    else
      wattroff(id_disp_win, A_REVERSE);
    mvwaddstr(id_disp_win,i+1,strlen(id_disp_reg_names[i])+3,id_disp_contents[history_num_in][i]);
  }
  wrefresh(id_disp_win);

  // Handle updating the DISP window
  for(i=0;i<num_disp_regs;i++){
    if (strcmp(disp_contents[history_num_in][i],
                disp_contents[old_history_num_in][i]))
      wattron(disp_win, A_REVERSE);
    else
      wattroff(disp_win, A_REVERSE);
    mvwaddstr(disp_win,i+1,strlen(disp_reg_names[i])+3,disp_contents[history_num_in][i]);
  }
  wrefresh(disp_win);
  
  // Handle updating the EX/WB window
  for(i=0;i<num_ex_wb_regs;i++){
    if (strcmp(ex_wb_contents[history_num_in][i],
                ex_wb_contents[old_history_num_in][i]))
      wattron(ex_wb_win, A_REVERSE);
    else
      wattroff(ex_wb_win, A_REVERSE);
    mvwaddstr(ex_wb_win,i+1,strlen(ex_wb_reg_names[i])+3,ex_wb_contents[history_num_in][i]);
  }
  wrefresh(ex_wb_win);

  // Handle updating the LSQ/WB window
  for(i=0;i<num_lsq_wb_regs;i++){
    if (strcmp(lsq_wb_contents[history_num_in][i],
                lsq_wb_contents[old_history_num_in][i]))
      wattron(lsq_wb_win, A_REVERSE);
    else
      wattroff(lsq_wb_win, A_REVERSE);
    mvwaddstr(lsq_wb_win,i+1,strlen(lsq_wb_reg_names[i])+3,lsq_wb_contents[history_num_in][i]);
  }
  wrefresh(lsq_wb_win);
  
  // Handle updating the CDB window
  for(i=0;i<num_cdb_regs;i++){
    if (strcmp(cdb_contents[history_num_in][i],
                cdb_contents[old_history_num_in][i]))
      wattron(cdb_win, A_REVERSE);
    else
      wattroff(cdb_win, A_REVERSE);
    mvwaddstr(cdb_win,i+1,strlen(cdb_reg_names[i])+3,cdb_contents[history_num_in][i]);
  }
  wrefresh(cdb_win);

  // Handle updating the LDB window
  for(i=0;i<num_ldb_regs;i++){
    if (strcmp(ldb_contents[history_num_in][i],
                ldb_contents[old_history_num_in][i]))
      wattron(ldb_win, A_REVERSE);
    else
      wattroff(ldb_win, A_REVERSE);
    mvwaddstr(ldb_win,i+1,strlen(ldb_reg_names[i])+3,ldb_contents[history_num_in][i]);
  }
  wrefresh(ldb_win);

  // Handle updating the STQ window
  for(i=0;i<num_stq_regs;i++){
    if (strcmp(stq_contents[history_num_in][i],
                stq_contents[old_history_num_in][i]))
      wattron(stq_win, A_REVERSE);
    else
      wattroff(stq_win, A_REVERSE);
    mvwaddstr(stq_win,i+1,strlen(stq_reg_names[i])+3,stq_contents[history_num_in][i]);
  }
  wrefresh(stq_win);

  // Handle updating the Dcache window
  for(i=0;i<num_dc_regs;i++){
    if (strcmp(dc_contents[history_num_in][i],
                dc_contents[old_history_num_in][i]))
      wattron(dc_win, A_REVERSE);
    else
      wattroff(dc_win, A_REVERSE);
    mvwaddstr(dc_win,i+1,strlen(dc_reg_names[i])+3,dc_contents[history_num_in][i]);
  }
  wrefresh(dc_win);
  
  // Handle updating the Icache window
  for(i=0;i<num_ic_regs;i++){
    if (strcmp(ic_contents[history_num_in][i],
                ic_contents[old_history_num_in][i]))
      wattron(ic_win, A_REVERSE);
    else
      wattroff(ic_win, A_REVERSE);
    mvwaddstr(ic_win,i+1,strlen(ic_reg_names[i])+3,ic_contents[history_num_in][i]);
  }
  wrefresh(ic_win);
  
  // Handle updating the Icache window
  for(i=0;i<num_icc_regs;i++){
    if (strcmp(icc_contents[history_num_in][i],
                icc_contents[old_history_num_in][i]))
      wattron(icc_win, A_REVERSE);
    else
      wattroff(icc_win, A_REVERSE);
    mvwaddstr(icc_win,i+1,strlen(icc_reg_names[i])+3,icc_contents[history_num_in][i]);
  }
  wrefresh(icc_win);
  
  // Handle updating the Icache window
  for(i=0;i<num_dcc_regs;i++){
    if (strcmp(dcc_contents[history_num_in][i],
                dcc_contents[old_history_num_in][i]))
      wattron(dcc_win, A_REVERSE);
    else
      wattroff(dcc_win, A_REVERSE);
    mvwaddstr(dcc_win,i+1,strlen(dcc_reg_names[i])+3,dcc_contents[history_num_in][i]);
  }
  wrefresh(dcc_win);
  
  // Handle updating the MSHR window
  for(i=0;i<num_mshr_regs;i++){
    if (strcmp(mshr_contents[history_num_in][i],
                mshr_contents[old_history_num_in][i]))
      wattron(mshr_win, A_REVERSE);
    else
      wattroff(mshr_win, A_REVERSE);
    mvwaddstr(mshr_win,i+1,strlen(mshr_reg_names[i])+3,mshr_contents[history_num_in][i]);
  }
  wrefresh(mshr_win);
  
  // Handle updating the STALL window
  for(i=0;i<num_stall_regs;i++){
    if (strcmp(stall_contents[history_num_in][i],
                stall_contents[old_history_num_in][i]))
      wattron(stall_win, A_REVERSE);
    else
      wattroff(stall_win, A_REVERSE);
    mvwaddstr(stall_win,i+1,strlen(stall_reg_names[i])+3,stall_contents[history_num_in][i]);
  }
  wrefresh(stall_win);
  
  // Handle updating the ROB HEAD window
  for(i=0;i<num_rob_regs;i++){
    if (strcmp(rob_contents[history_num_in][i],
                rob_contents[old_history_num_in][i]))
      wattron(rob_win, A_REVERSE);
    else
      wattroff(rob_win, A_REVERSE);
    mvwaddstr(rob_win,i+1,strlen(rob_reg_names[i])+3,rob_contents[history_num_in][i]);
  }
  wrefresh(rob_win);

  // Handle updating the ROB2 HEAD window
  for(i=0;i<num_rob2_regs;i++){
    if (strcmp(rob2_contents[history_num_in][i],
                rob2_contents[old_history_num_in][i]))
      wattron(rob2_win, A_REVERSE);
    else
      wattroff(rob2_win, A_REVERSE);
    mvwaddstr(rob2_win,i+1,strlen(rob2_reg_names[i])+3,rob2_contents[history_num_in][i]);
  }
  wrefresh(rob2_win);
  
  // Handle updating the ARF window
  for(i=0;i<num_arf_regs;i++){
    if (strcmp(arf_contents[history_num_in][i],
                arf_contents[old_history_num_in][i]))
      wattron(arf_win, A_REVERSE);
    else
      wattroff(arf_win, A_REVERSE);
    mvwaddstr(arf_win,i+1,strlen(arf_reg_names[i])+3,arf_contents[history_num_in][i]);
  }
  wrefresh(arf_win);
  
  // Handle updating the ARF window
  for(i=0;i<num_prf_regs;i++){
    if (strcmp(prf_contents[history_num_in][i],
                prf_contents[old_history_num_in][i]))
      wattron(prf_win, A_REVERSE);
    else
      wattroff(prf_win, A_REVERSE);
    mvwaddstr(prf_win,i+1,strlen(prf_reg_names[i])+3,prf_contents[history_num_in][i]);
  }
  wrefresh(prf_win);
  
  // Handle updating the RAT window
  for(i=0;i<num_rat_regs;i++){
    if (strcmp(rat_contents[history_num_in][i],
                rat_contents[old_history_num_in][i]))
      wattron(rat_win, A_REVERSE);
    else
      wattroff(rat_win, A_REVERSE);
    mvwaddstr(rat_win,i+1,strlen(rat_reg_names[i])+3,rat_contents[history_num_in][i]);
  }
  wrefresh(rat_win);
  
  // Handle updating the misc. window
  int row=1,col=1;
  for (i=0;i<num_misc_regs;i++){
    if (strcmp(misc_contents[history_num_in][i],
                misc_contents[old_history_num_in][i]))
      wattron(misc_win, A_REVERSE);
    else
      wattroff(misc_win, A_REVERSE);
    

    mvwaddstr(misc_win,(i%5)+1,((i/5)*30)+strlen(misc_reg_names[i])+3,misc_contents[history_num_in][i]);
    if ((++row)>6) {
      row=1;
      col+=30;
    }
  }
  wrefresh(misc_win);

  //update the time window
  mvwaddstr(time_win,1,1,timebuffer[history_num_in]);
  wrefresh(time_win);

  //update to the correct clock edge for this history
  mvwaddstr(clock_win,1,7,cycles[history_num_in]);
  update_clock(clocks[history_num_in]);

  //save the old history index to check for changes later
  old_history_num_in = history_num_in;
}

// Parse a line of data output from the testbench
int processinput(){
  static int byte_num = 0;
  static int if_reg_num = 0;
  static int pred_reg_num = 0;
  static int if_id_reg_num = 0;
  static int id_reg_num = 0;
  static int id_disp_reg_num = 0;
  static int disp_reg_num = 0;
  static int ex_wb_reg_num = 0;
  static int lsq_wb_reg_num = 0;
  static int cdb_reg_num = 0;
  static int ldb_reg_num = 0;
  static int stq_reg_num = 0;
  static int ic_reg_num = 0;
  static int dc_reg_num = 0;
  static int icc_reg_num = 0;
  static int dcc_reg_num = 0;
  static int mshr_reg_num = 0;
  static int stall_reg_num = 0;
  static int rob_reg_num = 0;
  static int rob2_reg_num = 0;
  static int arf_reg_num = 0;
  static int prf_reg_num = 0;
  static int rat_reg_num = 0;
  static int misc_reg_num = 0;
  int tmp_len;
  char name_buf[32];
  char val_buf[32];

  //get rid of newline character
  readbuffer[strlen(readbuffer)-1] = 0;

  if(strncmp(readbuffer,"t",1) == 0){

    //We are getting the timestamp
    strcpy(timebuffer[history_num],readbuffer+1);
  }else if(strncmp(readbuffer,"c",1) == 0){

    //We have a clock edge/cycle count signal
    if(strncmp(readbuffer+1,"0",1) == 0)
      clocks[history_num] = 0;
    else
      clocks[history_num] = 1;

    // grab clock count (for some reason, first clock count sent is
    // too many digits, so check for this)
    strncpy(cycles[history_num],readbuffer+2,7);
    if (strncmp(cycles[history_num],"       ",7) == 0)
      cycles[history_num][6] = '0';
    
  }else if(strncmp(readbuffer,"z",1) == 0){
    
    // we have a reset signal
    if(strncmp(readbuffer+1,"0",1) == 0)
      resets[history_num] = 0;
    else
      resets[history_num] = 1;

  }else if(strncmp(readbuffer,"a",1) == 0){
    // We are getting ARF registers
    strcpy(arf_contents[history_num], readbuffer+1);

  }else if(strncmp(readbuffer,"p",1) == 0){
    // We are getting information about which instructions are in each stage
    strcpy(inst_contents[history_num], readbuffer+1);

  }else if(strncmp(readbuffer,"f",1) == 0){
    // We are getting an IF register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, if_reg_num, if_contents, if_reg_names);
      mvwaddstr(if_win,if_reg_num+1,1,if_reg_names[if_reg_num]);
      waddstr(if_win, ": ");
      wrefresh(if_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(if_contents[history_num][if_reg_num],val_buf);
    }

    if_reg_num++;
  }else if(strncmp(readbuffer,"K",1) == 0){
    // We are getting an PREDICTOR register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, pred_reg_num, pred_contents, pred_reg_names);
      mvwaddstr(pred_win,pred_reg_num+1,1,pred_reg_names[pred_reg_num]);
      waddstr(pred_win, ": ");
      wrefresh(pred_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(pred_contents[history_num][pred_reg_num],val_buf);
    }

    pred_reg_num++;
  }else if(strncmp(readbuffer,"g",1) == 0){
    // We are getting an IF/ID register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, if_id_reg_num, if_id_contents, if_id_reg_names);
      mvwaddstr(if_id_win,if_id_reg_num+1,1,if_id_reg_names[if_id_reg_num]);
      waddstr(if_id_win, ": ");
      wrefresh(if_id_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(if_id_contents[history_num][if_id_reg_num],val_buf);
    }

    if_id_reg_num++;
  }else if(strncmp(readbuffer,"d",1) == 0){
    // We are getting an ID register
    
    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, id_reg_num, id_contents, id_reg_names);
      mvwaddstr(id_win,id_reg_num+1,1,id_reg_names[id_reg_num]);
      waddstr(id_win, ": ");
      wrefresh(id_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(id_contents[history_num][id_reg_num],val_buf);
    }

    id_reg_num++;
  }else if(strncmp(readbuffer,"h",1) == 0){
    // We are getting an ID/DISP register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, id_disp_reg_num, id_disp_contents, id_disp_reg_names);
      mvwaddstr(id_disp_win,id_disp_reg_num+1,1,id_disp_reg_names[id_disp_reg_num]);
      waddstr(id_disp_win, ": ");
      wrefresh(id_disp_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(id_disp_contents[history_num][id_disp_reg_num],val_buf);
    }

    id_disp_reg_num++;
  }else if(strncmp(readbuffer,"r",1) == 0){
    // We are getting an DISP register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, disp_reg_num, disp_contents, disp_reg_names);
      mvwaddstr(disp_win, disp_reg_num+1,1,disp_reg_names[disp_reg_num]);
      waddstr(disp_win, ": ");
      wrefresh(disp_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(disp_contents[history_num][disp_reg_num],val_buf);
    }

    disp_reg_num++;
  }else if(strncmp(readbuffer,"e",1) == 0){
    // We are getting an EX/WB register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, ex_wb_reg_num, ex_wb_contents, ex_wb_reg_names);
      mvwaddstr(ex_wb_win,ex_wb_reg_num+1,1,ex_wb_reg_names[ex_wb_reg_num]);
      waddstr(ex_wb_win, ": ");
      wrefresh(ex_wb_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(ex_wb_contents[history_num][ex_wb_reg_num],val_buf);
    }

    ex_wb_reg_num++;
  }else if(strncmp(readbuffer,"l",1) == 0){
    // We are getting an LSQ/WB register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, lsq_wb_reg_num, lsq_wb_contents, lsq_wb_reg_names);
      mvwaddstr(lsq_wb_win,lsq_wb_reg_num+1,1,lsq_wb_reg_names[lsq_wb_reg_num]);
      waddstr(lsq_wb_win, ": ");
      wrefresh(lsq_wb_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(lsq_wb_contents[history_num][lsq_wb_reg_num],val_buf);
    }

    lsq_wb_reg_num++;
  
  }else if(strncmp(readbuffer,"i",1) == 0){
    // We are getting an CDB register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, cdb_reg_num, cdb_contents, cdb_reg_names);
      mvwaddstr(cdb_win,cdb_reg_num+1,1,cdb_reg_names[cdb_reg_num]);
      waddstr(cdb_win, ": ");
      wrefresh(cdb_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(cdb_contents[history_num][cdb_reg_num],val_buf);
    }

    cdb_reg_num++;
  }else if(strncmp(readbuffer,"m",1) == 0){
    // We are getting a LDB register
    
    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, ldb_reg_num, ldb_contents, ldb_reg_names);
      mvwaddstr(ldb_win,ldb_reg_num+1,1,ldb_reg_names[ldb_reg_num]);
      waddstr(ldb_win, ": ");
      wrefresh(ldb_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(ldb_contents[history_num][ldb_reg_num],val_buf);
    }

    ldb_reg_num++;
  }else if(strncmp(readbuffer,"j",1) == 0){
    // We are getting an STQ register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, stq_reg_num, stq_contents, stq_reg_names);
      mvwaddstr(stq_win,stq_reg_num+1,1,stq_reg_names[stq_reg_num]);
      waddstr(stq_win, ": ");
      wrefresh(stq_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(stq_contents[history_num][stq_reg_num],val_buf);
    }

    stq_reg_num++;
  }else if(strncmp(readbuffer,"A",1) == 0){
    // We are getting an Icache register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, ic_reg_num, ic_contents, ic_reg_names);
      mvwaddstr(ic_win,ic_reg_num+1,1,ic_reg_names[ic_reg_num]);
      waddstr(ic_win, ": ");
      wrefresh(ic_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(ic_contents[history_num][ic_reg_num],val_buf);
    }

    ic_reg_num++;
  }else if(strncmp(readbuffer,"B",1) == 0){
    // We are getting an Icache register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, icc_reg_num, icc_contents, icc_reg_names);
      mvwaddstr(icc_win,icc_reg_num+1,1,icc_reg_names[icc_reg_num]);
      waddstr(icc_win, ": ");
      wrefresh(icc_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(icc_contents[history_num][icc_reg_num],val_buf);
    }

    icc_reg_num++;
  }else if(strncmp(readbuffer,"C",1) == 0){
    // We are getting a STALL register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, mshr_reg_num, mshr_contents, mshr_reg_names);
      mvwaddstr(mshr_win,mshr_reg_num+1,1,mshr_reg_names[mshr_reg_num]);
      waddstr(mshr_win, ": ");
      wrefresh(mshr_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(mshr_contents[history_num][mshr_reg_num],val_buf);
    }

    mshr_reg_num++;
  }else if(strncmp(readbuffer,"D",1) == 0){
    // We are getting an Dcache register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, dc_reg_num, dc_contents, dc_reg_names);
      mvwaddstr(dc_win,dc_reg_num+1,1,dc_reg_names[dc_reg_num]);
      waddstr(dc_win, ": ");
      wrefresh(dc_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(dc_contents[history_num][dc_reg_num],val_buf);
    }

    dc_reg_num++;
  }else if(strncmp(readbuffer,"E",1) == 0){
    // We are getting an Icache register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer,dcc_reg_num, dcc_contents, dcc_reg_names);
      mvwaddstr(dcc_win,dcc_reg_num+1,1,dcc_reg_names[dcc_reg_num]);
      waddstr(dcc_win, ": ");
      wrefresh(dcc_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(dcc_contents[history_num][dcc_reg_num],val_buf);
    }

    dcc_reg_num++;
  }else if(strncmp(readbuffer,"w",1) == 0){
    // We are getting a STALL register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, stall_reg_num, stall_contents, stall_reg_names);
      mvwaddstr(stall_win,stall_reg_num+1,1,stall_reg_names[stall_reg_num]);
      waddstr(stall_win, ": ");
      wrefresh(stall_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(stall_contents[history_num][stall_reg_num],val_buf);
    }

    stall_reg_num++;
  }else if(strncmp(readbuffer,"o",1) == 0){
    // We are getting a ROB HEAD register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, rob_reg_num, rob_contents, rob_reg_names);
      mvwaddstr(rob_win,rob_reg_num+1,1,rob_reg_names[rob_reg_num]);
      waddstr(rob_win, ": ");
      wrefresh(rob_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(rob_contents[history_num][rob_reg_num],val_buf);
    }

    rob_reg_num++;
  }else if(strncmp(readbuffer,"s",1) == 0){
    // We are getting a ROB2 HEAD register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, rob2_reg_num, rob2_contents, rob2_reg_names);
      mvwaddstr(rob2_win,rob2_reg_num+1,1,rob2_reg_names[rob2_reg_num]);
      waddstr(rob2_win, ": ");
      wrefresh(rob2_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(rob2_contents[history_num][rob2_reg_num],val_buf);
    }

    rob2_reg_num++;
  }else if(strncmp(readbuffer,"u",1) == 0){
    // We are getting a ARF register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, arf_reg_num, arf_contents, arf_reg_names);
      mvwaddstr(arf_win,arf_reg_num+1,1,arf_reg_names[arf_reg_num]);
      waddstr(arf_win, ": ");
      wrefresh(arf_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(arf_contents[history_num][arf_reg_num],val_buf);
    }

    arf_reg_num++;
  }else if(strncmp(readbuffer,"U",1) == 0){
    // We are getting a ARF register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, prf_reg_num, prf_contents, prf_reg_names);
      mvwaddstr(prf_win,prf_reg_num+1,1,prf_reg_names[prf_reg_num]);
      waddstr(prf_win, ": ");
      wrefresh(prf_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(prf_contents[history_num][prf_reg_num],val_buf);
    }

    prf_reg_num++;
  }else if(strncmp(readbuffer,"x",1) == 0){
    // We are getting a RAT register

    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, rat_reg_num, rat_contents, rat_reg_names);
      mvwaddstr(rat_win,rat_reg_num+1,1,rat_reg_names[rat_reg_num]);
      waddstr(rat_win, ": ");
      wrefresh(rat_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(rat_contents[history_num][rat_reg_num],val_buf);
    }

    rat_reg_num++;
  }else if(strncmp(readbuffer,"v",1) == 0){

    //we are processing misc register/wire data
    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, misc_reg_num, misc_contents, misc_reg_names);
      mvwaddstr(misc_win,(misc_reg_num%5)+1,(misc_reg_num/5)*30+1,misc_reg_names[misc_reg_num]);
      waddstr(misc_win, ": ");
      wrefresh(misc_win);
    } else {
      sscanf(readbuffer,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
      strcpy(misc_contents[history_num][misc_reg_num],val_buf);
    }

    misc_reg_num++;
  }else if (strncmp(readbuffer,"break",4) == 0) {
    // If this is the first time through, indicate that we've setup all of
    // the register arrays.
    setup_registers = 1;

    //we've received our last data segment, now go process it
    byte_num = 0;
    if_reg_num = 0;
    pred_reg_num = 0;
    if_id_reg_num = 0;
    id_reg_num = 0;
    id_disp_reg_num = 0;
    disp_reg_num = 0;
    ex_wb_reg_num = 0;
    lsq_wb_reg_num = 0;
    cdb_reg_num = 0;
    ldb_reg_num = 0;
    stq_reg_num = 0;
    ic_reg_num = 0;
    dc_reg_num = 0;
    icc_reg_num = 0;
    dcc_reg_num = 0;
    mshr_reg_num = 0;
    stall_reg_num = 0;
    rob_reg_num = 0;
    rob2_reg_num = 0;
    arf_reg_num = 0;
    prf_reg_num = 0;
    rat_reg_num = 0;
    misc_reg_num = 0;

    //update the simulator time, this won't change with 'b's
    mvwaddstr(sim_time_win,1,1,timebuffer[history_num]);
    wrefresh(sim_time_win);

    //tell the parent application we're ready to move on
    return(1); 
  }
  return(0);
}

//this initializes a ncurses window and sets up the arrays for exchanging reg information
void initcurses(int if_regs, int pred_regs, int if_id_regs, int id_regs, int id_disp_regs, int disp_regs, 
                int ex_wb_regs, int lsq_wb_regs, int cdb_regs, int ldb_regs, int stq_regs, int stall_regs,
                int rob_regs, int rob2_regs, int arf_regs, int rat_regs, int prf_regs, int ic_regs, 
                int icc_regs, int mshr_regs,int dc_regs,int dcc_regs, int misc_regs){
  int nbytes;
  int ready_val;

  done_state = 0;
  echo_data = 1;
  num_misc_regs = misc_regs;
  num_if_regs = if_regs;
  num_pred_regs = pred_regs;
  num_if_id_regs = if_id_regs;
  num_id_regs = id_regs;
  num_id_disp_regs = id_disp_regs;
  num_disp_regs = disp_regs;
  num_ex_wb_regs = ex_wb_regs;
  num_lsq_wb_regs = lsq_wb_regs;
  num_cdb_regs = cdb_regs;
  num_ldb_regs = ldb_regs;
  num_stq_regs = stq_regs;
  num_ic_regs = ic_regs;
  num_dc_regs = dc_regs;
  num_icc_regs = icc_regs;
  num_dcc_regs = dcc_regs;
  num_stall_regs = stall_regs;
  num_mshr_regs = mshr_regs;
  num_rob_regs = rob_regs;
  num_rob2_regs = rob2_regs;
  num_arf_regs = arf_regs;
  num_prf_regs = prf_regs;
  num_rat_regs = rat_regs;
  pid_t childpid;
  pipe(readpipe);
  pipe(writepipe);
  stdout_save = dup(1);
  childpid = fork();
  if(childpid == 0){
    close(PARENT_WRITE);
    close(PARENT_READ);
    fp = fdopen(CHILD_READ, "r");
    fp2 = fopen("program.out","w");


    //allocate room on the heap for the reg data
    inst_contents     = (char**) malloc(NUM_HISTORY*sizeof(char*));
    int i=0;
    if_contents       = (char***) malloc(NUM_HISTORY*sizeof(char**));
    pred_contents     = (char***) malloc(NUM_HISTORY*sizeof(char**));
    if_id_contents    = (char***) malloc(NUM_HISTORY*sizeof(char**));
    id_contents       = (char***) malloc(NUM_HISTORY*sizeof(char**));
    id_disp_contents  = (char***) malloc(NUM_HISTORY*sizeof(char**));
    disp_contents     = (char***) malloc(NUM_HISTORY*sizeof(char**));
    ex_wb_contents    = (char***) malloc(NUM_HISTORY*sizeof(char**));
    lsq_wb_contents   = (char***) malloc(NUM_HISTORY*sizeof(char**));
    cdb_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    ldb_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    stq_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    ic_contents       = (char***) malloc(NUM_HISTORY*sizeof(char**));
    dc_contents       = (char***) malloc(NUM_HISTORY*sizeof(char**));
    icc_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    dcc_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    mshr_contents     = (char***) malloc(NUM_HISTORY*sizeof(char**));
    stall_contents    = (char***) malloc(NUM_HISTORY*sizeof(char**));
    rob_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    rob2_contents     = (char***) malloc(NUM_HISTORY*sizeof(char**));
    arf_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    prf_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    rat_contents      = (char***) malloc(NUM_HISTORY*sizeof(char**));
    misc_contents     = (char***) malloc(NUM_HISTORY*sizeof(char**));
    timebuffer        = (char**) malloc(NUM_HISTORY*sizeof(char*));
    cycles            = (char**) malloc(NUM_HISTORY*sizeof(char*));
    clocks            = (char*) malloc(NUM_HISTORY*sizeof(char));
    resets            = (char*) malloc(NUM_HISTORY*sizeof(char));

    // allocate room for the register names (what is displayed)
    if_reg_names      = (char**) malloc(num_if_regs*sizeof(char*));
    pred_reg_names    = (char**) malloc(num_pred_regs*sizeof(char*));
    if_id_reg_names   = (char**) malloc(num_if_id_regs*sizeof(char*));
    id_reg_names      = (char**) malloc(num_id_regs*sizeof(char*));
    id_disp_reg_names = (char**) malloc(num_id_disp_regs*sizeof(char*));
    disp_reg_names    = (char**) malloc(num_disp_regs*sizeof(char*));
    ex_wb_reg_names   = (char**) malloc(num_ex_wb_regs*sizeof(char*));
    lsq_wb_reg_names  = (char**) malloc(num_lsq_wb_regs*sizeof(char*));
    cdb_reg_names     = (char**) malloc(num_cdb_regs*sizeof(char*));
    ldb_reg_names     = (char**) malloc(num_ldb_regs*sizeof(char*));
    stq_reg_names     = (char**) malloc(num_stq_regs*sizeof(char*));
    ic_reg_names      = (char**) malloc(num_ic_regs*sizeof(char*));
    dc_reg_names      = (char**) malloc(num_dc_regs*sizeof(char*));
    icc_reg_names     = (char**) malloc(num_icc_regs*sizeof(char*));
    dcc_reg_names     = (char**) malloc(num_dcc_regs*sizeof(char*));
    mshr_reg_names    = (char**) malloc(num_mshr_regs*sizeof(char*));
    stall_reg_names   = (char**) malloc(num_stall_regs*sizeof(char*));
    rob_reg_names     = (char**) malloc(num_rob_regs*sizeof(char*));
    rob2_reg_names    = (char**) malloc(num_rob2_regs*sizeof(char*));
    arf_reg_names     = (char**) malloc(num_arf_regs*sizeof(char*));
    prf_reg_names     = (char**) malloc(num_prf_regs*sizeof(char*));
    rat_reg_names     = (char**) malloc(num_rat_regs*sizeof(char*));
    misc_reg_names    = (char**) malloc(num_misc_regs*sizeof(char*));

    int j=0;
    for(;i<NUM_HISTORY;i++){
      timebuffer[i]       = (char*) malloc(8);
      cycles[i]           = (char*) malloc(7);
      inst_contents[i]    = (char*) malloc(NUM_STAGES*10);
      if_contents[i]      = (char**) malloc(num_if_regs*sizeof(char*));
      pred_contents[i]    = (char**) malloc(num_pred_regs*sizeof(char*));
      if_id_contents[i]   = (char**) malloc(num_if_id_regs*sizeof(char*));
      id_contents[i]      = (char**) malloc(num_id_regs*sizeof(char*));
      id_disp_contents[i] = (char**) malloc(num_id_disp_regs*sizeof(char*));
      disp_contents[i]    = (char**) malloc(num_disp_regs*sizeof(char*));
      ex_wb_contents[i]   = (char**) malloc(num_ex_wb_regs*sizeof(char*));
      lsq_wb_contents[i]  = (char**) malloc(num_lsq_wb_regs*sizeof(char*));
      cdb_contents[i]     = (char**) malloc(num_cdb_regs*sizeof(char*));
      ldb_contents[i]     = (char**) malloc(num_ldb_regs*sizeof(char*));
      stq_contents[i]     = (char**) malloc(num_stq_regs*sizeof(char*));
      ic_contents[i]      = (char**) malloc(num_ic_regs*sizeof(char*));
      dc_contents[i]      = (char**) malloc(num_dc_regs*sizeof(char*));
      icc_contents[i]     = (char**) malloc(num_icc_regs*sizeof(char*));
      dcc_contents[i]     = (char**) malloc(num_dcc_regs*sizeof(char*));
      mshr_contents[i]    = (char**) malloc(num_mshr_regs*sizeof(char*));
      stall_contents[i]   = (char**) malloc(num_stall_regs*sizeof(char*));
      rob_contents[i]     = (char**) malloc(num_rob_regs*sizeof(char*));
      rob2_contents[i]    = (char**) malloc(num_rob2_regs*sizeof(char*));
      arf_contents[i]     = (char**) malloc(num_arf_regs*sizeof(char*));
      prf_contents[i]     = (char**) malloc(num_prf_regs*sizeof(char*));
      rat_contents[i]     = (char**) malloc(num_rat_regs*sizeof(char*));
      misc_contents[i]    = (char**) malloc(num_misc_regs*sizeof(char*));
    }
    setup_gui(fp);

    // Main loop for retrieving data and taking commands from user
    char quit_flag = 0;
    char resp=0;
    char running=0;
    int mem_addr=0;
    char goto_flag = 0;
    char cycle_flag = 0;
    char done_received = 0;
    memset(readbuffer,'\0',sizeof(readbuffer));
    while(!quit_flag){
      if (!done_received) {
        fgets(readbuffer, sizeof(readbuffer), fp);
        ready_val = processinput();
      }
      if(strcmp(readbuffer,"DONE") == 0) {
        done_received = 1;
        done_time = history_num - 1;
      }
      if(ready_val == 1 || done_received == 1){
        if(echo_data == 0 && done_received == 1) {
          running = 0;
          timeout(-1);
          echo_data = 1;
          history_num--;
          history_num%=NUM_HISTORY;
        }
        if(echo_data != 0){
          parsedata(history_num);
        }
        history_num++;
        // keep track of whether time wrapped around yet
        if (history_num == NUM_HISTORY)
          time_wrapped = 1;
        history_num%=NUM_HISTORY;

        //we're done reading the reg values for this iteration
        if (done_received != 1) {
          write(CHILD_WRITE, "n", 1);
          write(CHILD_WRITE, &mem_addr, 2);
        }
        char continue_flag = 0;
        int hist_num_temp = (history_num-1)%NUM_HISTORY;
        if (history_num==0) hist_num_temp = NUM_HISTORY-1;
        char echo_data_tmp,continue_flag_tmp;

        while(continue_flag == 0){
          resp=getch();
          if(running == 1){
            continue_flag = 1;
          }
          if(running == 0 || resp == 'p'){ 
            if(resp == 'n' && hist_num_temp == (history_num-1)%NUM_HISTORY){
              if (!done_received)
                continue_flag = 1;
            }else if(resp == 'n'){
              //forward in time, but not up to present yet
              hist_num_temp++;
              hist_num_temp%=NUM_HISTORY;
              parsedata(hist_num_temp);
            }else if(resp == 'r'){
              echo_data = 0;
              running = 1;
              timeout(0);
              continue_flag = 1;
            }else if(resp == 'p'){
              echo_data = 1;
              timeout(-1);
              running = 0;
              parsedata(hist_num_temp);
            }else if(resp == 'q'){
              //quit
              continue_flag = 1;
              quit_flag = 1;
            }else if(resp == 'b'){
              //We're goin BACK IN TIME, woohoo!
              // Make sure not to wrap around to NUM_HISTORY-1 if we don't have valid
              // data there (time_wrapped set to 1 when we wrap around to history 0)
              if (hist_num_temp > 0) {
                hist_num_temp--;
                parsedata(hist_num_temp);
              } else if (time_wrapped == 1) {
                hist_num_temp = NUM_HISTORY-1;
                parsedata(hist_num_temp);
              }
            }else if(resp == 'g' || resp == 'c'){
              // See if user wants to jump to clock cycle instead of sim time
              cycle_flag = (resp == 'c');

              // go to specified simulation time (either in history or
              // forward in simulation time).
              stop_time = get_time();
              
              // see if we already have that time in history
              int tmp_time;
              int cur_time;
              int delta;
              if (cycle_flag)
                sscanf(cycles[hist_num_temp], "%u", &cur_time);
              else
                sscanf(timebuffer[hist_num_temp], "%u", &cur_time);
              delta = (stop_time > cur_time) ? 1 : -1;
              if ((hist_num_temp+delta)%NUM_HISTORY != history_num) {
                tmp_time=hist_num_temp;
                i= (hist_num_temp+delta >= 0) ? (hist_num_temp+delta)%NUM_HISTORY : NUM_HISTORY-1;
                while (i!=history_num) {
                  if (cycle_flag)
                    sscanf(cycles[i], "%u", &cur_time);
                  else
                    sscanf(timebuffer[i], "%u", &cur_time);
                  if ((delta == 1 && cur_time >= stop_time) ||
                      (delta == -1 && cur_time <= stop_time)) {
                    hist_num_temp = i;
                    parsedata(hist_num_temp);
                    stop_time = 0;
                    break;
                  }

                  if ((i+delta) >=0)
                    i = (i+delta)%NUM_HISTORY;
                  else {
                    if (time_wrapped == 1)
                      i = NUM_HISTORY - 1;
                    else {
                      parsedata(hist_num_temp);
                      stop_time = 0;
                      break;
                    }
                  }
                }
              }

              // If we looked backwards in history and didn't find stop_time
              // then give up
              if (i==history_num && (delta == -1 || done_received == 1))
                stop_time = 0;

              // Set flags so that we run forward in the simulation until
              // it either ends, or we hit the desired time
              if (stop_time > 0) {
                // grab current values
                echo_data = 0;
                running = 1;
                timeout(0);
                continue_flag = 1;
                goto_flag = 1;
              }
            }
          }
        }
        // if we're instructed to goto specific time, see if we're there
        int cur_time=0;
        if (goto_flag==1) {
          if (cycle_flag)
            sscanf(cycles[hist_num_temp], "%u", &cur_time);
          else
            sscanf(timebuffer[hist_num_temp], "%u", &cur_time);
          if ((cur_time >= stop_time) ||
              (strcmp(readbuffer,"DONE")==0) ) {
            goto_flag = 0;
            echo_data = 1;
            running = 0;
            timeout(-1);
            continue_flag = 0;
            //parsedata(hist_num_temp);
          }
        }
      }
    }
    refresh();
    delwin(title_win);
    endwin();
    fflush(stdout);
    if(resp == 'q'){
      fclose(fp2);
      write(CHILD_WRITE, "Z", 1);
      exit(0);
    }
    readbuffer[0] = 0;
    while(strncmp(readbuffer,"DONE",4) != 0){
      if(fgets(readbuffer, sizeof(readbuffer), fp) != NULL)
        fputs(readbuffer, fp2);
    }
    fclose(fp2);
    fflush(stdout);
    write(CHILD_WRITE, "Z", 1);
    printf("Child Done Execution\n");
    exit(0);
  } else {
    close(CHILD_READ);
    close(CHILD_WRITE);
    dup2(PARENT_WRITE, 1);
    close(PARENT_WRITE);
    
  }
}


// Function to make testbench block until debugger is ready to proceed
int waitforresponse(){
  static int mem_start = 0;
  char c=0;
  while(c!='n' && c!='Z') read(PARENT_READ,&c,1);
  if(c=='Z') exit(0);
  mem_start = read(PARENT_READ,&c,1);
  mem_start = mem_start << 8 + read(PARENT_READ,&c,1);
  return(mem_start);
}

void flushpipe(){
  char c=0;
  read(PARENT_READ, &c, 1);
}

// Function to return string representation of opcode given inst encoding
char *get_opcode_str(int inst, int valid_inst)
{
  int opcode, check;
  char *str;
  
  if (valid_inst == ((int)'x' - (int)'0'))
    str = "-";
  else if(!valid_inst)
    str = "-";
  else if(inst==NOOP_INST)
    str = "nop";
  else {
    opcode = (inst >> 26) & 0x0000003f;
    check = (inst>>5) & 0x0000007f;
    switch(opcode)
    {
      case 0x00: str = (inst == 0x555) ? "halt" : "call_pal"; break;
      case 0x08: str = "lda"; break;
      case 0x09: str = "ldah"; break;
      case 0x0a: str = "ldbu"; break;
      case 0x0b: str = "ldqu"; break;
      case 0x0c: str = "ldwu"; break;
      case 0x0d: str = "stw"; break;
      case 0x0e: str = "stb"; break;
      case 0x0f: str = "stqu"; break;
      case 0x10: // INTA_GRP
         switch(check)
         {
           case 0x00: str = "addl"; break;
           case 0x02: str = "s4addl"; break;
           case 0x09: str = "subl"; break;
           case 0x0b: str = "s4subl"; break;
           case 0x0f: str = "cmpbge"; break;
           case 0x12: str = "s8addl"; break;
           case 0x1b: str = "s8subl"; break;
           case 0x1d: str = "cmpult"; break;
           case 0x20: str = "addq"; break;
           case 0x22: str = "s4addq"; break;
           case 0x29: str = "subq"; break;
           case 0x2b: str = "s4subq"; break;
           case 0x2d: str = "cmpeq"; break;
           case 0x32: str = "s8addq"; break;
           case 0x3b: str = "s8subq"; break;
           case 0x3d: str = "cmpule"; break;
           case 0x40: str = "addlv"; break;
           case 0x49: str = "sublv"; break;
           case 0x4d: str = "cmplt"; break;
           case 0x60: str = "addqv"; break;
           case 0x69: str = "subqv"; break;
           case 0x6d: str = "cmple"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x11: // INTL_GRP
         switch(check)
         {
           case 0x00: str = "and"; break;
           case 0x08: str = "bic"; break;
           case 0x14: str = "cmovlbs"; break;
           case 0x16: str = "cmovlbc"; break;
           case 0x20: str = "bis"; break;
           case 0x24: str = "cmoveq"; break;
           case 0x26: str = "cmovne"; break;
           case 0x28: str = "ornot"; break;
           case 0x40: str = "xor"; break;
           case 0x44: str = "cmovlt"; break;
           case 0x46: str = "cmovge"; break;
           case 0x48: str = "eqv"; break;
           case 0x61: str = "amask"; break;
           case 0x64: str = "cmovle"; break;
           case 0x66: str = "cmovgt"; break;
           case 0x6c: str = "implver"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x12: // INTS_GRP
         switch(check)
         {
           case 0x02: str = "mskbl"; break;
           case 0x06: str = "extbl"; break;
           case 0x0b: str = "insbl"; break;
           case 0x12: str = "mskwl"; break;
           case 0x16: str = "extwl"; break;
           case 0x1b: str = "inswl"; break;
           case 0x22: str = "mskll"; break;
           case 0x26: str = "extll"; break;
           case 0x2b: str = "insll"; break;
           case 0x30: str = "zap"; break;
           case 0x31: str = "zapnot"; break;
           case 0x32: str = "mskql"; break;
           case 0x34: str = "srl"; break;
           case 0x36: str = "extql"; break;
           case 0x39: str = "sll"; break;
           case 0x3b: str = "insql"; break;
           case 0x3c: str = "sra"; break;
           case 0x52: str = "mskwh"; break;
           case 0x57: str = "inswh"; break;
           case 0x5a: str = "extwh"; break;
           case 0x62: str = "msklh"; break;
           case 0x67: str = "inslh"; break;
           case 0x6a: str = "extlh"; break;
           case 0x72: str = "mskqh"; break;
           case 0x77: str = "insqh"; break;
           case 0x7a: str = "extqh"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x13: // INTM_GRP
         switch(check)
         {
           case 0x00: str = "mull"; break;
           case 0x20: str = "mulq"; break;
           case 0x30: str = "umulh"; break;
           case 0x40: str = "mullv"; break;
           case 0x60: str = "mulqv"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x14: str = "itfp"; break; // unimplemented
      case 0x15: str = "fltv"; break; // unimplemented
      case 0x16: str = "flti"; break; // unimplemented
      case 0x17: str = "fltl"; break; // unimplemented
      case 0x1a: str = "jsr"; break;
      case 0x1c: str = "ftpi"; break;
      case 0x20: str = "ldf"; break;
      case 0x21: str = "ldg"; break;
      case 0x22: str = "lds"; break;
      case 0x23: str = "ldt"; break;
      case 0x24: str = "stf"; break;
      case 0x25: str = "stg"; break;
      case 0x26: str = "sts"; break;
      case 0x27: str = "stt"; break;
      case 0x28: str = "ldl"; break;
      case 0x29: str = "ldq"; break;
      case 0x2a: str = "ldll"; break;
      case 0x2b: str = "ldql"; break;
      case 0x2c: str = "stl"; break;
      case 0x2d: str = "stq"; break;
      case 0x2e: str = "stlc"; break;
      case 0x2f: str = "stqc"; break;
      case 0x30: str = "br"; break;
      case 0x31: str = "fbeq"; break;
      case 0x32: str = "fblt"; break;
      case 0x33: str = "fble"; break;
      case 0x34: str = "bsr"; break;
      case 0x35: str = "fbne"; break;
      case 0x36: str = "fbge"; break;
      case 0x37: str = "fbgt"; break;
      case 0x38: str = "blbc"; break;
      case 0x39: str = "beq"; break;
      case 0x3a: str = "blt"; break;
      case 0x3b: str = "ble"; break;
      case 0x3c: str = "blbs"; break;
      case 0x3d: str = "bne"; break;
      case 0x3e: str = "bge"; break;
      case 0x3f: str = "bgt"; break;
      default: str = "invalid"; break;
    }
  }

  return str;
}

// Function to parse register $display() from testbench and add to
// names/contents arrays
void parse_register(char *readbuf, int reg_num, char*** contents, char** reg_names) {
  char name_buf[32];
  char val_buf[32];
  int tmp_len;

  sscanf(readbuf,"%*c%s %d:%s",name_buf,&tmp_len,val_buf);
  int i=0;
  for (;i<NUM_HISTORY;i++){
    contents[i][reg_num] = (char*) malloc((tmp_len+1)*sizeof(char));
  }
  strcpy(contents[history_num][reg_num],val_buf);
  reg_names[reg_num] = (char*) malloc((strlen(name_buf)+1)*sizeof(char));
  strncpy(reg_names[reg_num], readbuf+1, strlen(name_buf));
  reg_names[reg_num][strlen(name_buf)] = '\0';
}

// Ask user for simulation time to stop at
// Since the enter key isn't detected, user must press 'g' key
//  when finished entering a number.
int get_time() {
  int col = COLS/2-6;
  wattron(title_win,A_REVERSE);
  mvwprintw(title_win,1,col,"goto time: ");
  wrefresh(title_win);
  int resp=0;
  int ptr = 0;
  char buf[32];
  int i;
  
  resp=wgetch(title_win);
  while(resp != 'g' && resp != KEY_ENTER && resp != ERR && ptr < 6) {
    if (isdigit((char)resp)) {
      waddch(title_win,(char)resp);
      wrefresh(title_win);
      buf[ptr++] = (char)resp;
    }
    resp=wgetch(title_win);
  }

  // Clean up title window
  wattroff(title_win,A_REVERSE);
  mvwprintw(title_win,1,col,"           ");
  for(i=0;i<ptr;i++)
    waddch(title_win,' ');

  wrefresh(title_win);

  buf[ptr] = '\0';
  return atoi(buf);
}

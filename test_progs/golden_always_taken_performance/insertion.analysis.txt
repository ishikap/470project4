Instruction count              =        598
CPI                            = 2.250836
total_request_to_icache_count  =        2517
icache_misses_count            =          10
total_request_to_dcache_count  =         218
total_dcache_hits              =          97
branch_conditional_count       =         110
branch_unconditional_count     =          40
branch_call_count              =           0
branch_ret_count               =           0
Number of branch mispredicts   =          83
total_loads_in_ldb             =         232
total_lsq_forwarding           =          14
disp_rs_full_count             =           0
disp_rob_full_count            =           0
lsq_stq_full_count             =          30
lsq_ldb_full_count             =          60
mshr_dcache_full_count         =           0
mshr_icache_full_count         =          20

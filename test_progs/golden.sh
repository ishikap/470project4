#!/bin/bash

rm -rf CORRECT

mkdir -p CORRECT
declare -a nope=("CORRECT" "OUTPUT" "diff.sh" "golden.sh" "test.sh" "vs-asm" "project3" "other" "RESULTS.txt" "CPI" "rainbow.sh" "ANALYSIS")

for D in *; do
    declare -i wrong=0;

    # dont run certian folers
    for i in "${nope[@]}"
    do
        if [ "$D" == "$i" ]
        then
            wrong=1
        fi
    done

    if [ $wrong == 0 ]
    then
        for file in $D/*.s; do
            # strip extension from file
            file=$(echo $file | cut -d'.' -f1)
            echo "Assembling $file"
            ./vs-asm < $file.s > project3/program.mem
            echo "Running $file"
            cd project3
            make >/dev/null
            echo "Saving $file output"
            # save output to correct folder
            mv writeback.out ../CORRECT/$(basename $file).writeback.out
            mv program.out ../CORRECT/$(basename $file).program.out
            cd ..
        done
    fi
done

printf "\n\n!!!Done assembling and running CORRECT, starting work on OUTPUT!!!\n\n\n"
./test.sh

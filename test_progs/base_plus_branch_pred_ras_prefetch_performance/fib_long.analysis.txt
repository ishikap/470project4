Instruction count              =        627
CPI                            = 1.151515
total_request_to_icache_count  =         583
icache_misses_count            =          46
total_request_to_dcache_count  =          29
total_dcache_hits              =          29
branch_conditional_count       =          14
branch_unconditional_count     =           0
branch_call_count              =           0
branch_ret_count               =           0
Number of branch mispredicts   =           3
total_loads_in_ldb             =          29
total_lsq_forwarding           =           0
disp_rs_full_count             =           0
disp_rob_full_count            =           0
lsq_stq_full_count             =           0
lsq_ldb_full_count             =           0
mshr_dcache_full_count         =           0
mshr_icache_full_count         =          17

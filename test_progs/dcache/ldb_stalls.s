# Loads shouldn't evict, only stores should evict
    lda     $r2, 0x300
    lda     $r1, 0x800
    lda     $r3, 0x1
    lda     $r5, 0x10       # Loop 10 times
    lda     $r6, 0x1        # Incriment R3
    stq     $r3, 0x0($r2)   # Fill dcache with valid stores
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x8($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x10($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x18($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x20($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x28($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x30($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x38($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x40($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x48($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x50($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x58($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x60($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x68($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x70($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x78($r2)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x0($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x8($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x10($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x18($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x20($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x28($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x30($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x38($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x40($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x48($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x50($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x58($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x60($r1)  
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x68($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x70($r1)
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x78($r1)  # Now dcache is full of dirty stores
    addq    $r3, $r6, $r3   # r3++ 
    stq     $r3, 0x80($r1)  # Now evict
loop:
    ldq     $r3, 0x0($r2)   # A bunch of dcache hits
    ldq     $r3, 0x8($r2)
    ldq     $r3, 0x10($r2)
    ldq     $r3, 0x18($r2)
    ldq     $r3, 0x20($r2)
    ldq     $r3, 0x28($r2)
    ldq     $r3, 0x30($r2)
    ldq     $r3, 0x38($r2)
    ldq     $r3, 0x40($r2)
    ldq     $r3, 0x48($r2)
    ldq     $r3, 0x50($r2)
    ldq     $r3, 0x58($r2)
    ldq     $r3, 0x60($r2)
    ldq     $r3, 0x68($r2)
    ldq     $r3, 0x70($r2)
    ldq     $r3, 0x78($r2)
    ldq     $r3, 0x0($r1)
    ldq     $r3, 0x8($r1)
    ldq     $r3, 0x10($r1)
    ldq     $r3, 0x18($r1)
    ldq     $r3, 0x20($r1)
    ldq     $r3, 0x28($r1)
    ldq     $r3, 0x30($r1)
    ldq     $r3, 0x38($r1)
    ldq     $r3, 0x40($r1)
    ldq     $r3, 0x48($r1)
    ldq     $r3, 0x50($r1)
    ldq     $r3, 0x58($r1)
    ldq     $r3, 0x60($r1)
    ldq     $r3, 0x68($r1)
    ldq     $r3, 0x70($r1)
    ldq     $r3, 0x78($r1)
    ldq     $r3, 0x80($r1)
    lda     $r3, 0xdad          
    subq    $r5,    0x1,    $r5 # r5--
    bne     $r5,    loop        # branch if r5 != 0. Sees: 3, 2, 1, 0     
    call_pal 0x555
# Finially r3 should be equal to 0xdad

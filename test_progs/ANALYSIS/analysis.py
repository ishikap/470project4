#!/usr/bin/python3

import os
import sys
import re
import linecache
import fileinput

print('@')
print('@')
print('Analysis Options')
print('@')
print('@')
print('CPI: 1')
print('ICache Hit Rate: 2')
print('DCache Hit Rate: 3')
print('Branch Prediction Rate: 4')
print('LSQ Forwarding Rate: 5')
print('RS Full Count: 6')
print('ROB Full Count: 7')
print('Store Queue Full Count: 8')
print('Load Buffer Full Count: 9')
print('MSHR DCache Full Count: 10')
print('MSHR ICache Full Count: 11')
print('@')
print('@')
a = input('Enter a number from the above list: ')
file_name = input('Enter a file name for writing the analysis result: (For example: "cpi.txt") ')
#test_program_list = open("test_program_list.txt","w")   ## do this once
analysis_result_file = open(file_name, "w")

def main():
    os.chdir('..')
    onlyfiles = [f for f in os.listdir('OUTPUT') if f and os.path.isfile(os.path.join('OUTPUT', f)) and f.endswith(".csv")]
    for f in onlyfiles:
        # CPI
        if (a == 1):
            cpi = linecache.getline(os.path.join('OUTPUT', f),2)
            analysis_result_file.write(cpi[:-1]+",\t")
            analysis_result_file.write(f[:-4]+",\n")
        
        # ICache Hit Rate
        if (a == 2):
            icache_total = linecache.getline(os.path.join('OUTPUT', f),3)
            icache_miss = linecache.getline(os.path.join('OUTPUT', f),4)
            if(int(icache_total) != 0):
                icache_hit_rate = ((float(icache_total) - float(icache_miss))/float(icache_total)) * 100
                analysis_result_file.write(str(icache_hit_rate)+",\t")
            else:
                analysis_result_file.write("nil,\t")
            analysis_result_file.write(f[:-4]+",\n")
            
        # DCache Hit Rate
        if (a == 3):
            dcache_total = linecache.getline(os.path.join('OUTPUT', f),5)
            dcache_hit = linecache.getline(os.path.join('OUTPUT', f),6)
            if(int(dcache_total) != 0):
                dcache_hit_rate = ((float(dcache_hit))/float(dcache_total)) * 100
                analysis_result_file.write(str(dcache_hit_rate)+",\t")
            else:
                analysis_result_file.write("nil,\t")
            analysis_result_file.write(f[:-4]+",\n")
        
        # Branch Prediction Rate
        if (a == 4):
            br_cond = linecache.getline(os.path.join('OUTPUT', f),7)
            br_uncond = linecache.getline(os.path.join('OUTPUT', f),8)
            br_call = linecache.getline(os.path.join('OUTPUT', f),9)
            br_ret = linecache.getline(os.path.join('OUTPUT', f),10)
            br_nuke = linecache.getline(os.path.join('OUTPUT', f),11)
            if((int(br_cond) + int(br_uncond) + int(br_call) + int(br_ret)) != 0):
                br_pred_rate = (((float(br_cond) + float(br_uncond) + float(br_call) + float(br_ret)) - float(br_nuke))/(float(br_cond) + float(br_uncond) + float(br_call) + float(br_ret))) * 100
                analysis_result_file.write(str(br_pred_rate)+",\t")
            else:
                analysis_result_file.write("nil,\t")
            analysis_result_file.write(f[:-4]+",\n")
        
        # LSQ Forwarding Rate
        if (a == 5):
            loads_total = linecache.getline(os.path.join('OUTPUT', f),12)
            lsq_forward = linecache.getline(os.path.join('OUTPUT', f),13)
            if(int(loads_total) != 0):
                lsq_forwarding_rate = (float(lsq_forward)/float(loads_total)) * 100
                analysis_result_file.write(str(lsq_forwarding_rate)+",\t")
            else:
                analysis_result_file.write("nil,\t")
            analysis_result_file.write(f[:-4]+",\n")
        
        # RS Full Count
        if (a == 6):
            rs_full = linecache.getline(os.path.join('OUTPUT', f),14)
            analysis_result_file.write(rs_full[:-1]+",\t")
            analysis_result_file.write(f[:-4]+",\n")

        # ROB Full Count
        if (a == 7):
            rob_full = linecache.getline(os.path.join('OUTPUT', f),15)
            analysis_result_file.write(rob_full[:-1]+",\t")
            analysis_result_file.write(f[:-4]+",\n")

        # Store Queue Full Count
        if (a == 8):
            store_queue_full = linecache.getline(os.path.join('OUTPUT', f),16)
            analysis_result_file.write(store_queue_full[:-1]+",\t")
            analysis_result_file.write(f[:-4]+",\n")
        
        # Load Buffer Full Count
        if (a == 9):
            load_buffer_full = linecache.getline(os.path.join('OUTPUT', f),17)
            analysis_result_file.write(load_buffer_full[:-1]+",\t")
            analysis_result_file.write(f[:-4]+",\n")
        
        # MSHR DCache Full Count
        if (a == 10):
            mshr_dcache_full = linecache.getline(os.path.join('OUTPUT', f),18)
            analysis_result_file.write(mshr_dcache_full[:-1]+",\t")
            analysis_result_file.write(f[:-4]+",\n")
        
        # MSHR ICache Full Count
        if (a == 11):
            mshr_icache_full = linecache.getline(os.path.join('OUTPUT', f),19)
            analysis_result_file.write(mshr_icache_full[:-1]+",\t")
            analysis_result_file.write(f[:-4]+",\n")

        #test_program_list.write(f[:-4]+"\n")
    
    print('@')
    print('@')   
    print('Done: Results stored in file: ' + file_name)
    #print('Done: Test Program names stored in file: test_program_list.txt')
    print('@')
    print('@')
    #test_program_list.close()
    analysis_result_file.close()
    #continue_analysis = input('Enter 1 to continue or 0 to quit')
    #if(continue_analysis == 1):
    #    main()

    

if __name__ == "__main__":
    main()

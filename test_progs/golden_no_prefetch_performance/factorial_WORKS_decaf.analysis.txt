Instruction count              =       4453
CPI                            = 3.133842
total_request_to_icache_count  =        4401
icache_misses_count            =        1499
total_request_to_dcache_count  =         740
total_dcache_hits              =         476
branch_conditional_count       =         142
branch_unconditional_count     =         233
branch_call_count              =         108
branch_ret_count               =         108
Number of branch mispredicts   =          30
total_loads_in_ldb             =        1454
total_lsq_forwarding           =         714
disp_rs_full_count             =          51
disp_rob_full_count            =           0
lsq_stq_full_count             =        2356
lsq_ldb_full_count             =         746
mshr_dcache_full_count         =           0
mshr_icache_full_count         =           0

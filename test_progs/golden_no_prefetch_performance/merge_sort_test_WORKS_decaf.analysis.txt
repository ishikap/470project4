Instruction count              =      32202
CPI                            = 4.691075
total_request_to_icache_count  =       33902
icache_misses_count            =       10144
total_request_to_dcache_count  =        7311
total_dcache_hits              =        6295
branch_conditional_count       =        5077
branch_unconditional_count     =         579
branch_call_count              =         136
branch_ret_count               =         136
Number of branch mispredicts   =         422
total_loads_in_ldb             =        9436
total_lsq_forwarding           =        2125
disp_rs_full_count             =           0
disp_rob_full_count            =           0
lsq_stq_full_count             =        4377
lsq_ldb_full_count             =          28
mshr_dcache_full_count         =           0
mshr_icache_full_count         =           2

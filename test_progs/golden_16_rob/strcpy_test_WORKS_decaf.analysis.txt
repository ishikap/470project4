Instruction count              =       2724
CPI                            = 2.364905
total_request_to_icache_count  =        3388
icache_misses_count            =         668
total_request_to_dcache_count  =         576
total_dcache_hits              =         509
branch_conditional_count       =         334
branch_unconditional_count     =         103
branch_call_count              =          34
branch_ret_count               =          34
Number of branch mispredicts   =          82
total_loads_in_ldb             =         795
total_lsq_forwarding           =         219
disp_rs_full_count             =           0
disp_rob_full_count            =         325
lsq_stq_full_count             =         218
lsq_ldb_full_count             =           0
mshr_dcache_full_count         =           0
mshr_icache_full_count         =         831
commit_2                       =         905
commit_1                       =         915
commit_0                       =        4648

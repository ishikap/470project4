Instruction count              =        590
CPI                            = 0.766102
total_request_to_icache_count  =         630
icache_misses_count            =          49
total_request_to_dcache_count  =           0
total_dcache_hits              =           0
branch_conditional_count       =          16
branch_unconditional_count     =           0
branch_call_count              =           0
branch_ret_count               =           0
Number of branch mispredicts   =           3
total_loads_in_ldb             =          16
total_lsq_forwarding           =          16
disp_rs_full_count             =           0
disp_rob_full_count            =          60
lsq_stq_full_count             =           0
lsq_ldb_full_count             =           0
mshr_dcache_full_count         =           0
mshr_icache_full_count         =          22
commit_2                       =         278
commit_1                       =          35
commit_0                       =         171

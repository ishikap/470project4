    lda $r0, 32
    lda $r1, 2
    lda $r2, 0  # i
    lda $r3, 1  # for i + 1
    lda $r4, 0x10  # for i < 0x10

    lda $r5, 32
    lda $r6, 8


    lda $r20, 32
    lda $r21, 32
    lda $r22, 32
    lda $r23, 32
    lda $r24, 32
    lda $r25, 32
    lda $r26, 32

    addq $r5,   $r6, $r5
    addq $r20,  $r6, $r20
    addq $r21,  $r6, $r21
    addq $r22,  $r6, $r22
    addq $r23,  $r6, $r23
    addq $r24,  $r6, $r24
    addq $r25,  $r6, $r25
    addq $r26,  $r6, $r26

    mulq $r0,  $r1, $r10        # r10 = 64
    mulq $r10, $r1, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    mulq $r10, $r3, $r10        # r10 = 128
    stq  $r31, 0($r10)          # MEM[128] = 0
    ldq  $r31, 0($r5)
    ldq  $r31, 0($r20)
    ldq  $r31, 0($r21)
    ldq  $r31, 0($r22)
    ldq  $r31, 0($r23)
    ldq  $r31, 0($r24)
    ldq  $r31, 0($r25)
    ldq  $r31, 0($r26)

    call_pal 0x555

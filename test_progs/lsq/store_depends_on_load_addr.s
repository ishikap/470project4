    lda $r0, 0xdad
    lda $r1, 0xdad
    ldq $r0, 0x100($r31) # Do that load R0 is now too big for memory!
    nop
    stq $r1, 0($r0) # Do that store, address depends on load
    nop
    call_pal 0x555

/*
    ENDING STATE
    ------------
    r0 = 201f0da0
    r1 = dad
    mem[0dad] = dad
    Memory is 64KB. Max address 0xFFFF
*/

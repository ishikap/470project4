    lda     $r0, 0x20
    lda     $r1, 0x10
    mulq    $r0, $r1, $r2
    stq     $r0, 0($r2)
    ldq     $r3, 0($r2)
    call_pal 0x555


/*
    Store dad to mem[9face9] then load it

    ENDING STATE
    ------------
    r0 = 20
    r1 = 10
    r2 = 200
    r3 = 20
    mem[200] = 20
*/

    lda     $r0, 0xda0
    lda     $r1, 0xbad
    mulq    $r0, $r1, $r2
    stq     $r2, 0($r0)
    ldq     $r3, 0($r0)
    call_pal 0x555


/*
    Store 9face9 to mem[dad] then load it

    ENDING STATE
    ------------
    r0 = da0
    r1 = bad
    r2 = 9f1520
    r3 = 9f1520
    mem[da0] = 9f1520
*/

    lda $r0, 0xdad
    lda $r1, 0xda0
    ldq $r0, 0($r31) # Do that load
    nop
    stq $r0, 0($r1) # Do that store, address depends on load
    nop
    call_pal 0x555

/*
    ENDING STATE
    ------------
    r0 = 201f0dad
    r1 = dad
    mem[da0] = 201f0dad
*/

    ldq $r0,    0x0($r31)
    ldq $r1,    0x8($r31)
    ldq $r2,   0x10($r31)
    ldq $r3,   0x18($r31)
    ldq $r4,   0x20($r31)
    ldq $r5,   0x28($r31)
    ldq $r6,   0x30($r31)
    ldq $r7,   0x38($r31)
    ldq $r8,   0x40($r31)
    ldq $r9,   0x48($r31)
    ldq $r10,  0x50($r31)
    ldq $r11,  0x58($r31)
    ldq $r12,  0x60($r31)
    ldq $r13,  0x68($r31)
    ldq $r14,  0x70($r31)
    ldq $r15,  0x78($r31)
    ldq $r16,  0x80($r31)
    ldq $r17,  0x88($r31)
    ldq $r18,  0x90($r31)
    ldq $r19,  0x98($r31)
    ldq $r20, 0x100($r31)
    ldq $r21, 0x108($r31)
    ldq $r22, 0x110($r31)
    ldq $r23, 0x118($r31)
    ldq $r24, 0x120($r31)
    ldq $r25, 0x128($r31)
    ldq $r26, 0x130($r31)
    ldq $r27, 0x138($r31)
    ldq $r28, 0x140($r31)
    ldq $r29, 0x148($r31)
    ldq $r30, 0x150($r31)
    call_pal 0x555

/*
    ENDING STATE
    ------------
    r0 = a43f0008a41f0000
    r1 = a47f0018a45f0010
    r2 = a4bf0028a49f0020
    r3 = a4ff0038a4df0030
    r4 = a53f0048a51f0040
    r5 = a57f0058a55f0050
    r6 = a5bf0068a59f0060
    r7 = a5ff0078a5df0070
    r8 = a63f0088a61f0080
    r9 = a67f0098a65f0090
    r10 =a6bf0108a69f0100
    r11 =a6ff0118a6df0110
    r12 =a73f0128a71f0120
    r13 =a77f0138a75f0130
    r14 =a7bf0148a79f0140
    r15 =00000555a7df0150
*/    

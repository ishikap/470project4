lda     $r0, 0          # r0 = 0
lda     $r1, 1          # r1 = 1
subq    $r0, $r1, $r2
addq    $r2, $r2, $r2   # r2 = -2
sll     $r2, $r1, $r3   # r3 = -2 << 1 = FFFFFFFFFFFFFFFC
srl     $r2, $r1, $r4   # r4 = -2 >> 1 = 7FFFFFFFFFFFFFFF
sra     $r2, $r1, $r5   # r5 = FFFFFFFFFFFFFFFF
call_pal 0x555

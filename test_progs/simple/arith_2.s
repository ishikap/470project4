# Same as arith_1.s, just switched the order of the operands

lda	    $r0,    0xdad           # r0  = dad
lda	    $r1,    0xbad           # r1  = bad
cmpeq   $r0,    $r1,    $r2     # r2  = 0
cmplt   $r0,    $r1,    $r3     # r3  = 0
cmple   $r0,    $r1,    $r4     # r4  = 0
cmpult  $r0,    $r1,    $r5     # r5  = 0
cmpule  $r0,    $r1,    $r6     # r6  = 0
addq    $r0,    $r1,    $r7     # r7  = 195A
subq    $r0,    $r1,    $r8     # r8  = 200
mulq    $r0,    $r1,    $r9     # r9  = 9face9
and     $r0,    $r1,    $r10    # r10 = 9ad
bic     $r0,    $r1,    $r11    # r11 = 400
bis     $r0,    $r1,    $r12    # r12 = FAD
ornot   $r0,    $r1,    $r13    # r13 = FFFFFFFFFFFFFDFF
xor     $r0,    $r1,    $r14    # r14 = 600
eqv     $r0,    $r1,    $r15    # r25 = FFFFFFFFFFFFF9FF
srl     $r0,    $r1,    $r16    # r16 = 0
sll     $r0,    $r1,    $r17    # r17 = 1B5A00000000000
sra     $r0,    $r1,    $r18    # r18 = 0

# Equal
cmpeq   $r0,    $r0,    $r19    # r19 = 1

call_pal 0x555

    lda $r10, 0x2     #95
    lda $r11, 0x2     #10
    mulq $r10, $r11, $r12   #Dest PRF 11 = 4
    mulq $r12, $r11, $r13   #94 = 8
    mulq $r13, $r11, $r14   #12 = 16
    mulq $r14, $r11, $r15   #93 = 32
    mulq $r15, $r11, $r16   #11 = 64
    mulq $r16, $r11, $r17   #92 = 128
    mulq $r17, $r11, $r18   #94 = 256
    mulq $r18, $r11, $r19   #12 = 512
    mulq $r19, $r11, $r20   #91 = 1024
    mulq $r20, $r11, $r21   #93 = 2048
    call_pal 0x555          #90

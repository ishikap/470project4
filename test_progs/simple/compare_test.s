lda     $r0, 0          # r0  = 0
lda     $r1, 1          # r1  = 1
subq    $r0, $r1, $r2   # r2  = -1
addq    $r2, $r2, $r3   # r3  = -2

# Compare two negative numbers
cmpeq   $r2, $r2, $r4   # r4  = 1
cmplt   $r2, $r3, $r5   # r5  = 0
cmplt   $r3, $r2, $r6   # r6  = 1
cmpult  $r2, $r3, $r7   # r7  = 0
cmpult  $r3, $r2, $r8   # r8  = 1

# Compare negative and positive numbers
cmplt   $r1, $r2, $r9   # r9  = 0
cmplt   $r2, $r1, $r10  # r10 = 1
cmpult  $r1, $r2, $r11  # r11 = 1
cmpult  $r2, $r1, $r12  # r12 = 0

call_pal 0x555

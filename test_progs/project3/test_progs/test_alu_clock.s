/*


*/
data1 = 0x0001
data2 = 0x0001
data3 = 0x5000
data4 = 0x0001
data5 = 0x05C5
data6 = 0x0C5C
data7 = 0x0001
data8 = 0x003F
    lda $r1,data1
    lda $r2,data2
    lda $r3,data3
    lda $r4,data4
    lda $r5,data5
    lda $r6,data6
    lda $r7,data7
    lda $r8,data8
    addq $r1,$r2,$r1     #r1= FFFF + 1 = 0
    subq $r3,$r4,$r3     #r3= 8000 - 1 = 7FFF
    and $r2,$r2,$r10    #r10= 1 & 1 = 1
    xor $r5,$r6,$r11    #r11= FFFF
    eqv $r5,$r6,$r12    #r12= 0000
    sll $r7,$r8,$r13    #r13= 1 shifted left by 63 = 8000
    srl $r13,$r8,$r14   #r14= 1 shifted right by 63 = 0001
    sra $r13,$r8,$r15   #r15= 1 shifted right by 63 = 0001 
    call_pal 0x555

    lda     $r2, 0x200       # Mem[128] = bad
    lda     $r1, 0x400
    lda     $r3, 0xbad
    lda     $r5,    0x4     #Loop 4 times
loop:
    stq     $r3, 0x0($r2)   # make dcache full
    stq     $r3, 0x8($r2)
    stq     $r3, 0x10($r2)
    stq     $r3, 0x18($r2)
    stq     $r3, 0x20($r2)
    stq     $r3, 0x28($r2)
    stq     $r3, 0x30($r2)
    stq     $r3, 0x38($r2)
    stq     $r3, 0x40($r2)
    stq     $r3, 0x48($r2)
    stq     $r3, 0x50($r2)
    stq     $r3, 0x58($r2)
    stq     $r3, 0x60($r2)
    stq     $r3, 0x68($r2)
    stq     $r3, 0x70($r2)
    stq     $r3, 0x78($r2)
    stq     $r3, 0x0($r1)
    stq     $r3, 0x8($r1)
    stq     $r3, 0x10($r1)
    stq     $r3, 0x18($r1)
    stq     $r3, 0x20($r1)
    stq     $r3, 0x28($r1)
    stq     $r3, 0x30($r1)
    stq     $r3, 0x38($r1)
    stq     $r3, 0x40($r1)
    stq     $r3, 0x48($r1)
    stq     $r3, 0x50($r1)
    stq     $r3, 0x58($r1)
    stq     $r3, 0x60($r1)
    stq     $r3, 0x68($r1)
    stq     $r3, 0x70($r1)
    stq     $r3, 0x78($r1)
    lda     $r2, 0x800          # change tag = evict
    lda     $r1, 0xc00
    lda     $r3, 0xdad
    subq    $r5,    0x1,    $r5
    bne     $r5,    loop
    call_pal 0x555              # doesnt work with larger clk

    lda $r0, 0xda0
    lda $r1, 0xda0
    ldq $r0, 0($r31) # Do that load
    nop
    stq $r1, 0($r1) # Do that store
    nop
    call_pal 0x555

/*
	You are not suppoed to store to 0xdad, the last 3
	bits are not 0! So project 3 gets a memory error,
	so i changed the store to 0xda0...

    ENDING STATE
    ------------
    r0 = 201f0dad
    r1 = dad
    mem[dad] = dad
*/

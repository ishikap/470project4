        lda $r0,  0
        lda $r1,  1
        lda $r2,  2
        lda $r3,  3
        lda $r4,  4
        lda $r5,  5
        lda $r6,  6
        lda $r7,  7
        lda $r8,  8
        lda $r9,  9
        lda $r10, 10
        lda $r11, 11
        lda $r12, 12
        lda $r13, 13
        lda $r14, 14
        lda $r15, 15
        lda $r18, 1
        lda $r16, 0x400
loop:
        addq $r0, $r0,      $r0
        addq $r1, $r18,     $r1
        addq $r2, $r2,      $r2
        addq $r3, $r3,      $r3
        addq $r4, $r4,      $r4
        cmpeq $r1, $r16, $r17
        addq $r5, $r5,      $r5
        addq $r6, $r6,      $r6
        addq $r7, $r7,      $r7
        addq $r8, $r8,      $r8
        addq $r9, $r9,      $r9
        addq $r10, $r10,    $r10
        addq $r11, $r11,    $r11
        addq $r12, $r12,    $r12
        addq $r13, $r13,    $r13
        addq $r14, $r14,    $r14
        addq $r15, $r15,    $r15
        nop
        beq  $r17, loop
        call_pal 0x555

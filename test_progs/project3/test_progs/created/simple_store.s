    lda     $r2, 0xda0
    lda     $r3, 0xbad
    stq     $r2, 0($r3)
    nop
    call_pal 0x555

/*
	AGAIN NOOOOOOOOOOOO! store to a valid adress...
	0xdad is not a valid address, so changing it to
	0xda0


    Just a basic test to check that we can store.

    ENDING STATE
    ------------
    r0 = 0xdad

    At dad in memory, should have stored dad

*/

Instruction count              =      11106
CPI                            = 0.861696
total_request_to_icache_count  =        6876
icache_misses_count            =        2293
total_request_to_dcache_count  =        1309
total_dcache_hits              =         573
branch_conditional_count       =         559
branch_unconditional_count     =           2
branch_call_count              =           0
branch_ret_count               =           0
Number of branch mispredicts   =          40
total_loads_in_ldb             =        1321
total_lsq_forwarding           =          12
disp_rs_full_count             =         172
disp_rob_full_count            =        1809
lsq_stq_full_count             =        2272
lsq_ldb_full_count             =           0
mshr_dcache_full_count         =           0
mshr_icache_full_count         =          27
commit_2                       =        4879
commit_1                       =        1349
commit_0                       =        3355

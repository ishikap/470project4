Instruction count              =      19455
CPI                            = 0.543048
total_request_to_icache_count  =       20497
icache_misses_count            =          23
total_request_to_dcache_count  =           0
total_dcache_hits              =           0
branch_conditional_count       =        1023
branch_unconditional_count     =           0
branch_call_count              =           0
branch_ret_count               =           0
Number of branch mispredicts   =           3
total_loads_in_ldb             =           0
total_lsq_forwarding           =           0
disp_rs_full_count             =           0
disp_rob_full_count            =           0
lsq_stq_full_count             =           0
lsq_ldb_full_count             =           0
mshr_dcache_full_count         =           0
mshr_icache_full_count         =           0

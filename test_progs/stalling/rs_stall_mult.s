    lda     $r12,   0x1     #95
    lda     $r10,   0x2     #95
    lda     $r5,    0x4     #Loop 4 times
loop:
    mulq    $r12,   $r10,   $r12    #2, 64, 2048, 65536
    mulq    $r12,   $r10,   $r12    #4, 128, 4096, 131072
    mulq    $r12,   $r10,   $r12    #8, 256, 8192, 262144
    mulq    $r12,   $r10,   $r12    #16, 512, 16384, 524288
    mulq    $r12,   $r10,   $r12    #32, 1024, 32768, 1048576 (0x100000)
    subq    $r5,    0x1,    $r5     #3, 2, 1, 0
    bne     $r5,    loop
    call_pal 0x555

    # RS size = 4

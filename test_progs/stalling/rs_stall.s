    lda     $r12,   0x1     #95
    lda     $r10,   0x2     #95
    lda     $r5,    0x4     #Loop 4 times
loop:
    mulq    $r12,   $r10,   $r12    #2, 20, 56, 128
    addq    $r12,   $r10,   $r12    #4, 22, 58, 130
    addq    $r12,   $r10,   $r12    #6, 24, 60, 132
    addq    $r12,   $r10,   $r12    #8, 26, 62, 134
    addq    $r12,   $r10,   $r12    #10, 28, 64, 136 (0x88)
    subq    $r5,    0x1,    $r5     #3, 2, 1, 0
    bne     $r5,    loop
    call_pal 0x555

    # RS size = 4

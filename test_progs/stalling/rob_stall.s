    lda     $r12,   0x1     #95
    lda     $r10,   0x2     #95
    lda     $r5,    0x4     #Loop 4 times
loop:
    mulq    $r12,   $r10,   $r12    #2, 4, 8, 16
    addq    $r10,   $r10,   $r11    #4
    addq    $r10,   $r10,   $r11    #4
    addq    $r10,   $r10,   $r11    #4
    addq    $r10,   $r10,   $r11    #4
    subq    $r5,    0x1,    $r5     #3, 2, 1, 0
    bne     $r5,    loop
    call_pal 0x555

    # ROB/RS size = 4

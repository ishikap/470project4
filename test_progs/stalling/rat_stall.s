   /* To stall RAT without stalling RS or ROB, use parameters NUM_PRF_ENTRIES=36, ROB_SIZE=8, NUM_RS=4*/

     lda     $r2,    0x2
    lda     $r3,    0x3
    lda     $r5,    0x5
loop:
    mulq    $r2,    $r3,    $r6
    addq    $r6,    $r2,    $r6
    addq    $r6,    $r2,    $r6
    addq    $r6,    $r2,    $r6
    addq    $r6,    $r2,    $r6
    subq    $r5,    0x1,    $r5
    bne     $r5,    loop
    call_pal    0x555


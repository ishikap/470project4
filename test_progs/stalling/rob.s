# check RoB stalling, a chain of dependent instructions need to be made
# NOTE: RoB size was changed from 64 to 4, and RS changed back to 16!!!
# Everything should get overwritten to 0
        lda         $r0,    0
        lda         $r1,    1
        lda         $r2,    2
        lda         $r3,    3
        lda         $r4,    4
        lda         $r5,    5
        lda         $r6,    6
        lda         $r7,    7
        lda         $r8,    8
        lda         $r9,    9
        lda         $r10,    10
        lda         $r11,    11
        lda         $r12,    12
        lda         $r13,    13
        lda         $r14,    14
        lda         $r15,    15
        lda         $r16,    16
        lda         $r17,    17
        lda         $r18,    18
        lda         $r19,    19
        lda         $r20,    20
        lda         $r21,    21
        lda         $r22,    22
        lda         $r23,    23
        lda         $r24,    24
        lda         $r25,    25
        lda         $r27,    27
        lda         $r28,    28
        lda         $r29,    29
        lda         $r30,    30
        mulq        $r0,    $r0,    $r1
        mulq        $r1,    $r1,    $r2
        mulq        $r2,    $r2,    $r3
        mulq        $r3,    $r3,    $r4
        mulq        $r4,    $r4,    $r5
        mulq        $r5,    $r5,    $r6
        mulq        $r6,    $r6,    $r7
        mulq        $r7,    $r7,    $r8
        mulq        $r8,    $r8,    $r9
        mulq        $r9,    $r9,    $r10
        mulq        $r10,    $r10,    $r11
        mulq        $r11,    $r11,    $r12
        mulq        $r12,    $r12,    $r13
        mulq        $r13,    $r13,    $r14
        mulq        $r14,    $r14,    $r15
        mulq        $r15,    $r15,    $r16
        mulq        $r16,    $r16,    $r17
        mulq        $r17,    $r17,    $r18
        mulq        $r18,    $r18,    $r19
        mulq        $r19,    $r19,    $r20
        mulq        $r20,    $r20,    $r21
        mulq        $r21,    $r21,    $r22
        mulq        $r22,    $r22,    $r23
        mulq        $r23,    $r22,    $r24
        mulq        $r23,    $r24,    $r25
        mulq        $r25,    $r25,    $r27
        mulq        $r27,    $r27,    $r28
        mulq        $r28,    $r28,    $r29
        mulq        $r29,    $r29,    $r30
        mulq        $r30,    $r30,    $r0
        call_pal    0x555

/*
    ENDING STATE
    ------------
    r0 = 0
    r1 = 0
    r2 = 0
    r3 = 0
    r4 = 0
    r5 = 0
    r6 = 0
    r7 = 0
    r8 = 0
    r9 = 0
    r10 = 0
    r11 = 0
    r12 = 0
    r13 = 0
    r14 = 0
    r15 = 0
    r16 = 0
    r17 = 0
    r18 = 0
    r19 = 0
    r20 = 0
    r21 = 0
    r22 = 0
    r23 = 0
    r24 = 0
    r25 = 0
    r26 = ? --Didn't change
    r27 = 0
    r28 = 0
    r29 = 0
    r30 = 0
    r31 = 0 --Didn't change
*/




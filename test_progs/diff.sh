#!/bin/bash

GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RED=$(tput setaf 1)

NumArgs=$#
Arg1=$1

if [ $NumArgs -ne 1 ]
then
    if [ $NumArgs -ne 0 ]
    then
        printf "${RED}Wrong number of arguments!\nPlease pass in a '1' for only program.out or '0' for both.${NORMAL}\n"
        exit 1
    else    # Assume simulation if nothing
        Arg1=0
    fi
fi

# program.out
printf "\n!!!program.out files!!!\n\n" | tee -a RESULTS.txt
for file in OUTPUT/*.program.out; do
    declare -i wrong=0;
	
    # Get the base filename of the file
	file=$(echo $file | cut -d'.' -f1)

    grep "HALT" CORRECT/$(basename $file).program.out >/dev/null
    if [ $? != 0 ]
    then
        wrong=1
    fi

	diff <(grep "@@@" CORRECT/$(basename $file).program.out 2> /dev/null) <(grep "@@@" OUTPUT/$(basename $file).program.out 2> /dev/null) 2>&1 > /dev/null

	if [ $? -eq 0 ]; then	
		# Good diff output
		printf "${NORMAL}%-20s:${NORMAL} ${GREEN}Success${NORMAL}\n" $(basename $file).program.out | tee -a RESULTS.txt
	else
        # Just add new line to keep this error seperate
        if [ $wrong == 1 ] 
        then
            echo | tee -a RESULTS.txt
        fi 

        # Bad output
        printf "${NORMAL}%-20s:${NORMAL} ${RED}Failed${NORMAL}\n" $(basename $file).program.out | tee -a RESULTS.txt
	fi

    if [ $wrong == 1 ]
    then
        printf "${RED}No HALT found in CORRECT file!${RED}\n${NORMAL}Probably halted on illegal instrution or memory address.${NORMAL}\n\n" | tee -a RESULTS.txt
    fi
done

# writeback.out, only if not synth
if [ $Arg1 -eq 0 ]
then
    printf "\n!!!writeback.out files!!!\n\n" | tee -a RESULTS.txt
    for file in OUTPUT/*.writeback.out; do
        # Get the base filename of the file
        file=$(echo $file | cut -d'.' -f1)
        diff CORRECT/$(basename $file).writeback.out OUTPUT/$(basename $file).writeback.out 2>&1 > /dev/null

        # If there is a difference, then note it and continue. Otherwise, continue to check program.out
        if [ $? -eq 0 ]; then	
            # Good diff output
            printf "%-20s: ${GREEN}Success${NORMAL}\n" $(basename $file).writeback.out | tee -a RESULTS.txt
        else
            # Bad output
            printf "%-20s: ${RED}Failed${NORMAL}\n" $(basename $file).writeback.out | tee -a RESULTS.txt
        fi
    done
fi

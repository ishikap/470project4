#!/bin/bash

#Could send ouput to a file and this could read it in?

GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RED=$(tput setaf 1)


declare -a fg_color=(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 ,14, 15, 16, 17, 18, 19, 20)
declare -i index=0

var=1
#stty -icanon time 0 min 0

while true
do
    read -rn1 input
    if [ "$input" = "q" ]
    then
        tput sgr0
        printf "\n"
        exit 1
    else
        printf "${RAINBOW}"
        var=`expr $var + 1`
        #echo $var
        RAINBOW=$(tput setaf $((var)))
        if [ "$var" = 275 ]
        then
            var=0
        fi
    fi
done

#stty sane

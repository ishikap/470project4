Instruction count              =        554
CPI                            = 1.563177
total_request_to_icache_count  =         485
icache_misses_count            =         171
total_request_to_dcache_count  =          96
total_dcache_hits              =          96
branch_conditional_count       =          27
branch_unconditional_count     =          15
branch_call_count              =           2
branch_ret_count               =           1
Number of branch mispredicts   =           6
total_loads_in_ldb             =         232
total_lsq_forwarding           =         136
disp_rs_full_count             =           0
disp_rob_full_count            =           0
lsq_stq_full_count             =         437
lsq_ldb_full_count             =           0
mshr_dcache_full_count         =           0
mshr_icache_full_count         =          61
commit_2                       =         214
commit_1                       =         127
commit_0                       =         557

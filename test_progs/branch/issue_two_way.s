# This is ment to test that the RS will issue two ways.
# A mult will resolve multiple RS adds and should issue
# more then one way.
        lda         $r0,    0       # i variable
        lda         $r1,    0x50    # do the loop 0x50 times
        lda         $r2,    1       # increment by 1
        lda         $r4,    0       # value for multipling
start:
        mulq        $r4,    $r4,    $r4     # 0 * 0 = 0 --RS should wait here
        addq        $r4,    $r4,    $r5     # 0 + 0 = 0 --Then these should all wakeup  
        addq        $r4,    $r4,    $r6      
        addq        $r4,    $r4,    $r7      
        addq        $r4,    $r4,    $r8      
        addq        $r4,    $r4,    $r9      
        addq        $r4,    $r4,    $r10      
        addq        $r4,    $r4,    $r11      
        addq        $r4,    $r4,    $r12      
        addq        $r4,    $r4,    $r13      
        addq        $r4,    $r4,    $r14      
        addq        $r4,    $r4,    $r15      
        addq        $r4,    $r4,    $r16      
        addq        $r4,    $r4,    $r17      
        addq        $r4,    $r4,    $r18      
        addq        $r4,    $r4,    $r19      
        addq        $r4,    $r4,    $r20      
        addq        $r0,    $r2,    $r0     # i++
        cmplt       $r0,    $r1,    $r3     # if i < 0x50, continue loop. else, halt
        bne         $r3,    start                
end:
        call_pal    0x555



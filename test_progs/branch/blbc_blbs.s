# Test for the branches that are kind of weird (i.e., blbc, blbs)

        lda     $r0, 0          # r0 = 0
        lda     $r1, 1          # r1 = 1
        subq    $r0, $r1, $r2   # r2 = -1
        blbc    $r2, not_taken  # not taken, lowest bit is 1
        blbc    $r0, taken1     # taken, lowest bit is 0
        br      not_taken       # should not execute, time to fail
taken1:
        blbs    $r0, not_taken  # not taken, lowest bit is 0
        blbs    $r2, end        # taken, lowest bit is 1
        br      not_taken       # should not execute, time to fail
end:
        lda     $r30, 2
        call_pal 0x555

not_taken:
        lda     $r30, 0xbad
        call_pal 0x555

/*
    ENDING STATE
    ------------
    r0      0
    r1      1
    r2      FFFF FFFF FFFF FFFF
    r3-r29  0
    r30     2
    r31     0
*/

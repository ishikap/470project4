        lda     $r0,    0       # i variable
        lda     $r1,    0xdad   # do the loop dad times
        lda     $r2,    1       # increment by 1
start:
        cmplt   $r0,    $r1,     $r3                         # if i < dad, continue. else, go to end
        beq     $r3,    end
        addq    $r0,    $r2,    $r0                         # i++
        beq     $r31,   start
end:
        call_pal 0x555



/*
    ENDING STATE
    ------------
    r0      dad 91
    r1      dad 0
    r2      1   1
    r3      0   89
    r4-r31  0

*/

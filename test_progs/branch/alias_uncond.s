#check to make sure bimodal predictor does not alias because it is unconditional
#We have 32 entries, so index PC with 5 bits
        lda         $r0,    0       # i variable
        lda         $r1,    0x100   # do the loop dad times
        lda         $r2,    1       # increment by 1
start:
        br          func            # PC is 12 (3 for index), but it is unconditional
func:
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        addq        $r0,    $r2,    $r0     # i++
        cmplt       $r0,    $r1,    $r3     # if i < dad, continue loop. else, halt
        bne         $r3,    start           # PC is 140, 35 (3 for index), alias
        call_pal    0x555

/*
   Make sure that the unconditional branch does not alias with the conditional
   branch.
*/

/*
    ENDING STATE
    ------------

*/

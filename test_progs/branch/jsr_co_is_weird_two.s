func_addr = 20
start:
        lda         $r0,    func_addr
        jsr        ($r0)
        jsr_coroutine               # Should go to the line after the last jsr_co, which is a halt
        lda         $r0,    1       # this should never run
        br          start
func:
        jsr ($r26)                  # Jumps back to jsr_coroutine
        call_pal    0x555

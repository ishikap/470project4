    lda $r10, 0x2     #95
    lda $r11, 0x2     #10
    lda $r5, 0x10      #Loop 4 times
loop:
    mulq $r10, $r11, $r12   #Dest PRF 11 = 4
    mulq $r12, $r11, $r12   #94 = 8
    mulq $r12, $r11, $r12   #12 = 16
    mulq $r12, $r11, $r12   #93 = 32
    mulq $r12, $r11, $r12   #11 = 64
    mulq $r12, $r11, $r12   #92 = 128
    mulq $r12, $r11, $r12   #94 = 256
    mulq $r12, $r11, $r12   #12 = 512
    mulq $r12, $r11, $r12   #91 = 1024
    mulq $r12, $r11, $r12   #93 = 2048
    subq $r5, 0x1, $r5
    bne $r5, loop
    call_pal 0x555          #90

    bsr     $r26, func
    call_pal 0x555
func:
    beq     $r31,   end
    bsr     $r26,   bar
end:
    ret

bar:
    nop     # Really long so the branch resolves before the ret
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    ret

/*
    ENDING STATE
    ------------
    r0      1
    r1-31   0

    Check that it did not mispredict the return
*/

        lda $r0, 0
        lda $r1, 0
        cmpeq $r1, $r0, $r2
        beq $r2, end   # wont take it
        lda $r0, 1
        call_pal 0x555
end:
        call_pal 0x555

/*
    ENDING STATE
    ------------
    r0      1
    r1-31   0
*/

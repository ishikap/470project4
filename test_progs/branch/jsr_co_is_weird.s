func_addr = 16
start:
        lda         $r0,    func_addr
        jsr        ($r0)
        jsr_coroutine               # Should go to the line after the last jsr_co, which is a halt
        br          start
func:
        jsr ($r26)                  # Jumps back to jsr_coroutine
        call_pal    0x555

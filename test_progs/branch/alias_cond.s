#check to make sure bimodal predictor does alias because it is conditional
#We have 32 entries, so index PC with 5 bits 
        lda         $r0,    0       # i variable
        lda         $r1,    0x50    # do the loop 0x50 times
        lda         $r2,    1       # increment by 1
        lda         $r4,    1       # value for always not taken
start:
        beq         $r4,    end     # PC is 16 (4 for index), always not taken
func:
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        addq        $r0,    $r2,    $r0     # i++
        cmplt       $r0,    $r1,    $r3     # if i < 0x50, continue loop. else, halt
        bne         $r3,    start           # PC is 144, 36 (4 for index), alias     
end:
        call_pal    0x555

/*
   Make sure that the conditional branch does alias with the other branch.

    IT DOES! -> Checked via DVE, mispredicts everytime on branch at 144. 
*/

/*
    ENDING STATE
    ------------
    r0 = 0x50

*/

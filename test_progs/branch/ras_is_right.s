    bsr $r26, func
    call_pal 0x555
func:
    lda $r0, 1
    ret

/*
    ENDING STATE
    ------------
    r0      1
    r1-31   0

    Check that it did not mispredict the return
*/

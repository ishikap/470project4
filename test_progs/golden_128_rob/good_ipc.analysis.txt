Instruction count              =      19455
CPI                            = 0.529067
total_request_to_icache_count  =       20499
icache_misses_count            =           5
total_request_to_dcache_count  =           0
total_dcache_hits              =           0
branch_conditional_count       =        1023
branch_unconditional_count     =           0
branch_call_count              =           0
branch_ret_count               =           0
Number of branch mispredicts   =           3
total_loads_in_ldb             =           0
total_lsq_forwarding           =           0
disp_rs_full_count             =           0
disp_rob_full_count            =           0
lsq_stq_full_count             =           0
lsq_ldb_full_count             =           0
mshr_dcache_full_count         =           0
mshr_icache_full_count         =           9
commit_2                       =        9215
commit_1                       =        1026
commit_0                       =          52

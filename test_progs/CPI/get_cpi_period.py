#!/usr/bin/python3

import os
import sys
import re

if len(sys.argv) == 2:
    PERIOD  = sys.argv[1]
else:
    print('\n!!! WRONG number of inputs incorrect WRONG!!!')
    print('Please input the clock period.\n')
    sys.exit()

def main():
    os.chdir('..')
    onlyfiles = [f for f in os.listdir('OUTPUT') if f and os.path.isfile(os.path.join('OUTPUT', f)) and f.endswith(".program.out") and f != "halt.program.out"]

    #print("Hello world")
    for f in onlyfiles:
        with open(os.path.join('OUTPUT', f), 'r') as file_handle:
            data = file_handle.read()
        cpi = re.search(r"@@  (\d+) cycles / (\d+) instrs = (\d+)\.(\d+) CPI", data)
        s = cpi.group(3) + "." + cpi.group(4)
        through = float(s) * float(PERIOD) * float(cpi.group(2))
        if cpi:
            print ("File: %-45s CPI: %-15s Execution Time: %-10s" % (f, s, str(through)))

if __name__ == "__main__":
    main()

#!/usr/bin/python3

import os
import sys
import re

def main():
    os.chdir('..')
    onlyfiles = [f for f in os.listdir('OUTPUT') if f and os.path.isfile(os.path.join('OUTPUT', f)) and f.endswith(".program.out")]

    for f in onlyfiles:
        with open(os.path.join('OUTPUT', f), 'r') as file_handle:
            data = file_handle.read()
        cpi = re.search(r"@@  (\d+) cycles / (\d+) instrs = (\d+)\.(\d+) CPI", data)
        if cpi:
            print(f + "," + cpi.group(3) + "." + cpi.group(4))


if __name__ == "__main__":
    main()

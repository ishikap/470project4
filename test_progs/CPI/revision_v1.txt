Hello world
address_mispredict.program.out,5.000000
alias_cond.program.out,0.734493
alias_uncond.program.out,0.521713
basic_branch_plus.program.out,9.000000
basic_branch.program.out,8.500000
blbc_blbs.program.out,3.875000
branch_mult.program.out,2.066667
correct_prediction.program.out,4.400000
issue_two_way.program.out,0.535536
jmp.program.out,7.666667
jsr_and_return.program.out,8.000000
jsr_co_is_weird.program.out,7.250000
jsr_co_is_weird_three.program.out,5.800000
jsr_co_is_weird_two.program.out,7.250000
loop_cond.program.out,0.502605
loop.program.out,0.855072
ras_is_right.program.out,6.000000
rras_helps_us.program.out,7.666667
ldb_stalls.program.out,0.840557
stores_evict.program.out,2.414474
badnewarr_WORKS_decaf.program.out,2.720000
badsub_WORKS_decaf.program.out,1.254513
bubble_sort_test_WORKS_decaf.program.out,1.420015
bubble_sort_WORKS_decaf.program.out,1.484980
char_at_test_WORKS_decaf.program.out,1.294179
factorial_WORKS_decaf.program.out,1.185942
fib_WORKS_decaf.program.out,1.286998
merge_sort_test_WORKS_decaf.program.out,1.177256
fib.program.out,1.149660
queue_mod_WORKS_decaf.program.out,3.416667
simple_WORKS_decaf.program.out,1.039427
strcpy_test_WORKS_decaf.program.out,1.675844
strlen_test_WORKS_decaf.program.out,2.538462
test_strcmp_WORKS_decaf.program.out,1.390476
btest1.program.out,2.585153
btest2.program.out,2.145055
copy_long.program.out,0.796610
copy.program.out,1.592308
evens_long.program.out,0.814465
evens.program.out,1.890244
fib_long.program.out,0.717703
fib_rec.program.out,1.260393
insertion.program.out,1.220736
mult_no_lsq.program.out,2.046763
nop.program.out,1.279070
mult.program.out,1.993846
objsort.program.out,1.380177
parallel_long.program.out,0.651648
parallel.program.out,1.000000
parsort.program.out,0.731767
saxpy.program.out,1.291892
sort.program.out,1.447739
cdb_stall_store.program.out,4.166667
load_stall.program.out,2.483871
load_store_independent.program.out,5.500000
simple_load.program.out,27.000000
simple_store.program.out,5.500000
speculative_loads.program.out,3.022727
store_depends_on_load_addr.program.out,5.666667
store_depends_on_load_data.program.out,5.500000
rob.program.out,3.633333
store_load_forwarding_1.program.out,5.600000
store_load_forwarding_2.program.out,5.600000
store_load_forwarding_3.program.out,4.666667
store_load_forwarding_4.program.out,4.285714
store_load_forwarding_5.program.out,4.571429
store_stall.program.out,4.205882
prefetching.program.out,0.938604
add_dependent_1000.program.out,2.011976
add_dependent.program.out,5.000000
add_two_numbers.program.out,1.565217
arith_1.program.out,1.736842
arith_2.program.out,1.750000
compare_test.program.out,2.076923
false_dependency.program.out,2.500000
immediate_load_r31.program.out,16.000000
immediate_load.program.out,16.000000
load_same_reg.program.out,8.500000
multiply_dependent.program.out,6.333333
multiply_immediate.program.out,1.636364
multiply.program.out,7.333333
nop_does_not_wait.program.out,1.750000
nop_halt.program.out,8.500000
shift_testing.program.out,3.285714
cdbq.program.out,2.500000
rat_stall.program.out,2.131579
rob_stall.program.out,1.967742
rs_stall_mult.program.out,4.838710
rs_stall.program.out,2.774194
good_ipc.program.out,0.528193

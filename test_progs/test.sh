#!/bin/bash

GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RED=$(tput setaf 1)

NumArgs=$#
Arg1=$1

if [ $NumArgs -ne 1 ]
then
    if [ $NumArgs -ne 0 ]
    then
        printf "${RED}Wrong number of arguments!\nPlease pass in a '1' for synth or '0' sim.${NORMAL}\n"
        exit 1
    else    # Assume simulation if nothing
        Arg1=0
    fi
fi

# First long synth
if [ $Arg1 -eq 1 ]
then
    printf "Running first synthesis.-------------------------------------------\n"
    cd ..
    make syn
    cd test_progs
fi

declare -i timeSec_sim=40
declare -i timeSec_synth=200

# Remove old
rm -rf OUTPUT
rm RESULTS.txt

# make the output folder if not there
mkdir -p OUTPUT
declare -a nope=("CORRECT" "OUTPUT" "diff.sh" "golden.sh" "test.sh" "vs-asm" "project3" "other" "RESULTS.txt" "CPI" "rainbow.sh" "ANALYSIS")

for D in *; do
    declare -i wrong=0;

    # dont run certian folers
    for i in "${nope[@]}"
    do
        if [ "$D" == "$i" ]
        then
            wrong=1
        fi
    done

    if [ $wrong == 0 ]
    then
        for file in $D/*.s; do
            # get rid of file extension
            file=$(echo $file | cut -d'.' -f1)

            echo "Assembling and Running $file"
            cd ..
            if [ $Arg1 -eq 0 ]
            then
                timeout $timeSec_sim make PROGRAM=test_progs/$file.s >/dev/null
            else
                timeout $timeSec_synth make syn PROGRAM=test_progs/$file.s >/dev/null
            fi

            # only save file if it didn't timeout
            if [ $? != 0 ]
            then
                cd test_progs
                if [ $Arg1 -eq 0 ]
                then
                    printf "${RED}PROGRAM TIMED-OUT AFTER %ds. POSSIBLE BUG!${NORMAL}\n" $timeSec_sim | tee -a RESULTS.txt
                else
                    printf "${RED}PROGRAM TIMED-OUT AFTER %ds. POSSIBLE BUG!${NORMAL}\n" $timeSec_synth | tee -a RESULTS.txt
                fi
                cd ..
            else
                echo "Saving $file output"
                # save output to output folder
                if [ $Arg1 -eq 0 ]
                then
                    mv writeback.out test_progs/OUTPUT/$(basename $file).writeback.out
                    mv analysis.txt test_progs/OUTPUT/$(basename $file).analysis.txt
                    mv analysis.csv test_progs/OUTPUT/$(basename $file).csv
                    mv sim_program.out test_progs/OUTPUT/$(basename $file).program.out
                else
                    #mv analysis.data test_progs/OUTPUT/$(basename $file).analysis.data
                    mv syn_program.out test_progs/OUTPUT/$(basename $file).program.out
                fi
            fi
            cd test_progs
        done
    fi
done

printf "\n\n!!!Done assembling and running OUTPUT, begining to diff files!!!\n\n\n"
if [ $Arg1 -eq 0 ]
then
    ./diff.sh 0
else
    ./diff.sh 1
fi
